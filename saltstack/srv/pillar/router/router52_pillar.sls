{
	"data": "ENC[AES256_GCM,data:cxiZJMCAdBU0tRhCK31L4ZdR0kUDLzGtSe44Y2rdFHsPgM6Rxr8EcliSmIH8IEDUH4oG4BmYaO4TI4dnClqBzz5Q2vS10s7KsAC6sV9gLsRtxcIozl+QuUtDclmBDxPxWbFR1P9nmTtVOCuXb8JCICUy/itq2tMVPtbDXy0xdNN2c0tJW6ylYRHDBxe0y1GeAL2qOApGAP9hIhbwR0EgDBgRkh5GltS4lx3QUbhnDwsvqMWj4O+IOdAC/PYhViw+GfauCTdINX2jlGMQ/kEfwAICaNWclD7Oc1KSNonm8ZqDZjS/X0fgL9qXPJgy/Np1LCOLpLWRMoT3yg5ciTlkCFFJAfn9HmnIFJYS5T4eb+63pnJ8Qs7XVTKKlR3KG+0PfMk0M4A8xWInskD1pkzIxsp+l5gPmfkU6BtxwGx0/A49ekKL6d0qp8YE2bdUKGFisW6NuYlUO1TvTofQFhEiXbqiHjIJ06pUUogQKrviUaHprYQfgdIM+T992R/XzFBN11GUvO0=,iv:LiV2mPEUgUFGifwaIGS0r+EHM7MPNWS1G/1IIEzhjQw=,tag:lP+EJDDUl/vfgwBBchY3UQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:30Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwGcygPUWVibwoaJTOhrV5wIAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMqpe76gcDtZGqYNb5AgEQgDtM+GgV+RA//cyKq0PJ8sur9ENlAI9FGLWBIYnqZVRwgcN2XAZsmVsAJI7ffwbMuItPSazI597SdDVIAA==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:30Z",
		"mac": "ENC[AES256_GCM,data:Z6L3wkYxM1Zt1q2gl9OTE+EuHRoTH3Q7csl9FPkfiiNU6cayuzDDXNUHec49UBBJZQcDgmU8VUgz5S5F3ckFKsMGTZYlykmu3MVVXcaziQ5Y9xICnsCDOPDenAUVC3Iq/DPQ9Fc89GiUAQ8lGgbsTblcsYgK7XNCJXQ9zuvaJs8=,iv:JsWER5mkktuMAmQzVKm9eF+FEibM0nHqOyNS9wbBXSo=,tag:4d/tRk62n4pbZFbOiwB8zw==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}