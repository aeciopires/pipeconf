#!/bin/bash
#------------------------
# Authors: Aecio Pires
# Date: 09 abr 2020

# Objective: Script to export network asset configuration backup.
#
#--------------------- REQUIREMENTS --------------------#
# Install packages: git, docker
#------------------------

#------------------------
# Local functions

#--------------------------------------------------------
# comment: Displays help for the script.
# sintax: usage
#
function usage() {

echo "Script to export network asset configuration backup."
echo "Example:"
echo " "
echo "$0 DOCKER_IMAGE_SPROXY DEVICES DIR_BKP DIR_SALT_BASE DIR_SALT_PROXY_PILLAR DIR_SALT_PROXY_STATES"
exit 3
}



#------------------------
# Declaration of Variables
DEBUG=true
PROGPATHNAME=$0
PROGFILENAME=$(basename "$PROGPATHNAME")
PROGDIRNAME=$(dirname "$PROGPATHNAME")
DOCKER_IMAGE_SPROXY=$1
DEVICES=$2
DIR_BKP=$3
DIR_SALT_BASE=$4
DIR_SALT_PROXY_PILLAR=$5
DIR_SALT_PROXY_STATES=$6
BIN_SOPS="/usr/local/bin/sops"
SOPS_CONFIG_FILE="/home/pipeconf/.sops.yaml"
ENCRYPT_BACKUP_FILES=true
CHECK_AWS_CONFIG_FILE=false
AWS_CONFIG_FILE="/home/pipeconf/.aws/credentials"
CPU_NUMBER="128"
SHOW_TIME_REPORT=true
#------------------------

# Load script with our libs and defaults functions
[ -x "$PROGDIRNAME/lib.sh" ] && . "$PROGDIRNAME/lib.sh"

# Testing if variables are empty
checkVariable DOCKER_IMAGE_SPROXY "$DOCKER_IMAGE_SPROXY"
checkVariable DEVICES "$DEVICES"
checkVariable DIR_BKP "$DIR_BKP"
checkVariable DIR_SALT_BASE "$DIR_SALT_BASE"
checkVariable DIR_SALT_PROXY_PILLAR "$DIR_SALT_PROXY_PILLAR"
checkVariable DIR_SALT_PROXY_STATES "$DIR_SALT_PROXY_STATES"

# Testing if the required commands exists
checkCommand git docker $BIN_SOPS

# Verifying that the configuration files exists and have read permission
if $CHECK_AWS_CONFIG_FILE ; then
    if [ ! -r $AWS_CONFIG_FILE ]; then
        echo "[ERROR] O arquivo '$AWS_CONFIG_FILE' não tem permissão de leitura ou não existe.";
        exit 3
    fi
fi

if [ ! -r $SOPS_CONFIG_FILE ]; then
    echo "[ERROR] O arquivo '$SOPS_CONFIG_FILE' não tem permissão de leitura ou não existe.";
    exit 3
fi

# Exporting environment variables that will be used in the following commands
export TIME_REPORT_FILE=$(mktemp)
export DIR_SALT_BASE=$DIR_SALT_BASE
export DIR_SALT_PROXY_PILLAR=$DIR_SALT_PROXY_PILLAR
export DIR_SALT_PROXY_STATES=$DIR_SALT_PROXY_STATES
export DIR_BKP=$DIR_BKP
export DOCKER_IMAGE_SPROXY=$DOCKER_IMAGE_SPROXY
export CPU_NUMBER=$CPU_NUMBER

# Creating the alias for the salt-sproxy
shopt -s expand_aliases;
alias timerstart='starttime=$(date +"%s%3N")'
alias timerduration='durationtime=$(($(date +"%s%3N")-$starttime))'
alias timerstop='timerduration; echo command_duration=$durationtime'
alias salt-sproxy='startSaltSproxy(){ \
docker run --rm --network host \
-v $DIR_SALT_BASE/master:/etc/salt/master \
-v $DIR_SALT_PROXY_PILLAR:/etc/salt/pillar/ \
-v $DIR_SALT_PROXY_STATES:/srv/salt/ \
-v $DIR_BKP:/tmp/backups/ \
$DOCKER_IMAGE_SPROXY \
salt-sproxy -b $CPU_NUMBER $@; }; \
startSaltSproxy';

# Getting the list of asset accessible via Salt-Proxy Napalm
# and managing the state of asset configuration via Salt

if [ "$DEVICES" == 'ALL' ]; then


# Searching for grains ID in the PILLAR file with the name in the pattern
# DIR_SALT_PROXY_PILLAR/pillar/DEVICE_TYPE/DEVICE-NAME_pillar.sls
# The result is a comma-separated list of DEVICE-NAME
echo "[INFO] Obtendo a lista de ativos de rede...";
timerstart;

DEVICES_LIST=$(find "$DIR_SALT_PROXY_PILLAR" -type f | \
sort | \
grep -v top.sls | \
cut -d "." -f1 | \
rev | \
cut -d "/" -f1 | \
rev | \
awk -F"_pillar.sls|.sls|_pillar" '{print $1}' | \
tr "\\n" "," | \
sed 's/.$//')

timerstop;

    if $DEBUG ; then
        echo "[DEBUG] Lista de ativos de rede => $DEVICES_LIST";
    fi

    echo "[INFO] AT02 - Decifrando os arquivos com as credenciais de acesso de cada ativo de rede...";
    timerstart;
    DEVICE_FILES=$(find "$DIR_SALT_PROXY_PILLAR" -type f | sort | grep -v top.sls )

    for device_file in $DEVICE_FILES; do 
        $BIN_SOPS --config $SOPS_CONFIG_FILE -d -i "$device_file";
    done
    timerstop;
    reportTime "$TIME_REPORT_FILE" "AT02" "$durationtime";

    echo "[INFO] AT03 - Detectando quais ativos de rede estão acessíveis e obtendo informações...";
    if $DEBUG ; then
        echo "[DEBUG] Iniciando o contêiner do Salt-SProxy...";
    fi
    timerstart;
    salt-sproxy -L "$DEVICES_LIST" net.connected;
    timerstop;
    reportTime "$TIME_REPORT_FILE" "AT03" "$durationtime";

    echo "[INFO] AT04-09 - Obtendo o backup e aplicando possíveis alterações...";
    if $DEBUG ; then
        echo "[DEBUG] Iniciando o contêiner do Salt-SProxy...";
    fi
    timerstart;
    salt-sproxy -L "$DEVICES_LIST" state.apply;
    timerstop;
    reportTime "$TIME_REPORT_FILE" "AT04-09" "$durationtime";

    echo "[INFO] Obtendo os arquivos iniciais cifrados com as credenciais de cada ativo de rede...";
    cd "$DIR_SALT_PROXY_PILLAR" > /dev/null || exit 100

    timerstart;
    git checkout .
    timerstop;
    cd - > /dev/null || exit 100

    if $ENCRYPT_BACKUP_FILES ; then
        echo "[INFO] AT10 - Cifrando os arquivos de backup dos ativos de rede...";
        cd "$DIR_BKP" > /dev/null
        sudo chmod -R 777 .
        BKP_FILES=$(git status -s | cut -c4- )
        timerstart;
        for bkp_file in $BKP_FILES; do
            $BIN_SOPS --config $SOPS_CONFIG_FILE -e -i "$bkp_file";
        done
        timerstop;
        reportTime "$TIME_REPORT_FILE" "AT10" "$durationtime";
        cd - > /dev/null
    fi

else
    if $DEBUG ; then
        echo "[DEBUG] Ativo(s) de rede selecionado(s) => $DEVICES";
    fi

    echo "[INFO] AT02 - Decifrando os arquivos com as credenciais de acesso de cada ativo de rede...";
    DEVICES_LIST=($(echo $DEVICES | tr ',' '\n'));

    timerstart;
    for device in ${DEVICES_LIST[@]}; do
        device_file=$(find "$DIR_SALT_PROXY_PILLAR" -type f | sort | grep -v top.sls | tr ' ' '\n' | grep "$device\_")
        $BIN_SOPS --config $SOPS_CONFIG_FILE -d -i "$device_file";
    done
    timerstop;
    reportTime "$TIME_REPORT_FILE" "AT02" "$durationtime";

    #DEVICE_FILES=$(find "$DIR_SALT_PROXY_PILLAR" -type f | sort | grep -v top.sls )
    #DEVICE_FILES=$(find "$DIR_SALT_PROXY_PILLAR" -type f | sort | grep -v top.sls | tr ' ' '\n' | grep $DEVICES)
    #timerstart;
    #for device_file in $DEVICE_FILES; do 
    #    $BIN_SOPS --config $SOPS_CONFIG_FILE -d -i "$device_file";
    #done
    #timerstop;

    echo "[INFO] AT03 - Detectando quais ativos de rede estão acessíveis e obtendo informações...";
    if $DEBUG ; then
        echo "[DEBUG] Iniciando o contêiner do Salt-SProxy...";
    fi
    timerstart;
    salt-sproxy -L "$DEVICES" net.connected;
    timerstop;
    reportTime "$TIME_REPORT_FILE" "AT03" "$durationtime";

    echo "[INFO] AT04-09 - Obtendo o backup e aplicando possíveis alterações...";
    if $DEBUG ; then
        echo "[DEBUG] Iniciando o contêiner do Salt-SProxy...";
    fi
    timerstart;
    salt-sproxy -L "$DEVICES" state.apply;
    timerstop;
    reportTime "$TIME_REPORT_FILE" "AT04-09" "$durationtime";

    echo "[INFO] Obtendo os arquivos iniciais cifrados com as credenciais de cada ativo de rede...";
    cd "$DIR_SALT_PROXY_PILLAR" > /dev/null || exit 100
    timerstart;
    git checkout .
    timerstop;
    cd - > /dev/null || exit 100

    if $ENCRYPT_BACKUP_FILES ; then
        echo "[INFO] AT10 - Cifrando os arquivos de backup de configuração dos ativos de rede...";
        cd "$DIR_BKP" > /dev/null
        sudo chmod -R 777 .
        BKP_FILES=$(git status -s | cut -c4- )
        timerstart;
        for bkp_file in $BKP_FILES; do
            $BIN_SOPS --config $SOPS_CONFIG_FILE -e -i "$bkp_file";
        done
        timerstop;
        reportTime "$TIME_REPORT_FILE" "AT10" "$durationtime";
        cd - > /dev/null
    fi
fi

if $SHOW_TIME_REPORT ; then
    echo "---------- BEGIN-REPORT TIME (miliseconds) -----------"
    cat "$TIME_REPORT_FILE"
    echo "---------- END-REPORT TIME -----------"
fi

rm "$TIME_REPORT_FILE"
