#!/bin/bash

# See time of steps in builds of the job

DIR="jobs_20201205/jobs/config_02_devices"
BUILD_LOGDIR_BASE="builds"
cd $DIR > /dev/null || exit

round=30;

#for build_number in $(find "$BUILD_LOGDIR_BASE" -maxdepth 1 -type d -exec basename {} \; | grep -v builds | sort -nr); do 
#    round=$((round-1));
#    echo "______________________";
#    echo "round $round => build $build_number";
#    grep "command_duration\|Duration\|Summary" "$BUILD_LOGDIR_BASE/$build_number/log";
#    grep "real\|Duration\|Summary" "$BUILD_LOGDIR_BASE/$build_number/log";
#    echo;
#done


for build_number in $(find "$BUILD_LOGDIR_BASE" -maxdepth 1 -type d -exec basename {} \; | grep -v builds | sort -nr | head -n $round); do
    echo "______________________";
    echo "round $round => build $build_number";
    grep "command_duration" "$BUILD_LOGDIR_BASE/$build_number/log" | head -n3;
#    grep "real" "$BUILD_LOGDIR_BASE/$build_number/log" | head -n3;
    echo;
    grep "Duration\|Summary" "$BUILD_LOGDIR_BASE/$build_number/log" | tr -s " " | cut -d " " -f3 | grep -v for | cut -d '.' -f1;
    echo;
    grep "command_duration" "$BUILD_LOGDIR_BASE/$build_number/log" | tail -n1;
#    grep "real" "$BUILD_LOGDIR_BASE/$build_number/log" | tail -n1;
    echo;
    round=$((round-1));
done

