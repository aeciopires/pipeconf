{
	"data": "ENC[AES256_GCM,data:RF9GAn/S5AECbeVwc8M0ocoTUxe36+ZMkPFAbdM09NQjNluLLTNviJev0icuH7uvzq4KZ97uAQv9AdDisA/PMyubyWHZk2MhQc/Eq93v7phQcWVHHlgDPoQCNSUYsDIoJtxet3azxGZu6K4ecXT5Fp1i2DgKVowaT5HJsCUedh5be9TTiC0tWjz6HyXYpJou+RX0FG9x40bvclYSMLNpI2CPZsFTOpIqbLkEc6qKzyspk6fq6KMyr30HLgq6eUhsvevVhUfOswNoi4Xc6Jl1ryTQxgGB4ZA+8TxiqY1MNBGlxUIqrDB3Dg3xKXy8wSUTL3B0/AxliaP6fLYu1F5/+kG7ozyMoGaL484Oew22Q1+MD00Y2PeqkQWaovpdiv5mRsVx05Om94OZQwQhHmx/3YT5+sOFe7qRfzlhKpP3MpFJzVwGeg9+2Nf3Yz6JYK2A0GnsO8ipaPcySpc8EJnVassU+rnTJkndUseDkfrexrnKS876U75q5FZ2aPoV+9/3irxJDg==,iv:K91ZaioDs++ugHmMFA75suEAboHqQ/855WjkVWA3HzM=,tag:42rnIrlIJTZtiY7ma/tBlg==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:38Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFLhk9/8UzakQ8H85+qTyjyAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMq9B7l/6ZFY01Y8YVAgEQgDscD05fyfSBoou2h/e35ldFhxO7tn2wY3smXqGX7INIMZObsnvrpCfbRN5XqgLhXnXCNNiJxx//Ah/XCg==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:39Z",
		"mac": "ENC[AES256_GCM,data:bOL0yeTdloR5HnUhpjnfdNuXfga8urn5Nr6QAU9Bt9qhihJ6vF4vvtdZ55F0a/2bMdv+aSHyVHqKkxqWNXg2Cz4jhl5UA/IvbSTHdBFTkVQKSvT7Tvfpw8GAf1AKefaff3DX1l2Je/itGeViJwqaPtHTDYfTSGgkZU7/Im3E7vY=,iv:D5em0ZibUS5hp12ojLoXNvCEchdi27akhXLFU0UnCrQ=,tag:GDSBhpQEqvslKVVEC9uQIw==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}