{
	"data": "ENC[AES256_GCM,data:dTCbW1GT1B/6E4wGZConohV4KYj3B+ewh3Zk5RMqc4iJRWyrHPczf6xyBcfL0L6M91yLc0VmOIoZVn2gwKLf+0RAkLkQIlnbxNGvp6uwooMzStZxFg9iDz8zaITAbfGq7ib1EitOGQLxnuNWsxBclfAWpUU01x4WeER5wSQN9oyM/kRDTmMp/a1VhwfU3jOjqCKvsYSORVgmTvwISyI5yaLUq5oTxpE7a7b5mXRzb/0f2YVgm1fH6xI/aYB5KSxYAlvUkOcedNC7Jof1omX2xdEhMOU2xKiOx4AgkmlNRChiD5vrQEn/wXnhWY5IBwlnlCWbRQL8cNU2YqCLbDVgQnQ0y2ESXyKKN3uuC0s+4I9lLbhWYkmsX4LM6wBmFmME9ir+L7OAO14w+r5YCah76XXTS+Cp9MwIj5afL9c+ioDdDK+Ftss1Z5+j7eIcpg/CG7MiYE1s6OTYXHVP1xsxJmTmms1SvE9Ofi9IpOvfLVEeR21tt6cDgX3yrzjoUCFmTLsC/KM=,iv:nzm2TH+9uuMv3P24snccIk/UxCG77uK2SVMjs8HVK7o=,tag:8/afIy0wPJC8bcYa2CCOnw==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:30Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHmXca5E+w6mDT26twTLYQIAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMJp03DD485F9OVd9FAgEQgDt0gn0gKawOmiYfhP9g/v2BZfXmOBmzPg32IRXQQQ6B2xskqwDK31zfVXMaNVcMKcvRJWMWsTQxX6teYQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:31Z",
		"mac": "ENC[AES256_GCM,data:Hrc0qjXiSJnmTX/hFLsY84fztSPReP1PjeOnwNvo6LrwTSGb3BlMXoZ8WcrNj6DtCf37UYLEbcby+K9hXFNqMes64AS2TTnssp8KO48G6Y7z2+cewQzVTAWhjWQfQExbfINEjgi68A6WvSvwhz/l46YnyXED+LQUmGEzjKNgGoI=,iv:pEi5nbhxSFczARxcbsj88x6FkTh65UHxXjMxI5Z9PFM=,tag:82fdifT6im0q5wArMtVpxA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}