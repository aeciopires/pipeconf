{
	"data": "ENC[AES256_GCM,data:eg/CWvOmsgK/3h0rIh096FJcVyoH/eD2vHCleBdV1Pbly7Dx4Xm7LU0fUS9s4eowCjV7RAYzhLoNabzKkT4TxZ+3qhTY3LYq/qD6XYSal5Rb4lEAJ14g+PZzIiqjz0qbbKF9fhIuijUOsquV06AJdw6D75X60Bw7gnksbgOnirYSX7tbgAJKDt2q+lBXjx246e878XoDBm/1w18sJvRIujWT6oxTZiCMJFIuMd6OlB/muBq9YQi5HLq9NQ2NP8ZaYDhdoo7fwcSro5ZiqZ30JavsvnWgnrwWfGCNuwstsoRc4nhG9zmdHVTKnF+hnWl/bROz0qkKKjjwhm/16Ilt0b+5dHKU2FL/3NYtfwvLujOXmJormSIFoYmNsKj+X4oRbmVeauOW++Ouq9OkZ78d6B1h9W7YlSf5e6uDWMOHOUsW1ufxGoVH5Y/WIPNhKEs8yW7+03r2sLWgEw==,iv:IIKb3gE0M4SBCROCi6NDv7USQk1jiE5uyBhghG0AhDo=,tag:KLwcwPAXOsL+3H6tp2shpw==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-21T21:38:41Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFR7QgoJgrf9ZNJ730ef435AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMFI+zljLf2ctFf4XEAgEQgDuAO/QPgmZC8ZIF0KhND48BjwIPLA/0nKg3WYc1xcPk4qBBxnuE1C5L8UuUTGPIL6Fs99AS0hGWdup/2Q==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-21T21:38:42Z",
		"mac": "ENC[AES256_GCM,data:2991BkuqA3fkXhdJMLOyG+RHMjRMFojBFBFZLCEF7aDg3xUiBwIg0VJqsZHWQkZKQsmaLeVsEfjtqjjV2hIM+X15IkoCOzyVGmcY/oJ+pdNz1wC7XpOLw+guS6ybRLHKsHtIKkeO2oniNkgKD9yyA3kBLfYLcYbyXETZ6zsuQhE=,iv:vTRX6IIxRUfauCtqBhxF2Q07dMoKKnKzNnoXO24h4uo=,tag:JZ1BosPkyIt5wxujKzCAKg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}