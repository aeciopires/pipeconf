{
	"data": "ENC[AES256_GCM,data:b2kcbHXCNdLrtrbbDZg5HcAxKxwJBrbNlKBVxtl+RgknNTwmFGbS3YnwALuXSxk4HJV2AdsIMwbJyy7WNi0pXlsI+waU5B7dWZbtpU8/K1MM0zjMTqncvbHgAbuKSoII4hI21wfw59rJ2peyxfx/5not5AJR3H8dwMRBioAv5cACptZbqOeXiGdE/2xv85rCy0kHLd4d7srC67aGwsdHgK/sYjwyYeBMcTZshJ+/vrC25SBpgQg9zyLVEv0QeNRMT/qFkR8xuVyuPaYtajX0yAS7tSsdqcXVjYdiI85FM1VP/f8ddeCozcBU2MZKHOxlTeEypwK6bzzS7Qm5XQ4cSv7BFRzi9MkdLUUrIU4fjymf3jG1E0uwAWZR/q5ALiSyQZaN+1bi0ousLFGBkrudZ3N5DwxSQpLWuy7Zj2VNSyEZkp0poyl9w5Q826pSr5389L/2BFH1HMO8lnLkqr14MpOmtYtp4J2Wz6Nj,iv:R+f/vQFaQmC70nVtJZDeCSvi25Aflsog1gxLf6aauxA=,tag:EP1kZyGcR9ladnW0EaqXrw==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:33:04Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwF7HFQgki6cez1lrtbvUWFlAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMuEuhkxnBdCOSOjeLAgEQgDtZWk2fMN5tYW7ZxxDoKHj6sE+jAyU1cvW/I1fpccL2E5sP7+0Z4RMQtlt4ku1IV0xi0LgKQwl9UIkjFw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:33:04Z",
		"mac": "ENC[AES256_GCM,data:yw0tSGvhyE0+AmBUwsjPjd6Vr+SR0Whun/arFaw7lZVRxm22b4c8GRh6CfEl1JrZ0ULkLKoyOh+vUbLs7rAKvKfVCsFsXFPqY8Ddw+nHBYaArCgdYaSkDA+GnoLgSgegdmGIgXxG/v1kkbzjJuASjDqbGMv8zaAUGX4latOIO84=,iv:dTFdLaauiXB+WhppggHlEhnUwxqNA8IV6T0pmQ3slDo=,tag:YZdhE82P7RTY348miyzbnA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}