{
	"data": "ENC[AES256_GCM,data:TYX9KBjenqqWwJ94OrMRBLqGujeAssxeYHnaUAw/YcDVQ12olXDTxbGyezXrsblwjWkjoavoqyAYywbzK/Eksm9f70TbbqQL43QPblNuEHjD/8rqED15IZDETG/dBvkAfFW/wzXXLqqMmx2A0pO/Ssavm9/IrPoQeKM54hQ/7t1yuTo/1vWwIT5atgJZ46DT34q5ywb+YJ5sYX2btGh0DKk7CtSQAp95MI0zZxa66rk0mi5xjN9rKWfnwoFqcARUM6rynZfiPaoIjr4TlXsYj6qRfax+pryc50h8laODSSR5RWktnpYc+qNj7svNHSMkv/Lwh6EwqvD4i4KJOGu6FMG2b1V0DqgsZhEZKq7kNMvgyDU6KUV2+r+qYJCNAj3qxix11NP2zjrUmP35M5qn/uZol03nHRiyftmM2gCMpzNlFlA0w8h81l3GfivDPHsnBhKyL01ys5wUOw==,iv:4ss9PtVL30e2DLD9bsTZye9tmDdObHq/3MupEm6WHP0=,tag:QKjkyyDRgz2SHn0BUy91wA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-21T21:39:47Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwF0+/IgflxB3i9Pz1sCMgD0AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMNJOeugRdWLPiSdNVAgEQgDte3cDwRJ2CfjlAVBYZD/cusrp6Z1kqtunrykJeYWo0sCuFAIHBZPB0vOE1bMFGBC8CB6Rf3vXKLGc38w==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-21T21:39:48Z",
		"mac": "ENC[AES256_GCM,data:M6zU1+VY0vzVRCZ5rBw+8JoB014+5LHA+oHvXxi87wyR8f8w71mshbwbmC1eZl2IdoKndVXVqKI2u0Yc/fxJB7Pr06nHw7ke1IlF28nxTEMDenuO6yaEE7AmhcYms0n/h0c26EzT7Q/60heOqLz4QW3crYYTbHnxZ690q3lBBDo=,iv:kvF9/7FJ9BqEd7yox9CPI1gVFR0uw31ne05O7mSqxek=,tag:uMyJhaJda7rPW7vAWiNl5Q==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}