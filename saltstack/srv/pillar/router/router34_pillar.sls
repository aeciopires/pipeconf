{
	"data": "ENC[AES256_GCM,data:ETRSWYeXLrf8vXiPnSS2tAHQmpnjpGIucsg7Di+gX5LY+YaPaZgz0ba7GkU2bXGGzxO7JMogH+qv+JX6IWCQcou2aF2OslZ7mGwNq+IE6IZeyKvUHVMxEvtUEoXmQerw/0xeO13hXlNp8w9EyEMEQxPMuITP911a1+h14JG54/d0pc1Z+5YgiTJVXTET1Lm/Jvue4xHOtv8mKqn73LZUP3MhlJVxSGwX8MGAbmC18XhGgoQlwjvotO1VTuJXhm6MYF26VvshthPWJfA8NWmLbFRSvSi4PIbgvcF7SbCH5J/gzCHCGPcP22xWlUxE+BIH895SdyZgvvWxGtjnbA4tjP4DicyKmrPCMYvU4WwVuvHBzNOVSFNsoeROaZ6vMmgACbJuYfhLSZEaAAWimzHjCE70C8t0EjFbDXBNaUYHAvhH/w3iSXKYchiVSCzjLJXswXTaANpuCDp0Quuc8jnOWCnj7E7UKZTB4j6+nn4x9/qirDInjNFmz0ZEEEOtxd+3Hl8Oog==,iv:i2pJztWU9jfDDhVOtQzB1bhpeHLjf24NXyr15s2w+Y8=,tag:Vw7foLr3xqt+Bn7owFUqpA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:16Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFqyNbdzMNb9aM4QvgT7iyAAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMYMsBgoGN72YjOU9RAgEQgDs0llrYeb1+O16OG5nfabNQ1hOqUTmF85vyKiCw9D3qxDZtNpCv0KKM4LhVBG2c2ltjGBWnMbgnV0J9Zw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:17Z",
		"mac": "ENC[AES256_GCM,data:vhiFoeQFSKiJjurFxgVXfVH1yCZ6d89Ldn0eqWvGK7TpJhoPihLJ03yY+kUGFZuYdlPpioucZZMEbvlsexwfaFYmv5zvLA21IKN5am2m22eFWi13/YFzxPV4XpwAzceU+UnQnY1d77qTde+uCUWUK3lhmpuJRUuvgfJ6HVEa7Jk=,iv:/skBG2DGgBv+i/UXWLVqPBQuM4AXq9sBiVOEz1OKUfo=,tag:lSM7Bn3ofdiUWOgc0G3fQw==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}