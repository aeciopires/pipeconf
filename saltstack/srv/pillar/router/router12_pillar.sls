{
	"data": "ENC[AES256_GCM,data:D10PIVm5TwkC5/BJRrFdq0m7uCyaQsAbo2eODIwyeuoytu+TlskhiFP9QlLUk5j2mV30UXZTn/gAuWYzfpnYEzMJoZXRz9BHHeCqQMRJvaSWtyJYfvjLDq3xyj/+4cuhv0CZ+n6+wTpPVrDfx4YrK3xV09FqxUozzZhGgaOQmJ37t92PujkO/v+kniZFqJX1se76YXgBvupL3mqggCNyWfRfBRJKZPPCBqdoj/IyT8sjISFa7ERGGR2ygXuJo3febuVa1NTQbV62uvU8VVrW3xz60A/I1kfb5lwSgnd4aD/S/SKXtYGSTRtwnNzxilmnPegl5orhyKa0GUgIGnYE5D6yT0NJ9nRSOR0irTHpcAJn+fYdVYHgmGLMzZ1wUm3VLmFxtoykHgYIlV0eBRMNmBp1WFf075/Aw4aLb4yjv/THfd/5fbU5j086p2+W4GOxpZ9v9i1pIISpFCO24LWPkLy6kZ6n+t4wHD5g,iv:hfeZQlmk1r5Ybwcf7sWgeDOptsaEbhHqgDgUCVvrwOI=,tag:v9HBO86WvHwhI6/vVeAmzQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:32:31Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwGflVUzJY6o4/BHvAZlan0/AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMokRBXlDMwAeB7IG0AgEQgDs95Q3oROt58X8thcEQaUfaZeBKtqu8mi1fkmmsfEDeTvAhtNAFzHAPQ7iGeX+ZU9OXs21OOIR4hboCfA==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:32:31Z",
		"mac": "ENC[AES256_GCM,data:+6iS4uxp3gWxXtommbvt8B3r8wIfs9FkZxcPOLdtBku8kHKpZkL+zwBPfpdyEj6DW/TvL1WmnDy+BYC2u19J1CLDXmNSLbkrAY0Y9FAqK14nhOg2YgnAnnofvSgE4bwbIdcuLIpZs5Kd3loCu3urFmpPG17W0rhp5FaalF4lbxE=,iv:JLE8gVHssToQU1kzu5jlgNBPXduUoPJE7njd/qAretA=,tag:osAID9Y1Sr+us3uM1ZW2Ww==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}