{
	"data": "ENC[AES256_GCM,data:J+fB2Fecp9bCzpAiqa6P5PUNpQ5mxuoatmjBID1VBLzeNihg6jIpTwaHz3mKSU6pknKuDBkgNrSPJCnU77CB2Uj2pIEdj8NoHJrC3OpchFM7Eicy1BCxgvacDi+7QrLe5VBfCX83jaXUFA5NrdOXqJS1Lp2y1/tnZrbLQ37m80MtWo9FgbenWXdUPZwXAWyDQpQlWcxiMgTWxSt2VHhVcQnRqgz7wI5MTNQBNX0ME87ayJSO2Ss3NYLaEADGG/QS42re8lGOdOF33+7Q0JvhED2KlACpkVRKgQjOrdh3JkH+Q7GjO7LoQwoHyKMozTSHDr+oNjYZiVKXf0eRLLeNBJm1h9gOWoSz9Y36sNP+eTPRoqWgwxjdFzurQnil/+pjet59xK9z8jOBeR/LnAfs0TwDHOv5VBrCqNkGDUL6KMkh5j4VgkJE64AE0cuOZowT2awAd9hfpRfvt+dPFs1C3sg4Q9kK4LpvF00MlmMR9mTEMW5USdDoQni1oVl4dpwEjtVnWcfmKqyZVqzb6uZWWilKGigWDDpFLKkQ5Li53w==,iv:hke1yi6NteQSpuE3tVzQCBASCjBVhuI2QmewktGdMs0=,tag:WgVSAUiUI7NPsKc2L9UtcQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-15T07:00:59Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwEd4nYPBsyUO8NX9vCI7mv0AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMYnT+QoV0ySvqW53qAgEQgDue5CEdH2Rl7B9S2TVFDS0LxZ9pruSYRnbz62MI1+RhIajp9Kko99Apl/SSWGLa06lxNQhN5rhYCMSKWw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-15T07:00:59Z",
		"mac": "ENC[AES256_GCM,data:T+Qrvkux0f8Mxw3u+azvkqvsFERuMQ4xtP50Z7rzafgHCvnfkRuq9zDQk1mdS4EYXdyTdU49pjZD2YnjgIjqIXNRbpct96T+3d8edaq0OWau/y1OEaR1kIYRicxezmBTQhYgQNPtLVzkpuUkGjM7E0hdYnTk1oBbTk2fhCfo2kA=,iv:jCwUdFDT6Ph1QURXAGkQ13NiO0lhUytPJVqagaOFjl0=,tag:cEefcyFAIWzkUapOKMUrOg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}