{
	"data": "ENC[AES256_GCM,data:qgKwNLwcgo4CiP24mHXHfMmSp1ch1zKINBr7M2R508j7SgHJRAA7LyiFx80kkI0nNRqoYTl7iFFB9GcD6oyUDOuLV+ut32x6rYX3gKkWlznuMk6JyErW1oCo+OHaCCwnVWmogIo1cPaCjnxIcNZIEmmTiIYgVuFfnZzUtj+ZzeE95ZTfvVeCU1ywahJthpqzm+y/BV2eILBwotNZU54km9AYfAhUpw8yI7oyCfv/C1Z0D31/m9GkDq/AHUNfaTTnKiARnCRbbDFC/bdxJe02ZVXketKVwWxw+DSVAnuEcW8qrbIrGUTXAPXISsoEHaaIeaOsUhNFvNiNoK0iLClNzLDGZ1iadN7Fv3Zw59bOkYCGgITvZWjQ4I8YDv1hVm/2b0Y0CAoq/vjEc3yBRAAy7FvDt5+CKPafOnB2tWf1k/jOvItHv8WQn5jNcSMbdtQmmwWjdx+Qe5JzDysYRLBJuiL7H/cF8mkXTLY=,iv:DfXVON+YQRB3CSXP2ujudKEjtHaI1b26H7NsDPF/3HY=,tag:99kwXE278OuodGw4iuKLgw==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:32:50Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHoIhL33VrMaF6IuIxJClDyAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMMeLIuMfjRrFXpsjmAgEQgDtRAWmU/e54FQqGwgfWld+zOIBCd9mK8GAvV7z5X5JiMxW/jAlses0D7T50pkj9BYz23Mi1EfO7sUe4yw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:32:51Z",
		"mac": "ENC[AES256_GCM,data:5mkz8y0ETrX36LWRHpJBjmhFwbCQ+/79I/puJyNVjiyDyozhEzF8IWQ72JclglTDGnyw/gOXSnfqtGzzrjBAmMQnfcwe5r4/bYHWeO6Zvt33uGBj/4eECa1vJh16iq5Cr6gDJq1ACEKzIMTlh3ooHbH7pNgetU2FaiGKjbtA9yc=,iv:AhyGSjZC3N0UTGIdZ4z7QtYjpnAb1l/UEDXKEjZ5xKw=,tag:YCWz7Y1WGCarPdLqYlm43A==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}