{
	"data": "ENC[AES256_GCM,data:OGhMDCzQPrq4DI+NwnQv9a7jlAmU83WwG85jw61E/IxK8aRv5OVH6tPTghjvExtfHrnzqm5Gu73Av4fYQf3Lk/SemcmuVk/PDM8vWhCzUVGLzCcmtTbNl77OjklXX/eRZGGL5t+8lzC2b6okCLtjF2QAVUn7wVoLOwbrBQNa+Fke6lWKdV5ZhDDBjcQ7yYX87UoLhZYvwzJH1LL3Er5nUPII4HTjJmGYXLEp6KQJ6d7roqMXNbrSN0cTagjRhWWJrFpqdH6djCq2MrqyHj8WrlgvrqKFBn9nfVLNK5JVwhgSM13sJoZJB0rqR855d+17AznVlRTMUby4TOzPf7kAtgxanqw+grtlRAo2PsZnrYdiJVjLq5vBwEcW9TUVAUB24KXcuhWO9fN/2z+XPr/eA4g/OVbhC5sghmYDM/r6MUPs5SuvnlPUBy5qLR3wVrtZBDG3iuKLqGIe611mjkeoAdElDcyl8WSViLZ5/diY6usD/p/n4nfol3WJ1+QVIBRAhivX1X6UKIgtSULIBeVQsLwgCnqU+TpgEQ5Q5Zt8H0kQkCZVSw==,iv:KKhyt6hs14bk5fJe2yJ3WmG9CTWrz6AuNd9xC6ZX6KY=,tag:DO2IO4dlV4NYcmnbqhvVLA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:16:54Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwE3lBOpnDZqbZDlTpJVrxOoAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMyiWbOaV3ril4j19UAgEQgDtmnIFI52yGnKai4OiSP4NuToKbxZURmTwXL2Udlt7acmnFmktjc0NoORiyRQJ1bLkV0EobHVgSO5LrJw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:16:54Z",
		"mac": "ENC[AES256_GCM,data:XDYcJwZG/2XEY1TkNe+s6QuhPtNpSiUpRMoX1CINE/ouKqDNp89CgMW3ofWPSAdVFR1l4GX9u87nALuKmuO9jUHCVNfORIoQyy4A4btM4ckhIe4O/8QtarvNmAHztg/sXdZ6QozrfZwuIzJnsrZPbdEryMzhuIICuUFtYK36QV4=,iv:mr8dCijga53cyrDIZbpdj+vSDPsOd8Ms6EEr4QfLZw4=,tag:I2/LoGIJYdZOV48Vku0T0Q==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}