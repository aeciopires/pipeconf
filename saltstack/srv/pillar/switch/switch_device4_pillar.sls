{
	"data": "ENC[AES256_GCM,data:VzUYDthVPYYGc4N/0KoCjy1NG+S55ir6SYe/TBaFTq6bAftpklajB+PxH/snssTAokiNqhg9ng2xFzoyY6g97G6uYQA0qIRdogBmGJjO0ai5UGDKbXEZR02RKpMZFAtZctxlqjGHBHX6n6ip2dkWeXjRqOhgtk2TwZQLaUsUnISgkY2hNTVpK/Qk9IA7OKrZgluYf5BSgd+800WRQKLz3shugem85hsApSQUeFtmgi0YH92KIuAuHc6Q7Y3kFEhjFokRZx4HUxEsX7tgBfIJTHA/dgfnnRczBBQ/vw7RVQgKdXTetxzEj7pcMA/swPU+s+UJgZ2UEtLbATCZQsEDF1+afQuC4rEIoNDgEkSWN+1PxUD5LpIS2NNqgMStWCbsa5ZJXOd320nm1Vex5BJs8AYBlDHGukEUJz5dPQVNhlZGxupjEGroGoxfLUV00Y2BmPdNtxbk5u6R,iv:EhNjXQfmhcHivWb24c0jmR9HxU0iWi529Qmpe40UFAg=,tag:6hJ9xbUq9PFK4K1Ug/N5Tw==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-25T07:50:55Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwGfCnxH4U2OA9sLRkQss2CoAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMTKxWh8lSXfd+BuGnAgEQgDv/RKGVVCgyWTDytO5KpJ5t9YPoiYtjmBKEiNsPW2oee3JnJHoETs1u1HkGd8CPzpo3FYFby88IwmUG8g==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-25T07:50:56Z",
		"mac": "ENC[AES256_GCM,data:2HWN1UEIMKZqKdW8yxjgh+9v/0n5UPVkX8jDQD3Lxo+u/7AFnppw1rH851vUHEvel3JmMQZ6W+VvSlBIPeaYNMTGmsgwN/s1PG7uNkotk7J1MZ6DgP+HmGxSwjXbi2A3Me3XwPXb1ufVVtQfjXP0qMnmen+tIJe8TqyYQIyBV1Q=,iv:FGuIHHuL7P3sjUe6L8ClG8TR89qsjBh/mKToNW0jy5k=,tag:E7gMQ+LXlXMBJOvuu0OUNQ==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}