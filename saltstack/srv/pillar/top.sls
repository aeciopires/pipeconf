base:
  router*:
    - router/{{ grains['id'] }}_pillar
  switch*:
    - switch/{{ grains['id'] }}_pillar
