{
	"data": "ENC[AES256_GCM,data:AnZqEe/eEDkhMb5rXQBea2ZP+NQ6Lf16lr3JfQnT28NU9ihLoJ88Zzp2ImGD6IlUsdGkqHeviTqeGsLQY7o2v34Gt96zqZOJbaf02/bvTYXzopj2zyziv5PiyCO6bNeSODeRE531GhRs8mR3MBIzUQj+kmjhWLD/NQ3QEvU1hI659O5640P1MFb76HIrMhIfVuIFvdqTMWZjQYVVa/e2YWB5pYBr1OEEoybXV6oqVlrsai+bh9YdGF4QMzl/KmdSM+0fz406UCW/1RRyWUjz8z/wNy1EYfWfYf4n6mq2Fi9YVw+utY4y7KbD4Bw2r0JYFmHg/TbQXW9FZWaNbLCD7FrsMgaEbZvZhqowp7w1I/BuD8Mk2DK/tTioJV2d7Sw7VYyuimQnb/bl+Zzw2y3xMk1adHhEYoCLu51JQdusGrHB4nGIRKs1+iowQ9vHmJLvVUwGJDm+Hce4GTCKKV233BBL+OTZPrQION3JAecfwAyOB0zFIl9K6EyKZtxMAcOBIlatFP4QOA+aigNA2fTqXXquXAAnGzAN/d2AQVMNMy9F7bOn1Q==,iv:YTwQO+fp7kQj6gtjt9AMg1gX3KWKOmj5dbTFLQvCc1s=,tag:jY4lhi5e4hXuiK5KSgyx4A==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:16:25Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFWNKIk9qRaWRwDXyMrp7LbAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMsc7NG/+Xzqo0iSHWAgEQgDvvdsFW5IbeewYE5489eKN2HLRJvG+R3C+KuiqgXzXesgU4u5X3X5amru824S0p0/fDerxo6ofma0w5Bg==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:16:26Z",
		"mac": "ENC[AES256_GCM,data:IVZCxgKTTzN/VaKlVlL7NbcUKdJQ+8+gJpXf71LzQgbMpAPe8EBC95TwPq0koEa+f9FPZG/O5+FDnT6diWMyOo1BCHZXeumn4rCtehvpzFWYflBoCQnE0q3DOlhBT8COficNgOsB8ohPTcyItBLnB2e/hbzIpBcdLhE8HyoqY6g=,iv:kMX/yFktkq0klXas5MyQOpZ/F+uihJImqdKRnoUukQU=,tag:jLok9rCNJcbepC046F421g==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}