{
	"data": "ENC[AES256_GCM,data:+XjfMf2bCquT6skxExhTtBcL1bJXR/YocBwfnVG36ylE9vBfc080vQgvMloe+PN6+v6etcimA2Vl9vyZo618mBWaYtW8fsjcUL/RVstbfpNyMmhuc9cKuGov1zAlOFfUPM42QdmHVtNOkloLgJ50mfBZtynJZWrkMQMKh/fydAht24MyDeORoZTbhZ9dp7dRWeik9KRV669y5D2cOuYw2h9gnXkcyAvsVNZLLmYNe/oetZPWx3v+qwmVQjy6H1aXsPcmDMdSu+0VaUSdxMxzjI7IUX1iH7LyCyPOQPRsELYwrQnhCGbMEQesNxZ3ossdJTTPeShPT+vUhFi6qwSuri9HZeDq6mf00edgPuhw1PBhbN8P4btPQa4E4zVoCPxJG5HKYldyCTyeu1vTqF3GBa27MNFk66Q3tm7Tm6qZm1ZQTV8yTQ9PULngUSgR3H8LlpSHsigv/R+9yiulpecN95MiDpWOpQAiUgDWc6b0dUNEJovoIEnvs0Y4qsdsZDSh5JS7qj6S9DqUIcQlB2qKBJdUjgUJTaKrGMweXizdzWQ35kXoXQ==,iv:4GXSDyrDNposceLlbeWIauxOgRSre3HB55gvQ2ot4eg=,tag:rIHXiHrSnmjykyCLadEHAA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:16:19Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHkvEjXA0cXlDDV16YZxmhqAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMzAzNomWy99JAhwaDAgEQgDt/gV+oS1avIf34W/dBrw5HxItN5wOlmEe0DqyGFVasBKqxxtfd7Fkr3Duo7XC5ApjYEwsU1A5d/jw27w==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:16:20Z",
		"mac": "ENC[AES256_GCM,data:Hq0yBD0Fysktrej1LLrkk8Cjz0NVJeUf//+tRsQxebLQcL+6zqfUiYBNxZwCfetRFERGLzUKE/zCSZ5ACoO6s4gpcn6cffADPix3BKabWJ1oOhQfsHNcSPsV66q5uJjS9pnJkg7OSCTRh7BGSORwkbyagiMwiCZe72ikcY+o0gs=,iv:m+rTtJAZNabuUBLi5HyzL6796Sg5B3i3lNyj+/LsuRw=,tag:iSGXnzMjRPMwCvvUobqJ5Q==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}