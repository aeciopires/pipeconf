{
	"data": "ENC[AES256_GCM,data:xA8q7EzBYDX4lHqvCGTxJ6QLQU/kwzNDR2MjlzQmUrT9GJZnW43qRbbyeEKk/ndLcX1nVIbYekin83IEk8DklhGgn+G3fARXwGnUrerN3357taKNQkuEr7T8wm27I4gUtb/OcKQ9qrfcLfsEsUmZQngqWB5nFHW14wcMhFuxs4Yc5pPLDpu8JYdqrJDfpFJjrJ2a3Q+cF+7E3rA2klj2w4gL1vm/eeiylI/ckkNgyuFeqsSc8dghygBviDfZkCJRnHwWvkl4OjCujtNpWS8mnGhIUT9z6DXvU/ZefjFK+MVqd9r7+XXXqpaz+Ui/lc+Z/t4CAxasg+9PnGlVfMrMg4VQGfsbdF4ZZVQQ6UdEl2lmc5J1jaEJNGupjeohj8EOaPGmG4fNUtjt3Qmi0x2N0M7rnraWrRH+rpBMAqQE9HoqS4raVLBHLbTFuc4oSvzhyIQg44I4EaS+a4OAg1G+gaRfDyfa38K3/fVr,iv:2W8S5tdz/qS3u4TKpF2CRkQ6vDJL7p3Fz4uougOk8fg=,tag:o1V/YTmi2IehKdwzaAyqOw==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:32:33Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwEosxwdNCiDVwVg6/2ObD9uAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMAEGEx46TEg9gteJHAgEQgDuUtg7mbEDlY3qOeIQ9NkR5KUBmVZ1q7bAC1P8sCdXB6OcQb9G5yIjggjdPViFelbKraEy04ZZ1/VIbtA==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:32:34Z",
		"mac": "ENC[AES256_GCM,data:cAywMvlpBtmLV72erSA+ZYOBfQtgH59HXYTmsioVc+K6hmLPNR3CLhBmiWabl6+RArbXKEOz5kOb4dtmbZSCD4V1Ips6aeRnQUT4PkNBbBggVVlqaT6KQkABqDPzVE32Fno5uTS/itfVbY0HVy+vvUFjiOzo68002oi6HY1rAmU=,iv:oo1LYWhA6gKc42jHwDHtH+F49RVnLdQVyrV3QNQGE4g=,tag:Uz4Qt/Lt0RVoFMpnnT0kLA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}