/*
  Keeps only logs of the last 10 job runs and for 30 days
  Allows you to select the Jenkins slave node that will run the Job. It is expected to fail the first time, as this is when the pipeline will be read and the parameters will be created

  Defines the secret to be used in the Gogs integration webhook

  Allows the job to run whenever there is a push event in the Git repository.

  Source: https://javadoc.jenkins.io/allclasses-noframe.html
*/
properties([
  parameters([
    [$class: 'NodeParameterDefinition',
      name: 'PIPECONF_NODE',
      allowedSlaves: ['pipeconf-node', 'pipeconf-node-gcp'],
      defaultSlaves: ['pipeconf-node'],
      description: 'Selecione o node PipeConf para executar este job.',
      nodeEligibility: [$class: 'IgnoreOfflineNodeEligibility'],
      triggerIfResult: 'multiSelectionDisallowed'],
  ]),
  /*
  [$class: 'GogsProjectProperty', gogsBranchFilter: '', 
    gogsSecret: hudson.util.Secret.fromString('jenkins'),
    gogsUsePayload: false
  ], 
  pipelineTriggers([[$class: 'GogsTrigger']]),
  */
])


// Function that searches for files in a directory and returns the list of file names in a given pattern corresponding to the names of network assets
def listDevices(DIR_SALT_PROXY_PILLAR) {
    // Searching for grainds ID in the PILLAR file with the name in the pattern DIR_SALT_PROXY_PILLAR / pillar / DEVICE_TYPE / DEVICE-NAME_pillar.sls
     // The result is a list of DEVICE-NAME
    devices = sh ( returnStdout: true,
                   script: '''INITIAL=$(find $DIR_SALT_PROXY_PILLAR -type f | \\
                        sort | \\
                        grep -v top.sls | \\
                        cut -d "." -f1 | \\
                        rev | \\
                        cut -d "/" -f1 | \\
                        rev | \\
                        awk -F"_pillar.sls|.sls|_pillar" '{print $1}')
                        echo "ALL \\n $INITIAL"
                    '''
                 )
    return devices
}

pipeline {
/*
  backupConfig

  This text will be ignored during the execution of the pipeline.

  # --------------------- TASKS PIPELINE REQUIREMENTS -------------------- #

  # ------ STEPS TO BE EXECUTED ONLY IN THE 1st BUILD ------------------ #
  1) You need to add in Jenkins a new credential with the IDs:

    'git-access' => access credentials for the Git repository that
                    will store the settings of the network asset.
                    The repository URL must be defined in the variables
                   'GIT_PROTOCOL' and 'GIT_URL' in this same pipeline.
    'pipeconf-node-ssh-access' => credentials for accessing the Salt-SProxy server,
                               that will have the access settings to the
                               network asset
    'slack' => secret text type credential that has the Slack access ID,
               see step 7 for the ID.

  Access the Jenkins web interface and click on the menu:
  Jenkins Dashboard -> Credentials -> System.
  Then click on 'Global credentials (unrestricted)' and then click
  in: 'Add Credentials'.

  2) Configure the integration between Jenkins and each project at GIT using the
  webhook.

  3) Configure a node-slave in Jenkins with the ID: 'pipeconf-node'. This node
  it must be the same one that will have the Salt-SProxy service. To do this, access the
  Jenkins in the Jenkins Dashboard menu -> Manage Jenkins -> Manage Nodes -> New Node.
  The access credentials to be used to access this host must be the
  which are defined in the ID 'pipeconf-node-ssh-access'

  4) Configure the access information for network asset on the host that
  runs the Salt-SProxy service.

  5) Configure the email sending service on the host 'pipeconf-node'.
  Example: Postfix https://kifarunix.com/configure-postfix-to-use-gmail-smtp-on-ubuntu-18-04/

  6) Configure the user registered on the host 'pipeconf-node' to access it
  via SSH and use sudo without password (https://www.vivaolinux.com.br/dica/Liberar-comandos-de-root-no-sudo-sem-pedir-senha).

  7) Install and configure the Slack Notification plugin. See the tutorial on the page:
  https://plugins.jenkins.io/slack/
*/

  // Keeps only logs of the last 10 job runs and for 30 days.
  //options {
  //    buildDiscarder logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '30', numToKeepStr: '10')
  //}

  // The 'agent' section defines the name of the node registered in Jenkins that should execute this pipeline
  //agent { label 'pipeconf-node'}
  agent {
    node {
      label "${PIPECONF_NODE}"
    }
  }

  // The 'environment' section contains the declaration of variables used in the pipeline
  environment {
    // Environment variables should preferably be capitalized
    // to facilitate the differentiation of pipeline reserved words.
    // 'null', means that the variable will start with the null value (empty)
    LAST_TAG = null

    // Single quotes are used to unify the text
    WORKSPACE_DIR = pwd()
    PIPECONF_DIR  = "${WORKSPACE_DIR}/pipeconf"
    GIT_PROTOCOL  = 'http'
    GIT_URL       = 'pipeconf-server.domain.com.br:81/root/device-config'
    GIT_BRANCH    = 'master'
    MAIL_SENDER   = 'jenkins@pipeconf-server.domain.com.br'
    MAIL_SYSADMIN = 'aeciopires@gmail.com'
    //MAIL_SYSADMIN = 'jenkins@pipeconf-server.domain.com.br'
    MAIL_TEAM     = 'jenkins@pipeconf-server.domain.com.br'
    MAIL_NOTIFICATION  = true
    SLACK_NOTIFICATION = true

    // The value of this variable must be the same name as the defined Git repository
    // in the GIT_URL variable
    DIR_TMP = 'device-config'

    // Salt values
    DIR_SALT_BASE         = "${PIPECONF_DIR}/saltstack/srv"
    DIR_SALT_PROXY_PILLAR = "${DIR_SALT_BASE}/pillar"
    DIR_SALT_PROXY_STATES = "${DIR_SALT_BASE}/salt"
    DOCKER_IMAGE_SPROXY   = 'mirceaulinic/salt-sproxy:allinone-2020.7.0'

    // The value of this variable must be the same as the base directory name defined in
    // Salt, more precisely in the file 'export_config.sls'
    DIR_BKP = '/tmp/backups'

    DEVICES = 'router1,router2,router3,router4'
  }

  stages {
    stage('AT01 - Checkout configuration') {
      steps {
        echo 'Checkout dos arquivos de configuração dos ativos de rede...'

        // Creating a temporary directory to store asset settings
        sh "mkdir -p $WORKSPACE_DIR/$DIR_TMP; "

        dir("$WORKSPACE_DIR/"){
          // Checkout the git repository that version the asset settings
          withCredentials([usernamePassword(credentialsId: 'git-access', passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
            sh("""sudo git clone ${GIT_PROTOCOL}://${GIT_USERNAME}:${GIT_PASSWORD}@${GIT_URL} ${DIR_TMP};""")
          }
          
          // Checkout do repositório git que versiona as configurações dos equipamentos
          withCredentials([usernamePassword(credentialsId: 'git-access', passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
            sh("""git clone ${GIT_PROTOCOL}://${GIT_USERNAME}:${GIT_PASSWORD}@pipeconf-server.domain.com.br:81/root/pipeconf ${PIPECONF_DIR};""")
          }
        }
      }
    }

    stage('AT02-10 - Backup and Update the configuration of assets') {
      steps {
        script {
          echo '+++++> Obtém o backup da configuração dos ativos de rede...'
          // Managing the state of asset configuration
          sh """cd ${PIPECONF_DIR};
                chmod +x jenkins/pipelines/*.sh;
                bash jenkins/pipelines/exportBackup.sh ${DOCKER_IMAGE_SPROXY} ${DEVICES} ${DIR_BKP} ${DIR_SALT_BASE} ${DIR_SALT_PROXY_PILLAR} ${DIR_SALT_PROXY_STATES};
             """
        }
      }
    }

    stage('AT11 - Send config to git') {
      steps {
        // Accessing the temporary directory that stores the asset settings
        dir("$WORKSPACE_DIR/$DIR_TMP") {
          // Sending asset backup to Git
          withCredentials([usernamePassword(credentialsId: 'git-access', passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
            sh """sudo cp -R ${DIR_BKP}/* .;
                  sudo chmod -R 777 .;
                  git config --global user.email \$USER@\$(hostname --fqdn);
                  git config --global user.name \$USER;
                  git config --global push.default matching;
                  git checkout -B ${GIT_BRANCH};
                  git add *;
                  git pull origin ${GIT_BRANCH};
                  if git diff-index --quiet HEAD; then
                    echo "[INFO] Nothing to commit";
                  else
                    echo "[INFO] Send changes to git repository";
                    git commit -m '${currentBuild.fullDisplayName} - Job de backup dos ativos de rede';
                    git push ${GIT_PROTOCOL}://${GIT_USERNAME}:${GIT_PASSWORD}@${GIT_URL} ${GIT_BRANCH};
                  fi
               """
          }
        }
      }
    }
  }
  post {
    always {
      // Removing the temporary directory to avoid conflict when running
      // git clone on the next run of the pipeline
      sh """sudo rm -rf "$WORKSPACE_DIR/$DIR_TMP";
            sudo rm -rf "$PIPECONF_DIR";
            sudo rm -rf "$DIR_BKP";
         """
    }
    success {
      // AT12 - Send notifications
      script {
        if (MAIL_NOTIFICATION == 'true') {
          mailSuccess()
        } else {
          echo "[INFO] Email notification is disabled."
        }
      }
    }
    failure {
      // AT12 - Send notifications
      script {
        if (MAIL_NOTIFICATION == 'true') {
          mailFailure()
        } else {
          echo "[INFO] Email notification is disabled."
        }

        if (SLACK_NOTIFICATION == 'true') {
          slackFailure()
        } else {
          echo "[INFO] Slack notification is disabled."
        }
      }
    }
  }
}

/*
  My functions
*/

void mailSuccess() {
  // When the pipeline runs successfully, the following email will be sent
  mail to: "${MAIL_SYSADMIN}", cc: '', bcc: '',
  from: "${MAIL_SENDER}",
  subject: "[OK] Backup dos ativos de rede - Job: ${currentBuild.fullDisplayName}",
  body: " Sucesso no backup da configuração dos ativos de rede: '${DEVICES}'. \
        \n Acesse o Dashboard a seguir para visualizar o resumo do log. \
        \n ${env.JENKINS_URL}/blue/organizations/jenkins/${env.JOB_NAME}/detail/${env.JOB_NAME}/${env.BUILD_NUMBER}/pipeline \
        \n \
        \n Ou acesse a URL ${currentBuild.absoluteUrl}/consoleFull para visualizar o log completo. \
        \n \
        \n \
        \n Detalhes do job: \
        \n Nome e ID: ${currentBuild.fullDisplayName} \
        \n Duração: ${currentBuild.durationString} \
        \n Nome do node que executou o job: ${PIPECONF_NODE} "
}

void mailFailure() {
  // When the pipeline execution fails, the following email will be sent
  mail to: "${MAIL_SYSADMIN}", cc: '', bcc: '',
  from: "${MAIL_SENDER}",
  subject: "[ERROR] Backup dos ativos de rede - Job: ${currentBuild.fullDisplayName}",
  body: " Falha no backup da configuração dos ativos de rede: '${DEVICES}'. \
        \n Acesse o Dashboard a seguir para visualizar o resumo do log. \
        \n ${env.JENKINS_URL}/blue/organizations/jenkins/${env.JOB_NAME}/detail/${env.JOB_NAME}/${env.BUILD_NUMBER}/pipeline \
        \n \
        \n Ou acesse a URL ${currentBuild.absoluteUrl}/consoleFull para visualizar o log completo. \
        \n \
        \n \
        \n Detalhes do job: \
        \n Nome e ID: ${currentBuild.fullDisplayName} \
        \n Duração: ${currentBuild.durationString} \
        \n Nome do node que executou o job: ${PIPECONF_NODE} \
        \n Diretório do node utilizado pelo job: ${WORKSPACE_DIR}"
}

void slackFailure() {
  // A notification will also be sent to the slack... on the #pipeconf channel
  slackSend (color: '#FF0000', message: "@here [ERROR] Backup dos ativos de rede: '${DEVICES}' no node '${PIPECONF_NODE}' - Job '${currentBuild.fullDisplayName} - Acesse o log em: ${currentBuild.absoluteUrl}/consoleFull .");
}
