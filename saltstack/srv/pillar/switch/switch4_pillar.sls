{
	"data": "ENC[AES256_GCM,data:BEGSO2F8Xlh0Flr5aAaULhz8vIcYskDjq7In2rm9fKq2XsAcjrfpauGA47FIWCHWwPAztKiMcBaIEVBjVYG6w4jYT8aM+9N6agyr5iNmU0AfGuwUHx+cYMuUxbFnNvWrpRXQo+Tlf5L4uQZZ10kY8/z1gTeUrQEVSut/XB6RxXb8wnvM2VFYOHHz1/CgXRU5b04QYAcAgeV1zX8GSsHw5WemmbTPr7e9/+VWWcj0KrlyJDD0Uq2ycZN72d9ITFTcFXfi/I8jEFR/WLEuiMT8080ryyMpCzyAN7jwRVerFd03P2stj1L1iYhM99EVEP3ksmQnSSo7W2nPIqrJgm9QVbaHHQFXkZ0SS3AvyOuesWbx2SYEvaZfNPVPHxYNtC03HkqWU41mGSUznF5i1JIMItF4Cl0d8ST/sfCAdXAoeOWzVYtQo7yjf3fwIFIaEQZLmXI1FiyYc92xq3YXJTCF7JdcUQasp7ms5S+iSXoaj+tCg8oi6ePQ429uaa0MBQF+Iac4tr5YJcyqiAOqhpLT5jw434yKvXH06fewYgdM58aa4dLK/Q==,iv:HLoToRrSoOPDIcOLz9pRy56wyR6JyYmQvPsPC3XZkq0=,tag:LRU4l4sYYoH5B/MdUK4hug==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:16:35Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFvgLMYR2MmvgxwSPwgteGfAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM398IKT9Pl9KrDzgXAgEQgDtBzP1kBq5b5q9XxPu+Zy59hUNSpaODvOPorMKamLw3/ysbfzLS3fIroLh3J18wOtAZA8qKURJ0ZiEWMg==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:16:36Z",
		"mac": "ENC[AES256_GCM,data:uQP94yqBBHAiEE2V4Yb9tidpUMpc/MlC4vOZ3MuGEEdqDmuZNOD3NHy7iLtYP+t3cO0Q09fIIgyTvUo3v3pHHV0qQ07wmIhz0Rg/S0lt7lCL5qrB80oKX3cikc1aXE/+2iSVze55pvjTZ4UR8RA5CARu1oVSrUSgb1wHmzgzDV8=,iv:pVqbY92wjCnGxDcALTQbhh4osH+1x3/oMoD7NWaG7Mo=,tag:2hfhKQVFdoe3fEDgPWgw/w==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}