{
	"data": "ENC[AES256_GCM,data:dlLVIpHoetdUW5pxd+0z/+efsfzrOyf0sYInpf6CZeCbiFvCgHjZlzlslukYWXVsNa1UWPo7YcHH1KpScvhS7zM0wfCvzqk83+Ga0h0qjea99OYryN7Ey2+Ap191XMX8TRyiMa5tMdhTZKQ2kwuaBCyQDg+UwdHfs28ccC3eP/ktbndBfqWX0h8T8lPZO2Pwt1rHN8g8M3yIdxZ/7KSwXcJo1XDYEKCXtbg74RB321X1vd3A0eaadvgmaQVA1vbdCURZly3h7tMruk9o0VeDYGsU8CoKrUL0LzmwFw4l8uHGDpCHrvikYKUbbt2/cw9OEiUd0TTa/cBAlLZ98gikfrPguH+ffGgMR1Qr6GNUZhy0XDn2rwqJLaocabCtOG+LS90dNqCk761L0I3Dmo54ZqA2Qku/DBgX3+P3GnHV/HJhjwChH784KwcJ+MbNRkLj7dyY00Q9BqZKgdzASpZmjoVdAKVIWtlSIfwmB81iRcFNYsDZooS4NJG4Yu7BPsQsBKcrypTsI3pOGthz3T9mBh353st/pejqfjCgbGFvRATcLAnjoA==,iv:/YNdeZeVRlAOAt7NehEW0sa7YH+SRhMq5fgUaa02fR0=,tag:+xfq9KVpCSdgY2IdjakkvQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:17:30Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwGBuRzq/Tfu+sfDBTAeOMzmAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM6Aacmm5V3SN6V3TRAgEQgDtlS+4qRMHHKetpsUkDDaFsp2RR5YiC0SBgl3cDDUGYGdzOP5vpk3G5Uqi92HRyT6mYXuVQdgTRjWpS2g==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:17:31Z",
		"mac": "ENC[AES256_GCM,data:rah6CMu3s8ZCFu13T18BSaJTk5dXSVQ1ANy+Vr2lUsiWFr8K48a/thP15PHZtClMWa+bnrgvmlg9hG0+mFXqa+edhj6Gy4GVx8V99cubfN+5v+Yjkj/1al/4PlDpSXxtixr5holfITEypyPVIn+/j+sCOI062oYN0mwbGaR2Plo=,iv:hgX2iO3va8q4QrD25AhLDN5ij3YIdgO1JGECH5X4hO4=,tag:WpFyNVHCQAkwZuo/bYmV/Q==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}