{
	"data": "ENC[AES256_GCM,data:P+VPFBUc5GB4y/LJdl3Z4hgLGQollWoZn/sVUnwPUWt9ZkKceMZn+sSCXdWmDloCUpAHKWTAPh51AnApIdh4LHX4TTDn5Dvrff7kWu7H+Uh56os8uyk/p/QS7s4dTUoMrE2LuQ0GFLji2tTFUTFENMFY9wHjn/0VXbhMb8QmXo3lh9mn3+abmgMVSSdSAaX4GKSivl9OLmPi+YHibaCbqiPoGQZM3MmJ5pDVMdpF98sRxwyeYkt7z0Qw5N6x9aXZuGEtH9AwA/1FjcyIR4S0firnzD/fAjLK6dTgMkDdO0OzjgEJcCI8ctKRUtehq++pAnu6kAKSSfANpTczNV4yh5qFoOOCfPR23tTl7K/UXt8M6mRek0nk5yllrzZ76l8wS71Y9CpLQF4He9UAGNPchjetXsotCAIGsZEKlxNDNX5TbCKioQG5FfdYhzdYwELHGZOd3BWunVUzLk3bMmbvKuz8bwTJVqkFrw8jyB9vk2DW5zafnzXUG1IRui3eKimtVNkIEnPwzAZ/XFkfAp/bv5R2CGoidZMnGsLPqD79OTD2tf6oBw==,iv:ZuiN2nDnV145FlVuQFKJLWkFQSm+I71okazU4LB0oqs=,tag:rDvd7dTkCuXKEEgx4yzI3Q==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:17:21Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwF25A8kPQnhtQZG42awgUECAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM4iYNGJhVXFmEwuswAgEQgDt+5jteeCbe8rACwYOQNPhhGFVsSGG4ZAS7cUTV52mqD29hq4n0yJRsSkjGTWPdMrmc5a25QgJd4JGNyA==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:17:22Z",
		"mac": "ENC[AES256_GCM,data:PLRw3mgT8UvXj0o2nlNb7b++G7jaONPvJeX/JhgR4pP0d9SO+T1utH+M5vqlkQ7J44yEykeiikLgknX7aV1o1K2sK4WJ1UaVPh1xIjKNYQCchBhvMyhPTItGVaxlKGp0Qaz9WP8LUhGbzyMmu3TLy7s0tJ81C2oOzwT4Gha5TVI=,iv:ERv34wPhK5eQlhtBrDe5aqmXtHTwveD4BONcT53WSxc=,tag:9tBz0/G6KX74FtLGKamFjw==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}