{
	"data": "ENC[AES256_GCM,data:4l2R1ZtEhFjDY+uzUsi7wZTrXFBKr+6EyrWIKH4w/REcxOsDy4ym0X9gV2mbm8sQslnmBT1pIJxgnB+wLZj33+8eIMSClD+CwI+DNakoX/JW/j5QAPZJQKtQMwzBxgdVZymw3PUVrqfxuUQ9tzyvw3IepDFdQiyThbSudrMP/FWoRz9w4WvVcRhfll/BNSSxjvsZmwgnNpOOZpD+8SxsAnovk8o6WUJ0VVqqCHSZjfulWH56QJjoELBAZ9cUF/DfcnQxjvbUqPNNTVuF+6wC57jU1H6S1Ewrvc2261agsWsKT+7lbzcAZKavAir6iocKHxL5FKFeRBZoCPNX4K/CKaMc49SQYawU48B7F25WVnj7p6TciXdEUTG3DDrLudFf0SNXhtRpgeD1jlFOIFQqBBU/d7TQRTvRcIij52Zsr+RcHgRlvg+K16Ggus8C2YbW/IDG6aCok1VbCve4D26nqNtPhZevITfHIrrLd7sCIC0fHOvMWHXRohNndNg51RkCU/a3tA==,iv:hAj3asZ2h/Qvh1nmftZTBKpw6nyKsIPaefYXW4O5SyE=,tag:TBzDeR5gedM/Ce1qBwoEuQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:39Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwErZOlkaXVLOC2N1kboduI5AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMyOY1uN7D1Rgzk8/bAgEQgDsmAk5PmEQH1MoFPwctSW5IpfLmUJjExQEhstffo7+OtIMHtxMCfmacx6z4KJMiBR4TF8PcvFbm8pWGog==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:40Z",
		"mac": "ENC[AES256_GCM,data:NWTIzs6MWeCWO+BQ+1dvY+4pcqxX1/Y/YIo72pqqm6bt6DS7vm0V9ShX62DnZo+7NSN/4x9/bY8VscThVsv+SWDHu6qxdJI29SbCCkHMjwdx5QOeiBAzr/PJ8S4TzhSU6pyTgYIpptsp9xuUQr+LKsY2lxAfLegiQsCj2E/HhoY=,iv:M3qL3zagZlni/mv617xRLCMRvyouVfgFCx6Xz44BtjI=,tag:tAYOKcA8PLitcUq7lxX6+g==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}