{
	"data": "ENC[AES256_GCM,data:4SmTIzjnLKVGCjln8fcp0dppcWz5bgvLagCGp8z4ibniH8ODUuYm8nf0E+gyxWdFA4HNqcspsL6tCojKIvh9nf/ou0+rjRHt6VwgDS9O1Qz6yBrPZAWntALiGcrExXu7cbuLTScd6UnY26E+AOV7sKoGn7RWY01zl0pAwYa5eRCQMmewAIyEbTVw650hTOC6MsN3GOVItWO4zpMx9Kw0EZcE8IGCcestM5FfxgUoEdHv7jxBfV3NWDqsMbY3av2Fy4+NX9WpgMjXG1cMtD6leiTuytSQSeP9n1y66FnJqJDq+BlSaWOES9TkIkEJRXnocf/j9WIgIE8WFPQJnmHM7rX08GAO8OcyAFWdFIv/oakKFghbXVE3Ob8mRbx7BBgr6ysHjhZBj7H2yiMxqDOBdOBReq2ONW+6/E/vGRNXjp3FiKhNOmU2cNE+yAFDCMQfFy9lyw0AE7YuLPCD4wsJXSnfJ48FTVqYL4arn3JJtSFsLExA9/yeoFbMYFDMGoztIjCl6GPCnbejhkfAujyZnB0d9KA9gWLX+Qf3hMACyHTSSIfkTw==,iv:9XYMwGcQj5xaosKLbqaf88ai/NwBIqOMT6yJlxLyWzA=,tag:GXfGw15uRZr8N6K28fpZaQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:16:30Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwE4nN6XE4URoxfZXP6eaNtkAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMTJ1YRHbobC3uWv6tAgEQgDuUt30wzTwyrF7m5rehDFS5X5+rDGZvXF0bPCoqxoKH3fYthgq1kqdaN+bEqgaSxk/76R3sB+pRgDobgQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:16:31Z",
		"mac": "ENC[AES256_GCM,data:PAGxXCBgIV7RCcgD9U28g2BJisimASbmYbUTAH8l/F1wbXYEKOg8QFDFCF4fv/qC99FNIPcOYrGl78H+FXReziNQdExFiWwrsJ6uzC7xRuN0v/pXYFFUsRaxwrpk5HkF5DBelo/7i8rn3BSLV2+zWAvH7qmu7P5oG6Bzx2D9joc=,iv:lOYmZHKmS1+le+Xmykojr/vSmyC9/TaW28gh8OlaaAQ=,tag:WCvfjmiAhpREcYJ/NYF77A==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}