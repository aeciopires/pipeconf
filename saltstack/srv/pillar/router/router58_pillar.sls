{
	"data": "ENC[AES256_GCM,data:ppSK6WB1kksfsZfhd6IoL9Sc8RvGm3FZLs+weE2Ih5o9QN73tlCLJZIzzumpk/fTV8342c7l+80vP9EYhGfJG783ymBsaT8ub5RQ7B+GKvjgHwqeJedO7F9H38R//CJAzbsjDX1z3TAKkvlYADyaxzIRQO+h8T0ubWMtNcewI0ciCQSEHQstab118H7OiGojBG5hyEQjdpGv++UnVVivGjs7cmme/sQdCL4lB3/PetjiLmAnpgSA2rH9w+fpL1OmV0Kxc8UAgyyCw5U/mlhtQO1vquIytQyiwF4ycQYcbFSoxetmMH3QgJ+4hi5Oq1qbHqZScOWaQXrzuumFjG/GZC7aPO8XvOSMSRDFdfM2YwcquMwUfaZSii8dzumhkCQGlJsWUdgMV9gxK+Kv4ccTEtqb0jCynf2ZnPUB9kS8jx9MEamvYwCPiT288dOP7uwOZHNOFxdzNEJxddrF5Y1vQ4bFg8E81I3SEuJSXuVBErkEDSSS1+sQ8CZ1aF5kCx+N6r7BiA==,iv:qp/SM59rPfXsBVgCAu4KBRaW+wFDXTMLC4b6BMHJxGo=,tag:/H8EdYr8aC9eqvHxJU4rlA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:34Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHqnBFUsNY7KAOYO6SGSEuUAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMjU+BrgB2v91ynkMWAgEQgDudyOQ+IBkADsLnkr8qHdNvJXIsSa9RU0N/5wCNtFz2wbn4oB8B/YjncVxK1RAst3jv5sdMYjpmmzLzBw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:35Z",
		"mac": "ENC[AES256_GCM,data:YueN0bxKzvIJkRlHBjNNfnB7pe1UiL2OBFrIUSnwqZHtvyr14VsKL5N4iXP5SBHDE0ZSDYyVk32TMywqrVhu4JMSOvG58FvZlAubpDXzpFfZVidpeMsj1UQNglgPujN6UrMORkkuI2rWpx1rprMGg3inyKvtkP4dGa17q1LhQik=,iv:nJMuKbsue7agGQxLFHEYI2IUqyTjew9MJR0+3FMjhbw=,tag:qHLWCjCUH9U93n7pradDUg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}