<!-- TOC -->

- [[OPCIONAL] Instalando o GNS3 na GCP](#opcional-instalando-o-gns3-na-gcp)
  - [Executando o Wireguard em um conteiner Docker](#executando-o-wireguard-em-um-conteiner-docker)
- [Acessando os ativos de rede no GNS3 remoto](#acessando-os-ativos-de-rede-no-gns3-remoto)
- [[OPCIONAL] Criando um Roteador Cisco c3640 no GNS3](#opcional-criando-um-roteador-cisco-c3640-no-gns3)
- [[OPCIONAL] Criando um contêiner Docker para Switches](#opcional-criando-um-contêiner-docker-para-switches)
- [[OPCIONAL] Configuração Manual do Salt-SProxy](#opcional-configuração-manual-do-salt-sproxy)
- [[OPCIONAL] Cadastro Manual dos Ativos de Rede](#opcional-cadastro-manual-dos-ativos-de-rede)
  - [[OPCIONAL] Exemplos de Uso Manual do Salt-Sproxy](#opcional-exemplos-de-uso-manual-do-salt-sproxy)
  - [[OPCIONAL] Obtendo a Configuração de Equipamentos com o PipeConf](#opcional-obtendo-a-configuração-de-equipamentos-com-o-pipeconf)

<!-- TOC -->

# [OPCIONAL] Instalando o GNS3 na GCP

Execute as instruções deste tutorial: https://binarynature.blogspot.com/2018/08/gns3-v2-google-compute-engine.html?m=1

Na parte referente a configuração do Wireguard, pule e execute seguindo as instruções da seção a seguir.

Na configuração final do GNS3, acesse o menu *Help > Setup Wizard*.

Selecione a opção *Run appliance on a remote server (advanced usage)*. Clique em *Next*.

Na tela seguinte defina os seguintes parâmetros:

* Host: aeciopires.duckdns.org
* Port: 3080/TCP

Não precisa habilitar a autenticação.

Clique em Finish.

Acesse o console da GCP em:. https://console.cloud.google.com. Selecione o projeto no qual foi criado o servidor ``gns3server``, usando o ansible e as instruções do tutorial anterior.

Acesse o menu *VPC Network > Firewall Rules*. 

Localize a regra *default-allow-wireguard*. Edite a regra.
No campo *Source IP ranges* apague a entrada ``0.0.0.0/0``, para evitar que qualquer pessoa tenha acesso ao servidor remoto do GNS3 e use indevidamente causando transtornos financeiros e técnicos.

Ainda no campo *Source IP ranges*, adicione o seu IP público. Pode ser obtido com o seguinte comando:

```bash
curl ifconfig.me
```

Ao final do IP informe a máscara de rede /32. Exemplo: para o IP ``172.159.238.22``, a entrada correta deve ser ``179.159.238.22/32``. Dessa forma só você terá acesso ao servidor remoto do GNS3. 

> **Atenção**: Essa entrada deve ser mudada sempre que você obter um novo IP público, caso esteja usando uma conexão de Internet residencial/empresarial na qual não há um IP público fixo.

Nesta mesma regra de firewall, localize o campo *Protocols and ports*. 

Marque a opção ``tcp`` e adicione as portas ``22``, ``3080`` e o range de portas ``5000-10000``. Exemplo de como ficará a regra: ``22,3080,5000-10000``.

Na regra opção ``udp`` e adicione o range de portas ``10000-20000``. Exemplo de como ficará a regra: ``51820,10000-20000``.

O restante da configuração da regra deve ser mantida como está. Clique em ``Save``.

## Executando o Wireguard em um conteiner Docker

Criando um conteiner docker do Wireguard.

```bash
sudo mkdir -p /docker/wireguard/

docker create \
  --name=wireguard \
  --cap-add=NET_ADMIN \
  --cap-add=SYS_MODULE \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/London \
  -e SERVERURL=aeciopires.duckdns.org \
  -e SERVERPORT=51820 \
  -e PEERS=1 `#optional` \
  -e PEERDNS=auto `#optional` \
  -e INTERNAL_SUBNET=172.16.253.0 `#optional` \
  -p 51820:51820/udp \
  -v /docker/wireguard/config:/config \
  -v /lib/modules:/lib/modules \
  --sysctl="net.ipv4.conf.all.src_valid_mark=1" \
  --restart unless-stopped \
  linuxserver/wireguard
```

Iniciando o conteiner do Wireguard.

```bash
docker start wireguard
```

Visualizando os logs do conteiner do Wireguard.

```bash
docker logs -f wireguard
```

As informações do ``peer1`` pode ser encontradas em ``/docker/wireguard/config/peer1/peer1.conf``.

```bash
cat /docker/wireguard/config/peer1/peer1.conf
```

> **Atenção**: Ao final da execução do comando ``ansible-playbook site.yml`` mostrado no tutorial anterior, serão exibidas umas linhas semelhantes a seguir.

```bash
TASK [print wireguard public key] ***********************************
ok: [104.154.103.35] => {
    "public_key": "1oT8aB55GaUXTTmcAuTd4YpvlniDv8wkFqhx8UNtewU="
}
```

Copie o valor do campo ``public_key``. 

Edite o arquivo ``/docker/wireguard/config/peer1/peer1.conf`` e substitua o valor do campo ``PublicKey`` pela nova chave pública recém copiada do terminal da GCP.

Reinicie o conteiner do wireguard com o seguinte comando.

```bash
docker restart wireguard
```

As informações do ``server`` podem ser encontradas em ``/docker/wireguard/config/wg0.conf``.

```bash
cat /docker/wireguard/config/wg0.conf
```

Parando o conteiner do Wireguard.

```bash
docker stop wireguard
```

Mais informações sobre o Wireguard podem ser obtidas nas páginas a seguir.

* https://hub.docker.com/r/linuxserver/wireguard
* https://www.wireguard.com/install/
* https://www.wireguard.com/
* https://docs.gns3.com/1EjaCJlcM0RYIyyE_MbJOYNatEBnffOX5tcnJShR_KfY/index.html
* https://docs.gns3.com/appliances/internet.html
* https://gns3.com/qa/cannot-find-internet-for-gns3-vm
* https://raw.githubusercontent.com/GNS3/gns3-registry/master/appliances/internet.gns3a
* https://www.reddit.com/r/gns3/comments/dosuc1/internet_appliance_fails/
* https://gns3.com/marketplace

# Acessando os ativos de rede no GNS3 remoto

> É pré-requisito que os ativos de rede simulados no ``gns3-server`` remoto estejam com o SSH devidamente configurado, com usuário e senha remoto cadastrados e que haja conectividade entre o PipeConf e os ativos simulados no GNS3. O GNS3 cria uma rede privada para os ativos simulados, geralmente na rede 192.168.122.0/24. Essa rede usa uma conexão NAT (*Network Address Translation*). Para que o PipeConf consiga acessar esses ativos, será necessário usar o ``sshuttle``.

Instale o pacote ``sshuttle`` na sua estação de trabalho utilizando o seguinte comando:

```bash
pip3 install sshuttle
```

Execute o seguinte comando para estabelecer a conexão com o host remoto que executa o ``gns3-server``.

```bash
sshuttle -r IP_PUBLIC_HOST NETWORK_NAT_GNS3
```

> **Atenção**: Mantenha o comando em execução, se o processo do ``sshuttler`` for encerrado, a conexão via SSH com o ativo de rede ser encerrada também.

Exemplos:

```bash
sshuttle -r gns3@192.168.1.40 192.168.122.0/24
sshuttle -r aeciopires.duckdns.org 192.168.122.0/24
```

Onde:
* **gns3**             => pode ser trocado pelo usuário SSH da VM que executa o GNS3.
* **192.168.1.40**     => pode ser trocado pelo IP público ou privado (o que tiver acessível) da VM que executa o GNS3 e que também é acessível a partir do host no qual está sendo executado o PipeConf. Assim, o ``sshuttle`` criará um túnel e adicionar uma rota para a rede 192.168.122.0/24 que só existe na VM do GNS3.Tem que manter esse ``sshutle`` rodando o tempo todo, se ele fechar, o PipeConf perderá a conexão com os ativos.
* **192.168.122.0/24** => pode ser trocado pelo endereço e máscara de rede usado pelo GNS3 para simular os ativos de rede. Para obter o endereço de rede utilizado pelo GNS3, você pode consultar o endereço na interface de rede ``virbr0`` com o seguinte comando:

```bash
ip addr show dev virbr0

4: virbr0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 0e:dc:a4:68:18:76 brd ff:ff:ff:ff:ff:ff
    inet 192.168.122.1/24 brd 192.168.122.255 scope global virbr0
       valid_lft forever preferred_lft forever
```

Agora abra outro terminal e execute o seguinte comando para acessar uma ativo de rede simulado no GNS3 como se estivesse na mesma rede.

```bash
ssh USER@ADDRESS_DEVICE
```

Exemplo:

```bash
ssh aecio@192.168.122.199
```

> **Atenção**: Dependendo do ativo de rede simulado no GNS3, pode ser que a conexão SSH seja rejeitada porque o ativo utiliza cifra de criptografia muito antigas e vulneráveis. Isso pode ser resolvido em uma ambiente de testes (não faça isso em produção) seguindo as instruções [desta página](https://blog.ffelix.eti.br/ssh-no-matching-key-exchange-method-found-their-offer-diffie-hellman-group1-sha1/) ou [esta página](http://blog.welrbraga.eti.br/?p=2915). 

Basicamente você pode criar o arquivo ``/home/pipeconf/.ssh/config`` com o seguinte conteúdo:

```
Host 192.168.122.*
    Ciphers aes128-cbc,aes256-gcm@openssh.com
    KexAlgorithms +diffie-hellman-group1-sha1
```

Essa configuração é feita apenas uma vez e no computador que executa o PipeConf no lado do cliente SSH, que é quem vai acessar o ativo simulado no GNS3.

Referências:

* https://gns3.com/community/featured/tip-on-labbing-with-a-gns3-oob-n
* https://binarynature.blogspot.com/2019/06/ssh-local-port-forwarding-remote-gns3-server.html
* https://www.youtube.com/watch?v=mngey5I0RIo
* https://www.youtube.com/watch?v=z73Zrhh_n3U
* https://sudonull.com/post/148932-GNS3-and-dynamips-on-a-remote-server
* https://blog.ffelix.eti.br/ssh-no-matching-key-exchange-method-found-their-offer-diffie-hellman-group1-sha1/
* http://blog.welrbraga.eti.br/?p=2915

# [OPCIONAL] Criando um Roteador Cisco c3640 no GNS3

Veja as instruções neste [tutorial](other_files/enable_ssh_ios.txt).

# [OPCIONAL] Criando um Switch Arista no GNS3

Veja as instruções neste [tutorial](other_files/enable_ssh_eos_arista.txt).

# [OPCIONAL] Criando um contêiner Docker para Switches

Switch Cisco:

```bash
docker run -d --name switch2 -p 3222:22 -e SWITCH_MODEL="cisco_generic" internap/fake-switches
```

ou

```bash
docker run -d --name switch2 -p 3222:22 -e SWITCH_MODEL="cisco_2960_24TT_L" internap/fake-switches
```

Switch Brocade:

```bash
docker run -d --name switch3 -p 3223:22 -e SWITCH_MODEL="brocade_generic" internap/fake-switches
```

Switch Dell:

```bash
docker run -d --name switch4 -p 3224:22 -e SWITCH_MODEL="dell_generic" internap/fake-switches
```

Referência: https://hub.docker.com/r/internap/fake-switches/

# [OPCIONAL] Configuração Manual do Salt-SProxy

Existem pipelines no diretório [``../jenkins/pipeline``](../jenkins/pipeline) que automatizam a configuração do Salt-Sproxy.

Os passos a seguir são opcionais para configurar o Salt-SProxy manualmente.

Crie as seguintes variáveis de ambiente de acordo com a distribuição GNU/Linux.

Debian/Ubuntu:

```bash
sudo echo "export SALT_PROXY_PILLAR_DIR=/srv/pillar" >> /etc/bash.bashrc

sudo echo "export SALT_DIR_BASE=/srv" >> /etc/bash.bashrc

sudo echo "export SALT_PROXY_STATES_DIR=/srv/salt" >> /etc/bash.bashrc

sudo echo "export TMP_DIR_BACKUPS=/tmp/backups" >> /etc/bash.bashrc

sudo echo "alias salt-sproxy='f(){ docker run --rm --network host -v \$SALT_DIR_BASE/master:/etc/salt/master -v \$SALT_PROXY_PILLAR_DIR:/etc/salt/pillar/ -v \$SALT_PROXY_STATES_DIR:/srv/salt/ -v \$TMP_DIR_BACKUPS:/tmp/backups/ -ti mirceaulinic/salt-sproxy:allinone-2020.7.0 salt-sproxy -b 128 \$@; }; f'" >> /etc/bash.bashrc

source /etc/bash.bashrc
```

CentOS:

```bash
sudo echo "export SALT_PROXY_PILLAR_DIR=/srv/pillar" >> /etc/bashrc

sudo echo "export SALT_DIR_BASE=/srv" >> /etc/bashrc

sudo echo "export SALT_PROXY_STATES_DIR=/srv/salt" >> /etc/bashrc

sudo echo "export TMP_DIR_BACKUPS=/tmp/backups" >> /etc/bashrc

sudo echo "alias salt-sproxy='f(){ docker run --rm --network host -v \$SALT_DIR_BASE/master:/etc/salt/master -v \$SALT_PROXY_PILLAR_DIR:/etc/salt/pillar/ -v \$SALT_PROXY_STATES_DIR:/srv/salt/ -v \$TMP_DIR_BACKUPS:/tmp/backups/ -ti mirceaulinic/salt-sproxy:allinone-2020.7.0 salt-sproxy -b 128 \$@; }; f'" >> /etc/bashrc

source /etc/bashrc
```

Crie o diretório de backup dos arquivos de configuração a serem exportados dos equipamentos.

```bash
export TMP_DIR_BACKUPS=/tmp/backups

mkdir -p $TMP_DIR_BACKUPS
```

# [OPCIONAL] Cadastro Manual dos Ativos de Rede

Existem pipelines no diretório [``../jenkins/pipeline``](../jenkins/pipeline) que automatizam o cadastro dos ativos de rede.

Os passos a seguir são opcionais para cadastrar os ativos de rede manualmente.

Crie o diretório ``/srv/pillar/router``.

```bash
mkdir -p /srv/pillar/router
```

Crie ou edite o arquivo ``/srv/pillar/top.sls`` e informe o seguinte conteúdo.

```
base:
  router1:
    - router/router1_pillar
```

Crie o arquivo ``/srv/pillar/router/router1_pillar.sls`` e informe o seguinte conteúdo.
Substitua ``IP_ROUTER`` pelo endereço IP do roteador, ``USERNAME`` pelo nome do usuário SSH
do roteador, ``PASS`` pela senha do usuário, ``SSH_PORT`` pelo número da porta SSH
(a padrão é 22/TCP) ou ``TELNET_PORT`` pela porta do Telnet (a padrão é 23/TCP) e ``ENABLE_PASS``
pela senha do modo privilegiado do equipamento para alterar a configuração
(deixe as aspas simples vazias se não tiver definido nenhuma senha).

Exemplo 1: Para configuração via SSH.

```
proxy:
  proxytype: napalm
  driver: DRIVER
  host: IP_ROUTER
 username: 'USERNAME'
 passwd: 'PASS'
  optional_args:
    port: SSH_PORT
    transport: ssh
    secret: 'ENABLE_PASS'
    dest_file_system: 'flash:'
```

Exemplo 2: Para configuração via Telnet.

```
proxy:
  proxytype: napalm
  driver: DRIVER
  host: IP_ROUTER
  username: 'USERNAME'
  passwd: 'PASS'
  optional_args:
    port: TELNET_PORT
    transport: telnet
    secret: 'ENABLE_PASS'
    dest_file_system: 'flash:'
```

O nome do driver varia de acordo com o equipamento que será gerenciado. A lista de equipamentos suportados pelo Napalm está disponível em: https://napalm.readthedocs.io/en/latest/support/index.html

O protocolo de transporte informado anteriormente no parâmetro ``transport``, também pode variar de acordo com o driver selecionado. A lista de drivers e protocolos de transportes suportados está disponível em: https://napalm.readthedocs.io/en/develop/support/index.html#the-transport-argument

Se for gerenciar os equipamentos usando o Napalm Yang Doc, a lista de equipamentos suportados está disponível em: https://napalm.readthedocs.io/en/yang_doc/support/index.html

O valor do parâmetro ``dest_file_system`` também muda de acordo com o equipamento. É necessário configurar previamente onde serão armazenadas as configurações
enviadas ao equipamento antes de aplicar. Veja os seguintes links.

* https://napalm.readthedocs.io/en/develop/support/index.html#list-of-supported-optional-arguments
* https://www.cisco.com/c/en/us/td/docs/switches/lan/catalyst3850/software/release/3se/system_management/configuration_guide/b_sm_3se_3850_cg/b_sm_3se_3850_cg_chapter_010011.html
* https://networklessons.com/cisco/ccna-routing-switching-icnd1-100-105/cisco-ios-filesystem
* http://cisco2960.over-blog.com/2013/12/cisco-ios-file-system.html
* https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/ifs/configuration/15-s/ifs-15-s-book/ifs-using.html

Crie o arquivo ``/srv/master`` com o seguinte conteúdo.

```
nodegroups:
  routers: 'router*'
  switches: 'switch*'

autoload_dynamic_modules: False
pillar_roots:
  base:
    - /srv/salt/pillar
    - /etc/salt/pillar

file_roots:
  base:
    - /srv/salt/

use_superseded:
  - module.run
```

Teste a conexão com o equipamento via Salt.

```bash
salt-sproxy 'router1' net.connected
```

> **Atenção**: Os passos anteriores precisam ser repetidos a cada equipamento de rede (switch/roteador) que for gerenciado manualmente via Salt+Napalm.

## [OPCIONAL] Exemplos de Uso Manual do Salt-Sproxy

> **Atenção**: Os comandos dessa seção são opcionais e devem ser utilizados apenas para throubleshooting.

Obtenha os dados do equipamento.

```bash
salt-sproxy 'router1' grains.get model
salt-sproxy 'router1' grains.get vendor
salt-sproxy 'router1' grains.get os
salt-sproxy 'router1' grains.get version
salt-sproxy 'router1' grains.get uptime
salt-sproxy 'router1' grains.get host
salt-sproxy 'router1' grains.get interfaces
salt-sproxy 'router1' grains.get username
salt-sproxy 'router1' grains.get serial
```

Obtenha a tabela ARP do equipamento.

```bash
salt-sproxy 'router1' net.arp
```

Obtenha os endereços IPs das interfaces do equipamento que estiverem configuradas.

```bash
salt-sproxy 'router1' net.ipaddrs
```

Obtenha informações de MAC, status e taxa de transferência de cada interface do equipamento.

```bash
salt-sproxy 'router1' net.interfaces
```

Obtenha a configuração do SNMP.

```bash
salt-sproxy 'router1' snmp.config
```

Execute comandos pontualmente no equipamento (esses comandos foram homologados em equipamentos que executam o IOS).

```bash
salt-sproxy 'router1' net.cli "show version"

salt-sproxy 'router1' net.cli "show ip interface brief"
```

Obtenha métricas do estado do equipamento.

```bash
salt-sproxy 'router1' net.environment
```

Obtenha a lista de fatos do equipamento (semelhante aos grains obtidos individualmente anteriormente).

```bash
salt-sproxy 'router1' net.facts
```

Obtenha a configuração em execução na memória do equipamento.

```bash
salt-sproxy 'router1' net.config source='running'
```

Obtenha a configuração de inicialização do equipamento.

```bash
salt-sproxy 'router1' net.config source='startup'
```

Simule a alteração do NTP Server em todos os equipamentos que executam o
ios (Cisco Switch/Router).

```bash
salt-sproxy 'router1' net.load_config text='ntp server 172.17.0.1' test=True

salt-sproxy 'router1' net.load_config filename=/tmp/backups/startup/router2/20190304_11-57-01.cfg commit=False test=True

salt-sproxy 'router1' net.load_config filename='salt://a.txt' commit=False test=True
```

Neste último comando será simulada a aplicação da configuração definida no arquivo ``/srv/salt/a.txt`` que você no servidor Salt-Master.

Exemplo:

```
hostname RTeste
!
ip domain name aeciopires.com
ip name-server 8.8.8.8
!
interface Ethernet0/0
 ip address 172.16.3.1 255.255.255.0
 half-duplex
!
interface Ethernet0/1
 ip address 172.16.4.1 255.255.255.0
 half-duplex
!
ntp server 172.17.18.1
!
end
```

Referência: https://docs.saltstack.com/en/latest/ref/modules/all/salt.modules.napalm_network.html

## [OPCIONAL] Obtendo a Configuração de Equipamentos com o PipeConf

Alguns arquivos de exemplos de pillars e states podem ser encontrados no repositório: https://gitlab.com/pipeconf/tree/master/saltstack/srv

```bash
git clone https://gitlab.com/pipeconf/
cd pipeconf
cp -R saltstack/srv /
sops --config /home/pipeconf/.sops.yaml -d -i saltstack/srv/pillar/<NAME_FILE>
salt-sproxy 'router*' state.apply
salt-sproxy 'router*' state.apply -l debug
salt-sproxy -N routers state.apply
salt-sproxy -N routers state.apply -l debug
salt-sproxy -L router1,router2 state.apply
salt-sproxy -L router1,router2 state.apply -l debug
```

> **Atenção**: ``/tmp/backups`` é o diretório no qual será salvo o backup da configuração do equipamentos.

Referência:

* https://hub.docker.com/r/mirceaulinic/salt-sproxy
* https://salt-sproxy.readthedocs.io/en/latest/#docker
* https://salt-sproxy.readthedocs.io/en/latest/examples/index.html
* https://salt-sproxy.readthedocs.io/en/latest/opts.html
* https://salt-sproxy.readthedocs.io/en/latest/scale.html
* https://www.hostingadvice.com/how-to/set-command-aliases-linuxubuntudebian/
* https://docs.saltstack.com/en/latest/ref/modules/
* https://docs.saltstack.com/en/latest/ref/states/all/salt.states.module.html
* https://docs.saltstack.com/en/latest/topics/proxyminion/state.html
* https://docs.saltstack.com/en/latest/topics/troubleshooting/
* https://github.com/napalm-automation/napalm-salt/tree/master/saltstack
* https://www.linode.com/docs/applications/configuration-management/introduction-to-jinja-templates-for-salt/
