{
	"data": "ENC[AES256_GCM,data:t6ltAT+XN5d38AwXmVtle7Wi00+Rkptgp5qbhEUet0lam0PLqeFlC+/kkjOBPg+QJ4c+Bj01sZogqpetG1pfvGjICfTckJ36VFZhCC3ghbKPx4DJqb3hQyKBfJgRgJEr88+UW6V8eoBBPTU2lEZwKC3Ra7GKhjeV8Z8Y+6df6UYTsLbialwQKuNQCt7XVkIpiOEOea8J/4TzvldY+KK3ggiUhkHRhn6o0p9304DJpv7aoNWoCTxIidPPiuYx4cOoyMS/8TFf2Y2ZI3L8S0VhoA/JQCf3SAOvBie5RBMddGtVRg9+rM7O1XZOkQcgjDZjIJTPs3KZhYCxBttcp//75vavVzDdbpWCUurmqns0hUfamPUyGTM/gkV7mhTwFlNy///eAYbtrd8SC63if87ugdF4qLoUNkOIpcAa4sGFzyh82Fw/Ky0mdWy2cSW9veC9lDvu1nx5ZoZjbeYwOEqMQ6sry6uPlQkqohoRTOQ+SA==,iv:fld4mXCNNuKGCl3olxjKOO5k5G7z8pWoTL3324NFF+M=,tag:F0ih/HuciTL2/5mt/KKfzA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-09-13T04:11:29Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwEOepc+Ec4tStm1vp8bZYGHAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMaB02VXbBy9khz67SAgEQgDuqvLb4npv96aKAr1+J2Ba6wOfhkW28Gkd9ASdoaK4Zu8/VyK0VMNW/WczkAU4ipnrLZaBEp9t4t4EdNA==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2020-09-13T04:11:30Z",
		"mac": "ENC[AES256_GCM,data:RusN5t2xybgGX1BMbZ+fu+ZMij+p6noMt3gAFArZcJlK17hjnxUOuD6FavTGkyyzp3aRdn3Wb+VkVKyMO8BwryxDpBlixEdOSeLqdmJ5lbsOsfCnDCi7HI0P7A9tliudaJOTgMTA20oZXlfV2xSZG3frJiGZky6Up7xHAV+kwRA=,iv:5ir2iVdyYhQVpuDqgnsMY12e6SYEL4nzYr3qk89r2M4=,tag:AJONAxDMgr4d6djHxupWMw==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}