{
	"data": "ENC[AES256_GCM,data:8FylIWJGAr3ywYycDw3Vf7UUebJebhjoKdskH6Yq4gldkf6NgDWr+k7jKLuYuXYhdbQ7c9KyyMMkvXzy+RXuV74H4sdTvl8R03LIh9NygEf3W+FtkvZGzWKOA1fmoQNLGM89pUxGnB+5p9E58wYLQl4OPvzj1cnvRYLx2kJb67aZzdD4pf3iXZh1okkJuBRdxvoUrU9niIizlp/0RQlVkbfsGjpPBGSPY3v/TQYQyzq4j/mNIv7dI2nv+C3Au560fOj79t3kMFJwM+NeWXu9yoe7Z6lLDJK7D4Z32Oac02X6fO+LaZEGQedTVs5h+rvmulwMH1DVToi9PM5Lwk0DWFseMynKha6FWQSih+QEyPzDAlFnZtmbmjUIAaPsLc2Az0KyjURDxWyHmVqnZG9Z6mWgQaUgU0pAvK6ZgsmEVzgsvj92OyXM0lYHuuh+G0TpU4wM5imgG6+aOcN/0tvhDNLoAkDPNgEYMEEuKXG05QoZW3BbL+sow7PtzUE02q0qqzdr7BI=,iv:1i6qS44REVRI5c0wAmvU3eri2A0ElVZDYZZ+1Re+ahI=,tag:U/HEKk7NHoRwCjfHqSRZCg==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:20Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFCH7+QCXb9nyF1HqVA6+nmAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM50rfgCjzLQcH4aUjAgEQgDsz3Zt1MBznFlbM1SRA2hPFk1AQwFbQpoxXiF35HFaJ1YYxO1YfFGiwwkNQ18rKi91yCVZPU6KOieVthQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:20Z",
		"mac": "ENC[AES256_GCM,data:JjC4kGl/zf6Jq32MxgS65vTAmNOHrcx5mOO6rJv82dFSmfP/UpX69GFNEOmUKFr/aLyIaQeo4tSoXHXyXOOP62bkdNR0RyrikMgzbE1dO4lC/VTTRlp/FUBRypZBSxoefTM4MVhG0YKjNa1T0Q2oZaOWIKTIhBIhmqZRZvY2NOU=,iv:Zie8e6E9j56TvwYk4frdDB+LNn0Ipk00RswH/uqxkUw=,tag:PsOyk4YSYE6zO625UGAFMQ==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}