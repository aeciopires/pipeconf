{
	"data": "ENC[AES256_GCM,data:WtJT9eOepeiJq9i7N8FKl07LyZ+OGG8Xkyt0QqwXHlKs92wShPxC5g/v3b3KvpaDybawHDcRx2ApfIrh5/9KHlvJAZnrJMZFzy2r+NYJL2NoeJznQHyC3ES/NkMWrApRNdKMO+0nDUfo2kyqywM6vQlmI2EYqPc8G+17TG5SGF/QCMgaPR4Rw7ahkHiAF/a/poP0Hri1j8hXMuFGSMmjiXdtzMy9sriPg9jjgexjw4sUuKHgzWc0mJpX9DE0EDJasKZCkzfffSskdzrtGRaiiAyYm7HW5ifyqiXoqcQMto6f2XYC9NZyzqVh2WmM2zYsUwVs7f1ltQ8wvp0OCYVi1dou5YWjfKlaZaslNlvLfJce1LPjfTcycGWWvfKdc+MjdVgJPOcw5YvEjbvtTgkSG46IEKFerpOrwT05WuSj55DvIwCKi0K2BVA23CWnu1q6OlVTfVudcM29zxa9HM7mYrSNY9amAnEBBkn1A4BfsvZa1i+Fs6x/22+gpMzN0UXpeH6IBcHBYoXbtVW45vNFxXjJtU6jt8kzjqzPDHFCCWlWHCpGnA==,iv:zgtf12vEJd/18kDRE2qyKMOJCZKQFUhmQAZSTCj1j7s=,tag:sV+ljNW1KiYYQAXM0GwQeQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:17:08Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwH4LnqMxgyshqErzC7r5NyiAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMFNqyrZONHZD8C0+iAgEQgDvjtjbdxJBoTfXmDBMCrtpG4h1AnXLHSXI+oUQL44MR2T73n/I09OuH4noJfBnTuF68LZAIwronDtAoWA==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:17:08Z",
		"mac": "ENC[AES256_GCM,data:AGvigM3TilE7ncfVa8WF+KS0EVl4VPwxJgYIz1jsCMI9NZc+znbEnXXKL1uMHDef6tKCTNrGWyyQFkRDqioErF/kengZ9kkiW9JTI912xe5ib7rMAEwRjnG3T2Elr64g+Nkfy06eeozDYk47qU1AMQ3i/EXi8gwl+nH95JceisA=,iv:yHzrnmh4sJ4dbVZwhp3VFJ5go87yU1zdH9a3dOfphwI=,tag:rqLEt4EAHNAPq+2ur1O6kQ==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}