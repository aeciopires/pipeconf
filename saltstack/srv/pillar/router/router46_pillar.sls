{
	"data": "ENC[AES256_GCM,data:w5sEQIu/N278K0ntnGbrtJnDAO6X6CTT28ARvpkQA23l7FvlKzRJTb5rpBk9qc99xDXa5gVbVVHLZ8UpFtHEvEfmCL0zYKrsNHFMDxeHe/uCH3dXFfO1MyvCFKg7mWNDxThGSKgO5XplgK+3aQ8nrftGB8Pp3HOMI7rEnr6BHJC7/r4iNH/1cyz8ziczDt9BWQNX7y/xxQvJERrK3Gtzxk7KNAbZoGXKqtnFb3dk7vXkoSnzohNmX7NH5wucft/i3Glb0Dvx3L98XTKFi8Y9gDD2nqMKJvnWtS3YdnlT4DUR4sIiQGSiaLOR23CzAmsMQfc7qHRXiXvxnLVOMYEfgmAlYdG6seG/Lw1zpNnUR8Go7elf0j7j89aE0ltVDRyi0FGHnpeLugZs+Mwj2QhIXdblNGQZuy/qdYIr0HE772R3LJoFn/KqJTOKSH7aLiMVsdxPI5LBwhL5vhFlNCWQwabZ9gVfmcsbghrM0cVZ6D7140ur44j51VAYUd95oRUIOWly,iv:/I3PrzCrjPmCirKz89GCLKGMqQ9aVOlXr/x8OB91Kfs=,tag:LVespSk95dsKUzJSpIx0iA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:25Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwEkt9P1yLE7nf0pqFM3iXSdAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMsMCUBQdviz0/V/zbAgEQgDukOFwEnSdQW+qsWi+vYhs7axkpRsYfvQl6kvxMgPEs+Y+hre1gC5AGYfF07fFmjn3alzkJCIMGpjcZRw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:26Z",
		"mac": "ENC[AES256_GCM,data:bsMyIfDJI9+g9t1Yl0eZB2Vr18k48uWMnHiHKmEx2sVg5+c1cCx+SAMYgyEg1Q8zpabET04sxZJbp4hkzAHE0eKRryf8Exc/T8v9hq2ylse8RTi2TgL73HvIeOrn85Y9hfpF3GKpex3UP3oEkfrghpaH/XTpeIvdOEo4nkORBmQ=,iv:luQMcyGVoesXzgOH9pslrH+A5KzomwJEFvtg82s/S2g=,tag:yTDP8QxqHYV4wd6ijPu1OA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}