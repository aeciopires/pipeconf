# logging_pipeconf #

<!-- TOC -->

- [logging_pipeconf](#logging_pipeconf)
- [[Extra] Instalando a Stack de Visualização de Log do PipeConf](#extra-instalando-a-stack-de-visualização-de-log-do-pipeconf)
  - [Grafana Loki](#grafana-loki)
  - [[OPCIONAL] Fluentbit](#opcional-fluentbit)
  - [Configurando a Comunicação entre o Grafana e o Loki](#configurando-a-comunicação-entre-o-grafana-e-o-loki)
  - [Liberação das Portas no Firewall](#liberação-das-portas-no-firewall)

<!-- TOC -->

# [Extra] Instalando a Stack de Visualização de Log do PipeConf

## Grafana Loki

[Loki](https://grafana.com/oss/loki) é um sistema de agregação de log multi-tenant, escalável horizontalmente, altamente disponível e inspirado no [Prometheus](https://prometheus.io). Ele é projetado para ser muito econômico e fácil de operar. Ele não indexa o conteúdo dos logs, mas sim um conjunto de rótulos para cada fluxo de log. Ele usa rótulos dos dados de log para consultar.

Crie o diretório para armazenar os logs processados pelo Loki:

```bash
sudo mkdir -p /docker/loki/logs

sudo mkdir -p /doker/loki/etc

sudo chmod -R 777 /docker/loki/logs
```

Crie o arquivo ``/docker/loki/etc/local-config.yaml`` com o seguinte conteúdo:

```yaml
auth_enabled: false

server:
  http_listen_port: 3100

ingester:
  lifecycler:
    address: 127.0.0.1
    ring:
      kvstore:
        store: inmemory
      replication_factor: 1
    final_sleep: 0s
  chunk_idle_period: 5m
  chunk_retain_period: 30s
  max_transfer_retries: 0

schema_config:
  configs:
    - from: 2018-04-15
      store: boltdb
      object_store: filesystem
      schema: v11
      index:
        prefix: index_
        period: 168h

storage_config:
  boltdb:
    directory: /loki/index

  filesystem:
    directory: /loki/chunks

limits_config:
  enforce_metric_name: false
  reject_old_samples: true
  reject_old_samples_max_age: 168h

chunk_store_config:
  max_look_back_period: 0s

table_manager:
  retention_deletes_enabled: false
  retention_period: 0s
```

Configure o plugin Loki para do Docker conforme mostrado no arquivo [README.md](README.md#instalando-o-docker)

Inicie o contêiner do Loki com os seguintes comandos:

```bash
docker run -d --name loki \
  -p 3100:3100 \
  --log-driver loki \
  --log-opt loki-url=http://172.17.0.1:3100/loki/api/v1/push \
  --restart=always \
  -v /docker/loki/logs:/loki \
  grafana/loki:latest

docker run -d --name renderer \
  -p 8081:8081 \
  --restart=always \
  -e "ENABLE_METRICS=true" \
  grafana/grafana-image-renderer:3.5.0
```

Veja o log do contêiner com o seguinte comando.

```bash
docker logs -f loki
docker logs -f renderer
```

Altere o comando de inicialização de cada contêiner Docker mostrado nos arquivo [README.md](README.md) e [Monitoring.md](Monitoring.md) para serem inicializados com as seguintes opções:

```
  --log-driver loki \
  --log-opt loki-url=http://172.17.0.1:3100/loki/api/v1/push \
```

Inicie o Grafana conforme mostrado no arquivo [Monitoring.md](Monitoring.md#grafana).

Referências:

* https://dev.to/thakkaryash94/docker-container-logs-using-fluentd-and-grafana-loki-a15
* https://github.com/thakkaryash94/docker-grafana-loki-fluent-bit-sample
* https://docs.docker.com/config/containers/logging/json-file/
* https://yuriktech.com/2020/03/21/Collecting-Docker-Logs-With-Loki/
* https://hub.docker.com/r/grafana/loki
* https://blog.ruanbekker.com/blog/2020/08/13/getting-started-on-logging-with-loki-using-docker/
* https://www.youtube.com/watch?v=qE6hEHNH9dE&feature=youtu.be&t=73

## [OPCIONAL] Fluentbit

O [Fluentbit](https://fluentbit.io) é um processador e encaminhador de Log de código aberto e multiplataforma que permite coletar dados/logs de diferentes fontes, unificá-los e enviá-los para vários destinos. É totalmente compatível com ambientes [Docker](https://www.docker.com) e [Kubernetes](https://kubernetes.io).

O *driver* de registro [Fluentd](https://www.fluentd.org) envia registros de contêiner para o coletor Fluentd como dados de registro estruturados. Em seguida, os usuários podem usar qualquer um dos vários plugins de saída do Fluentd para gravar esses logs em vários destinos.

Vamos usar o Fluentbit para coletar os logs do contêiner do Docker e encaminhá-los para o [Loki](https://grafana.com/oss/loki) e, em seguida, ver os logs no [Grafana](https://grafana.com).

Crie o diretório para armazenar a configuração do FluentBit:

```bash
sudo mkdir -p /docker/fluentbit
```

Crie o arquivo ``/docker/fluentbit/fluentbit.conf`` e informe o seguinte conteúdo:

```conf
[INPUT]
    Name      tail
    Path      /docker/*/*.log
[Output]
    Name loki
    Match *
    Url ${LOKI_URL}
    RemoveKeys source
    Labels {job="fluent-bit"}
    LabelKeys container_name
    BatchWait 1
    BatchSize 1001024
    LineFormat json
    LogLevel info
```

Inicie o contêiner do Fluentbit com o seguinte comando:

```bash
docker run -d --name fluentbit \
  --restart=always \
  -e "LOKI_URL=http://172.17.0.1:3100/loki/api/v1/push" \
  -v /docker/fluentbit/fluentbit.conf:/fluent-bit/etc/fluent-bit.conf \
  -v /var/lib/docker/containers/:/docker/ \
  grafana/fluent-bit-plugin-loki:latest
```

Veja o log do contêiner com o seguinte comando.

```bash
docker logs -f fluentbit
```

Referências:

* https://dev.to/thakkaryash94/docker-container-logs-using-fluentd-and-grafana-loki-a15
* https://github.com/thakkaryash94/docker-grafana-loki-fluent-bit-sample
* https://grafana.com/docs/loki/latest/clients/fluentbit/
* https://sematext.com/blog/docker-logs-location
* https://kevcodez.de/posts/2019-08-10-fluent-bit-docker-logging-driver-elasticsearch/
* https://fluentbit.io/articles/docker-logging-elasticsearch/
* https://docs.fluentbit.io/manual/pipeline/inputs
* https://docs.fluentbit.io/manual/pipeline/inputs/tail

## Configurando a Comunicação entre o Grafana e o Loki

O Grafana estará acessível na URL http://172.17.0.1:3000. Acesse o menu **Configuration > Datasources**. Clique no botão **Add Datasource**. Em seguida, selecione o datasouce **Loki**.

Defina as configurações conforme mostrado na imagem a seguir.

| ![1](images/datasource_loki.png)datasource_loki |
|:---:|

Agora acesse a guia **Explore** na barra lateral esquerda ou acesse a URL http://172.17.0.1:3000/explore. Clique no menu suspenso **Log Labels**, aqui poderá fazer as queries necessárias para visualizar o log. 

Mais informações sobre como utilizar o Loki podem ser obtidas em:

* https://grafana.com/docs/grafana/latest/features/datasources/loki/
* https://grafana.com/docs/loki/latest/

## Liberação das Portas no Firewall

Para acessar os serviços da stack de *logging* do PipeConf é necessário liberar as seguintes portas no firewall.

| Origem          | Destino            | Porta/Protocolo  | Fluxo de Pacotes | Descrição dos Serviços         |
|:----------------|:-------------------|:-----------------|:-------------------------------|:-----------------|
| Rede do Usuário | IP-PipeConf-Server | 2020/TCP         | Entrada & Saída  | Fluentbit funcionando com HTTP |
| Rede do Usuário | IP-PipeConf-Server | 3100/TCP         | Entrada & Saída  | Loki funcionando com HTTP      |
| Rede do Usuário | IP-PipeConf-Server | 8081/TCP         | Entrada & Saída  | Renderer funcionando com HTTP  |
