{
	"data": "ENC[AES256_GCM,data:5+jdoJz0QYLr5CAR2EC7MYRG6xki9Fh4lVoH3+aIbCaZPfS/PfVyk8UcwTxk3Wf6t0Q7uacQU4gLlbvnhGbXDXW8TzOQJeeivgdZ6oE2Pnflra7WFBKCBvoafm7n8xnpOWa9eluA7pGVudXcs+KETuys79nKBotgvFSaG+ozJBlwinmCJoVLbUpegxvCqtOtmdLLBR3PKLjq7rYn6RhsdNNPnl0BWUp8QqmVSbwLnbE7+qBx5ZkLd1Z6cyhcByLB5VGgZ61yLDJfPCG3sJiclU5Zt5yvvuVnSD28sNalxCCST1jn+TZvaV6G0a2zgConG8OlOcj7zMjPUVtB7xpFdkrAWpRm28ngagdntrKfGRoyUVcOzndgTIuP4jmWIJPLdyn9MefrwP10TCBsNo0WfKZkeIWSboM0ZeSAlftZE04lfNXIJ48VGGt24oDSo1nzplBelT15yzdkvk9vFqcr/Qfi25wgCrgWKy2CDoMjQPb0nW29N6XPqLmCvYMT4SLKwAGPnw==,iv:JDvJT0LzLFTaXFxShCkcbiWAVJ+4VqtBQlW+0OSKbm4=,tag:TnJGwJuqfCOhWDIrbysOPw==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:15Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwEznPpK9+FXNnmtl/21Ko5oAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMUSzGvVbsiZVqDWHZAgEQgDvumSSapOTL2x5JPIMj69jn5ZKvC+voTVv9aUdKClHd4V4pFpTuBvMkNPVfuE/YykTZZQXGiZJ2YUxitQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:16Z",
		"mac": "ENC[AES256_GCM,data:68PYLit03GSTN8dP39puIKoJAbZmi7krZwFBFsod6s55RZKHEy5oJe/ClUXr/O/AnjMq48qhX6b4RWj1PT4s75KLetvc7b0x0ahLiwDPvy6cVv5+TlJgertFArE9BRSNtrFre3A8nuHscOgk1WjnUeBQoAwLWPuSntKXnVhCpl0=,iv:DEU67LmmKqiEuXjEhTvcGin6jUlDyJRGXWYE/kQaAiA=,tag:boMA41W0p9hZJr2v/eIYzA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}