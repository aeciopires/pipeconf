{
	"data": "ENC[AES256_GCM,data:kd46TbErdBmFTt30Uglp0M5Onl9zYwQPZ5F9A5Wrc4zpaq6cqpdvY1u+H0lq+PwGa+enFSwqQPAe9R6Awf9x0/PSpqE9voq8zEZKX8mjIl/joz/HrCSXer0pSdWhgnCdUrZ38mW7zBriJ8uUPrE25zKw0srMAGq/gfvKXbONph39DDyvSW2WD4o0RxQfSwfvjtOCZfnITDZlE2vwqdATg1UgAQHOf964WJxjluNrXOqL8cwKIG/F601977JvNG1QOh3vIem0P+yJvzr4PDmdxnJbCWIen6K53ejpv4QK8Ny64yPucCzWxh4pKvI7km6Hw8gTASYb2kONbdvLGrVFL2b/DZqpCcpb1nVr4aVAnAiJ1MGk/9WwQQaj+KNUUbCQVFg1w2P2DoMUtCLrf5LIdzfPrZHh7Wy7hBYIK//Hw9HjJix/UX+RS6XO0riqq5yMLxorrKrbbqhPcSgHE0DBlTra7S8TsdU/JbBiqpwfuuwRkMngQoFxRkaKF9lfP1VlRsJMh7w92tDaCHvx3asD2ThulpoZj4qu/4ZUFu9EuM/KlP2eGA==,iv:Rgpc4lwkp2nkzyykcMo2I3sjLHG46ANYqUsGKrjlOnY=,tag:JmiNm88qL7612RQUk8hmYA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:17:03Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHjcxsRjC6pDL2BJiA4Kkc8AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMVk4mxD7PjTfvuEwKAgEQgDtpTWxARxyRnzFwplAfYT7WxRNUTQolMre54jxZoRFqxAt/BdIKTCNoFzVAeT/1BP7QBpllxLRHTE1Yrw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:17:04Z",
		"mac": "ENC[AES256_GCM,data:5A1Ky3qzqjyQKkanhbH7fzkKd7kFh5KZTcMqW1UNI0qkQtd5M+LyCUVp3ZxPxNo9oahSqIrxkM1DABnD41qeqAaPN1Gx1VabQaA/ND4u2Plf8hTF1fiKO8XtBFvNmfkKEt+2TvGagb/YO4EPjpyyAphRQT95TB+sMsC3qHtsGC8=,iv:Plb9leSc2+VEpUV6+70KXesRjUIRkVl0dlFfmmcPyHM=,tag:xnB6m3hONpWqY6xKezhF9A==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}