{
	"data": "ENC[AES256_GCM,data:Kda0WlkB5X5OwZgpuq/FMJPYpDxyfCqlge+yHnVCuQ+/gW830k6QdJAMDNJnoaBSY9zQXY5eGIji6VdlHAQHiNsG26JJQ0YgQd8LEPVvQAw6rDsg7YdTC3VPx/082v7fhgN5Y3puqK9xJPodkUWZ5E6451rdICLSXEhHfujdnu7iAf4cxwhR9WputoadFEnHs4WbDuk2MOoNIfNNxutPAPpd/3dCsd6gMs9CE22gc3neW/ZJju8KXaLVykKo5cDT/yfTjQ7oSDSwRIJtveMKVZqSNlkU86EYqkjNP828XKWqGBxCvwbqL3uOkBX02MUPQhNP+n+C6g2uGKM+WSm1b7dTz95HohCTJe9rcH92YgfzRpJAmPIR4wojdI91OkT+lN2FdNzJ4akWBeLDTZLhaaePiKsSXccQWSza4vvW2PUw3IotHOsDSwYPgxNZVBvt2lKUBWKlCQ+IgMdsEDUegiZnVHlczKi4nn+BA45IUW+UzSyg95Aw/mn8K5l9+oEedbIi4zAJ9h09wIn+SQ11+aexnVFFsNsnGFDmZ88skfBXYd+Xew==,iv:yWVS9kzzPtrCV6/Rd+5O+uDD4D9Jj9nwJKLOK2HdWzQ=,tag:UiwNdhZoDCn4hkF77x0WVw==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:16:59Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwExjeLoTjo9gb0GnUsX9k1rAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM8oku5ZFPlpCEfPUoAgEQgDvrI5zgQL1Wodwi43WQyi1rnCzdfmLVpYNVoqoVfqkNBEtm/gzxuF6tAeBJVe7QJJ9vzlFpZYyiU9+jFQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:16:59Z",
		"mac": "ENC[AES256_GCM,data:lb/tJQW4KG8xL8UFMCq6kZbUUbxZSylxv7iIaeov/WqRuWfMyBHvHoQc6lpOot5wo408/wJL95NRWueDxm0cS86Mmp/1a/CJAjZbW4rpRz0vwtltfflQ3/nr9h+LnAHlFlvtkhsWLXXSdWEdS6Mq5/njturB3ETfxB9Mapk0lvw=,iv:QCvieCePWrxOlLJ5pYnnM3MIq2hgeALFDjhS2X/YWuY=,tag:ILYMP2jtwNhrM7zWeJt8XQ==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}