{
	"data": "ENC[AES256_GCM,data:wFXCpzDrbAl6dVIzIwhjxwuNdUiv8Jxc4V7GmI0EirUukfYbHQLyqUQofLhfjFNLvuLmfWW2Wz1Py8F3KWGRlpe6CcHDBK4S8KkwxqAXhy7S/stBWXp+MS1uI9cVhQnYR3wW8n7kkpphfmgDHfKhCZB1l26yO0xbXClYDsbPHr2TtiWJBdj9sNtogv9oKOCeP1L1t8NhBIDECY6p7sA+HO0wvND2fDn7cWmm/iC1QcPZQYvnc2B9xtoSd/pFN4OsDqM4Yx9rY0+5GWIX78mI6NY+Lc9O5kcHQSzfbnhYQBRCXWslgyFUtbUnrD1ze+90oOGbVMvSl63LNoXJqRz6x5RYMOC+P1FZx7jcjEUZQ1M6ajeDxG4S8bGcusl6d7t5c/50OyqLBBfhryEJwX6C71wNYZCR5FjrHecE0g1fY0VRhSm+TAkLu3q/q+xiM8u9Fn4ZnnGLb8oB40L3jATGURogFFpCgFpwk/o=,iv:WuXE67VqgH4g6b5f/uzX1n3B+aPA8PExELzk6gjkjQs=,tag:tKDT1rvrtt6jQa35ZSiOBg==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:33:01Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHb3sh1W0NwV/Zhaxlj4u/bAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMR5iMdp/t7ISnF/8EAgEQgDuBL8jqV5c6kyxzEgHnHgLVS0/sG60UYjGrkauVjm32EIadQdSIkcRPKaa+7/r9xAvao6ujc1Eb1PSdYQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:33:01Z",
		"mac": "ENC[AES256_GCM,data:U3pkIM0HgfQJAERv5ugqD5hqBIrDDdYoJb+usOy+mCdxibs0Gah/wnomAXSwf1JIJKqnpjGuy4C4Jg7isOuhyKrM3TwPp4cw1QSDAKxHuS+Fz19+UIL/uVy7q5J0RrwuQcESjabfBAtqqEy0GROtCl44WnQBgvuo95qcSldU7d0=,iv:lsuCGIVO5pPjv9frKGUdTypg4JMJc/g13zgyu5vAk+w=,tag:sM9l/Vy2HO1RB2Dv0rJ8fg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}