{
	"data": "ENC[AES256_GCM,data:BCu07bt1s/4oC6v+PeJWTSL1vQJ/xgqpzC43H2I9vFxYAywwK4AUdu/XBp2xjPay7SfAZiqzLXJPveutttPrb+rD3yeXPe1S1Ypnry/e3gQO3sHiWMvD4qwYAdiz/EeJZ/y658TJYi5mK7Y+AUgstwiXe26ca7An5f4zXBMi9BYdnZ52KKwynrhuF66BCtbw7K/VOHLvv63/njnw4cWIG7pMalWCJEPoc2v5BCv2A13jTHHYFKpYMW6XxuKmjngL7yFb66XAQaVNSIpLNjCcTrrTJCkOFnkYfxGnO1NeQ733tIKOlrb2kgP6gwg7+BMot4AVKLD8ozGhcqDTmR8So6H5GbSbM8iAKA1TQUrSRTPmz0P9/AjNEa/xQxgf3uPVNHtffx1xxIEyTXyb70cVOYJrYLKkVig9/tpgYsbpn31G/XwSv/ssZhDIrIGa9lF+HpaZihm9BQ0PY5cLyReAiR8AhneBQSy7OL29iEn5Mhqu9Es/wnh4bDDl0FD5oZNgIyiP0A==,iv:IAoFapAdHqKSTk2auKYQsUeUt5sD36MDd3PtyOU3S7U=,tag:53+PZbXBGoFiZbXfeuDdKA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:31Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwEhWGGQ03I3hu0/frVgOQy+AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMIpVydfergna+C9EdAgEQgDvF/KZLhtd3peke8l1Vrn3cJNs+a50oh2NQtWtw8j1j4aCgaZO+srLpXqHYCzzdCF7AUWzNQILbkQ68Tw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:32Z",
		"mac": "ENC[AES256_GCM,data:G5Sq8sjvj8oOXJVXFJhGmSJbqkdhjqWaupgijDXdiB9opYkrUG+7JEbjZX7kRsEPTqbvkjGYBZSXT0sefPqLOdRPoZuXh9F6WHfAkE1npJuEBX61jTvjEcn9n2+mdgCurrM1UoXE6rg87Zk3IzwcCJrS+xSEAjz0Fs5jD9xGrTE=,iv:gakqSuw1FR73dC4eC4P9O0cIo0+mBync9Ybp8XPGj6Q=,tag:V6Wf82eba7YkDnqBY3qCsQ==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}