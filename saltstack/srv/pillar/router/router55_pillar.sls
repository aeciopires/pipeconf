{
	"data": "ENC[AES256_GCM,data:cbdaZa0a9IxtlKeuEmoznW00FLDbBR6oGPEbn9I0XGOLawkUXyO1OZ83ox6m4Lv/AsmOlZ3lFXzIT+7ecmnTrRDkSyiwsKkzTkvASyI/FzEsxUSoYAzTz8phHWsrjFDcv8vc1miHtmk1/ktl+BlSMkV0ZkFyTMLwKs6A6dI1nFP6G2S4WLAXJYxwcoVdg2YbAtNf2Ef/IRat3gmvZ8rHg4gEG1hgALYkPhK2Bv3AhvfPWsFw6zIU5L+hD8JOiXOWDklwf0BKsmK7C8mkzlOK720+LnYrsg+jO5oeepOr9Rj7wSjvDU3NPQYs+EnsI3ZCHlnE+0TtkaAxoU3p83HKNvLEzEhFdJI3BDbOFHMac+eJXvsoZI2W69wQlMVfN4zkNXQcFdcNbaA316ReHumkApN1evPk0zZRoGD02NXlcQx2vmgxizi2vrOzilsW9oDX+93/a6YxF0aJ9L6JfDEVANL117xp1GwAkXeaHoJUUw6tgv2rBuLajZsJifEAJIQn7rwUpg==,iv:KRybVA9xmybNctAVhblj6/kviIiDnTcrb8NLok5vVeQ=,tag:GsQl+5/5NdEnQ7Stzt7zlA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:32Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwGe2MGxg2VIy8s4aSRz8pK3AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMSCe/3WZ9v0qVmx+iAgEQgDv814Xil95nowO24Zf8R2nUKCnWvcw2A1CgrncLsxtHTYPdBoIYodfN99Fw+8i19E6qH9Smc4hDxUcaDA==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:33Z",
		"mac": "ENC[AES256_GCM,data:dpxFLcr9gcxAKuEVZRvfWy7RVtcqZ+ZzPONaFTwB09KA9BjKXvAEMg351NKz1gCeUXxKMcctoH6ucmA28zwyfUGD4+VGAYsCfEJbJDGHHiABMRWQM9EHfLW2Hr0Itgd5eNv0xffFvCbDDOPJGYxt99XWT8F1QPnDsIIkuvhYJiY=,iv:Wb6Kg2EJzYc9mRkzfPv8kND9Ke9+ez8vm2y3HtWLPOQ=,tag:pb0XFXS/o2tOT7PrtnqW+A==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}