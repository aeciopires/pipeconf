{
	"data": "ENC[AES256_GCM,data:dKYn78cM05eK7a05ig35M2SFtgnvpxMpAgK44O29RqjlHBNxfvZ7JRNfirw238rhnC6X1zImk0FqDMlLjdQQA+G74mZ2tN3foInl6pirwgIHvvLQaullTbPH0jByNgoyCKQVY+JACyZTDyLYKyE1hXnOn7808DNuVGe3XojwZ6xJZ8+2t9wQRWIBw5lOBzXZQfhJYM/0w72m9l48NN+Lj5mMWf5uhD4T9O9Sf0jz7vCIUGmdCIw2Q4ergfDikwVZnmFi8iLChyCcb4VeF9TAZoR8fuZlynEcNfrUnHRd32oEoebUJn2F6PVY38dMJlOym5NrfRM3DiDV2zgUMb++klkadTEJrMJhAFcl6aUDJSN9P1W3Kt3y6wkdxteRK/bRCat8R+EoUeSeUIURQ4a5RB+O2phf989j8Oh/I+hOmYPMBn8r8knq08HC1jmUibtEfiM6xbD8NLsvpjJsLousRu5duo5RKMiJDpifP85epRpnbyHXAMrFv4U9opF7c1INFRO4mtg=,iv:QCQOF6RV/hDYfpM9GiAnUuCYOW1ApB7nb55q+2L88vQ=,tag:5KmXEoflVbpvQS15K84qJA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:21Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwGcaNM8hnH/b9BtbDfsBcTmAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMwqGccFle2Kon8N0pAgEQgDteACnQ93kINtFgeYLPVtBrtSuoc+1F6o2mk15eeBSRr3/sqRt8dzMg6xCLI2YwYpqODLeDF9ICURVtGA==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:21Z",
		"mac": "ENC[AES256_GCM,data:HgYv9fpFVC2nru5RCqpf380W1Yos3Cx6MbxsYVhR6gcHqIusPAL2hZ715wdOlcXC2L6/btqlWPsIrHOChALIPeeIbnxmZsi3J5eIWZ0LoOGlROGjtW+U8URPFuM0KuKjjJ3jOtQOPzF5f1VPMRAX2HOQYZDm+vN5bNFB0+pEtzA=,iv:vdX86b7pcM0CCAbyElvOmVfY1JU/LcJkLAivAX5V3YY=,tag:+Vv6IqkD8V+1moPWq/aTPg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}