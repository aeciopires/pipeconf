{
	"data": "ENC[AES256_GCM,data:SXvyPo/75r8YIRokrqaclH9YuuSkj7ELWouHSCjv3CnSzqaYR7deFVZjpiKYCV4+cWKnKFbmm/YjgWCrFnxJEKUIYE8hozHw2rTSn3LbVp/ANdG+OQvi0T66IpL/ToM7CK5cldbHDexjl3Dxe+0Oucyna6ZlrZmsR0vEvOnulOXzIG09PcUMAH8hKoD7giVuaaQxDqQc46yqS352MmTAg8pSk6HRNFFJgIUaHBmdaLp0GunadKTx+wi+Afd1HYIAROvBMSHW8aqUS/JRT1947D5E60Um9QyKw3ljfjybO5PDlIln9ipYZQEzgLEyWwr4sDM66OHQvknW9P6W/tDFJMPiHDVIs2L9OU9PG7FpvmMh0WsJLwCo8j0OeQw23gjPEUzRopB9Cto3o0t4vQSuy61kD0Yz/Ckg30g36vsHkmVFBNNQ3ClzseQv0b8enAk5hOg7Er2cv51unl/PScF92gIZdyp6BCDO40gDlJkinSO27WC9P/aGANEp6NWjUWnABISsnv4=,iv:1tyFECLEcPIk4gBRoQ0P0MTVnU0M1DFeTisq/I0//mE=,tag:c7I39A1aUBtnHZG0I5NhHw==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:35Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFYfNGR5dzgBO9yyvmspvTBAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMHomndeq9u/WG3vUaAgEQgDtODnGYDbS4ksuUG2/9JplBjVqQCFJTFu0IeYfJziC95UBXQKqBRTWjAUmYitmPT9nIiYxngqPZOdoY6w==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:36Z",
		"mac": "ENC[AES256_GCM,data:Tuw6Xt/+qgmoupNX9S7wEDgJrZCpPHc3lYtNphvBXN4wewjWEKXRMoPyai+G9PcVBOFKf7yiSPXKU0Htkj9vCat2a9dqlqsxDwb7q8+Fi+YAvw6d8GJ6OU6TX/lQ4jL7n0wNq6TkRZliY+X0owcGC7mWd/cP/thZ9ydPTp0Qx0c=,iv:1PkhBh/FtNa17U68K1l6E2RpJej6vTjKv8mKkzNnt+M=,tag:YVKzf5AuRYfz1/X4qoQH9w==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}