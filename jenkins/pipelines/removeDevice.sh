#!/bin/bash
#------------------------
# Authors: Aecio Pires
# Date: 09 abr 2020

# Objective: Script to remove network asset of git repository.
#
#--------------------- REQUIREMENTS --------------------#
# Install packages: git
#------------------------

#------------------------
# Local functions

#--------------------------------------------------------
# comment: Displays help for the script.
# sintax: usage
#
function usage() {

echo "Script to remove network asset of git repository."
echo "Example:"
echo " "
echo "$0 DEVICES DIR_SALT_PROXY_PILLAR"
exit 3
}

#------------------------
# Declaration of Variables
DEBUG=true
PROGPATHNAME=$0
PROGFILENAME=$(basename "$PROGPATHNAME")
PROGDIRNAME=$(dirname "$PROGPATHNAME")
DEVICES=$1
DIR_SALT_PROXY_PILLAR=$2
#------------------------

# Load script with our libs and defaults functions
[ -x "$PROGDIRNAME/lib.sh" ] && . "$PROGDIRNAME/lib.sh"

# Testing if variables are empty
checkVariable DEVICES "$DEVICES"
checkVariable DIR_SALT_PROXY_PILLAR "$DIR_SALT_PROXY_PILLAR"

# Testing if the required commands exists
checkCommand git

# Exporting environment variables that will be used in the following commands
export DIR_SALT_PROXY_PILLAR=$DIR_SALT_PROXY_PILLAR

DEVICES_LIST=($(echo ${DEVICES} | tr ',' '\n'));

for device in ${DEVICES_LIST[@]}; do
    # Get device file
    device_file=$(find "$DIR_SALT_PROXY_PILLAR" -type f | sort | grep -v top.sls | tr ' ' '\n' | grep "$device")

    # Getting the quantity of files
    quantity=$(wc -w <<< ${device_file})

    if [ "${quantity}" -eq "1" ]; then
        echo "[INFO] Removendo o arquivo '$device_file' do repositório git.";
        if [ ! -f "$device_file" ]; then
            echo "[ERROR] O arquivo '$device_file' não foi encontrado e refere-se ao ativo '$device'.";
            exit 4;
        fi
        git rm "$device_file";
    elif [ "${quantity}" -eq "0" ]; then
        echo "[ERROR] Não foi encontrado nenhum arquivo de configuração referente ao ativo '$device'.";
        exit 4;
    else
        echo "[ERROR] Foram encontrados mais de uma ocorrencia para o nome de ativo '$device'."
        echo "Arquivos:"
        echo "$device_file"
        echo "Por favor, seja mais específico ao informar o nome do ativo para evitar remover um arquivo inesperado.";
        exit 1;
    fi
done