{
	"data": "ENC[AES256_GCM,data:+FWzc80O+mNWsJHJCGZxNwuHjR8G+ybTrteTDdX1pQCIASIr6hvceCSl2apcr1fn3CJnBhD4tWq9fwBiLJV+3V57hBi00Swz91oRhOTN3vCbI2HbCMdKXrpF/iWJklOEHMxqICerv56NCicIEEnmjuGMllUAmEYyA3U1IVe/rWgGMB2JQ7BmkpxrVKraISAp08OiKY4Z5bdRzZSsTXTs2vFzh6k9Bx0S0kPpR0dLWhXXfYi8rDEoqFWE4uuTBBbXbne9jN8+3SNYo2o20haY8EVEAzKwkmKqjp/+crBW6y7zQWS6VRx2gPzdxP2g0T3iSTNDL7Uzy/lIiC08xY7se/C1i9Z1F+GGFRKUjl1VDHJ7ln/xcKQPMwMdPmYufcs5mrytdcK2TdSEoapKlDPp8bzJYxCFZsUjNVLbP34piuC3kKe8MVtltz8i7UbzZdi8nHlvYNZcvRgf1ZIAzZezvqYZ0QsPORJjcdbZ,iv:IaNh0Yckn3qTYUo1MpZb00g0l/STZMhVQZE6751Jfic=,tag:vCiGt/kHsFw0wI8br47f9g==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:33:20Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFXthGrA2vocg1Ms0jc6o2zAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMW4pgTq/cG/LSbT/LAgEQgDtseIR2jVXn3MkCiZxe+9ZsX4OnLTDEqtFtQcZ6CQQJX0BZirUyoAwNHiOQINtnNHijLDuSVV3JbnLnPQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:33:21Z",
		"mac": "ENC[AES256_GCM,data:DEi8/PK3ziynx0op8i4RUF2vz/mIoUV/G6fsTRJ2CNuY5VVGFRbf0TGo7uVyAWC5pXyzMhDAWClkSe86vsZ7RmVSVc3QnB1vGxSHyKYr1f1QNCbzCofH0MMsxPkaahcSKMjljhI84A4ppwS6JonzwjZ2totWPivxL2z9b806x1U=,iv:1/T9n/q2w+NKyro6enzdtieSwvUswMSJqxMurbHljO8=,tag:IJ5j+FawJAHUVpWseLyHmg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}