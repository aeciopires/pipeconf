{
	"data": "ENC[AES256_GCM,data:U9Ftcjqlvl3ij/3bJX2m6j5iu8qkj0+nLBkznDjc8FhN6g2PSsmrolXQcJKr1nkRP/4ynbPz+3IreFCCUpSERL00W9fcIlns7iAKxrmyMo0US2EZRViLoIkpxp4jt5NQseXIXeU+v6FFCbelmCOew7N0Iqv6O5kAXwUWiRMWhcHdHJv8IIIG8XAlIFWXgamTFPdxNiCmCTI2ZRt0fTrr1rHEWGzgltAsue5zlPWQ5DyVT9h+zMNvoWWbxUKZ+fhtYrxSRwGBC9FzSFiX2IJGfQ7/66msFjaeUC4f9qDiFlm1F4naZCXEac2x8f4sEOsF8XIZ9ZQTMNf66FEcdipamkcEU/YcvHw8iGA2G9s7DkMpkK/xEC8FdhVe/FWfEJJZkEXlgonKU6HFL/LTgq7Umo5wtu30GxD8GMe6WEUkCF1Vpd0VJ+9wjKX1EI4R6bSLOTOw/S8xzas5hw==,iv:RWRhlDC72xXOLu2FUHDjRByT0LtonAYehnsJ3Of9FD8=,tag:+qgo5eS8tFboNL+3z0ukoA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-21T21:35:15Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHtCva54yKz3uVMIoWZZibVAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMeMTksZbxDiVMgMe7AgEQgDu3IJeM/L4wUpkDD9iZXlci07H6AWnB35kKK7nE+0Q2WZQevpRs9BGtrT6fZGaj163tyMzh40D3MJTmOg==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-21T21:35:16Z",
		"mac": "ENC[AES256_GCM,data:Qg6lSE1gRWQQ/CTmgyJ9YDlfWFrub4ZLDYrqLt0aUuG9hgYHbA1ZHIaydio87EelsCdykbQGzKcaowY505HMzxTOfvMivECMVYp15blpPvsjJBU1DG+gnS+GIEykZWntm/Bwg+s2ZBr47bTptwUOuJgPaoqKYXRl0l5XAvxsBDo=,iv:fFCoQ2R9sh5OrShYG/PxTtYLnPHurkGUjjemlZNUU3U=,tag:yhUCwilZoyIzrxCAYUWDcg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}