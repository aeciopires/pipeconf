{
	"data": "ENC[AES256_GCM,data:MwQrFLQABATd//99Y0rKfMfQ/U4boiI/WjxQZWPreIUTEphVNtFr6Wq/MG8INCjnwpcrpv0eLdvkqrIHFYZIQKcGMNtRkG0Rv51oiXlCgiyWD+MwIiv8k1AF4Y6CLgwXyjb2L2fG18IlhEn2b8sKH4VWYzLYb0I9IYifYUHhmzqsSMbF94k+1LAN7HfimE0o1X+FFm8JROl9wbClrKeqXZWZCpcICrKSV1aMRNfhLno7g8PWif/+MWG6AYDSPWsV4jPzfb2hxHRsoHLnlWDkihObNGqMT0X4zWAeT5WW1TTr8bfzD2uYHvTrj66bOCiWIpmWDRAOYG+E9DPpcbPm6zEdiReT7zjsuXbhLyobRBQm/OBgE6G9bnyqhivmwXfdoSHa7J/GJubehXn+viz/uKcvTBcs7PgkVP5jLwH1AV3bFRNTGEmHvkG70rw23HPot22sFOPxO56fZgfjbkidEI7I9rN2gUYfimsRucHTwv3oL/C9QsOPd3xGjd7lJVgBwExwCG0=,iv:cAtcKr/G5IfbEUkv1IHJD9wtoLSvdcKyGiA2ByAylT8=,tag:s2AKvywATXSu2Owly1bevA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-15T07:00:48Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHn/1bD7Tl0FR7+67RkhL/uAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM3qKmo5S1jux6Jz0kAgEQgDvtB9O66ZI+wLGC1tPJBYDs/zE3b1+Adchc8S4yOYrv+Vy5L6/QV9X491YX9lJFB/GVRqkOb/nePbTIFw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-15T07:00:49Z",
		"mac": "ENC[AES256_GCM,data:IV+8YJieXzZMFomAe+cOnxOGZK45t/16q4FUDKWX0OmuMpsh21IOoN6SkldG39BKkt/ckM+JI6UqPocX5lByptb1uGhEJwd9q2E1iYBnlrUUfZjtSZTYtR1M5TbQw1TASFTTn/NCcl0svxENVvsqWz61xgH3ZHCSBNBVyckIfTI=,iv:W4gbc43z0mVsYDEqRb6Y+anjN5kPKT3LDJ3FvDpgxIQ=,tag:kVgykdnyA0ZLSCzYzDrSaQ==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}