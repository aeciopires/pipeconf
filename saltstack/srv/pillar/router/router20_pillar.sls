{
	"data": "ENC[AES256_GCM,data:wHomMmhJyvO+WNZrNXz1QoAGxwngCx30xmmDOPQFixeZEV87XfNsv6z3Gn7i3zp9t0pPzRAZrolVMC9ka/vqUBKEUYgyf2rPY1gCiUsaCaJfVQzu5UEgcqCFCwFPDmQQ3uOACeu5moeKnYyRQp2E7u5LXagUH3ildMVqNdC2cA0/w01xsClOW7M85qQ5mVKcu6Sz+kwTTsUciGZnCcj5HJXPm/O4CI9knbgmpn67HXx3R5VSjj2AlmINEjMPl3btoOkJVN0Xj72E3krf07Aw66NjiYwqJXNN59QKhphl4mXxctxbtFsvNdhtwUU7r4CMelnAmXdWDYkKKriooOAhkb6ig1LkwlVVq0n5EH+hobL4r09GkK4ScOquuTv8636hUKzS2xjXrwlS5e7yBJ7NBbx1/QHB7FsTypjDm6B3vQQ8Gt1EfGcqRDG7CaT9GeDCqrDUcRLBN/OdS/DjjOL8AwMzw5OOZB4AT3ZC,iv:BLy+ekSYtuwxlhaQ5o0ONuXvCfv4QNugxkvGPvMsJoA=,tag:8anX7ItnKsaPwaNfECoO5w==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:32:55Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwG5xmlb8vhuBqMKmXQvmVv2AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM49SRXnOUxX+hqEvDAgEQgDvRUTibtgMrtHTq4rEyRV0aKtOhg6kzBe7AG2v1Y44xPQ27xFhfICZkGaOilyLWoZV42F6u3OL1/skGwA==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:32:56Z",
		"mac": "ENC[AES256_GCM,data:9aaN1DNxML/dXDeElsyFpD+Gbf69LWvatMf1kUwZ9yqJzUsLZZRAwOm4aGErwzzRmEM4+M3pKJ+gM+JV49qKW9agwOuK7nSDd9u8NXkmd2fqQVVkHZm1iAepZS2hc1U0Ptc4OkNfSwFYjaDE0dufosphj2BDWUghMpA2/EPjqPQ=,iv:IrAbOsMr0t3As1GbxjvJjL+A/Fati5ssxY09igGbJ7A=,tag:vlIRG7EOCUqJ5tmPQf8W7Q==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}