{
	"data": "ENC[AES256_GCM,data:SE5itOmat87FAiEc5R8B8dB7CTFcEJq7sNx28LOtIg5XU0rj2SsOr728DBXqpYQ7UIHzFUoWyRX3ru0QlofMwFvjyype2TR4I8XLia3M8pQNsbPoKbUo/S5cMiYMLk0tXcMLAFBULxQA4ol32nhvVvRngSmbqZbUYYWjpNVDN+PpPlZ28uNW0yL46cIfdRYOgm49pKohC1Bx93FWTnEhv82GyVK1YzZ0Ak56HY9MzmMkYmIxXksxkUtbH/Zq2MezuZvBWaqfO77efF7e2H5o8DVh0qnrd+tc4ISHzn6tT+WcfbPTv2NIZHexRgs3Er0lKm8HSu+uPK0erBu73vVsaWQerhiKmwMNgq9MM8IyLGIYZ40yKH1wlNAU/G3kYhR+X4+ucLOLca7qd2lg/2qByTdpFicRNxEONqbq3Gl6J9quF+LegjXM/AWhJzO6sJ88Vj/Kxek4+kl2URN7QnIMymrLB3X1v+hwq9vu,iv:o70mLqSj3XX0eJrEgIOe4EsJKfkPFwMb6koH9owod+o=,tag:YAhqy41/ycOrCkjA6CEfGA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:33:14Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFdFqE2i2zx1nuK2WhzBxs8AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM/BcsauKV/rTMjCQsAgEQgDvtR1G2rVzXu/BMrZS58DD7nh67G50mP2OWpD87qfVloYTQNjXTS1uD3CsksKiA55bXU5/XUkoqv9W6ug==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:33:15Z",
		"mac": "ENC[AES256_GCM,data:hcUMjhUEBv+gRC0ppSUFZ1RFvLLK/KBkFyHTqXF3XWamsYDVsiWYlTLfI/4ET5AUMuVm61N6KufUMV3j21MzuM47w3UgpLN31SlXccgmNWwJ1LEg+JmAzgqx1U2/ykRgM3rQpwCSlfP/nYGobu8t1IJd+XHj/F3dFlw5RCtVNPA=,iv:KhkAPZNmDOFu2Q6ivpW6RgqVzAxUppmR6oSHPFFZtqQ=,tag:9l+hmPnjDlj5Av/BnqopHg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}