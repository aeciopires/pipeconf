{
	"data": "ENC[AES256_GCM,data:T4ljOXbfRYU5g4cci7TzzFw8I5mdA58gKJDETp4iuCeZ8BH2bRb/vH34cb9/0bXsHD3/72Abhxss5NfbSddn1yw/gTBZ5/4Pkqe79u9LmryBpz9YlUn5RWqwycvdLvsDdS7qO/unHDBxSCS+qxMQhIhYyDpBlsD9Jmx0Q4vcN/d+G2UNVarYKVm4CxdPgJJNUEbYD9R7DSGbQ2coUybX9syzlbYZDn8xZks4+C8Iheoo0E6+YTffAO2Q5V11DlTl1APRhNtA3Z9wBxqB/o4gO9cEBDthYhbJdLzL8dQRGb6lPmC6mYmbUiUu0Fuf+E5mbqN7d5VGpcN19kBwH8KSKgoDDwdWeT7XK29le6aYI/qnu3xGNzOwOMWcYdQTrOGY/SXSxu52+mWgGHiYBW0kd0NqbkHwXdRWeaasFY90rtyRc1QL1bxMFY37HnOrmHfNz2cMMOFcGNJ/eQ==,iv:mbZTzRvUQgQTDA2B/vQ+FgKhs1AQg9VI6ixd4dgr27U=,tag:Y6oJdjMEWJ9VQjcKgaUawQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-21T21:32:45Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFD7M/oO+HZgJRi7Leio13QAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMrsWpG0fOM8xMmx7DAgEQgDsZ++R8jl6KjaWfQjiPf5PBtFiMfUm6h6361pkOsYjc+oWCh2A6bhdKOlMGFRDRx/uCDZQoikmtgSomBA==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-21T21:32:46Z",
		"mac": "ENC[AES256_GCM,data:sRzoX5NzWaOKRSLtLUuK7H1pzBfZkg2q3jJwW/cy/SFVFAUz8F6Fash/jHLR7XEsXkPvM7xRO/39lDcjsaMlyfcJmqOyO1oBK2ohYxvlie39tzEXfesf+nAw6MLrthZVj94KY7H18K0X2xD4rL9RLP3YhNCMd7jmAj6hJcEl7gY=,iv:Gl4hz5SqdNMQ82diOjtrSb035yvjl8/LbEclbXFView=,tag:6kUSMNbxToIRUCL3c/EyzA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}