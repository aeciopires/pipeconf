{
	"data": "ENC[AES256_GCM,data:DsilCOTTvKainbc/PAn/4NSluIJ2Kha34ODrTaFVE9o42LRsXCaiLhXlOaW5KUa00flFS1iNWqqJYGSHgDy+BM5bc34usPcO3BDTgp7QTtaNtcGeF6ifiOGUF3kOYbbbeM2o/QrtW/+kkGx6yaZrOnuwwKx/t2HKQpNz5+YOVTrZahkjIi5YUwQ6ETUu93XH8BO5B0pO5YZBK+N4yt1DHUB88GQ6A5pnQetKB9wK70UaP4GPgdiSBhz30jR0+sQ+az7yxPd5gGz93BYj7h3KKtEHwFozknTLnuRg80Sc0IEAOn3rGw67tsorNUATHVK/vnRh9jyxtDYBlUx+QwwDOFNPBI45XQrRpjheQ4DV375Ou1biF6lzLOivs8TkDCzIpoJf42DXpUn+3uo08Iv1K91yMxWSSlx8iYXkP2YrNZoOKjpStCN0oGTDke5zIiYfLynFfIMyI/Zg36UpUDXsivUKm0O8Se1yTAr+anKg10pU5mxVhHGkZZaOCbMfcqSb5VS43uUW5gZuxu8MFUxzYgt3c3E99U8eiJF+833NqpNOGv0fNA==,iv:OVe2jSnqBL8W0T/8fQccetQHUDMfGyjA9iFAPfSxqvg=,tag:Waxre2XSPBvoRIcsVGp+dA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:16:44Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwEskK2dcrx409YAR6KTYVUzAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMIU9PgJ1QLZ3NS89DAgEQgDuqGCFmbRAoBVN9ChhXgvky1GxRmreWAXwtIQJT28SYT1wY1FL+Ow8Kxudtjr90+MBmSw8rFqxojVoQSA==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:16:45Z",
		"mac": "ENC[AES256_GCM,data:x+2HZzrrwrbvcdhWmxovNZ62KK+8VoRwUbloYAo+2l0KM6mEHqQT2JFN7jA2AVcjl9i8u9+z3A+0973JFD4Jl1DEQFCEbPLNeMtXbulODfoyw6TuS6z2snHatg3EqfAxbuIPX8P2jk2QaW1dpkhIAN83CuqifLuNlgvtkUbzB5k=,iv:9bxbfdnhzkw8LLVbNrRTa4eyTysMm8xBRZXfBBcH9ZY=,tag:wJBSrfqeTA+bE6X4CpaDmQ==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}