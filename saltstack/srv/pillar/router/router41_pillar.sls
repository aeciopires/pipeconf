{
	"data": "ENC[AES256_GCM,data:qY0x16gzJ4WfyT1nEIJbOWSrXeUEgEQAH0q9MeTHQumwiCu8XU5tmZHjZOHsmnV884+w48jWKmj7wZqXZEd8tp+S7QA+HY9KQbyh6U1okxaafOdqVH/czTYvio3GTOQrHxJl6JbpeCkxXR+bV6Hk8KrWCgDxLmbOLVYcyyFD20zBrYhYy8oyslomtS71C1Q5RUYeyZnuX3qlHa2qRxLfrOlMPuaTf33VOGHA8gcr7sodJJO1ekF8+ralkF5EF8uBSDIgxrG00u2v/e+9+jNyQCixx7INfmJgenUXGMtHVIZoiJ7gXTUjhKcjCGxQbsOYezRn5jM1BM7IAqlTLWwp/kGVonioGMznHq6ZMIWExTv1pehrGvy/RoyD9CLV3QEeFLJmSUTNirIfK5WXCkV7+WTd8N38JoH1V8vmlPjNPrto5VcWm+o0XSKm4iMfBsaKonj5hwJqbMlj0jG+2yx9GLn2BurBRDtC3ld9tW7cZXATvCRNlndPwuTFfBBjCq+86HSCfa0=,iv:t+3xVVZvH+y8W7Hqeu9oTJpV3Y9vJBRpF0M8xK5HAww=,tag:fJ66q0IeoY234IeeSHnXPA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:21Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHrnbdcQ6TEhBRdrUlbKQ1cAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMcBp0yMpFKyqPvH13AgEQgDtyuyaTvLbS/RfoJvfZMbtl+roDK9XA5Ipq/LmxJot01SUb8/igO9BbABPb5CEC5QA5GcrATXw4lmeA5A==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:22Z",
		"mac": "ENC[AES256_GCM,data:Krme7M/OvjY0BsyAfnhT7Py8oZF180iVRXNa5FY2N2of5ZrAtgNFJd1LvSThwkgH2ZBXH4hED4ZwYU3FI6WBXwGbXOQViy/XLj44udtRITnlYBG3gCspy3WVsoqSb9//u9S90lvMfK3VTpBB1lpEBTWNd9YLIyC4f1bp0n10Vfo=,iv:/+JGEIBQ+fJ5xLBKAOMrKZUCH/FdeqCymRaHF9Pj+/s=,tag:hJlTZ4PyaGYspkU7xZXC/A==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}