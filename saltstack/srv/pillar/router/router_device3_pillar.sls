{
	"data": "ENC[AES256_GCM,data:uluVq1vOQ4jNLDgZ+hlK512sC078IbTucYkieDE8kT0IDaj/ffi7R7VN65DnCnu30ZaVVGYSh0eHsQRxnBs0a8knTmywmGG3gh18uIx0f7MBzwjaD1NzkGnPHO7d5HUPYmj0SFXIJXTAb2TJRPWEVF0kEMvQ/pDwDVWnKbgD4cHGL6zBGUBrDgfuN4hI2A0luDfnmbVC3dWjIOR+kh8ZVvv4KiCcntVYTcSJgSOZB+K1m1TzBId+iLwHgj7I5wIvqmwvhReErigDttayKQLUh83rXCpaVveWEnfvjdYB7kP6EJWJTCq5gcnxTJuxdldetiJtWkhQ3q7E6EuMJ2ulecfMgY0ylGzN4gBva7aYOxiWEkUlv8JkJQA4SKjNUbVqu4ge4/N3XJ7ExvYL4SnrvB4yNp6SQ5eUZPBWBEbJaykdCU9NaCgDrbijogw7cjrmAhtN4sH+xvNq,iv:Rp9fd/81KVgPRqYcnRx02DQMgNnG5l0qNGS0hjkCrqs=,tag:HhCJto5+MKtarqehNC98mg==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-25T07:45:06Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwGv92uXfLNjKEt7uIR/lEnAAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM3EkR13NsqMNo452WAgEQgDtlVAO1VnwYnDQMA3bzTwh0HKNqzOJEJN8THXf6uT5vG/uV10drQapXSfXdixHJ62zd9ComO3I/c+Vp/A==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-25T07:45:06Z",
		"mac": "ENC[AES256_GCM,data:SqAY4uzuRggNQNMde3c3tIK1ii4zO0bcmTMrUhRZTbq/Sg6ER/eB1Oh3U7wHTsq7hvZU/Dz7alUSRH4NpgNjqvGkmUFvmjF5JJ33TfcdzJT6/QHDrFmc8Mp5S8WVpxyaq3zTIaHJxvmySfHx8DEKXPqLpNIfFDUFqCH1RVBkTGU=,iv:TGd4B6DUkwD4NlSmdAMhc0ASreb6lbC3FYH/Extik5A=,tag:DGl7oSnW0AXvSgHFoQ/kXA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}