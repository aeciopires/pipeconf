{
	"data": "ENC[AES256_GCM,data:QZQafNm6cjY5z61nX+q7Tb7SE6uFe1ZMiTFZ/UIODZ72FLuceS1hmh24tJJOwJFrXi5wsPqarniGW0H13dpKyyKoCsGpYBxccqCCOrxq34dBWEfWXXftxiQgQCUcUmhY+HikQYWYX5OLMAC0davIRxsxFUCFgt9b32JGEcR+46jz68IHqNpy0aCNWvTX6mmED/4sdpAef5GOPVwwaz2RntqReuZOQJU9jbeaeyIQWb/e5o2WpWeCMp+zAvO7i2ljFkKLF/ThZQqo9UlQX6gwu+dplhOy1g2VUoO1+olnNhoqf3Ml5PYYe2kLBbKTwmnWQo4/qExtzoN8n7GKKdM/CaoeTQsH2lxwZNgoDyrH8mQ3vvqeRFdcgH4UpTWO5JZYnAEOA6Gcr929l77DPr1AGoImDGAHS3+Tz9VehPSsS7I6gbzzi0ngwoGN/uyFAyCIha6A/vIlCGaK+OdN3V0r3rmuRLloASi7wsaR,iv:/nEY9ZpWIndjDy/Sz77LMB12S8ZEuH7U3QSWWR22PU8=,tag:VnhgN15NbQZ/Y+0E5BlMPw==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:32:47Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHqYFPglzYRSgd0+MM5PzxcAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQML2GbTw1xMM/E1hm/AgEQgDuVwR1t1351SxS2NsxWwMxRB5SPXtoV3dYZS3Q+mudvJD7U6p658hn2NKm0kzo4VsCr21aKGWO6tqhpfQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:32:48Z",
		"mac": "ENC[AES256_GCM,data:3wv5tDgA+7Ko3eFpYtGnN7fBottXz7WTG0TYplubzQhMBF5oe9aYOvablPH8yLdX4xpEEi3ALme3J3NjsiRQD0MlziUP/u9eT2GVQMyRhJmkE3bqjZMOpiQkAZ2d/1NtO4r6fwczHT1+DKnXnqUeAAXg8g4yV3yOOHhFtnzmr14=,iv:WJwVku5mC2OSg8QAaaPrdVXoxO9VVmuV2noFVlcnhWk=,tag:+Bjg3J06ejPvK2TSJ1XSAg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}