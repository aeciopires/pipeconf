{
	"data": "ENC[AES256_GCM,data:3L9QRutoCgPfzIi8sqHOjRnp7TylbsuSOBmzcZ4ZemM7WJGFMp7uRflpClD9dODDZWvkjIkmoAJQ2AJzyFCoagtHwBRflZBWYgAYGgGJSFaYGw4X8MJ0GDdJMrg7r0OsqQLjJpRHWDmy2StfNA5ErMD37NmSd/XTiC4F9O8Or/EArto17QDmk7ZA8e7RnC0KzGxtgkfYqgn2D3QElPnx49dQ9jbs3fIHRDEcyR1lK6UPrth33S/VjhrjOnb5ptM/L+T8B87VHKSlMyd0JwFomt/ju3plN3qmTeIgIl0AZOYmmdGhJct/wOXHOe8Y95I4RDbe5aNYxiKUYq8IPBmLYaldb6nMrinYhl3z4ss1gLy622Ita6Ovf1piYK6MjNS15h5q3GUKRQYA284d5TgsYxqoat8+yIjzOTMPrkOqbHl7K40Epmp6AxVtJCFkt0LX62YcrmxQsKGyUOgtkbi1Jct5qqlB75unr9g=,iv:LA1kaPi3BtUSZ2eLB+Lr10wohrbhKwUFeplqS32rKy8=,tag:aF6hbWDEud133rcxp8ombA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:32:42Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwE55S/YW4aw1zaPEuge4tfIAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMNAR5tgfkk38+EfvJAgEQgDtdOg/77GbG8JqUHfhp9LMFtjFeGe6mPfvXS/8uDPxUOvroxRB72YaS0yno+mr0K3PL6lmxJ7v1AI5s4w==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:32:42Z",
		"mac": "ENC[AES256_GCM,data:Su3QBiUiA6vdRAtTjbu6or13ECwLUDKwFsCtCXCVZcGGPwoErZIcVbqfNWmQ+Pro1suhgVUAGRmpsa4p/bO1P5xbpo7sDo/Wx0pW2uR0lQIYbxX6yABCg2vd74kRHf16/hG07wFA7EvBeOWacr+xgFTbyGV1/wj/0oP4yTj9m1M=,iv:vZy3yo64AOGEurDRj/i2dYfypPAfY3TQUh82vIlG+40=,tag:P9lIbFiu3EVcMZeZB+MkWQ==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}