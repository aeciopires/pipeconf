# Exportando a configuração 'running' do equipamento
/tmp/backups/running/{{ grains.id }}/after_{{ salt.status.time('%Y%m%d_%H-%M-%S') }}.cfg:
  netconfig.saved:
    - source: running
    - makedirs: true

# Exportando a configuração 'startup' do equipamento 
/tmp/backups/startup/{{ grains.id }}/after_{{ salt.status.time('%Y%m%d_%H-%M-%S') }}.cfg:
  netconfig.saved:
    - source: startup
    - makedirs: true





