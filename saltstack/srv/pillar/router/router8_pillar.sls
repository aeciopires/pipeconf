{
	"data": "ENC[AES256_GCM,data:qJdnvBOYfd9qu8QY1Wzn5CbeCxULVtAfH4F6Hm4zN5n5rhpMbO5m5LxKB23ezPPjCJIzEMgem2vzquIhhCZDBVbnnudbvKt5ntCoBspYzCTD4CI02wRYDFuGgqq0finUbHO4A25agMpOAwRbgdjDWltxMZTDhBSFbgNI5x2BjCz4ro9KWeKaaakwegts6O6GeU9mfl98AqqJYeiUNr5V+WKDLXhspxAnv7vBG5H7g855hQlZksHfvbyKl+uSm8nQLxC1BE/S5iHo68W74ktcIEvuBQ/VUsySjr+4aeRUdH9d/boaM5YmWN7w9IBt8aygLgOtgsP2Uuud+ESUlw1CTcOSOf3P+9yvoYnBAIM3BGoY/K+oqKPDFOScL2n2/1qzSBrNoHzIDarZ0g5MRqrIHLBVXYym6wezAt15HcUJEoTXAjQD3jLTHssM+t4shT/OxM82zIB7gnJJEvTjkpeUw7j9rMTlaD8wCNAX,iv:wvniZ7yA9g68x5pVemzZj4x6cyvbZe9bO/bt05z6HtQ=,tag:Hm3PKOJ517ezmMXU4yLbPA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:33:23Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwEZp48U5AZHzG5dizBf0E18AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM16G2eaQiQbmf8p9IAgEQgDsbdcyeq97AbSoYCsYEELYUg72a3m694ME66G63H6bPb2BlakP6+4kVPgM+J8jwh8oSHlAzTPaD/EwH6Q==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:33:23Z",
		"mac": "ENC[AES256_GCM,data:eVT8zYk7sbY+fExGzLfldtc7nFqwxw5iQKnsYfpWzHE2egRjOl9r29200H6Ho5B1pVRRVfsFm0t0aCUHzxPoBrGEKHfryEacIG6h3/7J40JCj4bkNgWmAUd3FVBkPhYnlOpGcDZOZFFJpqMGHFN0R2utXTf3KM8mPwSwYZygVgE=,iv:f+UuYGxBhemAIsODEb4l5oroTrUJim8G4TuQdDfJc9Q=,tag:FhatcJsSEUp+M4/P/CeNow==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}