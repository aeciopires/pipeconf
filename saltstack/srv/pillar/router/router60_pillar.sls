{
	"data": "ENC[AES256_GCM,data:NndxXqZsJeVxaoNMKxw7a2Fo3u/B7AbjVyU+cTjOq2oSAhoNSgPlkpq0hM59clhtUJCu7buibEuOmCAIjpyE17fwHmyDLGr2QrtojIvR0Cks6D3SOP/7TYcvJwMBz59r2OcScP+CpdeXeYjZuoCAzJBbkhi48UCdHukJ23Zf1OQxkXUBP05nn5O4w5e3eemhrVJ8VusHRzdcz1sLeErEnXx1OtsRd00gN+pII3p/SVOwwsJqLq2GsA8o4MOUsRin+QFCRXpFzYUsYiGUJxDwMEtYWfZsNHREr2iaRyLlY9JCUHpSfsxUl3xOy+KwRCz9G0I9Kf7fNFDgMsnNj7qqwnT1v2kqm7sS8Qzdw5dmf3YG+SVySWFieiinlcrp8Jz9icvSXnuwIRK6MGVzgINDmB65B9cN09mxUStl3ZxJy4/zfKMYCW5HXOvCmwfS6X0Q517bjCprsqCPYPgT2+RxegV1fR6LZmueUEVc0iDT1BV7HN6FW6SQMvW20vJc4ili0XiiWg==,iv:QA/VnJCv2rfFyzURsUjMgkH3rqZ68EvzrzqxytcLJns=,tag:kGGVTdTVxSvuJF9H4XovKg==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:36Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwEmCe6glQ/PN7elwc17SowdAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMRBr1emq+2IF0fSLcAgEQgDs/sqkB4H5eDFTtT8AmzSHCbQawwa7O54aAaXMQSnvgMBA+xJDuB6yuHbLxKhRIQO6Bi8ZrqFIc/5xNog==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:37Z",
		"mac": "ENC[AES256_GCM,data:fbcurojrHd2AHLvPk8OFxvGGVSjAb62j/M0Ndo39QdcywuwifUneueqX60KWrzgnGJ2FbmmZVGU/JPfhZ/5vclVQh5w3BMio6xGYSb3MOAmUCDkv/4KJr4txmx5/eqGQYoGlB3sFeIfHQkrqiOsEzQ7v+UUoBnigVicn24aEMYE=,iv:gclk+gfistrNaoqW4Mw7FigdoL6kqLfD/Jk+YldeyaA=,tag:LLAPvfSWovzToq60u4/UZA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}