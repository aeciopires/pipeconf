{
	"data": "ENC[AES256_GCM,data:0z06mvZZxKyyqg3LUnKmswGeSHuoVhtOeHE2QqjehNNuoiGKMp4z8a3w/jFwnXmasVXNcT7+OXwA5ESTQbxF0dMkFao2SyTWeCCLB+oDDVK0OFzWT2hcfg2FqlqgdZBebBcCCo3hCMF+MgAdO5hrlqUItM0NC6bs6sbCdxZddITJ57dw3DZ/2bSt8UsRFAqdl4479cY0UOsTXiYPRA4rMVM2tg+qjH7H4AS1ISCwD7TMhmoDCIGH9Hx6kryLWg0UHTLHIP7EAb0tbRbLkuhEeW8LqpgdIgxz7ChjHiqcIPoEL9IvSxxc32RGcRLAMRvi+R8tabzrbx0NnemAUlYjq02LGRozqSxm7biYf1mL2nbgRM6xWr2+R/RMgGJEomqP5bPsYaIzJZwWHYUkC+YiJOLoLYbGihW6WDFaxPo6bLOjPHqXaveao2KiLf/F2eBS2hwgs5ka947LEvOIPSLp3PrCMly8z8uV25Y=,iv:gFwlJY3bv/gmfEBXXCjrDwXsP0xuN1fWrdRlZ82PLdo=,tag:T54Dopsw3bA5zyafyu2BEA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:32:25Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHQ7E/BBQoFuLNEKoSa8MqxAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMtQ5GLSrzUFh+TMcMAgEQgDsrAozMMwbQsNwSrZcLN9faYBqDiCWLjiee5YhXeryCHs5uJzSEr6RCx3wnszmObHV6TxtBq9xDq/G2RQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:32:26Z",
		"mac": "ENC[AES256_GCM,data:ujf2pX8sg+AtwkRIuCTV08Jm/Rtwt8fa2/nMuoVlcBxWAe0BGVAuq9Ka2sWxFVkI3922isFq8SWIcykbp2fbt6nR4onMuuyEeNUkNRVA9uLnGwFPjLGsyQqIuLMOc8Nx/57ejXkE9FuHWKuee/cEFoGG1hEP8owbet3Xn467jb0=,iv:3wnJHX0DPrLSa6wfIKCiBDvAft+r3pCFosaOiGpvtvs=,tag:a3PVAsCsSnP4Ujg/x4vC5A==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}