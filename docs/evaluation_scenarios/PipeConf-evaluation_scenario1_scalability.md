<!-- TOC -->

- [Workflow do cenário 1 de avaliação do PipeConf](#workflow-do-cenário-1-de-avaliação-do-pipeconf)
- [Atividades realizadas pelo PipeConf](#atividades-realizadas-pelo-pipeconf)
  - [Configurações do ativo de rede](#configurações-do-ativo-de-rede)

<!-- TOC -->

# Workflow do cenário 1 de avaliação do PipeConf

**Objetivo**: Comparar as métricas de CPU, memória e o tempo que o PipeConf utiliza para gerenciar a configuração de **n** ativos de rede, sendo **n=[1, 2, 4, 8, 16, 32, 64, 128]**.

Edite os arquivos de pillar dos ativos de rede e adicione a seguinte configuração.

```yaml
ntp.servers:
 - a.ntp.br
```

Faça commit das mudanças.

Localização dos arquivos de pillar para roteadores: http://pipeconf-server.domain.com.br:81/root/pipeconf/src/master/saltstack/srv/pillar/router

Fonte:

* S. House and M. Ulinic, Network Automation at Scale, O’Reilly Media,Sebastopol, vol. 1, 2017, pag 48.

# Atividades realizadas pelo PipeConf

**Passo 1)** Acessar o repositório git que versiona os arquivos de backup dos ativos de rede;

**Passo 2)** Decriptografar os arquivos com as credenciais de cada ativo de rede;

**Passo 3)** Executar o contêiner do Salt SProxy para detectar quais ativos estão conectados;

**Passo 4)** Acessar o ativo de rede via SSH para obter algumas informações a cerca do modelo e fabricante;

**Passo 5)** Obter o backup da configuração a partir da memória (running-config), antes de aplicar qualquer alteração;

**Passo 6)** Obter o backup da configuração a partir da inicialização do ativo (startup-config), antes de aplicar qualquer alteração;

**Passo 7)** Configurar o servidor NTP (Network Time Protocol), endereço a.ntp.br para sincronizar a hora;

**Passo 8)** Obter novamente o backup da configuração a partir da memória (running-config), após aplicar as alterações;

**Passo 9)** Obter o backup da configuração a partir da inicialização do ativo (startup-config), após aplicar as alterações;

**Passo 10)** Criptografar os arquivos de backup da configuração dos ativos de rede;

**Passo 11)** Enviar os arquivos de backup para serem versionados no Git;

**Passo 12)** Enviar notificações aos usuários por email e Slack a cerca do final da execução do PipeConf.

## Configurações do ativo de rede

Roteador Cisco modelo c3640

| Parâmetros                  | Valores       |
|:----------------------------|:--------------|
| Usuário SSH                 | aecio         |
| Senha SSH                   | teste         |
| Senha do modo administrador | teste         |
| Porta SSH                   | 22/TCP        |
| Endereço IP                 | 192.168.122.X |

Os endereços IP dos ativos de rede variam de 192.168.122.2 a 192.168.122.254, conforme a atribuição automática de endereços pelo serviço DHCP (*Dynamic Host Configuration Protocol*).
