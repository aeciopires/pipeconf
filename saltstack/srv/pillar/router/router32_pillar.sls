{
	"data": "ENC[AES256_GCM,data:pWya5FBeh0iJOCsnizcC5y09vYwp+itg7s5JIUc+DdjA2CyBuKqn+2gBEha5ZV7H64Jpi/lcCPuKweC6brvD8kcJlDRuCXVczSNzQC9kbbTmyKpUqgfkaf8jUTO6LLxAuqir2aO7Pllij0V3b8CT0D9lOnn1mg3lJggq5Gk2qylhn1HW9z1t3cNtdyOZ4yO4vosQjJ8/be+g/yq3YM94CAubYUWECnXZ1OdSAnDKQ7r0+SyfkdtABP2F4jtkooVteBD6SFfvidNgvlZLHVTJsKjJBWcM/TfzuF4p8Z+uMNefEzBHnIiqeUPvQvJDJbyFy4Rdm7Y4k5sGrlOffZIZiEegVDmHy7PSnaicpxd6QAwAT2pKtC8kPezgaJu8o9jmkXULMF577WySC7xIOWmhenuWhLXs87Eg6HaG3fAuhbyiRxsWoEIzpa+ix3GzMojslIPjCMjtw8dqaw==,iv:hcaPItZ1Mmi5DNXq4c+CDIUXyrgNHJ6Qoy8iuVLj9vU=,tag:fwMWaBB8OW9pUHKTrrkxag==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-21T21:42:04Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwE+OpjK/0CaGoFfPFlv7mCYAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMPRTSDOirGuWSJtSXAgEQgDvl8IR7WUO1Wq/3U+HlDZh+wtG9JKYBW3oMH1aQ7b0+RRFWvb4CBUvMDeMVkwPI8XWf3IpZAhdzwdDPfA==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-21T21:42:05Z",
		"mac": "ENC[AES256_GCM,data:Bl5W/T+QUG72x1AnHSQHJKjhretI37cY1cuW6EtV9pZ7ekzCja7MoJKnjE59rS9jzlQZs6CmWacdfwCogcAUUxLi9232eyZLm+2aGAn33MllEQLaiJfsto86oanoQQ0UsJ9UZCH0GTvD9Att/beYyBJIbUy5oQ1rXxvzWoEj1DU=,iv:ABsJhrFO4tq5IU367KxZFsKh+/ZvbyP5+GLpturqK80=,tag:zNkS+Tne8doOL2snAhSCIw==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}