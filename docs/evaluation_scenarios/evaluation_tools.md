<!-- TOC -->

- [Ferramentas de Configuração de Ativos de Rede Heterogêneos](#ferramentas-de-configuração-de-ativos-de-rede-heterogêneos)
- [Unimus](#unimus)
- [pyATS](#pyats)
- [NetBox](#netbox)
- [Frinx](#frinx)
- [Itential Automation Platform](#itential-automation-platform)
- [NetRMI](#netrmi)
- [Apstra](#apstra)
- [Rconfig](#rconfig)
- [Network Automation Manager](#network-automation-manager)
- [NetWork Configuration Manager](#network-configuration-manager)

<!-- TOC -->

# Ferramentas de Configuração de Ativos de Rede Heterogêneos

Realizou-se uma busca extensiva na Internet através de motores de busca e sites empresariais específicos, bem como grupos de discussão especializados no Facebook (P.ex., a pergunta no grupo de discussão **Emuladores Simuladores de Redes**: https://bit.ly/37cItyB ; cuja temática é diretamente relacionada e conta com a participação de mais 5.000 profissionais, a maioria brasileiros.). Entretanto, não houveram respostas relevantes e as ferramentas encontradas são detalhadas a seguir. 

Ao todo foram investigadas as 10 ferramentas citadas na Tabela a seguir. Apenas o **Unimus** se mostrou parcialmente viável, tendo em vista que realiza bem menos tarefas que o PipeConf e que possui uma licença gratuita para configuração de até 5 ativos. Entretanto, mediante contato com a equipe de desenvolvimento da ferramenta, foi possível obter uma licença de quatro semanas para configurar até 32 ativos simultâneos. Isso possibilitou uma avaliação de escala mais próxima do desejado.

| Ferramentas | Métricas de tempo | É pago? | Período de teste | Observações |
|-------------|-------------------|---------|------------------|------------|
| [Unimus](#unimus) | &check; | &check; | &check; | Gratuito até 5 ativos e licença adicional de cada ativo custa anualmente US\$ 4.5 + IOF; |
| [pyATS](#pyats) | - | - | &check; | Gratuito, porém de complexa usabilidade; Nos testes realizados não foi possível gerenciar nenhum ativo utilizando-o. |
| [NetBox](#netbox) | - | - | &check; | Usa o Napalm apenas para receber o *backup* da configuração e não exibe métricas de tempo relacionada a esta atividade; Essa funionalidade é essencial para as análises pretendidas |
| [Frinx](#frinx) | - | &check; | &check; | - |
| [Itential Automation Platform](#itential-automation-platform) | - | &check; | &check; | - |
| [NetRMI](#netrmi) | - | &check; | &check; | - |
| [Apstra](#apstra) | - | &check; | &check; | - |
| [Rconfig](#rconfig) | - | &check; | &check; | - |
| [Network Automation Manager](#network-automation-manager) | - | &check; | &check; | - |
| [NetWork Configuration Manager](#network-configuration-manager) | - | &check; | &check; | - |


---------------
---------------
---------------


# Unimus

* Site oficial: https://unimus.net 
* Funcionalidades: 
  * https://wiki.unimus.net/pages/viewpage.action?pageId=10092755
  * https://unimus.net/features.html 
* Preço: É gratuíto para gerenciar a configuração de até 5 ativos de rede utilizando todas as funcionalidades.
Acima disso, custa US$ 4.5 + IOF por ano
  * https://wiki.unimus.net/display/UNPUB/Pricing
  * https://unimus.net/pricing.html
* Os requisitos de hardware e software do estão disponíveis em: https://wiki.unimus.net/display/UNPUB/Hardware+requirements
* Documentação:
  * https://wiki.unimus.net/display/UNPUB/Installing+Unimus
  * https://github.com/crocandr/docker-unimus
  * https://wiki.unimus.net/display/UNPUB/Common+first-launch+issues
  * https://wiki.unimus.net/display/UNPUB/Running+Unimus+behind+a+HTTP%28S%29+proxy
  * https://wiki.unimus.net/display/UNPUB/Configuring+server+port+or+bind+address
  * https://wiki.unimus.net/display/UNPUB/Unimus+HTTPS+with+a+self-signed+cert
  * https://wiki.unimus.net/display/UNPUB/Architecture+overview

# pyATS

* Site oficial: https://developer.cisco.com/pyats/ 
* Demonstração: https://www.youtube.com/watch?v=aMoXuXWohTA 
* Funcionalidades:
  * https://developer.cisco.com/pyats/
  * https://developer.cisco.com/docs/pyats-getting-started/ 
* Preço: é gratuíto.
* Requisitos de Hardware: https://developer.cisco.com/docs/pyats-getting-started/ 
* Documentação:
  * https://hub.docker.com/r/ciscotestautomation/pyats/
  * https://developer.cisco.com/pyats/
  * https://developer.cisco.com/docs/pyats-getting-started/
  * https://developer.cisco.com/docs/pyats
  * https://pubhub.devnetcloud.com/media/pyats-getting-started/docs/intro/introduction.html
  * https://github.com/CiscoTestAutomation/xpresso
  * https://github.com/CiscoTestAutomation
  * https://hub.docker.com/u/ciscotestautomation
  * https://www.youtube.com/watch?v=ktEBApbHlEA
  * https://developer.cisco.com/docs/genie-docs/
  * https://www.youtube.com/watch?v=AfmzZGJzBcQ
  * https://www.youtube.com/watch?v=THgHwS-zVt8
  * https://www.youtube.com/watch?v=23hPg88pZBo
  * https://www.youtube.com/results?search_query=how+to+use+pyats
  * https://codingnetworks.blog/how-to-install-pyats-in-ubuntu-linux-starting-the-journey/
  * https://www.rogerperkin.co.uk/network-automation/python/pyats-genie-tutorial/
  * https://github.com/CiscoDevNet/pyats-coding-101
  * https://developer.cisco.com/codeexchange/github/repo/juliogomez/netdevops/
  * https://netdevops-labs.readthedocs.io/en/sevt-review/labs/nso-with-ansible/
  * https://community.cisco.com/t5/tools/pyats-genie-connect-function-can-t-ssh-to-asr9k-with-interactive/td-p/4070060
  * https://developer.cisco.com/docs/pyats/#!connection-to-devices/device-connections
  * https://pubhub.devnetcloud.com/media/pyats-getting-started/docs/intro/introduction.html
  * https://developer.cisco.com/docs/unicon/

# NetBox

* Site oficial: https://github.com/netbox-community/netbox 
* Funcionalidades: https://netbox.readthedocs.io/en/stable/core-functionality/ipam/
* Preço: É gratuíto.
* Documentação: https://netbox.readthedocs.io/en/stable/ 

# Frinx

* Site oficial: https://frinx.io 
* Demonstração: https://demo.frinx.io 
* Funcionalidades: 
  * https://docs.frinx.io/frinx-odl-distribution/supported-devices.html 
  * https://drive.google.com/file/d/1_aqbOSA1Tvej7lHq_xkwpdErw0WCPLp2/edit 
* Preço: Sob demanda conforme contato: https://frinx.io/contact
* Requisitos de Hardware:
  * https://docs.frinx.io/frinx-odl-distribution/oxygen/getting-started.html#system-requirements 
  * https://github.com/FRINXio/FRINX-machine 
* Documentação:
  * https://docs.frinx.io/frinx-machine/getting-started/getting-started-with-frinx-machine.html 
  * https://docs.frinx.io/index.html 
  * https://github.com/FRINXio/FRINX-machine

# Itential Automation Platform

* Site oficial: https://www.itential.com 
* Demonstração: https://www.itential.com/request-a-demo 
* Funcionalidades:
  * https://www.itential.com/products/automation-platform/
  * https://www.itential.com/products/itential-automation-capabilities/ 
  * https://www.itential.com/products/configuration-manager/ 
* Preço: Sob demanda: https://www.itential.com/company/contact-us/ 

# NetRMI

* Site oficial: https://www.infoblox.com/products/netmri/ 
* Demonstração: 
  * https://info.infoblox.com/resources-evaluations-netmri 
  * https://www.infoblox.com/resources/?category=Demos 
* Funcionalidades:
  * https://www.infoblox.com/wp-content/uploads/infoblox-datasheet-netmri.pdf
  * https://www.infoblox.com/products/netmri/
* Preço: Sob demanda em: https://info.infoblox.com/contact-form 

# Apstra

* Site oficial: https://apstra.com 
* Demonstração: https://go.apstra.com/free_trial 
* Funcionalidades:
  * https://apstra.com/products/#capabilities 
  * https://apstra.com/products/#features 
* Preço: Sob demanda em: https://apstra.com/contact/ 

# Rconfig

* Site oficial: https://www.rconfig.com 
* Demonstração: https://www.rconfig.com/demo 
* Funcionalidades: https://www.rconfig.com/#whyrconfig 
* Preço: As informações sobre o preço estão disponíveis em https://www.rconfig.com/#pricing

# Network Automation Manager

* Site oficial: https://www.solarwinds.com/network-automation-manager 
* Demonstração: https://oriondemo.solarwinds.com/Orion/SummaryView.aspx?ViewID=242 
* Funcionalidades:
  * https://www.solarwinds.com/network-automation-manager 
  * https://www.solarwinds.com/-/media/solarwinds/swdcv2/bundled-products/network-automation-manager/resources/datasheets/nam-datasheet.ashx?rev=a6b3b169113541899d7e4cdd1749a0ec 
* Preço: Sobre demanda em: https://www.solarwinds.com/company/contact-us
Opções de licenciamento: https://www.solarwinds.com/licensing-options 
* Documentação:
  * https://documentation.solarwinds.com/en/Success_Center/NAM/Content/NAM_Installation_Guide.htm
  * https://documentation.solarwinds.com/en/success_center/nam/Content/NAM_Getting_Started_Guide.htm
  * https://www.solarwinds.com/network-automation-manager/registration
  * https://www.solarwinds.com/downloads

# NetWork Configuration Manager

* Site oficial: https://www.manageengine.com/network-configuration-manager/cisco-device-configuration-management.html 
* Demonstração: https://www.manageengine.com/network-configuration-manager/download.html?hometop 
* Funcionalidades:
  * https://www.manageengine.com/network-configuration-manager/view-device-models.html 
  * https://www.manageengine.com/network-configuration-manager/features.html 
* Preço: https://store.manageengine.com/network-configuration-manager/ ou sob demanda aqui: https://www.manageengine.com/network-configuration-manager/get-quote.html 
* Requisitos de Hardware: https://www.manageengine.com/network-configuration-manager/system-requirements.html
* Documentação: https://www.manageengine.com/network-configuration-manager/manage-device-configuration.html 

> Requer o uso de sistemas proprietários com licenças extras (Windows Server e SQL Server Database)  e grandes configurações de Hardware https://www.manageengine.com/network-configuration-manager/system-requirements.html