{
	"data": "ENC[AES256_GCM,data:RkCjnZ8TOLHtQCsMr4K3rsApWmt4mzDqypL31DTIITyEHavV3NKvTiIA2fIEiz305NUkvXLm55n3Y68hrwvfjKdjCG22d7WpiKBazFInQAKbWjCWBkWSGEVE6AxjjoGSQ/OqUXzwLQl6OaDMt5kmYeTj8IM3/acHDwKm5mSWJbT0CAi0d5N0jLKvygTzYxBpV2Lu1sEAbuB1AeZPhDZvDBiGk+ixrE4txKG2kjQWcfd5W4g8q79jhpVZMdmN/OmGqWpQsPz4xhNQeq/so10a8Xj8cbw6kZLB72WiCPFfA4XYhcGhq6t9jOCP2q146/Iu58wu+eTgJF27M1yTUh+rh1YyaUM0N0D/2Qhw1oE8N9lieSTuKkEfh60fcb/vF1QdehVmFbMDOkwj4OXjidE071cDFrhzAbFA7Q3rBDA9X0GCUvcS9WSvcSnRsd30tA58DqQqOi0X9Uo1WJZAVbbFoHcsSMErkcajQICR,iv:d9mvV7Rz1xKuR82SuIN1Nbw8qtrgphIh9dr2qJJeB9k=,tag:Brsxgv6j+Gpq8rXJIuXg4Q==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:33:25Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwF7+1rgTYyVBOAS1uRxQeTUAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM+kIITnGhW5PD4x+9AgEQgDtQXhyrHEOppZ8alNRytVpD+tWL/8BECGKxluAT0OiCFLKBLNCnrGGxz3+rrS8ck9zxNTJ3AxpO7dQWcg==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:33:26Z",
		"mac": "ENC[AES256_GCM,data:yiSJWG9653sQruSQ+Fvv+GfU1u0urYLUM+qCiNZk/De1qEhJaWAqBgYMPEj9Pls4UQWk38nTIeBOGQNJHHbFydITWnohA2X2TY6wrC33U1prAiheEXsFEGaxLgcjShYSMinvHx7OxyIl+RiRUhWs+iVuVCO/LBXiSSydB0o7lCY=,iv:BKk61NaiaAWuK1HHLATLsZwctw4zs4/PJyO7AqFPNB8=,tag:OFgcppG3A7oCAatdKnZZeQ==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}