# monitoring_pipeconf #

<!-- TOC -->

- [monitoring_pipeconf](#monitoring_pipeconf)
- [[Extra] Instalando a Stack de Monitoramento do PipeConf](#extra-instalando-a-stack-de-monitoramento-do-pipeconf)
  - [CAdvisor](#cadvisor)
  - [NodeExporter](#nodeexporter)
  - [Prometheus](#prometheus)
  - [Grafana](#grafana)
  - [Configurando a Comunicação entre o Grafana e o Prometheus](#configurando-a-comunicação-entre-o-grafana-e-o-prometheus)
  - [Importando Dashboards](#importando-dashboards)
- [Liberação das Portas no Firewall](#liberação-das-portas-no-firewall)
- [[OPCIONAL] Exportando métricas coletadas pelo Prometheus no formato CSV](#opcional-exportando-métricas-coletadas-pelo-prometheus-no-formato-csv)

<!-- TOC -->

# [Extra] Instalando a Stack de Monitoramento do PipeConf

A instalação da stack de monitoramento do PipeConf é opcional e não faz parte da instalação padrão.

Para a stack de monitoramento funcionar é necessário instalar o Docker conforme mostrado na página [README.md](README.md).

Os serviços mostrados nas seções a seguir devem ser instalados no servidor ``PipeConf-Server``.

> **Atenção**: Esta stack de monitoramento não contempla o AlertManager serviço utilizado para envio de notificações/alertas. Inicialmente, a stack foi pensada apenas para visualização de métricas em dashboards.

Referências:

* https://www.skedler.com/blog/monitoring-servers-and-docker-containers-using-prometheus-with-grafana/
* https://github.com/stefanprodan/dockprom
* https://medium.com/@bhargavshah2011/monitoring-docker-containers-using-cadvisor-and-prometheus-5350ae038f45
* https://medium.com/@mertcan.simsek276/docker-monitoring-with-cadvisor-prometheus-and-grafana-adefe1202bf8
* https://prometheus.io/docs/guides/cadvisor/
* https://mfarache.github.io/mfarache/Monitoring-docker-containers-grafana/
* https://github.com/aeciopires/adsoft/blob/master/aws_services/install_docker_monitoring.sh
* https://grafana.com/grafana/dashboards/179
* https://grafana.com/grafana/dashboards/893
* https://www.robustperception.io/understanding-machine-cpu-usage
* https://tech.showmax.com/2019/10/prometheus-introduction/

## CAdvisor

O [Cadvisor](https://github.com/google/cadvisor) é um serviço responsável por exportar as métricas do serviço Docker e dos contêineres.

Execute os seguintes comandos para instalar o CAdvisor.

```bash
docker run -d --restart=always \
  --volume=/:/rootfs:ro \
  --volume=/var/run:/var/run:ro \
  --volume=/sys:/sys:ro \
  --volume=/var/lib/docker/:/var/lib/docker:ro \
  --volume=/dev/disk/:/dev/disk:ro \
  --publish=82:8080 \
  --detach=true \
  --name=cadvisor \
  --device=/dev/kmsg \
  gcr.io/cadvisor/cadvisor:v0.45.0
```

Será criado o contêiner com as seguintes informações.

| Properties          | Default Values |
|:--------------------|:---------------|
| Port HTTP host      | 82/TCP         |
| Port HTTP contêiner | 8080/TCP       |

Veja o log do contêiner com o seguinte comando.

```bash
docker logs -f cadvisor
```

> **Atenção**: Se quiser centralizar o log do contêiner no Loki + Grafana, conforme mostrado no arquivo [Logging.md](Logging.md), inclua as seguintes opções no comando de inicialização do contêiner. Dessa forma ao invés de olhar o log individualmente com o comando ``docker logs -f NOME_CONTEINER``, você poderá ver o log de todos os contêineres de forma centraliza no Grafana.

```
  --log-driver loki \
  --log-opt loki-url=http://172.17.0.1:3100/loki/api/v1/push \
```

O CAdvisor estará acessível na URL http://172.17.0.1:82. As métricas estarão disponíveis para coleta em http://172.17.0.1:82/metrics

Referência: https://github.com/google/cadvisor

## NodeExporter

O [NodeExporter](https://github.com/prometheus/node_exporter) é um serviço responsável por exportar as métricas do computador físico ou virtual que executa os contêineres Docker.

Execute os seguintes comandos para instalar o NodeExporter no Ubuntu.

**Recomendado para ambientes de produção:**

```bash
curl -sSL https://cloudesire.github.io/node-exporter-installer/install.sh | sudo sh
```

**Recomendado para ambientes de teste:**

```bash
docker run -d --name node-exporter \
  --restart=always \
  --net="host" \
  --pid="host" \
  -v "/:/host:ro,rslave" \
  prom/node-exporter:v1.3.1 \
  --path.rootfs=/host
```

Será criado o contêiner com as seguintes informações.

| Properties          | Default Values |
|:--------------------|:---------------|
| Port HTTP host      | 9100/TCP       |
| Port HTTP contêiner | 9100/TCP       |

Veja o log do contêiner com o seguinte comando.

```bash
docker logs -f node-exporter
```

> **Atenção**: Se quiser centralizar o log do contêiner no Loki + Grafana, conforme mostrado no arquivo [Logging.md](Logging.md), inclua as seguintes opções no comando de inicialização do contêiner. Dessa forma ao invés de olhar o log individualmente com o comando ``docker logs -f NOME_CONTEINER``, você poderá ver o log de todos os contêineres de forma centraliza no Grafana.

```
  --log-driver loki \
  --log-opt loki-url=http://172.17.0.1:3100/loki/api/v1/push \
```

O Node Exporter estará acessível na URL http://172.17.0.1:9100. As métricas estarão disponíveis para coleta em http://172.17.0.1:9100/metrics

Referência: https://github.com/prometheus/node_exporter

## Prometheus

O [Prometheus](https://prometheus.io) é um serviço de coleta de métricas de aplicações e serviços de rede.

Execute os seguintes comandos para instalar o Prometheus.

```bash
sudo mkdir -p /docker/pipeconf/prometheus/data

sudo chown -R 65534:65534 /docker/pipeconf/prometheus/data

sudo wget http://aeciopires.com/files/prometheus.yml -O /docker/pipeconf/prometheus/prometheus.yml

sudo wget http://aeciopires.com/files/alert.conf -O /docker/pipeconf/prometheus/alert.conf

docker run -d -p 9090:9090 \
  --restart always \
  --name prometheus \
  -v /docker/pipeconf/prometheus/data:/prometheus \
  -v /docker/pipeconf/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml \
  -v /docker/pipeconf/prometheus/alert.conf:/etc/prometheus/alert.conf \
  prom/prometheus:v2.37.0 \
  --storage.tsdb.retention.time=2y \
  --config.file=/etc/prometheus/prometheus.yml \
  --storage.tsdb.path=/prometheus \
  --web.console.libraries=/usr/share/prometheus/console_libraries \
  --web.console.templates=/usr/share/prometheus/consoles
```

Será criado o contêiner com as seguintes informações.

| Properties                          | Default Values                             |
|:----------------------------------- |:-------------------------------------------|
| Port HTTP host                      | 9090/TCP                                   |
| Port HTTP contêiner                 | 9090/TCP                                   |
| Volume dir host general config      | /docker/pipeconf/prometheus/prometheus.yml |
| Volume dir contêiner general config | /etc/prometheus/prometheus.yml             |
| Volume dir host alerts config       | /docker/pipeconf/prometheus/alert.conf     |
| Volume dir contêiner alerts config  | /etc/prometheus/alert.conf                 |
| Volume dir host data                | /docker/pipeconf/prometheus/data           |
| Volume dir contêiner data           | /prometheus                                |

Veja o log do contêiner com o seguinte comando.

```bash
docker logs -f prometheus
```

> **Atenção**: Se quiser centralizar o log do contêiner no Loki + Grafana, conforme mostrado no arquivo [Logging.md](Logging.md), inclua as seguintes opções no comando de inicialização do contêiner. Dessa forma ao invés de olhar o log individualmente com o comando ``docker logs -f NOME_CONTEINER``, você poderá ver o log de todos os contêineres de forma centraliza no Grafana.

```
  --log-driver loki \
  --log-opt loki-url=http://172.17.0.1:3100/loki/api/v1/push \
```

O Prometheus estará acessível na URL http://172.17.0.1:9090.

Edite o arquivo ``/docker/pipeconf/prometheus/prometheus.yml`` e informe o seguinte conteúdo.

```yaml
global:
  # Intervalo de raspagem de metricas. O padrao eh a cada 60s
  scrape_interval:  30s
  # Intervalo para avaliacao de regras. O padrao eh a cada 60s
  evaluation_interval: 60s
  # scrape_timeout eh definido como o padrao global (10s).


alerting:
  alertmanagers:
  - static_configs:
    - targets:
    # - alertmanager:9093

rule_files:
  - 'alert.conf'
  # - "first_rules.yml"
  # - "second_rules.yml"

# metrics_path defaults to '/metrics' scheme defaults to 'http'.
scrape_configs:
  - job_name: 'prometheus'
    static_configs:
    - targets: ['172.17.0.1:9090']
  - job_name: 'cadvisor'
    static_configs:
    - targets: [ '172.17.0.1:82']
  - job_name: 'node-exporter'
    static_configs:
    - targets: [ '172.17.0.1:9100']
```

Em seguida, reinicie o contêiner do Prometheus com o seguinte comando.

```bash
docker restart prometheus
```

Referências:

* https://prometheus.io
* https://github.com/prometheus/prometheus/blob/master/Dockerfile
* https://github.com/prometheus/prometheus/issues/5976
* https://crashlaker.github.io/monitoring/2020/05/03/increase_prometheus_storage_retention_time.html
* https://github.com/prometheus/prometheus/issues/6188
* https://medium.com/@valyala/prometheus-storage-technical-terms-for-humans-4ab4de6c3d48
* https://groups.google.com/g/prometheus-users/c/DhbTNUiyUyo
* https://www.robustperception.io/configuring-prometheus-storage-retention
* https://www.prometheus.io/docs/prometheus/latest/storage/
* https://stackoverflow.com/questions/59298811/increasing-prometheus-storage-retention


##  Grafana

O [Grafana](https://grafana.com) é um serviço responsável exibir dashboards contendo as métricas coletadas por um sistema de monitoramento. Neste caso ele exibe nos dashboards as métricas coletadas pelo Prometheus.

Execute o seguintes comandos para instalar o Grafana:

```bash
sudo mkdir -p /docker/pipeconf/grafana/data;

sudo chown -R 472:472 /docker/pipeconf/grafana/data;

sudo chmod -R 775 /docker/pipeconf/grafana;

docker run -d --name=grafana \
  --restart always \
  -p 3000:3000 \
  -e "GF_INSTALL_PLUGINS=grafana-clock-panel,briangann-gauge-panel" \
  -e "GF_SERVER_PROTOCOL=http" \
  -e "GF_SERVER_HTTP_PORT=3000" \
  -e "GF_RENDERING_SERVER_URL=http://172.17.0.1:8081/render" \
  -e "GF_RENDERING_CALLBACK_URL=http://172.17.0.1:3000/" \
  -e "GF_LOG_FILTERS=rendering:debug" \
  -v /docker/pipeconf/grafana/data:/var/lib/grafana \
  grafana/grafana:9.0.6
```

Será criado o contêiner com as seguintes informações.

| Properties           | Default Values                |
|:-------------------- |:------------------------------|
| Port HTTP host       | 3000/TCP                      |
| Port HTTP contêiner  | 3000/TCP                      |
| User                 | admin                         |
| Password             | admin                         |
| Volume dir host      | /docker/pipeconf/grafana/data |
| Volume dir contêiner | /var/lib/grafana              |

Veja o log do contêiner com o seguinte comando.

```bash
docker logs -f grafana
```

> **Atenção**: Se quiser centralizar o log do contêiner no Loki + Grafana, conforme mostrado no arquivo [Logging.md](Logging.md), inclua as seguintes opções no comando de inicialização do contêiner. Dessa forma ao invés de olhar o log individualmente com o comando ``docker logs -f NOME_CONTEINER``, você poderá ver o log de todos os contêineres de forma centraliza no Grafana.

```
  --log-driver loki \
  --log-opt loki-url=http://172.17.0.1:3100/loki/api/v1/push \
```

O Grafana estará acessível na URL http://172.17.0.1:3000. Por padrão, o usuário é **admin** e a senha é **admin**. Logo após o primeiro logon, será solicitado a definição de uma nova senha para o usuário **admin**.

Referência: https://grafana.com

## Configurando a Comunicação entre o Grafana e o Prometheus

Acesse o menu **Configuration > Datasources**. Em seguida, selecione o datasouce **Prometheus**.

Defina as configurações conforme mostrado na imagem a seguir.

| ![1](images/datasource_prometheus.png)datasource_prometheus |
|:---:|

## Importando Dashboards

Acesse o menu **Dashboards > Manager**. Em seguida, clique no botão **Import**.

Copie o conteúdo do arquivo [dashboard_containers.json](dashboard_containers.json) e cole no campo de importação com o conteúdo JSON. Em seguida, clique no botão **Import**.

Pronto! O dashboard mostrado na figura a seguir será exibido.

> **Atenção**: O dashboard foi desenvolvido utilizando o Grafana 7.1.x.

| ![1](images/dashboard_containers.png)dashboard_containers |
|:---:|

# Liberação das Portas no Firewall

Para acessar os serviços da stack de monitoramento do PipeConf é necessário liberar as seguintes portas no firewall.

| Origem          | Destino            | Porta/Protocolo  | Fluxo de Pacotes | Descrição dos Serviços            |
|:----------------|:-------------------|:-----------------|:-------------------------------|:--------------------|
| Rede do Usuário | IP-PipeConf-Server | 82/TCP           | Entrada & Saída  | CAdvisor funcionando com HTTP     |
| Rede do Usuário | IP-PipeConf-Server | 9100/TCP         | Entrada & Saída  | NodeExporter funcionando com HTTP |
| Rede do Usuário | IP-PipeConf-Server | 9090/TCP         | Entrada & Saída  | Prometheus funcionando com HTTP   |
| Rede do Usuário | IP-PipeConf-Server | 3000/TCP         | Entrada & Saída  | Grafana funcionando com HTTP      |

# [OPCIONAL] Exportando métricas coletadas pelo Prometheus no formato CSV

> **Atenção**: O objetivo dessa seção é exportar métricas do Prometheus no formato CSV (*Comma-separated values*) para uso científico/acadêmico para possibilitar a elaboração de gráficos customizados e de maior qualidade para uso em artigos.

O [prom-metric-viewer](https://github.com/metalmatze/prom-metric-viewer) é uma ferramenta de linha de comando simples para visualizar métricas prometheus e exporters com estilo. Ele também inclui uma interface web para agilizar a visualização, classificação e pesquisa de métricas.

Instale o prom-metric-viewer com os seguintes comandos:

```bash
curl -L https://github.com/metalmatze/prom-metric-viewer/releases/download/0.1/prom-metric-viewer-0.1-linux-amd64 -o prom-metric-viewer

chmod +x prom-metric-viewer
```

Agora execute o prom-metric-viewer informando um *endpoint* contendo as métricas a serem visualizadas.

Exemplos de uso:

```bash
# Visualizando as métricas do Node Exporter no console
./prom-metric-viewer http://pipeconf-server:9100/metrics

# Visualizando as métricas do Node Exporter na interface web no endereço http://localhost:8080
./prom-metric-viewer --web http://pipeconf-server:9100/metrics

# Visualizando as métricas do CAdvisor no console
./prom-metric-viewer http://pipeconf-server:82/metrics

# Visualizando as métricas do CAdvisor na interface web no endereço http://localhost:8080
./prom-metric-viewer --web http://pipeconf-server:82/metrics

# Visualizando as métricas do Prometheus no console
./prom-metric-viewer http://pipeconf-server:9090/metrics

# Visualizando as métricas do Prometheus no na interface web no endereço http://localhost:8080
./prom-metric-viewer --web http://pipeconf-server:9090/metrics
```

O [styx ](https://github.com/go-pluto/styx) é um binário escrito em [Go](https://golang.org) utilizado para fazer consultas na API (*Application Programming Interface*) do Prometheus para obter os dados coletados no formato CSV de acordo com a métrica ou métricas selecionadas.

Instale o Go conforme mostrado neste [tutorial](https://github.com/aeciopires/go_mysamples/blob/master/REQUIREMENTS.md#go).

Instale o styx com o seguinte comando:

```bash
go get github.com/go-pluto/styx
```

Agora execute o styx para obter os dados no formato CSV a partir das métricas coletadas pelo Prometheus.

```bash
styx --prometheus http://pipeconf-server:9090 'container_memory_max_usage_bytes'

styx --prometheus http://pipeconf-server:9090 'container_memory_max_usage_bytes{name="ftp"}}'

styx --prometheus http://pipeconf-server:9090 'container_memory_max_usage_bytes{name="jenkins"}'
```

O [gnuplot](http://www.gnuplot.info) é um utilitário portátil de gráfico baseado em linha de comando para Linux, OS/2, MS Windows, OSX, VMS e muitas outras plataformas. O código-fonte é protegido por direitos autorais, mas é distribuído gratuitamente (ou seja, você não precisa pagar por ele). Ele foi originalmente criado para permitir que cientistas e alunos visualizassem funções matemáticas e dados interativamente, mas cresceu para suportar muitos usos não interativos, como scripts da web. Ele também é usado como um mecanismo de plotagem por aplicativos de terceiros como o Octave. O Gnuplot é suportado e está em desenvolvimento ativo desde 1986.

Instale o gnuplot no Ubuntu com o seguinte comando:

```bash
sudo apt install gnuplot
```

Use o styx para consultar uma métrica no Prometheus e exportar os dados para um arquivo no padrão no gnuplot.

```bash
styx gnuplot --prometheus http://pipeconf-server:9090 'go_goroutines' > goroutines.gnuplot
```

Agora use o seguinte comando para gerar o gráfico utilizando o gnuplot:

```bash
gnuplot -p < goroutines.gnuplot
```

Será aberto o visualizador de gráficos do gnuplot e você poderá exportar os dados para um arquivo PDF, SVG ou PNG.

Referências:

* https://gist.githubusercontent.com/aneeshkp/94d1bc1b082cf0d47c04837308057ebe/raw/b271a83dec85e2c7644979fc6bfae302b752663f/prom_metrix_to_csv.py
* https://medium.com/@aneeshputtur/export-data-from-prometheus-to-csv-b19689d780aa
* https://hub.docker.com/r/stohrendorf/csv-prometheus-exporter/
* https://github.com/stohrendorf/csv-prometheus-exporter
* https://www.robustperception.io/prometheus-query-results-as-csv
* http://demo.robustperception.io:9090/consoles/index.html
* https://speakerdeck.com/metalmatze/prometheus-styx
* https://promcon.io/2017-munich/slides/lightning-talks-day1-06.pdf
* https://github.com/metalmatze/prom-metric-viewer
