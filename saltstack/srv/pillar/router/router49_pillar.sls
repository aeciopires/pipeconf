{
	"data": "ENC[AES256_GCM,data:5/QKju5HMm8IMMSXIQ7jAgnhEeQVvThZgtSCWhzcS0MjM2TnWp8HfETp0jtXvnAUDtbI59HphsS0sAaGT3VzAl7urPPU8iv3WJ7iz2Cx3WvjW7etPmiIRpOMAhT4GF1uLgeeFhanNjJOmVni8PwPRxyy5lOr74r97rUqYROCSDx8EGHoj+adUcC4/jRj2szwCg0yxSUGTaoY4gsn/f1x75JrhZNZFyrK1EKrSvt8+ZNc9ULzEpOhikpRLRwHlv/W0/3dqezSzKqkTN/vgAqybtTnGOtioqH1WRvPoty82OmfFkjEOVuJorsYund62k2MdEwGIE2FtU0pmBrZj7gejALsZp4N12LqXyXZHerWmBxEVr7it/6uoSPtaw/jvPeFDHFAoWg9UY+xGhHR3QLqMNia6+Z49V5MPyqtLn16pH0hn5q6CPG7S0m4y3dpvHs9GW3j5FUZWsnATF7UDrfla7yjLQ4THmn9sEGFt1mxOAETsdYEAaPzmCCJryWpFNvMgEjC0zc=,iv:xvQWcC17rklfKGc4BLQsIuve6K7gLtLFD9OvGLM95co=,tag:hOikiDNI/vMQISK5hWfDqA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:27Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHLwhJCkWe+42UgD5bF7OtAAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMN3V9qHlDvZm91mX4AgEQgDullCF8udzc+PG+JydK5dLV+7dCuEr/ZSm0224xnOLAzIQkpDRyqYb0aPXn3IaaVS68mUprrjkXJz53Xw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:28Z",
		"mac": "ENC[AES256_GCM,data:Z03e577O2IpqggsFJQ+qv/d2tqB4ntcWNXDxdGQ6uZGIoRygigRRI97zfb2DqWrrhpntvlMAYo88g/0yl1/2k4nY/nRZrIXgjQuHjQJPUmfT3SvT5JgJauVUOLZ+JT2EPrJbfeQwPKSCKZ36bNRiU+mAxye2tupqUFtlvFnr41I=,iv:THnzAV4AM9dCd9dG4UGxwxerMgRTlJDAuy4qVdghYeA=,tag:Qn7RE/b24QhhDXgDKCShbA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}