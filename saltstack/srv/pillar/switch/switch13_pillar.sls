{
	"data": "ENC[AES256_GCM,data:vbcYEIPru8yu8H8Ajgh5CxSiDkU+J0tvDvHfTPSh0wIJLDVBrjkaFmVzx35TL0AOTG+tcuGgnDQwbosxQk/o5nnrLeNHyyJkUX+LxVZ5gvy2LpA5n/ApbyUgV/rhHBBXmtCrj2ttqzcME7KwKGmqXX1MIF2QiDOhhiD3nNsiTTPj7Mih6I4v2oI0XCvjELQyMDxc38GLJCLKlJCI2hvdp1JvjVCqmGrXlYTWUGIcbFpgYdILq6wmtP4XYpm/MT8SgVrLuoeOGnzXwNXq+DOnGSm7atFvJVEngwdbls5b3t2yjSCBUwTWybbputDKM1WYOiPYKLMIICLAA+KuoXYB/rASsr2c3acS1Jsz+CRTmvrs2fo0Mx5JIpKUeJCoi6yWK/Kbe6TgXw2Kgmhi7VtMWYYr+duSRVKdtscvUGzbcpFLZI/5xzoYLCwsl81v3H2rAalMh3fiOkFVMwvfuYy1+OFB23PA5WszkcOaW7tu/tO/4Tn1uaRtGg2vakFaPsAifYLXByXyLIQ8LdaGQdrQVfghj1nJD7J1S5n5cWtlCyqhUDtWKg==,iv:n4CXFIVBZOjAqcPLaB/rKxVvoxpIruXDk32+YUIE1NU=,tag:9yAxulj8umNtQ5XADf5P7g==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:17:16Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwEJS9OdKFluP5L4yffByctvAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMm+bhzgDTpxazPpz/AgEQgDsAKOwcHHfrP6smdT3+Gm66jgHqVfJgmleK68+3ozUBkf9u3Xyy1qbPZXTkSAeX3g3MkfagBdzXgRJu6g==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:17:17Z",
		"mac": "ENC[AES256_GCM,data:cCGfo079fHehMkidFoCWQ7f7qlGCL2nd/0ma+gsNDD48A3UP49rgQeEiXCzlk/ZKyHU5QvACUReWjU/7aEKhrfb2fMjNYv1O4Y0P10rP4Dxj7KwyL0hJYH8aUkivi+E2vxlplWpZDHPcfU0fKQL1xyhMxUALnVoeFob74bze7IQ=,iv:x41QmVYtMTgzG6vV1nv9aRT8vqIv2TJTqB2gtinZtKQ=,tag:ThWLJI9X30w0AqhTtvsAfA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}