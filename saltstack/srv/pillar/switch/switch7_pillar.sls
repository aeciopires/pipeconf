{
	"data": "ENC[AES256_GCM,data:m1HUdebFCWaJ3y4vyRLLuhN1whtYQhZ+5Wi8o3ee0/Y4MGGbivoIy9fAzEhrlwAiCEmzSAAf3RFZ4dAtPeroUULR5X+5iDla/StGEhzSDYQ1/HhyZwRPLA+JZybZIdRnJ1c3QLR0XPrXnONsZ3vOzs5zy3ulP2J1w7rFcNOWdL5yh2jZvQXD0b1ziDvTIqdCFQ41Y+yRREh33EjKlvVspApVl5wU2aYhIiSfRd/vUukR9Nrd+4xmovh2K96UKqZ1jL+iVvrbqcTh8z7t2uECT5RRykrsRKFmO2EqR7shAvBr5syBaPz19Q96iRMZ8KFX/eUDsgCQehHuumeWniYLE/3PlY1Wnu/bMZs87sPD1LcFjRW+za28wDqLaO01gvC0AhPw/l5S7+YWOlDp6XVno9eG9XgHLQvI8YmFdbD3+Sv84k++LdjFB7Dsdr4xQpisu/xmwYG7BGviTg7+ZdLyDCT8S7CgEu+g4HPIYzyAmMmRZxGbyZWcBmm1kCzwNudnqF+N0F1cHIPiI6m1HjgjkhFji/eqPxOnRNR48c6uofZv9IFkIw==,iv:5E/aUN+7JrudWhKExx0pJJtdVOZCXJaTdg8xhM8Vkwc=,tag:v6FuBRuoqah9N/A/S2d47A==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:16:49Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHUgpKTXqXU08lHbw1UogcnAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMwDQxiq53pNDYgm6mAgEQgDuMzyc44y12Aqx38DItgo0+KraCD1DgvHu/xtAQDQXv0Ya40PGZlTfrqi8CWCnYU8qvKspEPJFN2KG6dQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:16:49Z",
		"mac": "ENC[AES256_GCM,data:ZTQtiMIm4KFcZmuQWOImTJGBLLbybGG8oazehGqEs8YZw5Kp3E8YXzzjgSmtUMPBpTKZ7RXuzP8lu8Jyt9vQQKDqWm8kQuYkA4b2OUS3eXSpnSGjGhgWDMONbDpIS/Uh13dt7nZrdFTQKLfmnhuYVYkhE6RK1X7RNKRLa6IGbwo=,iv:mgBtIWCnF0hRe40P8b6x+QqUoBZGvFXJxAA6/lUbXew=,tag:BXAfmBaaa+SXGMsJGuL5jw==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}