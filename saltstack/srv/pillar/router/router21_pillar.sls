{
	"data": "ENC[AES256_GCM,data:CISxmwY/LOOaiRigwTT6Go6norAaiL+C18kTa3BAufKbDmBiH5tmvmx4pieViPHny4M3X3E1gH3JdHZPP6J/kS8I2uo6Y2eUqQvL5dZDfzNwfXH+PNiQq0ou2Fkb2CNSXeeLC8OpF7XEMHJ071gRI9PS3evUAUGktWLnqLf3uwuzgSLWHhGSDgIbxDLxw/+PRAYK06geA6Hc4/LVnol+HxGniPgMtzjJ5eMHk2oVrNQUYuUFwEYOKdFlq0QPEzYzWyhXaGpdbe4WmHWTDBQPyR4014SrceWMKyaS5AZhpgqGfqE+DzgeljhjL0SZAqp8xzNFQHELRuGD1xxilFQzw1JuHdAAz+CiW0R8soVLANgZa3DwZOAF67jdwBlvjjiBfohpxKFN4LXszK8VjqhkKDXtZ3EnIZO2YTs4DXrW0Igi7IK9Vb8t182HAMQVJc35zPluJHBz5ySo31K2KdH8pjJjNvcux++fWk22,iv:9IbpAmD6NtMf6YFw9PJl5PgzXRR18K58oA2iwwnyj7o=,tag:oyH/ytbfRumLXJbWyP0APA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:32:58Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwGea9S7UtExKD0Yi0BSysksAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMV7TuOU+LpS2hYDi9AgEQgDvzwoOVCvLoI9rCNgXZdVhnwe7cI4Z4E0+LL/7E0YiL/s9xL+yagb+ER9zYkJcYtsBtL6iKh/7cXxWUZw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:32:59Z",
		"mac": "ENC[AES256_GCM,data:JRZSCqomKwuaKYTyJkytViTr++0E4RARMs2CvkGqGVM6LFgDIiI1m2wW7vYJDmOS0c5SwoQcVkAfbq2Kjyv5VGTvOV9yN7VdHPaC6zQ5ig7yQkgG1kmAyBCpUq2tCXDgTLfbRpjw5awF1qazaeiObQW+n+o1ab22gKvhHOAjDlI=,iv:fO4ZM+mAyA/y+Av4TahAs4hbqu4H7JDCr58TRHHdztU=,tag:LA6UYlMnyELXN8emI8KoxA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}