{
	"data": "ENC[AES256_GCM,data:SRIlsxWU8XHdD5nZTHK77zT6vNxzNK6mam6R67Oa7J7Tbl2yKLrEmNqBGncPsqRDOtw1vPGYPv/bUMmneEmzMx+EN+k9Bx76Goh6A4qYOIy6MlTBUHmgUnioNL49JZbZqOY23EyAiJZmENDPUIR4/e3UgBFIL/T4HCBOxCMDI+cgruBCsSSKTuwGSWZXk09q0G668AXsapbJJUPe2sZOR0DiggxPDP3hStN8M28S2mFZW4GlmMmc4xPcrP77Mt+XHFu0TZU8bFKyxNngFAi/U5ASQi6I+3sB5lMAn3KMfTMXmL62ttpr33s6lLhy2gu+Em3mcGyNY3c82b25jAx4Z12ILtANOVuwTMQIzZSdESRVtDgz5HC2QzzcBwtzXQIC08z/RCfn440uyj8c2Pfzr+6E5XdfmcddoAiqXwYCckV30PtQexDgHMF5ZOuguHuk8CVynYFS61Ck+H8gleoWChsbOm+x59KY/9Co,iv:GJsOjguVS1Q9qu/1tUInEtNcnrqxGbXHM6n5IjzOXNU=,tag:tRHgVBhKfDCnPpgdJ7oeMg==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:32:28Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwE4tW9i+RlNJWAutpCC20GGAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMiuYke16lBGRJ1HhCAgEQgDvTshkhiu3wELmImHIxynOG5pF+Y0/KJx1gptbOXJUIhqLqVUkREuJ0sDT1jPESIayhWBnk7GxVY82Enw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:32:29Z",
		"mac": "ENC[AES256_GCM,data:YawsEmjT49AqAKq6asDFk/iaLcLwdLVr6cjRkNN28Wt+meWWte35bljsEA2lf/MwBlew63rG+mpS2irulzGNXVsU/kYHWVZwGzjv1uLlspc1ozvbDZi1ZNEz9RfBz4eyOsb80T9Da9hSJw+0wJNLGol1GxpClnzkG5mi5dvsiSo=,iv:LYw20V6A3HJU7VkIAyC6fBe8H7oD8gEFY/m1l2DjvBw=,tag:d3xGuSjKWYXpOcdbNFvDLg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}