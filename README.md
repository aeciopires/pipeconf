<!-- TOC -->

- [PipeConf](#pipeconf)
- [Português](#português)
  - [Instalação do PipeConf](#instalação-do-pipeconf)
  - [PipeConf na Mídia](#pipeconf-na-mídia)
- [English](#english)
  - [Install PipeConf](#install-pipeconf)
- [Developer](#developer)
- [Teacher Advisor](#teacher-advisor)
- [License](#license)

<!-- TOC -->

# PipeConf

[![Codeac](https://static.codeac.io/badges/3-10890132.svg "Codeac")](https://app.codeac.io/gitlab/aeciopires/pipeconf)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/8a666dbdc9cc4589b696af019d86aa02)](https://www.codacy.com/gl/aeciopires/pipeconf/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=aeciopires/pipeconf&amp;utm_campaign=Badge_Grade)

# Português

Este é o repositório de código do **PipeConf**, uma arquitetura integrada para configuração automatizada de ativos de rede heterogêneos.

O PipeConf é fruto de uma pesquisa de [Aécio Pires](https://www.linkedin.com/in/aeciopires) sob orientação dos professores [Dr. Paulo Ditarso Maciel Junior](http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4764113A9) e  [Dr. Diego Ernesto Rosa Pessoa](https://www.linkedin.com/in/diegorosapessoa), iniciada em 2019 no [Programa de Pós-Graduação em Tecnologia da Informação - PPGTI](https://www.ifpb.edu.br/ppgti) do [Instituto Federal da Paraíba - IFPB](https://www.ifpb.edu.br).

Esta pesquisa teve a participação de [Tiago José Bandeira Lourenço](https://www.linkedin.com/in/tiago-bandeira84), sob orientação do professor [Dr. Paulo Ditarso Maciel Junior](http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4764113A9).

## Instalação do PipeConf

Instale o PipeConf seguindo esse [tutorial](docs/README.md).

Este repositório contém a seguinte estrutura.

```bash
pipeconf
├── docs       # Documentação do PipeConf
├── jenkins    # Pipelines do Jenkins
├── LICENSE.md # Licença de uso
├── README.md  # Esta página
├── saltstack  # Código SaltStack
└── scripts    # Scripts de instalação
```

## PipeConf na Mídia

* [O egresso do PPGTI Aécio Pires e os professores do PPGTI Paulo Ditarso Maciel Jr. e Diego Pessoa publicaram um artigo no periódico IEEE Transactions on Network and Service Management.](https://www.linkedin.com/posts/programa-de-p%C3%B3s-gradua%C3%A7%C3%A3o-em-tecnologia-da-informa%C3%A7%C3%A3o_pipeconf-an-integrated-architecture-for-activity-6960580713033142272-6xDS/)
* [4º Simpif premia os melhores trabalhos científicos do IFPB](https://www.ifpb.edu.br/noticias/2021/11/4o-simpif-premia-os-melhores-trabalhos-cientificos-do-ifpb)
* [Egresso do PPGTI do IFPB conquista prêmio no Simpósio Brasileiro em Redes de Computadores](https://www.ifpb.edu.br/noticias/2021/08/egresso-do-ppgti-do-ifpb-conquista-premio-no-simposio-brasileiro-em-redes-de-computadores).
* [Trabalhos de alunos e professores do DCC são premiados em Simpósio Brasileiro de Redes de Computadores e Sistemas Distribuídos](https://dcc.ufmg.br/trabalhos-de-alunos-e-professores-do-dcc-sao-premiados-em-simposio-brasileiro-de-redes-de-computadores-e-sistemas-distribuidos/)
* [Professor do CI e equipe recebem prêmio por melhor artigo em simpósio de redes](https://www.ufpb.br/ci/noticias/professor-do-ci-e-equipe-recebem-premio-por-melhor-artigo-em-simposio-de-redes)
* [XXVI WGRS - PipeConf: Uma Arquitetura Integrada para Configuração Automatizada de Ativos de Rede Heterogêneos](https://www.sbrc2021.facom.ufu.br/pt-br/programacao/workshops/xxvi-workshop-de-gerencia-e-operacao-de-redes-e-servicos-wgrs)
* [Primeira defesa PPGTI acontece nessa sexta-feira (26/02)](https://www.ifpb.edu.br/joaopessoa/noticias/2021/02/primeira-defesa-ppgti-acontece-nessa-sexta-feira-26-02)

Slides em pt-BR:

* [SIMPIF - Nov/2021 - Solução Integrada para Configuração Automatizada de Ativos de Rede](https://speakerdeck.com/aeciopires/solucao-integrada-para-configuracao-automatizada-de-ativos-de-rede)
* [WGRS - Ago/2021 - PipeConf: Uma Arquitetura Integrada para Configuração Automatizada de Ativos de Rede Heterogêneos](https://speakerdeck.com/aeciopires/pipeconf-uma-arquitetura-integrada-para-configuracao-automatizada-de-ativos-de-rede-heterogeneos)
* [IFPB - Fev/2021 - Solução Integrada para Configuração de Ativos de Rede](https://speakerdeck.com/aeciopires/solucao-integrada-para-configuracao-de-ativos-de-rede)
* [CoffeOps - Fev/2021 - Pipeconf no CoffeOps Campinas](https://www.slideshare.net/aeciopires/pipeconf-no-coffeops-campinas)
* [SECT - Nov/2020 - Configuração de ativos de rede utilizando a abordagem infrastructure as code](https://www.slideshare.net/aeciopires/configurao-de-ativos-de-rede-utilizando-a-abordagem-infrastructure-as-code) 

Dissertações em pt-BR:

* [IFPB - Fev/2021 - Solução integrada para configuração automatizada de ativos de rede - Aécio Pires](https://repositorio.ifpb.edu.br/handle/177683/1264)

Artigos em pt-BR:

* [WGRS - Ago/2021 - PipeConf: Uma Arquitetura Integrada para Configuração Automatizada de Ativos de Rede Heterogêneos](https://sol.sbc.org.br/index.php/wgrs/article/view/17189)

Papers in en-US:

* [PipeConf: An Integrated Architecture for the Automated Configuration of Network Assets](https://ieeexplore.ieee.org/document/9845680)

# English

This is the code repository for **PipeConf**, an integrated architecture for automated configuration of heterogeneous network assets.

PipeConf is the result of a research by [Aécio Pires](https://www.linkedin.com/in/aeciopires) under the guidance of teachers [PhD Paulo Ditarso Maciel Junior](http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4764113A9) and [PhD Diego Ernesto Rosa Pessoa](https://www.linkedin.com/in/diegorosapessoa), started in 2019 at the [Postgraduate Program in Information Technology - PPGTI](https://www.ifpb.edu.br/ppgti) of the [Federal Institute of Paraíba - IFPB](https://www.ifpb.edu.br).

This research had the participation of [Tiago José Bandeira Lourenço](https://www.linkedin.com/in/tiago-bandeira84), under the guidance of the professor [PhD Paulo Ditarso Maciel Junior](http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4764113A9).

## Install PipeConf

Install PipeConf following this [tutorial](docs/README_EN-US.md).

This repository contains the following structure.

```bash
pipeconf
├── docs       # PipeConf documentation
├── jenkins    # Scripts and Jenkins Pipelines
├── LICENSE.md # Licence
├── README.md  # This documentation
├── saltstack  # SaltStack code
└── scripts    # Scripts of installation
```

# Developer

M.Sc. Aécio dos Santos Pires<br>
Site: http://aeciopires.com<br>
Linkedin: https://www.linkedin.com/in/aeciopires

M.Sc. Tiago José Bandeira Lourenço<br>
Linkedin: https://www.linkedin.com/in/tiago-bandeira84/

# Teacher Advisor

PhD Paulo Ditarso Maciel Junior - IFPB<br>
Linkedin: http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4764113A9

PhD Diego Ernesto Rosa Pessoa - IFPB<br>
Site: http://diegopessoa.com<br>
Linkedin: https://www.linkedin.com/in/diegorosapessoa

PhD Fernando Menezes Matos - UFPB<br>
Site: http://lattes.cnpq.br/8483422808548389<br>
Linkedin: https://www.linkedin.com/in/fernando-matos-4a44385

PhD Aldri Luis dos Santos - UFMG<br>
Site: http://lattes.cnpq.br/0957641516282703<br>
Linkedin: https://www.linkedin.com/in/aldri-santos-109a78116

# License

Copyright 2022
