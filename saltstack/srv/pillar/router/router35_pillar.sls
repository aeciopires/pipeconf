{
	"data": "ENC[AES256_GCM,data:sOQ4En/RkdIhvTMDsei5VOsfq9SISXqkEEMdFcshZGYFxp1HmdGBqRzVwjPv/w14JXP4ZLSKoDSYojKyOQE35zqEWregJsMR7iOFV04YlDkPgBexc6Rfr8WCMkchPKCgua3FZfcC59QIf8VZpHWpOzXF8Cjs3bE21R+/SLFos5d3ZGdnRn4UrAYODJ2Hu8SIi3YEKnay3SlQJQtGdPaQcr4XR48noUo6OEVcM8KG4X+q/owvBHVWrbjxyo+J9aToR6KgCVLWd61bsbyg3yjREbADtzCxuiglZfOKZDj003P/aQ8GnsBP72vfSEZ5dLq+MwYYOo6pCnLPXDzkz0ka0f/UV/hGmdRycZuoJkJuC4MQWgZ5YjU78ULUtaDF43FkSeogn+DFIaH5HzevpKLJkllrRH22Ws8AXyAzd7bV+V0fxNljZ7zo7AVZoHWAMj/g0BUP/aQ/ztJ/qqZz5F/MaZ1emyls6//nvr+1Ubexz3XQNQ/DggojBvA9wtGoJsLOZLsAIo0=,iv:hgWTFZ8awF1t3ilNppS8vKX8lZnnyTLmv0s8Ux2rLkM=,tag:EL5QlxS+ebDoafo47cW8vg==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:17Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFVjd/HPqeJKWbRm3QUR0P3AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMVxNH+l3SeYhdkBueAgEQgDtvRMO6N9RZhkz/GlR0jzknBaDllKYY74JQBNGrKf7crF5+G7pexdstWX/sUiCTTXuayqwOrKYC+MhY9A==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:17Z",
		"mac": "ENC[AES256_GCM,data:JxKUiV4YG8/dpN8Az5ZOtokpnnOKobCXqlN1ytzvP+vlAY4IdOkvrG2/eSfF1SKt7tkGeZzHtTPoN58nY/+gUowfh/S7bHGjz04fvEFlcjtZf5SyuYyK4z18wYgUEoob7nX7mjLYOuHm+Gk3z5Me7tkgtuIR5ht2MgUFdGPCZfA=,iv:AmSR8hu056lL2T1N9YpRnd5gM9NatPKGnuv92qtr1nQ=,tag:6MhU1oZQGpOlvolEOZUwyg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}