{
	"data": "ENC[AES256_GCM,data:pLxe8MZCwYUVK2BqSdwRs2QPsi3p++JMcLnNUZV4OncpBNJWZbh8FfD1a9Dg7C3Y3569vO6YKpmu0VuqwOZTdhW73lDg5h8H+TqTP9hlPmEY0GBM30VNkchaElqS28kWD9RSuqwwVCckqUyqsLhNdUTlRjNs+fbW0VLRRTtaKuqoZQy5LJ+LNDTiXvaaPCyPPC7dAxzhyVM915hpYN0KJ4eM3femg8uk3kcxilaOTybaQEuzAi1JhMd2EJICk3srdHe2YCIcQw0vSzgtu4nQHGufKIJg5eWKb7TSBXZwLXpkQBa82ez6tS2EEuTwZSli/CAVDHhw9St2+WjtmMqKOthCUTK18elfzOWBChoMLp/90rFd/e0wxFPbqK0BDjK4rXkfuvJVPVKfHJ7Uy7ZO/8ztiDod4YL8+uIw4GduMIV9h0XsPr5MqkiE5i7PeKCdQ6aXJi1wWnJoWwtvGvMGy0Bct2BGm7ZxZIem/vlReB6jHRZRopmeE9BkJMvBL5NWgS6kOys=,iv:LQvcnviB4GdOHCfG81kqSgRLvwap78xNFX8lgxgXaUg=,tag:XBe+hOuLc0/hTSOrW0A8qg==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:33Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFUecxqAVZNQKM6ZRUuq1v8AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM6bRXB0xge/zLaYmSAgEQgDt32R3tRYTMncj2SUhtibRz2kG+YVCVN5s/VO0u8tuoYtzzFc1mQLUCuGbaZtez85rtAXBV+xxuNwa7ug==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:34Z",
		"mac": "ENC[AES256_GCM,data:YzIPUYZWJJJIyuwWWWazLG5KSJc4FKj9hwnaadfFNcY1K1OB3UB8yQtSkBzcAQZCon3HuzQzmV3/RuYJakJEazh2R5xC98nfPWRdVOYReuTnkuMwc4b6sAugQh48mVAsMdNHrqQr470uZyqwa+oMOodfKLVDRR3Nu7O0i+Ek0cE=,iv:SpPkE0sg14FSvZiClm0daRx9hpp0F0cfe2O98A1E+j0=,tag:mF67r4t7rJ0YBBn+LLYRWw==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}