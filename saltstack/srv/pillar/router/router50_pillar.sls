{
	"data": "ENC[AES256_GCM,data:DHwqzY0BkvOu/iOj/GXkIyS+rmLCd8nZTZipSWFIkWIWESi80Pmhblywt8YS96JQb42Q3/BJ10fO0vTt7M2N9XFG+vY4qW76ztYUVc78h3q+ph9UqFPGFN/uoWBnYyT+L8EtRDW2GGdX/Arnttef37woW9Y9xM/vShItO9rm19TsNRUFy/Y3PJcoYW+Ku5en9HhLcnFGRSB9EFoTqCoVhiY4YFrV8gSl1e3wPH4lQ31earGCWmrmMu5mO45tMT9JhTbD1FUzT0X8oQOPyJtTkfJzaf2nORWeId+EJb4VnqjG9kQOUSHcjMfH+3B3yHwzGh91/F6jsdceZvyDCTM2Leg0KwleykN5wBIEN7Y37LQayGKKfmZLcJ96KPV7YSLvssE+s8F+sP/f5uFatSAc3uIJYFcseA3bJCcPWa2Q6txiSLlHv5f0raGt9a71hycaCsEaF9/hoIXTDiShZh9q6JWtusDmUDdP22SSgxW4ZMj/YomhcyPpY4VuqD7g4KWBuFPiBA==,iv:+V2IKvhtX+pA7q8Z5qxmoe1MDVzuZE8+f6ccivaRVGw=,tag:QeSBlWMXnu7MjZnXP1FzeQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:28Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwH6nzOTy5HCwjFAaygPfnIYAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM9gbIBXE9KlDLvXA3AgEQgDvieBKs7f8aNweNzkpWZXuWUq1c8S6mRwY6desgmZ1l8ABu9n6T55np1RdDf0/7x/t4wgoEP6PKt4bQMQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:29Z",
		"mac": "ENC[AES256_GCM,data:a0ok5gkuOw7haVxqz6+LQVCzAfsBQyoalCMtvMH9QLy+CA/2d2zmd9/U92BMvs6kafGuzj/u4szkItMlRGzh+VaTLOk4cYY+Y/kCm0fj2E+ZdrTO+3PYdsX234AQV/OszCf0A+1PnXG9REU9L4KzH+FQCWfs4nF12bMwjIXTxgk=,iv:vDerVby981iwrgxal6Vusu+Ayf3u04fML8bcPxQ6U/A=,tag:DB7ZJ7J6yR+IwyPr1zLKAg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}