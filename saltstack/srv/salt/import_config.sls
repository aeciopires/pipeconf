# Importando a nova configuração 'running' do equipamento
set_new_configs:
  netconfig.managed:
    - template_name: salt://templates/device.jinja
    - template_engine: jinja
    - skip_verify: False
#    - test: False
#    - commit: True
#    - replace: True
    - test: True
    - commit: False
    - replace: False
    - debug: True
# Obtendo novas configuracoes do equipamento definido no arquivo de pillar
# e atribuindo a seguintes variáveis a serem usadas no template device.jinja
    - hostname: {{ pillar.get('hostname') }}
    #- servers: {{ salt.pillar.get('ntp.servers') | json }}
    - servers: {{ pillar.get('ntp.servers', []) | json }}
