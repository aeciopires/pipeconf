<!-- TOC -->

- [Workflow do cenário 2 de avaliação do PipeConf](#workflow-do-cenário-2-de-avaliação-do-pipeconf)
- [Atividades realizadas pelo PipeConf](#atividades-realizadas-pelo-pipeconf)
- [Atividades realizadas pelo Unimus](#atividades-realizadas-pelo-unimus)
  - [Configurações do ativo de rede](#configurações-do-ativo-de-rede)
  - [Instalando o Unimus via Docker](#instalando-o-unimus-via-docker)
    - [Requisitos](#requisitos)
    - [Preço](#preço)

<!-- TOC -->

# Workflow do cenário 2 de avaliação do PipeConf

**Objetivo**: Comparar o tempo que o [Unimus](https://unimus.net) e o PipeConf utilizam para gerenciar a configuração de **n** ativos de rede, sendo **n=[1, 2, 4, 8, 16, 32]**.

Edite os arquivos de pillar dos ativos de rede e adicione a seguinte configuração.

```yaml
ntp.servers:
 - a.ntp.br
```

Faça commit das mudanças.

Localização dos arquivos de pillar para roteadores: http://pipeconf-server.domain.com.br:81/root/pipeconf/src/master/saltstack/srv/pillar/router

Fonte:

* S. House and M. Ulinic, Network Automation at Scale, O’Reilly Media,Sebastopol, vol. 1, 2017, pag 48.

# Atividades realizadas pelo PipeConf

**Passo 1)** Acessar o repositório git que versiona os arquivos de backup dos ativos de rede;

**Passo 2)** Decriptografar os arquivos com as credenciais de cada ativo de rede;

**Passo 3)** Executar o contêiner do Salt SProxy para detectar quais ativos estão conectados;

**Passo 4)** Acessar o ativo de rede via SSH para obter algumas informações a cerca do modelo e fabricante;

**Passo 5)** Obter o backup da configuração a partir da memória (running-config), antes de aplicar qualquer alteração;

**Passo 6)** Obter o backup da configuração a partir da inicialização do ativo (startup-config), antes de aplicar qualquer alteração;

**Passo 7)** Configurar o servidor NTP (Network Time Protocol), endereço a.ntp.br para sincronizar a hora;

**Passo 8)** Obter novamente o backup da configuração a partir da memória (running-config), após aplicar as alterações;

**Passo 9)** Obter o backup da configuração a partir da inicialização do ativo (startup-config), após aplicar as alterações;

**Passo 10)** Criptografar os arquivos de backup da configuração dos ativos de rede;

**Passo 11)** Enviar os arquivos de backup para serem versionados no Git;

**Passo 12)** Enviar notificações aos usuários por email e Slack a cerca do final da execução do PipeConf.

# Atividades realizadas pelo Unimus

**Passo 1)** Obter o backup da configuração a partir da memória (running-config), antes de aplicar qualquer alteração;

O passo 1 foi executado ao interagir com a interface web do Unimus:

**Passo 2)** Configurar o servidor NTP (endereço a.ntp.br), para sincronizar a hora.

Comandos cadastrados no Unimus para serem executados nos ativos de rede e realizarem o passo 2:

```bash
show clock
conf t
ntp server a.ntp.br
exit
show ntp status
write memory
show clock
```

## Configurações do ativo de rede

Roteador Cisco modelo c3640

| Parâmetros                  | Valores       |
|:----------------------------|:--------------|
| Usuário SSH                 | aecio         |
| Senha SSH                   | teste         |
| Senha do modo administrador | teste         |
| Porta SSH                   | 22/TCP        |
| Endereço IP                 | 192.168.122.X |

Os endereços IP dos ativos de rede variam de 192.168.122.2 a 192.168.122.254, conforme a atribuição automática de endereços pelo serviço DHCP (*Dynamic Host Configuration Protocol*).

## Instalando o Unimus via Docker

Crie os diretórios de armazenamento dos dados e configuração do Unimus:

```bash
sudo mkdir -p /docker/unimus/db
sudo mkdir -p /docker/unimus/config
```

Instale o Unimus com os seguintes comandos:

Opção 1:

```bash
docker run -tid --name=unimus-db -v /docker/unimus/db:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=supersecret -e MYSQL_DATABASE=unimus -e MYSQL_USER=unimus -e MYSQL_PASSWORD=secret mariadb

docker run -tid --name=unimus -p 8085:8085 -v /docker/unimus/config:/etc/unimus/ --link=unimus-db:db croc/unimus
```

Opção 2:

```bash
docker run -p 3306:3306 -d --name=unimus-db -v /docker/unimus/db:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=supersecret -e MYSQL_DATABASE=unimus -e MYSQL_USER=unimus -e MYSQL_PASSWORD=secret mariadb

docker run -d --name=unimus -p 8085:8085 -v /docker/unimus/config:/etc/unimus/ --network=host croc/unimus
```

Será necessário alterar a seguinte linha no arquivo ``/docker/unimus/config/unimus.properties``

Antes:

```
database.host = db
```

Depois:

```
database.host = 172.17.0.1
```

Em seguida, reinicie o contêiner do Unimus com o seguinte comando:

```bash
docker restart unimus
```

Terminada a instalação do Unimus, observe o log dos contêineres com os seguintes comandos:

```bash
docker logs -f unimus
docker logs -f unimus-db
```

Acesse o terminal do contêiner do Unimus com o seguinte comando:

```bash
docker exec -it unimus /bin/bash
```

No prompt de comandos do contêiner do Unimus, execute os seguintes comandos para permitir a conexão via SSH com os ativos:

```bash
apt-get update

apt-get install vim

mkdir ~/.ssh
```

Crie/Edite o arquivo ``~/.ssh/config`` e informe o seguinte conteúdo para permitir a conexão via SSH com os ativos de rede utilizando o algoritmo **diffie-hellman** (inseguro, mas padrão dos ativos):

```text
Host 192.168.122.*
    Ciphers aes128-cbc
    KexAlgorithms +diffie-hellman-group1-sha1
    StrictHostKeyChecking no

Host device*
    Ciphers aes128-cbc
    KexAlgorithms +diffie-hellman-group1-sha1
    StrictHostKeyChecking no

Host router*
    Ciphers aes128-cbc
    KexAlgorithms +diffie-hellman-group1-sha1
    StrictHostKeyChecking no
```

Acesse o Unimus no navegador com a seguinte URL: http://IP-SERVIDOR:8085. Siga as instruções do *wizard* ou deste [vídeo](https://www.youtube.com/watch?v=aDK_QTv71Gw) para definir o usuário e senha de administrador e utilizar o software.

Será necessário criar uma conta no site https://portal.unimus.net para obter uma *license key*.

A documentação oficial está disponível em: https://wiki.unimus.net/display/UNPUB/Home

### Requisitos

*Hardware*

Os requisitos de *hardware* e *software* do Unimus estão disponíveis em: https://wiki.unimus.net/display/UNPUB/Hardware+requirements

**Ativos de rede suportados**

A lista completa de ativos de rede suportados pelo Unimus está disponível em: https://wiki.unimus.net/pages/viewpage.action?pageId=10092755

### Preço

Para obter informações sobre o preço acesse as seguintes páginas:

* https://wiki.unimus.net/display/UNPUB/Pricing
* https://unimus.net/pricing.html

Referências:

* https://wiki.unimus.net/display/UNPUB/Installing+Unimus
* https://github.com/crocandr/docker-unimus
* https://wiki.unimus.net/display/UNPUB/Common+first-launch+issues
* https://wiki.unimus.net/display/UNPUB/Running+Unimus+behind+a+HTTP%28S%29+proxy
* https://wiki.unimus.net/display/UNPUB/Configuring+server+port+or+bind+address
* https://wiki.unimus.net/display/UNPUB/Unimus+HTTPS+with+a+self-signed+cert
* https://wiki.unimus.net/display/UNPUB/Architecture+overview
