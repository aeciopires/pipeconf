{
	"data": "ENC[AES256_GCM,data:QV9I/z+xcnelIWtQcgsOpANyuyaOOm0FCfwY0mQ/6S78F4JkvORa8RxccWLXvuWRbOIJPhmywgC8IjLtuw6HYgH495uigBmndKPD4q+x9UrN3xjUIFW5hKIAVVsBCVrn+D16W/B8heM+BNmsnpFcQcbnGqAbyxBbk09p46SF+CMVzBFYRu+thOqo8MC4YPzoGi+ypVagOMkaZ1j0m6QKFYgmo544/YI/I+Z+uLcWwschhyKKoz4Ivsa/KOeaLD0AMZOsBJIGKIrJQ88BO2H5TD7qkQq+7tr5nI1NNCl6fieCnGsyoNHayh2AavR18O/LCd/tmcTXEjh66J4HOdSjNAGN2gGRqO2jzWDs8hUSGzKa4Zten/TAdNhcUMdg4Qk/vI5EfzFhR3/oucG6iyPEInxVObB4ReO4yhog6vYoOYFkyk4ej0Yi0qTUnWJczmQF5Us4mObNJzQn7ZDM/i3r8ao7KBkGNP1uWDOStj0PkUy++Lezc0yhm0BBh87tNnPek2HsW8s=,iv:n6vn76g2oMs6If4gvj6FnyKEUNTg3mptuERPiPjN8cA=,tag:bOp45Coogp1gcnjyFsvO0w==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:24Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwH/HyM0c1RJnKsTPwqCRFL1AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMJrKdCIf4FbAAhAPnAgEQgDv56mRjekgZfhHRMZ1ZNlo5vigQ/MdHm2ocRJbGEhkL6EG/GyoUQs9j+juAjJ8UW1wQtk9Zxr8zilfWaw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:24Z",
		"mac": "ENC[AES256_GCM,data:K2dSs8anY48yvilXWFQrvS6n+dSg2ILYa+AeggpuDGXArF52UFNUwCmKR+EAI79MIiP/7iSi9NvhbUo3ho0NRVSdVhP/9BGRUVIh5eiyGzBd2EWCIMDrsQkXIzvv8wz4I17+dl4hGX+i27cTazaxmFODuaCnov9+PLZcTZ4xIos=,iv:rFgbJoTD3BuuwNMKnSyPv5NEExqFIm6xjM+GUlWHyEM=,tag:OBCNiLKcPDLBjXA/syu4kA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}