{
	"data": "ENC[AES256_GCM,data:e8/2da5Tfgni+i/G61RMw4kvfrAvK0QUFeI+OxF5wcucM6s6MT/sMp3yUC3ahN8T0JmdwFcpQPPrKvVyqgUHb/72A03JUdT9nk8hSPwOILzmkfGbqvgYbk6YtJkj+pLpd424oiJI7W9Wq8Agiu9rBHYVYQQH0Nct3qhQ7ApVHRif2QKhkdoPozcTKKDqXowqFDdY2hN2KKF/coRuYDqxdSz/DTMKe3ZymOnQ/RU92slrv76rlDD1U/gIVPkSvhntTTlcnMhL7+VV+yeF+MyDLXGHqjQrlAxHKFyfaTY9mQhA5QyIdtZzA9XxAIunw2Y7veODmyKs9GLc2UzZuR6A4RkxR9iieovQ501W6/LfrnpC/c2MgmQmqCdC+Xnic/8uNRDHbD6SnemTJQAl3ur3Lrgvd9A/gm29Zs0yYjWW4oppHUal159BIQcdJJrUXChTdf6h2axiXN9KyuvGo4OTN8DPV7QmhUQYpaD8lif7pQPsSxnq6m2W1LNhs6BOjDwRaNIhBj6xCP4bF08D5l/to2ffxGN7yGGuWkNDk+z2pd8TWRgfaw==,iv:s85wy2BslFJIeJEYjBt+qRaxvs9kbOF11nNZWdIb0lY=,tag:aMdycK35D9O7fiZ/IcC76A==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:17:11Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFJdstTgNNgTbkGKqf8uA4ZAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMK9alps66KwhLmZwtAgEQgDsCOjsgny34h50MZFd8urRwGhzGgXgVM9NbyYhpVACN0R3CK6l+amItxMPF36/TxGn5jlIrSn0XnM4GIQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:17:12Z",
		"mac": "ENC[AES256_GCM,data:SzdRYH8M6BOlqa/mArlXDX/yid6VDwdyNrk0DGgBhk+upWcY5ZeZtJ1DVqc3xnTtGUokQBcg7hFbJ5hQNOcN5yijR/yPDcfxjXYzYrfgds5vcF8EcUrVWUeYuyo+qdqFs27wvF6rcP61BC6d9CG8zNmaxpnNkqyZbeM+IARcwl0=,iv:qJNLnfOZNH95mE174+Ncbv8bBIu3RcztodA2KPv460Y=,tag:rRoDgVTUxdPFvCJ9atuxBw==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}