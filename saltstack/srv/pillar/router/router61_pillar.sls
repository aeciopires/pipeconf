{
	"data": "ENC[AES256_GCM,data:d1pvOSqdSKWAZqCZOZoYjd35zUc1XHh/qtxQl6Ue4UD2deA2k6O0thWZ5TkRreu5uNNGmPHiNhI8PJRyP5Y/OXqmDLEVltToPPI2H/b9pi5qsEek/d1bOGvwhXQ0wtfwVrW7bhO9ezhE6CA1DkPEEglPB7fFn6uUIV3CStJIR3banbalHzHRNkjhmM0fuKqgeA4b/qLKBpIwI5YV8n9w3O7smEHueQi+HP9px2D4iZejHDqd3u3C2TwJxQmMzK1fl6yfpXsNV81Hd3ns3byL1Y7kFDwLidQkOOQz/CTvVWTwSVwjZ7J4tPgrEflF4izSEbpQl5VywzR7Pe/xolqrQLWSZmCKubtr0OcGYyoco4Xqn8E9SiogZU4F3VwqCd5J/ApBuEfOdXTBrYUq/R/RcnrJCqTM9OkzAw8TPzWJ4hxx8SKK5X/i5GINzJE4RgbPZCoghgfTEelWaJJMsPHmeD61/bB7yxcTEoDCCdEF6JkUdeYuzUN5aPu5lesbhv7lOPCcwec=,iv:sH/n7h38SSZdziBgKJtkbFXzEB58CpvL372QdP7g6cs=,tag:HzQ5Xm2ZQ2bAq+ElZZLsqw==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:37Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFlp3myIMCoWVN8c5ZPq28xAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMxbsGb9BmZMZ9M9pqAgEQgDtuhDChGk+rtMGv/Hr7w0HXKTesPChrrETX8dcb3NZHhRa8mwEQWytBU9lPtHhTJ4yLMzwk1PMj3MNeFQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:37Z",
		"mac": "ENC[AES256_GCM,data:mmHZVmoj8/qTD1ZRaYeXVTV5J5EpMW43w4dHPukiFCLCjCaF4Fp3ztPcSxfdVK1uu76Jj2PqI0PlAYiBcG+fDBzouZu5beAT1P3YyA4Yy07kcnoBgIJ/Dny5z5rf63awtAZYpCrz0YHIkC612/86tqAG352J5J7UQSMQ2H5Nfww=,iv:igThqmT+ntHWSSnZUiZYXeikH2Jp49HFbkXgmRdLicg=,tag:1n6mZivgCHMXBoZvkBQ1mQ==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}