#!/bin/bash

#------------------------
# Authors: Aecio Pires
# Date: 02 jan 2022
#
# Objective: Install PipeConf.
#
#--------------------- REQUIREMENTS --------------------#
# 1) Install Distro GNU/Linux, preferably Ubuntu Server 20.04 64 bits.
#
# 2) Install Docker Community 20.10.x or higher.
#
# 3) Configure AWS-KMS and Sops (/home/pipeconf/.sops.yaml)
#    https://github.com/mozilla/sops#using-sops-yaml-conf-to-select-kms-pgp-for-new-files
#
#------------------------

#------------------------------------------------------
#------------------------------------------------------
# Local Specific Functions
#------------------------------------------------------
#------------------------------------------------------


#--------------------------------------------------------
# comment: Print usage help
# usage: usage
#
function usage() {

echo
echo "Install PipeConf."
echo
echo "Usage:"
echo
echo "$0 PARAMETERS_FILE [OPTIONS]"
echo
echo "Options available:"
echo
echo "---"
echo "-a ACTION_NAME => Run specific action. See 'Actions available' section."
echo "-d             => Enable DEBUG mode. Default: false."
echo "-h             => Show this message."
echo "---"
echo
echo "Actions available:"
echo
echo "---"
echo "$OPTION_ACTION"
echo "---"
echo
echo "Requirements:"
echo
echo "1) Install Distro GNU/Linux, preferably Ubuntu Server 20.04 64 bits."
echo
echo "2) Install Docker Community 20.10.x or higher."
echo
echo "3) Configure AWS-KMS and Sops (/home/pipeconf/.sops.yaml)"
echo "   https://github.com/mozilla/sops#using-sops-yaml-conf-to-select-kms-pgp-for-new-files"
exit 3
}


#--------------------------------------------------------
# comment: Install base packages
# usage: installBasePackages
#
function installBasePackages() {

checkCommand apt

echo "[INFO] Install base packages..."
apt update
apt install -y apt-transport-https ca-certificates curl vim \
               git openjdk-8-jdk unzip iputils-ping net-tools \
               wget sudo telnet gawk
}


#--------------------------------------------------------
# comment: Install and configure Docker
# usage: installDocker
#
function installDocker() {

checkCommand curl apt add-apt-repository systemctl usermod

if ! type docker > /dev/null 2>&1; then
    echo "[INFO] Installing Docker..."
    cd /tmp
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - ;
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable";
    apt update
    apt install -y docker-ce
    systemctl enable docker
    docker --version
    cd -

    echo "[INFO] Install plugin 'grafana/loki-docker-driver' in Docker..."
    docker plugin install grafana/loki-docker-driver:latest \
    --alias loki --grant-all-permissions

    systemctl restart docker
    docker plugin ls
fi

echo
echo "[INFO] Docker is installed."
echo

echo "[INFO] Allow '$USER' access Docker."
if ! type setfacl > /dev/null 2>&1; then
    apt update
    apt install -y acl
fi

systemctl start docker
systemctl enable docker
usermod -aG docker $USER
setfacl -m user:$USER:rw /var/run/docker.sock
}


#--------------------------------------------------------
# comment: CreateUser of PipeConf
# usage: createUserPipeConf
#
function createUserPipeConf() {

local pipeconf_user="${PIPECONF_USER}"

checkCommand getent passwd adduser usermod

if ! getent passwd $pipeconf_user > /dev/null; then
    echo
    echo "***************************"
    echo "[WARNING] Enter with password of $pipeconf_user user..."
    echo "***************************"
    echo
    adduser $pipeconf_user
    adduser $pipeconf_user admin
fi

echo "[INFO] Allow '$pipeconf_user' access Docker."
if ! type setfacl > /dev/null 2>&1; then
    apt update
    apt install -y acl
fi

usermod -aG docker $pipeconf_user
setfacl -m user:$pipeconf_user:rw /var/run/docker.sock
}


#--------------------------------------------------------
# comment: Install and configure Sops
# usage: installSops
#
function installSops() {

local sops_version=$(curl -s https://api.github.com/repos/mozilla/sops/releases/latest | grep tag_name | cut -d '"' -f 4)

checkCommand curl

if ! type sops > /dev/null 2>&1; then
    echo "[INFO] Installing Sops..."
    curl -LO https://github.com/mozilla/sops/releases/download/${sops_version}/sops-${sops_version}.linux
    mv sops-${sops_version}.linux /usr/local/bin/sops
    chmod +x /usr/local/bin/sops
    which sops
    sops --version
fi

echo
echo "[INFO] Sops is installed."
echo "[ATTENTION] Configure AWS-KMS and Sops (/home/pipeconf/.sops.yaml)"
echo "https://github.com/mozilla/sops#using-sops-yaml-conf-to-select-kms-pgp-for-new-files"
echo

}


#--------------------------------------------------------
# comment: Create container of Postgresql
# sintax:
#   createContainerPostgresql
# return: 0 is correct or code error
#
function createContainerPostgresql() {

local pipeconf_data_dir="${PIPECONF_DATA_DIR}"
local postgresql_version="${POSTGRESQL_VERSION}"
local container_name="${POSTGRESQL_CONTAINER_NAME}"
local postgresql_user="${POSTGRESQL_USER}"
local postgresql_password="${POSTGRESQL_PASSWORD}"
local postgresql_database="${POSTGRESQL_DATABASE}"
local host_port_postgresql="${POSTGRESQL_HOST_PORT}"
local host_volume_postgresql="${pipeconf_data_dir}/gogs/postgresql${postgresql_version}/data"
local pipeconf_node_address="${PIPECONF_NODE_ADDRESS}"
local result_code=0

debug set -x

createDir $host_volume_postgresql
chown -R 999:999 $host_volume_postgresql

if ! checkContainer "$container_name"; then
    echo "[INFO] Creating container of PostgreSQL..."
    docker run -d -p $host_port_postgresql:5432 \
    --name $container_name \
    --restart=always \
    -v $host_volume_postgresql:/var/lib/postgresql/data \
    -e POSTGRES_USER=$postgresql_user \
    -e POSTGRES_PASSWORD=$postgresql_password \
    -e POSTGRES_DB=$postgresql_database \
    postgres:$postgresql_version

    result_code=$?

    debug set +x

    echo "[INFO] Access PostgreSQL with informations:"
    echo "HOST=${pipeconf_node_address}"
    echo "PORT=${host_port_postgresql}"
    echo "USER=${postgresql_user}"
    echo "PASSWORD=${postgresql_password}"
    echo "DATABASE=${postgresql_database}"
else
    echo "[INFO] Container '$container_name' is running."
fi

return $result_code
}


#--------------------------------------------------------
# comment: Create container of Gogs
# sintax:
#   createContainerGogs
# return: 0 is correct or code error
#
function createContainerGogs() {

local pipeconf_data_dir="${PIPECONF_DATA_DIR}"
local gogs_version="${GOGS_VERSION}"
local container_name="${GOGS_CONTAINER_NAME}"
local host_port_gogs_ssh="${GOGS_HOST_PORT_SSH}"
local host_port_gogs_web="${GOGS_HOST_PORT_WEB}"
local host_volume_gogs="${pipeconf_data_dir}/gogs/data"
local postgresql_user="${POSTGRESQL_USER}"
local postgresql_password="${POSTGRESQL_PASSWORD}"
local postgresql_database="${POSTGRESQL_DATABASE}"
local gogs_url="http://${PIPECONF_SERVER_HOSTNAME}:${host_port_gogs_web}"
local gogs_ssl="${GOGS_SSL}"
local gogs_mail="${PIPECONF_MAIL}"
local result_code=0

debug set -x

createDir $host_volume_gogs

if ! checkContainer "$container_name"; then
    echo "[INFO] Creating container of Gogs..."
    docker run -d -p $host_port_gogs_ssh:22 -p $host_port_gogs_web:3000 \
    --name $container_name \
    --restart=always \
    -v $host_volume_gogs:/data \
    -v /etc/hosts:/etc/hosts \
    gogs/gogs:$gogs_version

    result_code=$?

    debug set +x

    echo "[INFO] Access Gogs with informations:"
    echo "${gogs_url}"
else
    echo "[INFO] Container '$container_name' is running."
fi

return $result_code
}


#--------------------------------------------------------
# comment: Create container of Jenkins
# sintax:
#   createContainerJenkins
# return: 0 is correct or code error
#
function createContainerJenkins() {

local pipeconf_data_dir="${PIPECONF_DATA_DIR}"
local jenkins_version="${JENKINS_VERSION}"
local container_name="${JENKINS_CONTAINER_NAME}"
local host_port_jenkins_jndi="${JENKINS_HOST_PORT_JNDI}"
local host_port_jenkins_web="${JENKINS_HOST_PORT_WEB}"
local host_volume_jenkins="${pipeconf_data_dir}/jenkins/data"
local pipeconf_server_hostname="${PIPECONF_SERVER_HOSTNAME}"
local pipeconf_server_address="${PIPECONF_SERVER_ADDRESS}"
local pipeconf_node_hostname="${PIPECONF_NODE_HOSTNAME}"
local pipeconf_node_address="${PIPECONF_NODE_ADDRESS}"
local wait=300
local result_code=0

debug set -x

createDir $host_volume_jenkins
chown -R 1000 $host_volume_jenkins

if ! checkContainer "$container_name"; then
    echo "[INFO] Creating container of jenkins..."
    docker run -d -p $host_port_jenkins_web:8080 -p $host_port_jenkins_jndi:50000 \
    --name $container_name \
    --restart=always \
    -v $host_volume_jenkins:/var/jenkins_home \
    --dns=8.8.8.8 \
    --add-host="$pipeconf_server_hostname:$pipeconf_server_address" \
    --add-host="$pipeconf_node_hostname:$pipeconf_node_address" \
    --env JAVA_OPTS="-Dcom.cloudbees.workflow.rest.external.JobExt.maxRunsPerJob=50" \
    jenkins/jenkins:$jenkins_version

    sleep $wait
    local initial_password=$(cat $host_volume_jenkins/secrets/initialAdminPassword)

    result_code=$?

    debug set +x

    echo
    echo "[INFO] Access Jenkins with informations:"
    echo "http://${pipeconf_node_address}:${host_port_jenkins_web}"
    echo "User: admin"
    echo "Password: $initial_password"
    echo
else
    echo "[INFO] Container '$container_name' is running."
fi

return $result_code
}



#--------------------------------------------------------
# comment: Create container of CAdvisor
# sintax:
#   createContainerCAdvisor
# return: 0 is correct or code error
#
function createContainerCAdvisor() {

local cadvisor_version="${CADVISOR_VERSION}"
local host_port_cadvisor="${CADVISOR_HOST_PORT}"
local container_name='cadvisor'
local result_code=0

debug set -x

if ! checkContainer "$container_name"; then
    echo "[INFO] Creating container of CAdvisor..."
    docker run -d --restart=always \
    --volume=/:/rootfs:ro \
    --volume=/var/run:/var/run:ro \
    --volume=/sys:/sys:ro \
    --volume=/var/lib/docker/:/var/lib/docker:ro \
    --volume=/dev/disk/:/dev/disk:ro \
    --publish="${host_port_cadvisor}:8080" \
    --detach=true \
    --name=$container_name \
    --device=/dev/kmsg \
    gcr.io/cadvisor/cadvisor:$cadvisor_version

    result_code=$?

    debug set +x

    echo "[INFO] Access CAdvisor with informations:"
    echo "http://172.17.0.1:${host_port_cadvisor}/metrics"
else
    echo "[INFO] Container '$container_name' is running."
fi

return $result_code
}


#--------------------------------------------------------
# comment: Create container of Node Exporter
# sintax:
#   createContainerNodeExporter
# return: 0 is correct or code error
#
function createContainerNodeExporter() {

local node_exporter_version="${NODE_EXPORTER_VERSION}"
local node_exporter_port=9100
local container_name='node-exporter'
local result_code=0

debug set -x

if ! checkContainer "$container_name"; then
    echo "[INFO] Creating container of Node Exporter..."
    docker run -d --name $container_name \
    --restart=always \
    --net="host" \
    --pid="host" \
    -v "/:/host:ro,rslave" \
    prom/node-exporter:$node_exporter_version \
    --path.rootfs=/host

    result_code=$?

    debug set +x

    echo "[INFO] Access Node Exporter with informations:"
    echo "http://172.17.0.1:${node_exporter_port}/metrics"
else
    echo "[INFO] Container '$container_name' is running."
fi

return $result_code
}


#--------------------------------------------------------
# comment: Get Docker image of Salt SProxy
# sintax:
#   getDockerImageSaltSproxy
#
function getDockerImageSaltSproxy() {

local salt_sproxy_version="${SALT_SPROXY_VERSION}"

docker pull mirceaulinic/salt-sproxy:$salt_sproxy_version

}



#--------------------------------------------------------
# comment: Executing according to the action
# usage: runAction ACTION
#
function runAction(){

local action="$1"

# Executing according to the action
# Don't remove the next line
# [LIST_ACTIONS_DONOT_REMOVE]
case ${action} in
    installBasePackages)
        installBasePackages
    ;;
    installDocker)
        installDocker
    ;;
    createUserPipeConf)
        createUserPipeConf
    ;;
    installSops)
        installSops
    ;;
    installGogs)
        createContainerPostgresql
        createContainerGogs
    ;;
    installJenkins)
        createContainerJenkins
    ;;
    installCAdvisor)
        createContainerCAdvisor
    ;;
    installNodeExporter)
        createContainerNodeExporter
    ;;
    installSaltSProxy)
        getDockerImageSaltSproxy
    ;;
    installPipeConfServer)
        installBasePackages
        installDocker
        createUserPipeConf
        installSops

        # Testing if command exists
        checkCommand docker sops

        createContainerCAdvisor
        createContainerNodeExporter
        createContainerPostgresql
        createContainerGogs
        createContainerJenkins

        #if [ "$CLOUD_KMS" == "aws" ]; then

            #if [ "$DEBUG" == true ]; then
            #    echo "[DEBUG] Init file..."
            #    # Variable shared by Jenkins pipeline
            #    cat "$AWS_CREDENTIALS_FILE"
            #    cat "$SOPS_CREDENTIALS_FILE"
            #    echo "[DEBUG] End file..."
            #fi
        #elif [ "$CLOUD_KMS" == "gcp" ]; then
        #fi
    ;;
    installPipeConfNode)
        installBasePackages
        installDocker
        createUserPipeConf
        installSops

        # Testing if command exists
        checkCommand docker sops

        createContainerCAdvisor
        createContainerNodeExporter
        getDockerImageSaltSproxy

        #if [ "$CLOUD_KMS" == "aws" ]; then

            #if [ "$DEBUG" == true ]; then
            #    echo "[DEBUG] Init file..."
            #    # Variable shared by Jenkins pipeline
            #    cat "$AWS_CREDENTIALS_FILE"
            #    cat "$SOPS_CREDENTIALS_FILE"
            #    echo "[DEBUG] End file..."
            #fi
        #elif [ "$CLOUD_KMS" == "gcp" ]; then
        #fi
    ;;
    *)
        echo
        echo "[ERROR] Unsupported action yet."
        echo
        usage
    ;;
esac
# Don't remove the next line
# [END_LIST_ACTIONS_DONOT_REMOVE]
}


#------------------------------------------------------
#------------------------------------------------------
# Global script variables
#------------------------------------------------------
#------------------------------------------------------


# Variables general of script
PROGPATHNAME=$0
PROGFILENAME=$(basename "$PROGPATHNAME")
PROGDIRNAME=$(dirname "$PROGPATHNAME")

OPTION_ACTION=$(awk '/LIST_ACTIONS_DONOT_REMOVE/,/END_LIST_ACTIONS_DONOT_REMOVE/' "$PROGPATHNAME" | grep ")" | grep -v "*" | cut -d")" -f1 | grep -v \$0 | grep -v "(" )
#OPTION_PARAMETER=$(cat "$PROGPATHNAME" | awk '/LIST_OPTIONS_DONOT_REMOVE/,/END_LIST_OPTIONS_DONOT_REMOVE/' | grep ")" | grep -v "*" | cut -d")" -f1 | grep -v \$0 | grep -v "(" )

#AUX_FILE=$(mktemp)
DEBUG=false
_DEBUG_COMMAND="on"
RUN_SPECIFIC_ACTION=false

#
#------------------------ END-VARIABLES ------------------------




#------------------------------------------------------
#------------------------------------------------------
# Main
#------------------------------------------------------
#------------------------------------------------------

# Checking if has options
if [ $# -eq 0 ]; then
    usage
fi

# Load script with our libs and defaults functions
[ -x "$PROGDIRNAME"/lib.sh ] && . "$PROGDIRNAME"/lib.sh
[ -x "$PROGDIRNAME"/variables.sh ] && . "$PROGDIRNAME"/variables.sh

if [ $(isRoot) = NO ] ; then
    echo "[ERROR] You must be root to run this command."
    echo "Run: sudo $PROGPATHNAME $*"
    exit 3
fi

# Detect GNU/Linux distribution supported
getDistribution

# Handling the options passed to the script
while getopts ":dha:" OPT ; do  # The ":" in begin indicates to don't show messages of error
# Don't remove the next line
# [LIST_OPTIONS_DONOT_REMOVE]
    case $OPT in
        a)
            RUN_SPECIFIC_ACTION=true
            ACTION=$OPTARG

            if [ "$DEBUG" == true ]; then
                echo "[DEBUG] Option -$OPT"
                echo "[DEBUG] Action: $ACTION"
            fi
        ;;
        d)
            if [ "$DEBUG" == true ]; then
                echo "[DEBUG] Option -$OPT"
            fi

            # Turn on DEBUG mode
            DEBUG=true
        ;;
        h)
            if [ "$DEBUG" == true ]; then
                echo "[DEBUG] Option -$OPT"
            fi

            usage
        ;;
        *)  echo "[ERROR] Invalid option: $OPT"
            usage
        ;;
    esac
# Don't remove the next line
# [END_LIST_OPTIONS_DONOT_REMOVE]
done
# We can now do something with $@ (rest of the command line)
shift $((OPTIND-1))


# Testing if generic variables exists
checkVariable CLOUD_KMS "$CLOUD_KMS"
checkVariable SOPS_CONFIG_FILE "$SOPS_CONFIG_FILE"
checkVariable POSTGRESQL_CONTAINER_NAME "$POSTGRESQL_CONTAINER_NAME"
checkVariable POSTGRESQL_VERSION "$POSTGRESQL_VERSION"
checkVariable POSTGRESQL_USER "$POSTGRESQL_USER"
checkVariable POSTGRESQL_PASSWORD "$POSTGRESQL_PASSWORD"
checkVariable POSTGRESQL_DATABASE "$POSTGRESQL_DATABASE"
checkVariable POSTGRESQL_HOST_PORT "$POSTGRESQL_HOST_PORT"
checkVariable GOGS_VERSION "$GOGS_VERSION"
checkVariable GOGS_CONTAINER_NAME "$GOGS_CONTAINER_NAME"
checkVariable GOGS_HOST_PORT_SSH "$GOGS_HOST_PORT_SSH"
checkVariable GOGS_HOST_PORT_WEB "$GOGS_HOST_PORT_WEB"
checkVariable GOGS_USER "$GOGS_USER"
checkVariable GOGS_PASSWORD "$GOGS_PASSWORD"
checkVariable GOGS_USER "$GOGS_USER"
checkVariable GOGS_PASSWORD "$GOGS_PASSWORD"
checkVariable GOGS_SSL "$GOGS_SSL"
checkVariable JENKINS_CONTAINER_NAME "$JENKINS_CONTAINER_NAME"
checkVariable JENKINS_VERSION "$JENKINS_VERSION"
checkVariable JENKINS_HOST_PORT_JNDI "$JENKINS_HOST_PORT_JNDI"
checkVariable JENKINS_HOST_PORT_WEB "$JENKINS_HOST_PORT_WEB"
checkVariable PIPECONF_USER "$PIPECONF_USER"
checkVariable PIPECONF_HOME "$PIPECONF_HOME"
checkVariable PIPECONF_DATA_DIR "$PIPECONF_DATA_DIR"
checkVariable PIPECONF_DOMAIN_NAME "$PIPECONF_DOMAIN_NAME"
checkVariable PIPECONF_MAIL "$PIPECONF_MAIL"
checkVariable PIPECONF_SERVER_HOSTNAME "$PIPECONF_SERVER_HOSTNAME"
checkVariable PIPECONF_SERVER_ADDRESS "$PIPECONF_SERVER_ADDRESS"
checkVariable PIPECONF_NODE_HOSTNAME "$PIPECONF_NODE_HOSTNAME"
checkVariable PIPECONF_NODE_ADDRESS "$PIPECONF_NODE_ADDRESS"
checkVariable SALT_SPROXY_VERSION "$SALT_SPROXY_VERSION"
checkVariable CADVISOR_VERSION "$CADVISOR_VERSION"
checkVariable CADVISOR_HOST_PORT "$CADVISOR_HOST_PORT"
checkVariable NODE_EXPORTER_VERSION "$NODE_EXPORTER_VERSION"

# Converts uppercase letters to lowercase
CLOUD_KMS="$(tolower "$CLOUD_KMS")"

# Verify support to cloud
echo "[INFO] Testing if cloud is supported for KMS (Key Management Service)..."
cloudSupported "$CLOUD_KMS"

# Executing a specific action of update
if [ "$RUN_SPECIFIC_ACTION" == true ]; then

    # Testing if variable is empty
    checkVariable ACTION "$ACTION"

    runAction "$ACTION"
    exit $?
fi
