{
	"data": "ENC[AES256_GCM,data:yeoJtJlYIZCxFpV3m0lhI0XqBnbgG9Ov2IFjudd/CTrBEI26mXBTZ8wxovabekhacftw5UY6MdELvRAKaxXPqpFSH7pbYF8bP5lzX+O7pA3/RmeDTUd0Js1/cFWHkPmareqB5NYzIi4oMiY2mZH5JmdUy0wLqUojFhrAkvLSdMbhjgU6zL0Y2xOT/wGfoj85/Lv84aYbR8Vlg58zGoQFTJfwxltDTgdV9tJKuadIP3jgeyJap56vftx3Z8cgy9gRgDmR+OmKbDpIjJMA2gdTIF+NqjBOB5fopH7hExuBk2qe+1EvRzqY4saMadiZHWdhybVWlwoWOCNuLwqs418KTEMWMcVzKb+/AVvX37AHA/OXzFaJs7FsGlp0J1PViXlQhZ4fCazDPfCQoJu/RozaHlGRegf2wkuBrc51N/LNVqdgp5OK1yvI/36/4PG8dqneGccKZFoy6YzffEZun+XClMgCkZ7lZ5KcVf0tbhQMo4A/lGT8DnUDmOWiS91Mt3hwuR46cw==,iv:G+ob0JqmnxchpNVcGuvsqok4dAP9HojvG1omo2r1Ou8=,tag:GIpouEiKw3WqBJGfWAiKbQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:19Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFZXWiHt6ahCjF4q3AyUFgcAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM10ZgPdkGWcvXLoezAgEQgDt2oUhg9Azgt1FVg66324flOLNvSxqX8IZWl8hxFf5CVNEu1qlZauhJm6iwUEchjVfJWYD1ixWGAvoDXw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:20Z",
		"mac": "ENC[AES256_GCM,data:c/mi4K67zBbBorRRUuIPVQtks4f7x/2WuLdsB9bVrr6GVjr2TF7wKfLmbmDzuYTDttnGIjxF2Nj4Hq4+h1D75Ondr7cVR/qJdPA+DJmNuHITN2jGBzDAtASS0D1C+E/NYhYhv/JHwtdMxby3HEj3gWJcaldY8p8D65bx33eGFUU=,iv:QuKAFV2YiNVyw3AIMuoCWU0S2Ag5q+bpT/FHtXc48ns=,tag:ZyWLZjSyN+uFNAMgP4E0Sg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}