<!-- TOC -->

- [Português](#português)
- [Tutorial de Instalação do PipeConf](#tutorial-de-instalação-do-pipeconf)
  - [Requisitos](#requisitos)
  - [Instalando o Docker](#instalando-o-docker)
  - [Criando uma Conta na AWS](#criando-uma-conta-na-aws)
  - [Criando uma Chave Simétrica no AWS-KMS.](#criando-uma-chave-simétrica-no-aws-kms)
  - [Instalando o SOPS](#instalando-o-sops)
  - [Iniciando os contêineres do Gogs](#iniciando-os-contêineres-do-gogs)
  - [Iniciando contêiner do Jenkins](#iniciando-contêiner-do-jenkins)
  - [Instalando o Salt-Sproxy](#instalando-o-salt-sproxy)
- [Configurando o Jenkins](#configurando-o-jenkins)
  - [Gerenciando Plugins](#gerenciando-plugins)
  - [Gerenciando Credenciais](#gerenciando-credenciais)
  - [Gerenciando as Configurações do Jenkins](#gerenciando-as-configurações-do-jenkins)
  - [Adicionando um Node Slave no Jenkins](#adicionando-um-node-slave-no-jenkins)
  - [Criando os Pipelines](#criando-os-pipelines)
  - [Configurando a Integração entre o Jenkins e o Gogs](#configurando-a-integração-entre-o-jenkins-e-o-gogs)
- [Liberação das Portas no Firewall](#liberação-das-portas-no-firewall)
- [PipeConf - Demonstration](#pipeconf---demonstration)

<!-- TOC -->

# Português

------------

# Tutorial de Instalação do PipeConf

## Requisitos

1) A seguir estão as recomendações de hardware para executar o PipeConf.

**PipeConf-Server** | **PipeConf-Node**

| Properties           | Default Values                  |
|:-------------------- |:--------------------------------|
| Operating System     | Ubuntu Server 20.04 LTS 64 bits |
| Memory RAM           | 3 GB                            |
| CPU                  | 1 core with clock 2 GHz         |
| HD                   | 20 GB SSD                       |

OBS.: Teoricamente, o PipeConf pode ser instalado no [Debian](https://www.debian.org/distrib) / [CentOS](https://www.centos.org) / [Rocky Linux](https://rockylinux.org), ou outra distro GNU/Linux, mas pode ser necessário adaptar alguns comandos mostrados ao longo deste tutorial.

Os hosts ``PipeConf-Server`` e ``PipeConf-Node`` podem ser um só, caso tenha acesso direto a rede na qual estão os ativos de rede. Caso contrário, serão necessários dois hosts: um para executar o ``PipeConf-Server`` e outro que atuará como ``PipeConf-Node``. Esse hosts podem ser servidores físicos ou máquinas virtuais.

A seguir são mostrados exemplos dos modelos de instalação do PipeConf.

![alt text](images/production_environment.png "Arquitetura PipeConf")

Em cada host, execute os seguintes comandos para instalar as dependências de software no Ubuntu 20.04 64 bits.

```bash
sudo su

apt update

apt install -y apt-transport-https ca-certificates curl vim git openjdk-8-jdk unzip iputils-ping net-tools wget sudo telnet gawk
```

## Instalando o Docker

Em cada host, instale o Docker-CE com os seguintes comandos.

```bash
cd /tmp

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - ;

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable";

sudo apt update

sudo apt install -y docker-ce

sudo systemctl enable docker

sudo docker --version
```

Em cada host, adicione o seu usuário ao grupo Docker com os seguintes comandos.

```bash
AUX_USER=$USER

sudo usermod -aG docker $AUX_USER

sudo setfacl -m user:$AUX_USER:rw /var/run/docker.sock
```

> **Atenção**: Configure o Docker para ser inicializado no boot do sistema operacional, seguindo as instruções da página: https://docs.docker.com/install/linux/linux-postinstall/

Em cada host, execute os seguintes comandos para criar o usuário ``pipeconf`` e dar permissão para executar o serviço ``docker``.

```bash
sudo adduser pipeconf

sudo adduser pipeconf admin

sudo usermod -aG docker pipeconf

sudo setfacl -m user:pipeconf:rw /var/run/docker.sock
```

Instale o plugin ``grafana/loki-docker-driver`` no Docker com o seguinte comando:

```bash
docker plugin install grafana/loki-docker-driver:latest \
  --alias loki --grant-all-permissions
```

Reinicie o serviço do Docker e verifique se o plugin foi instalado corretamente com os seguintes comandos:

```bash
sudo systemctl restart docker

docker plugin ls
```

Em cada host, execute os seguintes comandos para informar os nomes do hosts (altere os IPs internos das máquinas virtuais de acordo com o ambiente). Caso o ``PipeConf-Server`` e o ``PipeConf-Node`` sejam executados no mesmo host, basta repetir o IP nos seguintes comandos.

```bash
sudo echo "10.128.0.2 pipeconf-server.domain.com.br pipeconf-server" >> /etc/hosts

sudo echo "10.128.0.3 pipeconf-node.domain.com.br pipeconf-node" >> /etc/hosts
```

Em cada host, execute os seguintes comandos para testar a resolução de nomes.

```bash
ping -c3 pipeconf-server.domain.com.br
ping -c3 pipeconf-node.domain.com.br
```

Referência:

* https://novatec.com.br/livros/jenkins
* https://docs.docker.com/install/linux/docker-ce/ubuntu

## Criando uma Conta na AWS

Será necessário criar uma conta na AWS para utilizar o serviço KMS para gerenciar uma chave simétrica a ser utilizada para encriptar e decriptar os arquivos que conterão as informações dos hosts a serem gerenciados pelo PipeConf.

A encriptação será executada pelo comando ``sops`` (mostrado na seção seguinte). O ``sops`` tem suporte a outros tipos de serviço que gerenciam a chave simétrica, tais como: OpenPGP, GCP KMS, Azure Key Vault. 

> **Atenção:** Se escolher outro serviço para gerenciar a chave simétrica, a configuração desta seção pode ser ignorada e você deve configurar o ``sops`` adequadamente seguindo as instruções da documentação  oficial disponível em: https://github.com/mozilla/sops.

Execute os seguites passos para criar uma conta na AWS.

* Você precisará criar uma conta na AWS. Crie uma conta *Free Tier* na AWS https://aws.amazon.com/ e siga as instruções nas seguintes páginas.
  * https://docs.aws.amazon.com/chime/latest/ag/aws-account.html
  * https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/free-tier-limits.html.
* Ao criar a conta, você precisará registrar um cartão de crédito, mas como criará uma chave simétrica no serviço KMS usando os recursos oferecidos pelo plano *Free Tier*, nada será cobrado se você não exceder o limite para o uso dos recursos e do tempo oferecido e descrito no link anterior.
* Após criar a conta na AWS, acesse a interface web em: https://console.aws.amazon.com
* Clique no nome de usuário (canto superior direito) e escolha a opção *Security Credentials*. Em seguida, clique na opção *Access Key and Secret Access Key* e clique no botão *New Access Key* para criar e exibir o ID e o segredo da chave, conforme mostrado a seguir.

```
Access Key ID: YOUR_ACCESS_KEY_HERE
Secret Access Key: YOUR_SECRET_ACCESS_KEY_HERE
```

Em cada host, crie o seguinte diretório e arquivo.

```bash
sudo mkdir -p /home/pipeconf/.aws/
sudo touch /home/pipeconf/.aws/credentials
```

Edite o arquivo ``/home/pipeconf/.aws/credentials`` e adicione o seguinte conteúdo.

```bash
[default]
aws_access_key_id = YOUR_ACCESS_KEY_HERE
aws_secret_access_key = YOUR_SECRET_ACCESS_KEY_HERE
```

Substitua os termos ``YOUR_ACCESS_KEY_HERE`` e ``YOUR_SECRET_ACCESS_KEY_HERE`` pelo ID e secret da chave exibidos no console da AWS.

Defina a permissão de acesso correta para o diretório ``/home/pipeconf/.aws`` usando o seguinte comando.

```bash
sudo chown -R pipeconf:pipeconf /home/pipeconf/.aws/
```

Referência:

* https://docs.aws.amazon.com/general/latest/gr/managing-aws-access-keys.html

## Criando uma Chave Simétrica no AWS-KMS.

> **Atenção:** Se escolher outro serviço para gerenciar a chave simétrica, a configuração desta seção pode ser ignorada e você deve configurar o ``sops`` adequadamente seguindo as instruções da documentação  oficial disponível em: https://github.com/mozilla/sops.

Acesse a interface web da AWS (https://console.aws.amazon.com).

Acesse o serviços KMS e crie uma chave simétrica seguindo as instruções deste tutorial: https://docs.aws.amazon.com/pt_br/kms/latest/developerguide/create-keys.html

Para obter mais informações sobre o serviços KMS e o uso de chaves simétricas, acesse os seguintes links.

* https://docs.aws.amazon.com/pt_br/kms/latest/developerguide/overview.html
* https://docs.aws.amazon.com/pt_br/kms/latest/developerguide/concepts.html
* https://docs.aws.amazon.com/pt_br/kms/latest/developerguide/kms-dg.pdf

## Instalando o SOPS

Em cada host, instale e configure o ``sops`` com os seguintes comandos.

```bash
sudo su

function install_sops {
if [ -z $(which sops) ]; then
    VERSION_SOPS=$(curl -s https://api.github.com/repos/mozilla/sops/releases/latest | grep tag_name | cut -d '"' -f 4)
    curl -LO https://github.com/mozilla/sops/releases/download/$VERSION_SOPS/sops-$VERSION_SOPS.linux
    sudo mv sops-$VERSION_SOPS.linux /usr/local/bin/sops
    sudo chmod +x /usr/local/bin/sops
else
    echo "sops is most likely installed"
fi
}

install_sops

which sops

sops --version

exit
```

Exemplo do arquivo de configuração do ``sops``, que deve ficar em ``/home/pipeconf/.sops.yaml``:

```
creation_rules:
    - kms: 'PATH_ARN_KEY_SYMMETRIC'
      aws_profile: default
```

Onde ``PATH_ARN_KEY_SYMMETRIC`` deve ser substituído pelo ARN da chave simétrica criada no serviços KMS da AWS, conforme mostrado na figura a seguir.

![alt text](images/find-key-arn-new-new.png "PATH_ARN_KEY_SYMMETRIC")

Defina a permissão de acesso correta para o arquivo ``/home/pipeconf/.sops.yaml`` usando o seguinte comando.

```bash
sudo chown pipeconf:pipeconf /home/pipeconf/.sops.yaml
```

Documentação do ``sops`` disponível em: https://github.com/mozilla/sops

> **Atenção:** Se escolher outro serviço KMS para gerenciar a chave simétrica, a configuração desta seção pode ser ignorada e você deve configurar o ``sops`` adequadamente seguindo as instruções da documentação  oficial disponível em: https://github.com/mozilla/sops.

Exemplo de como seria a configuração usando o GCP-KMS:

```
creation_rules:
    - gcp_kms: projects/mygcproject/locations/global/keyRings/mykeyring/cryptoKeys/thekey
```

Para isso funcionar você precisa ter uma conta na GCP, criar uma chave simétrica no GCP-KMS, instalar o comando ``gcloud`` no host que é executado o PipeConf e configurar a atenticação com a API da GCP. Na dúvida, consulte a documentação oficial do ``sops`` e da GCP.

## Iniciando os contêineres do Gogs

Apenas no host ``PipeConf-Server``, execute os seguintes comandos para iniciar o PostgreSQL.

```bash
sudo mkdir -p /docker/pipeconf/gogs/postgresql12/data
sudo chown -R 999:999 /docker/pipeconf/gogs/postgresql12/data

docker run -d -p 5432:5432 \
  --name postgresql \
  --restart=always \
  -v /docker/pipeconf/gogs/postgresql12/data:/var/lib/postgresql/data \
  -e POSTGRES_PASSWORD=gogs \
  -e POSTGRES_USER=gogs \
  -e POSTGRES_DB=gogs \
  postgres:12
```

Será criado o contêiner do PostgreSQL com as seguintes informações.

| Properties           | Default Values                          |
|:-------------------- |:----------------------------------------|
| Port host            | 5432/TCP                                |
| Port contêiner       | 5432/TCP                                |
| Database             | gogs                                    |
| Username             | gogs                                    |
| Password             | gogs                                    |
| Volume dir host      | /docker/pipeconf/gogs/postgresql12/data |
| Volume dir contêiner | /var/lib/postgresql/data                |

No ambiente de produção a senha do PostgreSQL deve ser outra, mas você precisará lembrar durante a configuração do Gogs.

Veja os logs do PostgreSQL com o seguinte comando.

```bash
docker logs -f postgresql
```

> Atenção!!! Se quiser centralizar o log do contêiner no Loki + Grafana, conforme mostrado no arquivo [Logging.md](Logging.md), inclua as seguintes opções no comando de inicialização do contêiner. Dessa forma ao invés de olhar o log individualmente com o comando ``docker logs -f NOME_CONTEINER``, você poderá ver o log de todos os contêineres de forma centraliza no Grafana.

```
  --log-driver loki \
  --log-opt loki-url=http://172.17.0.1:3100/loki/api/v1/push \
```

Apenas no host ``PipeConf-Server``, execute os seguintes comandos para iniciar o Gogs.

```bash
sudo mkdir -p /docker/pipeconf/gogs/data

docker run -d -p 10022:22 -p 81:3000 \
  --name gogs \
  --restart=always \
  -v /docker/pipeconf/gogs/data:/data \
  -v /etc/hosts:/etc/hosts \
  gogs/gogs:0.12.3
```

Será criado o contêiner do Gogs com as seguintes informações.

| Properties           | Default Values             |
|:-------------------- |:---------------------------|
| Port HTTP host       | 81/TCP                     |
| Port HTTP contêiner  | 3000/TCP                   |
| Port SSH host        | 10022/TCP                  |
| Port SSH contêiner   | 22/TCP                     |
| User                 | root                       |
| Password             | adminadmin                 |
| Volume dir host      | /docker/pipeconf/gogs/data |
| Volume dir contêiner | /data                      |

Veja o log do Gogs com o seguinte comando.

```bash
docker logs -f gogs
```

> Atenção!!! Se quiser centralizar o log do contêiner no Loki + Grafana, conforme mostrado no arquivo [Logging.md](Logging.md), inclua as seguintes opções no comando de inicialização do contêiner. Dessa forma ao invés de olhar o log individualmente com o comando ``docker logs -f NOME_CONTEINER``, você poderá ver o log de todos os contêineres de forma centraliza no Grafana.

```
  --log-driver loki \
  --log-opt loki-url=http://172.17.0.1:3100/loki/api/v1/push \
```

Acesse a URL http://IP-SERVER:81 e configure o Gogs. Ao acessar o sistema pela primeira vez, serão solicitadas algumas informações para configurá-lo.

* No campo *Tipo de Banco de Banco de dados (Database Type)* selecione ``Postgresql``.
* Em *Host* informe ``172.17.0.1:5432`` (o IP padrão da interface ``docker0``).
* Informe a palavra ``gogs`` nos campos: *usuário*, *senha* e *nome do banco de dados*.
* Em *Modo SSL*, selecione a opção ``Disable``.

![alt text](images/config_gogs0.png "Configurando o acesso ao banco de dados pelo Gogs")

Na mesma página, um pouco mais abaixo, conforme mostra a figura a seguir, altere os seguintes campos.

* Em *Domínio (Domain)* informe ``domain.com.br``. 
* Em *URL do aplicativo* insira ``http://pipeconf-server.domain.com.br``.
* Os demais campos continuam com os valores padrão.

Crie uma conta para o usuário ``root`` no Gogs com a senha ``adminadmin``.

> **Atenção**: O usuário ``root`` do Gogs não tem relação com o usuário ``root`` do sistema operacional.

> **Atenção**: No ambiente de produção a senha do usuário ``root`` do Gogs deve ser outra, mas você precisará lembrar durante a criação de uma credencial no Jenkins.

No campo *Email do administrador* informe ``pipeconf@domain.com.br``.

Clique no botão ``Instalar Gogs``. Será exibida a página inicial do Gogs.

Referência:

* https://novatec.com.br/livros/jenkins

## Iniciando contêiner do Jenkins

Apenas no host ``PipeConf-Server``, execute os seguintes comandos para iniciar o Jenkins.

```bash
sudo mkdir -p /docker/pipeconf/jenkins/data

sudo chown -R 1000 /docker/pipeconf/jenkins/data

docker run -d -p 80:8080 -p 50000:50000 \
  --name jenkins \
  --restart always \
  -v /docker/pipeconf/jenkins/data:/var/jenkins_home \
  --dns=8.8.8.8 \
  --add-host=pipeconf-server.domain.com.br:172.17.0.1 \
  --add-host=pipeconf-node.domain.com.br:172.17.0.1 \
  --env JAVA_OPTS="-Dcom.cloudbees.workflow.rest.external.JobExt.maxRunsPerJob=50" \
  jenkins/jenkins:lts
```

Será criado o contêiner do Jenkins com as seguintes informações.

| Properties           | Default Values                |
|:-------------------- |:------------------------------|
| Port HTTP host       | 80/TCP                        |
| Port HTTP contêiner  | 8080/TCP                      |
| Port JNDI host       | 50000/TCP                     |
| Port JNDI contêiner  | 50000/TCP                     |
| User                 | admin                         |
| Password             | adminadmin                    |
| Volume dir host      | /docker/pipeconf/jenkins/data |
| Volume dir contêiner | /var/jenkins_home             |

Veja o log do Jenkins com o seguinte comando.

```bash
docker logs -f jenkins
```

> Atenção!!! Se quiser centralizar o log do contêiner no Loki + Grafana, conforme mostrado no arquivo [Logging.md](Logging.md), inclua as seguintes opções no comando de inicialização do contêiner. Dessa forma ao invés de olhar o log individualmente com o comando ``docker logs -f NOME_CONTEINER``, você poderá ver o log de todos os contêineres de forma centraliza no Grafana.

```
  --log-driver loki \
  --log-opt loki-url=http://172.17.0.1:3100/loki/api/v1/push \
```

Acesse a URL http://IP-SERVER e configure o Jenkins para acessar usando os dados da tabela anterior. Defina também a senha do usuário ``admin``.

Veja o hash da senha inicial do usuário ``admin`` do Jenkins com o seguinte comando.

```bash
cat /docker/pipeconf/jenkins/data/secrets/initialAdminPassword
```

Copie e cole a senha na página de instalação do Jenkins e siga as instruções posteriores. Na parte referente a instalação de plugins, escolha a opção ``Install suggested plugins``.

No final da instalação será pedido para você alterar a senha da conta ``admin`` do Jenkins ou criar uma nova conta de usuário que terá permissão de administrador. Depois disso será solicitado para você configurar a URL de acesso ao Jenkins que será usado para complementar os links gerados pelo sistema para acessar páginas internas. A URL deve ser http://pipeconf-server.domain.com.br.

Referência:

* https://novatec.com.br/livros/jenkins
* https://docs.docker.com/config/containers/container-networking/
* https://imagineer.in/blog/docker-container-dns-issue-in-airgapped-network/
* https://stackoverflow.com/questions/44076957/jenkins-pipeline-more-rows-in-stage-view
* https://issues.jenkins-ci.org/browse/JENKINS-46137
* https://plugins.jenkins.io/pipeline-rest-api/
* https://github.com/jenkinsci/docker/blob/master/README.md

## Instalando o Salt-Sproxy

Em cada host, baixe a imagem Docker do Salt SProxy (Super Proxy) com o comando a seguir.

```bash
docker pull mirceaulinic/salt-sproxy:allinone-2020.7.0
```

Referência:

* https://salt-sproxy.readthedocs.io/en/latest/examples/index.html
* https://salt-sproxy.readthedocs.io/en/latest/
* https://salt-sproxy.readthedocs.io/en/latest/#docker
* https://hub.docker.com/r/mirceaulinic/salt-sproxy/

# Configurando o Jenkins

## Gerenciando Plugins

Para adicionar, atualizar, remover, habilitar ou desabilitar plugins no Jenkins, acesse o menu *Gerenciar Jenkins > Gerenciar Plugins (Manage Jenkins > Manage Plugins)*.

* No campo *Filtrar (Filter)* é possível pesquisar pelo nome de plugins de acordo com a aba selecionada.

* Na aba *Atualizações (Updates)* são exibidas as novas versões dos plugins instalados. Para atualizar, basta clicar ao lado de cada plugin a ser atualizado e depois clicar no botão *Baixar agora, instalar e depois reiniciar (Download now and install after restart)*.

* Na aba *Disponíveis (Available)* são exibidos todos os plugins que podem ser instalados. É uma lista com mais de 1400 plugins e esse número só aumenta com o tempo. Para instalar, basta clicar ao lado de cada plugin a ser instalado e depois clicar no botão *Instalar sem reiniciar (Install without restart)* ou *Baixar agora, instalar e depois reiniciar (Download now and install after restart)*. Na página https://plugins.jenkins.io você também pode pesquisar pelo nome de plugins e ter acesso a documentação de uso.

* Na aba *Instalados (Installed)* são exibidos os plugins instalados. Para remover, basta clicar ao lado de cada plugin a ser removido e depois clicar no botão *Desinstalar (Uninstall)*. Nesta página nem todos os plugins instalados apresentam a opção de serem removidos individualmente porque são usados como dependência para outros funcionarem. Nestes casos, tem que localizar o plugin principal e removê-lo primeiro. O nome do plugin principal é exibido quando você passa o mouse em cima do botão *Desinstalar* e deixa parado por alguns segundos.

* Na aba *Avançado (Advanced)* são exibidas outras opções de instalação de plugins (fazendo upload do pacote, por exemplo) e configuração do Proxy HTTP, caso seja necessário para que o Jenkins tenha acesso à Internet.

Instale os seguintes plugins:

* Node and Label parameter;
* Git
* Gitlab
* Gitlab Hook
* Gogs
* Slack Notification
* Blue Ocean Core JS;
* Pipeline SCM API for Blue Ocean;
* Blue Ocean;
* Common API for Blue Ocean;
* Dashboard for Blue Ocean;
* Git Pipeline for Blue Ocean;
* GitHub Pipeline for Blue Ocean;
* Personalization for Blue Ocean
* Pipeline implementation for Blue Ocean;
* REST API for Blue Ocean;
* REST Implementation for Blue Ocean
* Web for Blue Ocean;
* Blue Ocean Pipeline Editor.

## Gerenciando Credenciais

O Jenkins pode armazenar os seguintes tipos de credenciais:

* *Secret text* – corresponde a um token de uma API (*Application Programming Interface*), como por exemplo o token de acesso de determinado usuário no Github;
* *Username and password* – que podem ser manipulados como componentes separados ou como um par de strings separado por dois pontos no formato ``username:password``;
* *Secret file* – corresponde ao conteúdo secreto em um arquivo;
* *SSH Username with private key* – corresponde a um par de chaves pública/privada do SSH;
* *Certificate* – um arquivo de certificado PKCS#12 e, opcionalmente, a senha do certificado;
* *Docker Host Certificate Authentication credentials* – corresponde as credenciais do certificado de autenticação do Docker Host.

Outros tipos de credenciais podem ser adicionados ao Jenkins por meio de plugins e para aumentar a segurança, as credenciais configuradas no Jenkins são armazenadas em um formulário criptografado no node ``master`` do Jenkins.

Cada credencial é associada a um ID, que pode ser definido manualmente ou de forma automática. Quando precisar acessar o conteúdo de determinada credencial, basta referenciar a ID. Isso minimiza as chances de expor as credenciais reais.

Para cadastrar uma nova credencial do tipo ``username and password`` execute os seguintes passos.

* Acesse novamente o dashboard inicial do Jenkins.
* Clique no menu *Credentials -> System -> Global credentials (unrestricted)*.
* Clique no menu *Add Credentials*.
* No campo *Kind*, escolha a opção *Username with password*.
* No campo *Scope* escolha a opção *Global (Jenkins, nodes, items, all child items etc)*.
* No campo *Username* informe o usuário ``pipeconf``.
* No campo *Password* informe a senha do usuário pipeconf.
* No campo *ID*, informe ``pipeconf-node-ssh-access``.
* No campo *Description* informe um texto para facilitar a identificação desta credencial futuramente, exemplo: ``Credencial de acesso via SSH ao host pipeconf-node``.
* Ao final clique no botão *Save*.

Repita o procedimento para criar uma credencial de acesso ao Gogs.

* Acesse novamente o dashboard inicial do Jenkins.
* Clique no menu *Credentials -> System -> Global credentials (unrestricted)*.
* Clique no menu *Add Credentials*.
* No campo *Kind*, escolha a opção *Username with password*.
* No campo *Scope* escolha a opção *Global (Jenkins, nodes, items, all child items etc)*.
* No campo *Username* informe o usuário ``root`` do Gogs.
* No campo *Password* informe a senha do usuário root do Gogs.
* No campo *ID*, informe ``git-access``.
* No campo *Description* informe um texto para facilitar a identificação desta credencial futuramente, exemplo: ``Credencial de acesso ao Git``.
* Ao final clique no botão *Save*.

Crie o canal ``pipeconf`` e uma conta no site https://slack.com.

Execute o procedimento a seguir criar uma credencial de acesso ao Slack.

* Acesse novamente o dashboard inicial do Jenkins.
* Clique no menu *Credentials -> System -> Global credentials (unrestricted)*.
* Clique no menu *Add Credentials*.
* No campo *Kind*, escolha a opção *Secret text*.
* No campo *Scope* escolha a opção *Global (Jenkins, nodes, items, all child items etc)*.
* No campo *Secret* informe o ID de acesso ao Slack.
* No campo *ID*, informe ``slack``.
* No campo *Description* informe um texto para facilitar a identificação desta credencial futuramente, exemplo: ``Credencial de acesso ao Slack``.
* Ao final clique no botão *Save*.

## Gerenciando as Configurações do Jenkins

Para definir ou alterar as configurações do Jenkins ou de algum plugin, acesse o menu *Gerenciar Jenkins > Configurar o sistema (Manage Jenkins > Configure System)*. Será exibida uma página dividida em várias seções nas quais você pode configurar os parâmetros conforme as necessidades e requisitos do ambiente. As seções desta página aumentam ou diminuem à medida que os plugins são instalados ou removidos.

Por enquanto, realize as seguintes configurações:

> **Atenção**: Aqui não será explicado como configurar um servidor de envio de email. Há vários tutoriais na Internet que ensinam a fazer isso. Mude os valores nos campos de configuração de email de acordo com o seu ambiente.

* Na seção *Jenkins Location*, altere o campo *Endereço de e-mail do administrador do sistema (System Admin e-mail address)* para conter o valor ``pipeconf@domain.com.br``. Quando o Jenkins enviar uma notificação por email, esse endereço será o remetente da mensagem.

* Na seção *Notificação de E-mail (E-mail-Notification)*:

  * Altere o campo *Servidor SMTP (SMTP Server)* para conter o valor ``172.17.0.1``. Esse é o IP padrão do Docker.

  * Altere o campo Sufixo padrão para e-mail de usuário (*Default user e-mail suffix*) para conter o valor ``@domain.com.br``.

  * Clique no botão *Avançado (Advanced)*. No campo *Porta SMTP (SMTP Port)* informe o valor ``25``. Essa é a porta padrão do Postfix quando não utilizado SSL.

  * Teste o envio de email clicando no marcador ao lado do campo Configuração de teste para enviar e-mail (*Test configuration by sending test e-mail*). Informe o seu email pessoal e clique no botão *Test Configuration*. Verifique se você recebeu um email de teste. Caso não tenha recebido, verifique o motivo analisando o arquivo de log ``/var/log/mail.log`` da máquina virtual ``pipeconf-server``.

* Na seção *Slack* :

  * No campo *Workspace* informe o nome do worskace a ser usado no Slack.
  * No campo *Credential* selecione a credencial ``slack``.
  * No campo *Default channel/member id* informe o canal ``pipeconf``, criado previamente no workspace a ser usado no Slack.

* Clique no botão *Salvar (Save)*.

## Adicionando um Node Slave no Jenkins

Para adicionar um node slave no Jenkins realize o seguinte passo a passo.

Acesse o host ``pipeconf-node`` e crie um par de chaves RSA sem senha para o usuário ``pipeconf`` com os seguintes comandos. Durante a execução do comando ``ssh-keygen -t rsa`` pressione ``Enter`` para todas as perguntas realizadas.

```bash
su pipeconf

ssh-keygen -t rsa
```

Acesse o host ``pipeconf-server``. Acesse o contêiner Docker do Jenkins e crie um par de chaves RSA sem senha para o usuário jenkins com os seguintes comandos. Durante a execução do comando ``ssh-keygen -t rsa`` pressione ``Enter`` para todas as perguntas realizadas (este passo precisará ser executado uma única vez, mesmo que precise adicionar vários nodes slaves).

```bash
docker exec -i -t jenkins /bin/bash
ssh-keygen -t rsa
exit
```

Ainda no host ``pipeconf-server`` copie o arquivo ``/docker/pipeconf/jenkins/data/.ssh/id_rsa.pub`` para o host ``pipeconf-node`` com o seguinte comando. Será necessário informar a senha do usuário ``pipeconf`` criado anteriormente. Fazendo isso, você estará estabelecendo uma relação de confiança entre o contêiner Docker que executa o Jenkins e o host ``pipeconf-node`` durante o acesso via SSH.

```bash
scp /docker/pipeconf/jenkins/data/.ssh/id_rsa.pub pipeconf@pipeconf-node.domain.com.br:/home/pipeconf/.ssh/authorized_keys
```

Acesse a interface web do Jenkins e clique no menu *Gerenciar Jenkins > Gerenciar Nós (Manage Jenkins > Manage Nodes)*. Clique na opção *Novo nó (New Node)*.

Informe o nome ``pipeconf-node`` e maque a opção *Permanent Agent*. Clique no botão *OK*.

Em seguida será mostrado um outro formulário. Preencha as informações conforme mostrada na figura a seguir. Atente para o fato de que no campo *Método de Lançamento (Launch method)* foi escolhida a opção *Lauch slave agents via SSH*.

![alt text](images/config_node_slave.png "Informações de cadastro do pipeconf-node")

* No campo *Host* informe ``pipeconf-node.domain.com.br``.
* No campo *Credential* selecione a credencial de acesso ao host pipeconf-node.
* No campo *Host Key Verification Strategy* escolha a opção *Non verifying Verification Strategy*.
* Confira novamente o preenchimento das informações e no final clique no botão *Salvar (Save)*.
* Agora clique no botão *Lauch Agent* e um agente Jenkins será instalado no host ``pipeconf-node``.
* Ao término da instalação, o menu *Gerenciar Jenkins > Gerenciar Nós (Manage Jenkins > Manage Nodes)*. O novo node será exibido

## Criando os Pipelines

Crie um pipeline para cadastrar ativos de rede com os seguintes passos.

* Clique em *Novo job (New item)*.
* Informe o nome *addDevice*.
* Clique na opção *Pipeline* e, em seguida clique no botão *OK*.
* Na página seguinte, localize a seção *Pipeline*.
* No campo *Definition* escolha a opção ``Pipeline script from SCM``.
* No campo *SCM*, escolha a opção ``Git``.
* No campo *Repository URL* informe o endereço ``http://pipeconf-server.domain.com.br:81/root/pipeconf``.
* No campo *Credentials* selecione a credencial de acesso ao Gogs.
* No campo *Branches to build* informe o valor ``*/master``.
* No campo *Script Path* informe o valor ``jenkins/pipelines/Jenkinsfile_addDevice.groovy``.
* Clique no botão *Salvar (Save)*.
* Em seguida, clique em *Construir Agora (Build Now)*.

> **Atenção**: É esperado que este pipeline falhe na primeira vez, pois os campos de parâmetros ainda não foram criados. Execute uma segunda vez.

Crie um pipeline para remover ativos de rede com os seguintes passos.

* Clique em *Novo job (New item)*.
* Informe o nome *removeDevice*.
* Clique na opção *Pipeline* e, em seguida clique no botão *OK*.
* Na página seguinte, localize a seção *Pipeline*.
* No campo *Definition* escolha a opção ``Pipeline script from SCM``.
* No campo *SCM*, escolha a opção ``Git``.
* No campo *Repository URL* informe o endereço ``http://pipeconf-server.domain.com.br:81/root/pipeconf``.
* No campo *Credentials* selecione a credencial de acesso ao Gogs.
* No campo *Branches to build* informe o valor ``*/master``.
* No campo *Script Path* informe o valor ``jenkins/pipelines/Jenkinsfile_removeDevice.groovy``.
* Clique no botão *Salvar (Save)*.
* Em seguida, clique em *Construir Agora (Build Now)*.

> **Atenção**: É esperado que este pipeline falhe na primeira vez, pois os campos de parâmetros ainda não foram criados. Execute uma segunda vez.

Crie um pipeline para fazer backup e gerenciar a configuração dos ativos de rede com os seguintes passos.

* Clique em *Novo job (New item)*.
* Informe o nome *devicesConfiguration*.
* Marque a opção *Build when a change is pushed to Gogs*.
* No campo *Use Gogs secret* defina uma senha (lembre-se dela para utilizar durante a configuração de um webhook no Gogs.
* Clique na opção *Pipeline* e, em seguida clique no botão *OK*.
* Na página seguinte, localize a seção *Pipeline*.
* No campo *Definition* escolha a opção ``Pipeline script from SCM``.
* No campo *SCM*, escolha a opção ``Git``.
* No campo *Repository URL* informe o endereço ``http://pipeconf-server.domain.com.br:81/root/pipeconf``.
* No campo *Credentials* selecione a credencial de acesso ao Gogs.
* No campo *Branches to build* informe o valor ``*/master``.
* No campo *Script Path* informe o valor ``jenkins/pipelines/Jenkinsfile_config.groovy``.
* Clique no botão *Salvar (Save)*.
* Em seguida, clique em *Construir Agora (Build Now)*.

> **Atenção**: É esperado que este pipeline falhe na primeira vez, pois os campos de parâmetros ainda não foram criados. Execute uma segunda vez.

## Configurando a Integração entre o Jenkins e o Gogs

Acesse a seguinte URL http://IP-SERVER:81.

* Faça login no Gogs com o usuário e a senha definida durante a instalação.
* Clique no botão **+**, ao lado do login no canto superior direito, e selecione a opção *Nova Migração*.
* Preencha as informações conforme mostrado na figura a seguir.

![alt text](images/config_repository_pipeconf.png "Cadastrando um repositório clone do pipeconf")

A URL usada na importação do projeto é ``https://gitlab.com/pipeconf`` e o nome do novo repositório deve ser ``pipeconf``.

* No Gogs, localize o projeto ``pipeconf`` e clique no botão *Configurações*.
* Clique no menu *Webhooks* (menu à esquerda).
* Clique no botão *Adicionar webhook*.
* Selecione a opção *Gogs* (essa área administrativa também está acessível em: http://pipeconf-server.domain.com.br:81/root/pipeconf/settings/hooks).
* No campo *URL de Playload* informe a URL ``http://pipeconf-server.domain.com.br/gogs-webhook/?job=devicesConfiguration`` (conforme mostrado na documentação do plugin do webhook do Gogs https://wiki.jenkins.io/display/JENKINS/Gogs+Webhook+Plugin).
* No campo *Secret* informe a mesma senha definida durante a configuração do Pipeline ``devicesConfiguration``.
* Deixe marcado a opção *Apenas evento push*.
* Clique no botão *Adicionar Webhook*.

> **Atenção**: Este passo a passo deve ser executado sempre que você precisar integrar um Job no Jenkins a um repositório Git armazenado no Gogs de forma que a cada alteração de push no repositório Git, o build do job seja executado no Jenkins automaticamente. Um repositório Git, gerenciado pelo Gogs, pode conter vários webhooks, cada um contendo uma URL diferente redirecionando para determinado job cadastrado no Jenkins.

Acesse novamente a URL do Gogs na seguinte URL http://IP-SERVER:81.

* Clique no botão **+**, ao lado do login no canto superior direito, e selecione a opção *Novo repositório*.
* Preencha as informações conforme mostrado na figura a seguir.

![alt text](images/config_repository_device-config.png "Criando um repositório device-config")

O nome do novo repositório deve ser ``device-config``.

# Liberação das Portas no Firewall

Para a comunicação entre os ativos de rede, o **PipeConf-Server** e o **PipeConf-Node** funcionar corretamente é necessário liberar as seguintes portas no firewall.

| Origem             | Destino                        | Porta/Protocolo  | Fluxo de Pacotes | Descrição dos Serviços        |
|:-------------------|:-------------------------------|:-----------------|:-----------------|:------------------------------|
| Rede do Usuário    | IP-PipeConf-Server             | 80/TCP           | Entrada & Saída  | Jenkins funcionando com HTTP  |
| Rede do Usuário    | IP-PipeConf-Server             | 443/TCP          | Entrada & Saída  | Jenkins funcionando com HTTPS |
| Rede do Usuário    | IP-PipeConf-Server             | 81/TCP           | Entrada & Saída  | Gogs funcionando com HTTP     |
| Rede do Usuário    | IP-PipeConf-Server             | 444/TCP          | Entrada & Saída  | Gogs funcionando com HTTPS    |
| Rede do Usuário    | IP-PipeConf-Server             | 10022/TCP        | Entrada & Saída  | Gogs funcionando com SSH      |
| IP-PipeConf-Server | IP-PipeConf-Node               | 22/TCP           | Entrada & Saída  | Node Agent do Jenkins         |
| IP-PipeConf-Node   | IP-PipeConf-Server             | 50000/TCP        | Entrada & Saída  | Node Agent do Jenkins         |
| IP-PipeConf-Node   | IP-PipeConf-Server             | 81/TCP           | Entrada & Saída  | Gogs funcionando com HTTP     |
| IP-PipeConf-Node   | IP-PipeConf-Server             | 444/TCP          | Entrada & Saída  | Gogs funcionando com HTTPS    |
| IP-PipeConf-Node   | Rede que contém ativos de rede | 22,23,80,443/TCP | Entrada & Saída  | Acesso aos ativos de rede     |

# PipeConf - Demonstration

| ![1](images/demonstration/0-devices_network.png)1-devices_network | ![2](images/demonstration/1-add_device.png)2-add_device |
|:---:|:---:|
| ![3](images/demonstration/2-pipeline_add_device_OK.png)3-pipeline_add_device_OK | ![4](images/demonstration/3-pipeline_add_device_FAIL.png)4-pipeline_add_device_FAIL |
| ![5](images/demonstration/4-list_jobs_add_device.png)5-list_jobs_add_device | ![6](images/demonstration/6-select_device_backup.png)6-select_device_backup |
| ![7](images/demonstration/7-select_device_backup.png)7-select_device_backup | ![8](images/demonstration/8-pipeline_device_backup_OK.png)8-pipeline_device_backup_OK |
| ![9](images/demonstration/9-pipeline_device_backup_FAIL.png)9-pipeline_device_backup_FAIL | ![10](images/demonstration/10-list_jobs_device_backup.png)10-list_jobs_device_backup |
| ![11](images/demonstration/11-backup_config_device_git.png)11-backup_config_device_git | ![12](images/demonstration/12-list_devices_backup_config_git.png)12-list_devices_backup_config_git |
| ![13](images/demonstration/13-list_files_backup_config_git.png)13-list_files_backup_config_git | ![14](images/demonstration/14-content_backup_config_git.png)14-content_backup_config_git |
| ![15](images/demonstration/15-list_devices_network_type_git.png)15-list_devices_network_type_git | ![16](images/demonstration/16-list_devices_network_git.png)16-list_devices_network_git |
| ![17](images/demonstration/17-config_file_encripted_devices_network_git.png)17-config_file_encripted_devices_network_git | ![18](images/demonstration/18-example_notification_email_FAIL.png)18-example_notification_email_FAIL |
| ![19](images/demonstration/19-example_notification_email_OK.png)19-example_notification_email_OK | ![20](images/demonstration/20-example_notification_slack.png)20-example_notification_slack |
