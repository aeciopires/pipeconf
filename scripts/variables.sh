#!/bin/bash

#------------------------------------------------------------
# Default variables of PipeConf
# change as needed
# Load with ". ./variables.sh"
#------------------------------------------------------------

# Postgresql variables
POSTGRESQL_CONTAINER_NAME='postgresql'
POSTGRESQL_VERSION='12'
POSTGRESQL_USER='gogs'
POSTGRESQL_PASSWORD='gogs'
POSTGRESQL_DATABASE='gogs'
POSTGRESQL_HOST_PORT=5432

# Gogs variables
GOGS_VERSION='0.12.3'
GOGS_CONTAINER_NAME='gogs'
GOGS_HOST_PORT_SSH=10022
GOGS_HOST_PORT_WEB=81
GOGS_USER='root'
GOGS_PASSWORD='adminadmin'
GOGS_SSL=false

# Jenkins variables
JENKINS_CONTAINER_NAME='jenkins'
JENKINS_VERSION='lts'
JENKINS_HOST_PORT_JNDI=50000
JENKINS_HOST_PORT_WEB=80

# PipeConf variables
PIPECONF_USER='pipeconf'
PIPECONF_HOME="/home/$PIPECONF_USER"
PIPECONF_DATA_DIR="/docker/$PIPECONF_USER"
PIPECONF_DOMAIN_NAME='domain.com.br'
PIPECONF_MAIL="${PIPECONF_USER}@${PIPECONF_DOMAIN_NAME}"
PIPECONF_SERVER_HOSTNAME="pipeconf-server.${PIPECONF_DOMAIN_NAME}"
PIPECONF_SERVER_ADDRESS='172.17.0.1'
PIPECONF_NODE_HOSTNAME="pipeconf-node.${PIPECONF_DOMAIN_NAME}"
PIPECONF_NODE_ADDRESS='172.17.0.1'

# Salt-SProxy variables
SALT_SPROXY_VERSION='allinone-2020.7.0'

# CAdvisor variables
CADVISOR_VERSION='v0.45.0'
CADVISOR_HOST_PORT=82

# Node Exporter variables
NODE_EXPORTER_VERSION='v1.3.1'

# Sops variables
SOPS_CONFIG_FILE="$PIPECONF_HOME/.sops.yaml"
CLOUD_KMS='aws'