{
	"data": "ENC[AES256_GCM,data:ZisRVB5ndPZHY4EceXPgWYnC27Bi+RcKoELZwQ9iU+IN8FnQee7hNZgSu1S2kIo0WCYgwt3LwBK2eIfHRXIfGWCp6mCqTbgtJ0jW4fZg2XLwhnidZU87Q76D728WOqqRC+eeEQ2g4Iqn5IrQx6140Uf+X2X7Lu6ecTwZYrICWadss0vyh/f0jFv2NpqAPdQynv7MJrxPNXhu8hf3LqS2iw4CdaRZVEuXBhNxYvYDxbDLL3RDa7u3pHrk+cFOp32Dy0bCmzOkkunMn9IrilG/yYPL7pgDnx2+LBknrssx/6cP2/JG2mzJdMCeDDQThWy/qPMBREpG6LaiZw7pSsONVaCBFlDpC/7wgK/z3IUY1Y0WdI4HlJS7Q4P0SKL2+wm+tdw7tQkejFQ5wt1OWnWXNUOMUgzbOZABzBFaRzwWsdTbXIS37RO3WqBaj8r1jn0WczONuf1/HKzS3g==,iv:6xo12be6DXm+RKspJPpyvBw4BFnLKrgFKDxSoj2gedc=,tag:QSxC55XgzlguVrUZ1iuaLg==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-21T21:40:49Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwErqBXPWl6A68NcjfYkRsXLAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMDdDlQM4kzpEmUCI7AgEQgDuyNkxwNAhzQoGjqt/vXC6q4xoD5flQcy3EcD7FDNk5Tq7kb5B5bDg8H/XpCh+Q4m5IcHpPnH3q5b0Ixg==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-21T21:40:50Z",
		"mac": "ENC[AES256_GCM,data:GI1IeTCzvwQvT3Ra2CERUGRoXXXgvs8C6slaDDGXc7FG2WvC313UmrBhcqt3TH9STDIVO7MY4XVe66HzYo2ezlEIllGGnB6Ms3Y+gwwhDiWVesMRpl721SSdfX5swnjgVuSmSLfLIelxmRqVm0cQtM6bEoPgz0WDllaiCMdGwvg=,iv:qMrCh2L06HhVG2UnN+HwVQnwDK5l5PWUwRsThEfDdZM=,tag:G2mYHhNkoE4AHcS+EchlSw==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}