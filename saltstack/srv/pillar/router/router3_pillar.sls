{
	"data": "ENC[AES256_GCM,data:4UbujE+ST7gRQgfdHax0BLrv0GkZj5LjJfIKUT5HNI6c/1cmYFaoIjnnR7xOeW7MaASbzWz5KDtEo+sDai63r868qewqwWBHArwKaGZOzMSax5ayAoxFnfilZMCo/cstw0AOtZ4OC0gtehwGKhgY2Ij+3WZA4u8kc/1TzFzhWQyvzPxMztHCD5VjT8m9ywesaI/a5uw8l3aP4Kl03tciaR5KsDm4MTWMPFpMCa68T+3NOpGpVwIEW+AUAn9VMR1Tr73b5vVubtTv5nmum4AYMoMxstpc1UUj0CglQCDHfY9YsEn6KHomIng+Ry+9aIO7aMWFGeLRIzG6uLqZj8BrzZio61KRexg1U/kG78+CBAe8t9Qi3N6V/FfvlKC6ZIulf52xZoLOvqksBJaGDn6mUZhlEl820r1CpnGBakQDm8YWwDLiF8XrlyH37j4NB6bvGtu6JCDWsgVq9EJWD/mFkng6hruv5xet4dgoVe0sJnQF438yOwtTgZbH7NY6at05ZabGHktH,iv:dwA1M5IVnMa4ArHJqRO3KJWcwCK7IpTzjI6x7SWKs3Q=,tag:Npibcmi1wHb/7ihZv5MnaQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-15T07:00:55Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHjJ2wDcjWkcSQ4B2jf5LnTAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMdkbZT94eZcmyzuVBAgEQgDu1EV0BdIYy8KBlRz1ioORZVVPqFvoac5kemCJqFphAHIiPUACUXZfSi/WgI8IVbEazO3El6jOYL+pIGQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-15T07:00:56Z",
		"mac": "ENC[AES256_GCM,data:0kBh6hCiD8uL/Ak+mp8pxfR1cBQut1D0/+DxHljq64btthGMp+xeMLCDDdFIVy39Qd5L1d8ck0Vne//36fN3ilz+7LoBr3Ie6cDJMKfi8OYAAMoiJruT3upEY7aLPXieRVk8WGw43jT6t3WuqSsybuCDegCYMyjQe6E+582gHzg=,iv:J71JIZziHuD5rfgocjz1MiZNfAod7e4xcOLDE+luhME=,tag:J/974BJpjjsqCxL86I/zoQ==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}