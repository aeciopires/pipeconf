<!-- TOC -->

- [Workflow do cenário 4 de avaliação do PipeConf](#workflow-do-cenário-4-de-avaliação-do-pipeconf)
- [Workflow manual](#workflow-manual)
  - [Pré-requisitos](#pré-requisitos)
  - [Passos do workflow manual](#passos-do-workflow-manual)
  - [Comandos para o roteador Cisco modelo c3640](#comandos-para-o-roteador-cisco-modelo-c3640)
- [Rascunho](#rascunho)
  - [Comandos para o switch Cisco modelo 2960 24TTL](#comandos-para-o-switch-cisco-modelo-2960-24ttl)

<!-- TOC -->

# Workflow do cenário 4 de avaliação do PipeConf

**Objetivo**: Comparar as métricas de tempo o PipeConf e um administrador de rede utilizam para gerenciar a configuração de **n** ativos de rede como código, sendo **n=[1, 2, 4, 8, 16, 32, 64]**.

Edite os arquivos de pillar dos ativos de rede e adicione a seguinte configuração.

```yaml
ntp.servers:
 - a.ntp.br
```

Faça commit das mudanças.

Localização dos arquivos de pillar para roteadores: http://pipeconf-server.domain.com.br:81/root/pipeconf/src/master/saltstack/srv/pillar/router

Fonte:

* S. House and M. Ulinic, Network Automation at Scale, O’Reilly Media,Sebastopol, vol. 1, 2017, pag 48.

# Workflow manual

## Pré-requisitos

Crie um servidor FTP com os seguintes comandos:

```bash
sudo mkdir -p /docker/ftp

sudo chmod -R 777 /docker/ftp

docker run -d -v /docker/ftp:/home/vsftpd -p 20:20 -p 21:21 -p 47400-47470:47400-47470 -e FTP_USER=teste -e FTP_PASS=teste -e PASV_ADDRESS=172.17.0.1 --name ftp --restart=always bogem/ftp
```

Endereço do servidor FTP: ftp://172.17.0.1. Usuário **teste** e senha **teste**.

Fonte: https://medium.com/ekode/criando-um-servidor-de-ftp-com-o-docker-34f109709109

## Passos do workflow manual

**Passo 1)** Acessar o ativo de rede via SSH (*Secure Shell*) a partir do **PipeConf-Server**, informando o login, porta, senha. O servidor **PipeConf-Server** foi escolhido por ter acesso direto à rede na qual estão conectados os ativos de rede;

Via [SSH](https://www.thegeekstuff.com/2013/08/enable-ssh-cisco/):

```bash
ssh -p SSH_PORT SSH_USER@DEVICE_ADDRESS
```

Via [Telnet](http://www.ftj.agh.edu.pl/wfitj/complab/doc/catalyst/cmdref23/sw_cli.htm#xtocid191780):

```bash
telnet -l TELNET_USER DEVICE_ADDRESS TELNET_PORT
```

**Passo 2)** Habilitar o **modo administrador** do ativo de rede, informando a senha;

**Passo 3)** Acessar a seção **configuration** do ativo de rede;

**Passo 4)** Obter o backup da configuração do ativo a partir da memória (``running-config``), antes de aplicar qualquer alteração na configuração;

**Passo 5)** Configurar o NTP Server no ativo de rede, para manter a hora sincronizada;

**Passo 6)** Aplicar a configuração na inicialização do ativo (``startup-config``), para que a mesma não seja perdida se o mesmo for desligado ou reiniciado;

**Passo 7)** Obter o backup da configuração do ativo a partir da memória (``running-config``) e da inicialização (``startup-config``), após aplicar a configuração;

**Passo 8)** Encerrar a sessão remota no ativo de rede;

**Passo 9)** Criptografar os arquivos de backup da configuração usando o sops;

**Passo 10)** Enviar os arquivos de configuração para serem versionados no Git.

## Comandos para o roteador Cisco modelo c3640

| Parâmetros                  | Valores         |
|:----------------------------|:----------------|
| Usuário SSH                 | aecio           |
| Senha SSH                   | teste           |
| Senha do modo administrador | ''            |
| Porta SSH                   | 22/TCP          |
| Endereço IP                 | 192.168.122.205 |

**Passo 1)**

```bash
ssh -p 22 aecio@192.168.122.205
```

**Passo 2)**

```bash
device> enable
```

**Passo 3)**

```bash
device# config terminal
```

**Passo 4)**

```bash
device(config)# ip ftp username teste
device(config)# ip ftp password teste
device(config)# exit
device# copy running-config ftp://192.168.122.1/device1_running_before
device# copy startup-config ftp://192.168.122.1/device1_startup_before
```

Fonte:

* https://www.cisco.com/c/en/us/support/docs/ios-nx-os-software/ios-software-releases-122-mainline/46741-backup-config.html

**Passo 5)**

```bash
device# config terminal
device(config)# ntp server a.ntp.br
device(config)# exit
device# show ntp status
```

**Passo 6)**

```bash
device# write memory
```

**Passo 7)**

```bash
device# copy running-config ftp://192.168.122.1/device1_running_after
device# copy startup-config ftp://192.168.122.1/device1_startup_after
```

**Passo 8)**

```bash
device# exit
```

**Passo 9)**

```bash
cd $HOME/Desktop/
git clone http://172.17.0.1:81/root/config_manual
cd config_manual/
cp -R /docker/ftp .
chmod -R 777 ftp/
sops -e -i ftp/device1_running_before
sops -e -i ftp/device1_startup_before
sops -e -i ftp/device1_running_after
sops -e -i ftp/device1_startup_after
```

**Passo 10)**

```bash
git add *
git commit -m "Add config device1"
git push
cd $HOME
```

# Rascunho

## Comandos para o switch Cisco modelo 2960 24TTL

Simulando o switch em um contêiner Docker.

```bash
docker run -d --name device1 -p 2221:22 -e SWITCH_MODEL="cisco_generic" internap/fake-switches
```

| Parâmetros                  | Valores    |
|:----------------------------|:-----------|
| Usuário SSH                 | root       |
| Senha SSH                   | root       |
| Senha do modo administrador | root       |
| Porta SSH                   | 2221/TCP   |
| Endereço IP                 | 172.17.0.1 |
