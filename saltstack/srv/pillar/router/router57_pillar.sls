{
	"data": "ENC[AES256_GCM,data:pw4WYMdmGMy9sHy4wl0TGrTikgkBqLLjaeB9Udv42ALl6eeMFKABxeQURj/CXYSxIg+fVtAyQQpORgr/Vs5codnP2H6GmDysnsHmNZMztPc2CbGVCmm9Bqp16MR7uhuR9Y1xg9yhjo4GxxIhJ6dCjFAD+uY7YZWNbn66oRW52lAsAUndMsmF9d4+HbR+Z1F1IoZYQThKcGRZYuytuQhl8umgsMtzEJUT/vJLAjOb1g7S+vvYxd6pCfSnocWC6JmkkuAFXAOSySHd/g48vQwXPNZeyo9Ow/kHVxaYbQd8bXIc8ezCSnvkaxDbI13D1YJKcsabpJyecBrBkcdEoTZU3Z17cpaICVkGxOCnabtuANIVv7nAXhAxc0fv12S/UiUntwvQ8KVvQqlAW4QEgKqHAF+bN0AxAOOdwtg3+AAEI1FyC2Ql+UHHN3ONCjyyJdx+spfXlemKNpkFkIq7LfJnuwBlWm5Jw8UJ5QVkqlVTXM4wigBW69jbjPSa3RBN2Xag+pmiVA==,iv:YCKdV7q+OKhDs3U1SwS1l0iFOp+knSlfKCxDupdQdAQ=,tag:JN98xGM8zFQosupN+3Gafg==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:34Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwG02huXVpLW8KDG0TfLzLKoAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMQ4reJabC8cdBesfrAgEQgDvD8OHGiEGBa2I0G0xv+KlCz2cyL1DG5gdLi+8YOx+Fwt0B3Xnh2Z8nbyhtRGgHBhekJ9xNndWYgvgp4w==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:34Z",
		"mac": "ENC[AES256_GCM,data:ZFJzkTbyn8C4U+7rd8UBblNBllLg5rbwfR+LwsqZEI1LcUE6TlGcwTdlrvm+jYXlNfKIZpJj83XC1bgrhYCL2GsqiDdSYnSdlR/3W6n/lZ2aPndm5xO3WT43cx8xU+US11wFGm33bmE42Vut4DnWoLsw9MXFk7aLA3rhPnUtMFY=,iv:DhSA+rW6f3/6k+TZaOAgNGfl19Vw0Sp5ARgULzRkxys=,tag:yqvL2jxlBQk9PaPcpZ3Dbw==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}