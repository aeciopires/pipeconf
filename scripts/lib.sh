#!/bin/bash

#------------------------------------------------------------
# Default function to general use
# Do nothing. Just define functions.
# Load with ". ./lib.sh"
#------------------------------------------------------------


#--------------------------------------------------------
# comment: Retries a command on failure.
#
# sintax:
# retry number_attempts command
#
# Example:
# retry 5 ls -ltr foo
#
# Reference: http://fahdshariff.blogspot.com/2014/02/retrying-commands-in-shell-scripts.html
#
function retry() {

# Creates a read-only local variable of integer type
local -r -i max_attempts="$1"; shift
# Create a read-only local variable
local -r command="$@"
# Create a local variable of integer type
local -i attempt_num=1
local -i time_seconds=30


until $command; do
    if (( attempt_num == max_attempts )); then
        echo "[ERROR] Attempt $attempt_num failed and there are no more attempts left!"
        return 1
    else
        echo "[WARNING] Attempt $attempt_num failed! Trying again in $time_seconds seconds..."
        sleep $time_seconds
    fi
done

}


#--------------------------------------------------------
# comment: Check if exists command
#
# sintax:
# checkCommand command1 command2 command3
#
# return: 0 is correct or code error
#
# Example:
# checkCommand git kubectl helm
#
function checkCommand() {

local commands="$@"

for command in $commands ; do
    if ! type $command > /dev/null 2>&1; then 
        echo "[ERROR] Command '$command' not found."
        exit 4
    fi
done

}


#------------------------------------------------------------
# comment: Check if file exists.
#
# syntax:
# checkFiles file [file ...]
#
checkFiles(){

# echo --
# echo FILES=$@
OK=YES
for file in "$@" ; do
    if [ ! -f "$file" ] ; then
        echo "[ERROR] File '$file' not found."
        OK=NO
        exit 1
    fi
done

}


#--------------------------------------------------------
# comment: Check if a variable is empty
#
# sintax:
# checkVariable name value
#
# return: 0 is correct or code error
#
# Example:
# checkVariable "variable1" "value1"
#
function checkVariable() {

local variable_name="$1";
local value="$2";
local debug="${DEBUG}"

if [ -z $value ] ; then 
    echo "[ERROR] The variable $variable_name is empty."
    # The function usage must create in script main
    usage
    exit 3
else
    if [ "$debug" == true ]; then
        echo "[DEBUG] ${variable_name}: ${value}"
    fi
fi
}



#--------------------------------------------------------
# comment: Check if container exists and remove if stopped
#
# sintax:
# checkContainer name
#
# Return: 0 if it exists and is running
#         1 if it does not exists or has been removed because it is stopped
#
# Example:
# checkContainer node-exporter
#
# Reference: https://stackoverflow.com/questions/38576337/how-to-execute-a-bash-command-only-if-a-docker-container-with-a-given-name-does
#
function checkContainer() {

local container_name="$1";

if ! docker top "${container_name}" > /dev/null 2>&1 ; then
    # Container not exists or is stopped
    docker rm "${container_name}"
    return 1
else
    # Container is running
    return 0
fi
}


#----------------------------------------------------
# comment: Converts lowercase letters to uppercase
# syntax: AUX=$(toupper $STRING)
# return: $AUX containing the uppercase string
#
function toupper(){

tr '[a-z]' '[A-Z]' <<< $*
}


#----------------------------------------------------
# comment: Converts uppercase letters to lowercase
# syntax: AUX=$(tolower $STRING)
# return: $AUX containing the lower case string
#
function tolower(){

echo $* | tr '[A-Z]' '[a-z]'
}


#--------------------------------------------------------
# comment: Get distribution name of GNU/Linux
#
# return: 0 is correct or code error
#
# Example:
# getDistribution "variable1" "value1"
#
# Reference: https://get.docker.com/
#
function getDistribution() {

local lsb_dist=""
local dist_version=""
local filename="/etc/os-release"
# Every system that we officially support has /etc/os-release

# Testing if file exists
checkFiles "$filename" || exit 4

if [ -r $filename ]; then
    lsb_dist="$(. $filename && echo "$ID")"
    lsb_dist=$(tolower "$lsb_dist")
fi

case "$lsb_dist" in
    ubuntu)
        if checkCommand lsb_release; then
            dist_version="$(lsb_release --codename | cut -f2)"
        fi

        if [ -z "$dist_version" ] && [ -r /etc/lsb-release ]; then
            dist_version="$(. /etc/lsb-release && echo "$DISTRIB_CODENAME")"
        fi

        echo "[OK] GNU/Linux distribution supported."
    ;;
    *)
        echo "[ERROR] GNU/Linux distribution not supported yet."
        exit 1
    ;;
esac
}


#--------------------------------------------------------
# comment: Verify support to cloud to update customer
# usage: cloudSupported CLOUD_NAME
# return: Nothing if supported or exit with code 1 if unsupported.
#
function cloudSupported(){

local cloud="$1"

case ${cloud} in
    aws|gcp)
        echo "[OK] Cloud supported: $cloud."
    ;;
    *)
        echo "[ERROR] Unsupported cloud yet."
        exit 1
    ;;
esac
}


#--------------------------------------------------------
# comment: Check semantic version
# sintax:
#   checkSemanticVersion STRING
# return: 0 is correct or code error
#
# Reference: https://stackoverflow.com/questions/24318927/bash-regex-to-match-semantic-version-number
#
function checkSemanticVersion() {

local version="$1"; 

SEMANTIC_VERSION_REGEX="^(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)(\\-[0-9A-Za-z-]+(\\.[0-9A-Za-z-]+)*)?(\\+[0-9A-Za-z-]+(\\.[0-9A-Za-z-]+)*)?$"

if [[ ! "$version" =~ $SEMANTIC_VERSION_REGEX ]]; then
    echo "[ERROR] The string $version isn't compatible with semantic version: https://semver.org"
    return 1
fi

local result_code=$?

return $result_code
}


#------------------------------------------------------------
# comment: Check if the user is root
# syntax: 
#   isRoot
# return: 0 if root or 1 if not root
#
function isRoot(){

local user_id

user_id=$(id | cut -d= -f2 | cut -d\( -f1)

if [ "$user_id" -eq 0 ]; then
    echo YES
    return 0
else
    echo NO
    return 1
fi
}


#------------------------------------------------------------
# comment: Check if the directory exists.
# syntax: 
#   checkDir DIR
# return: 0 is exists or 1 if not exists
#
function checkDir(){

local dir=$1

[ -d "$dir" ] && return 0 || return 1
}


#------------------------------------------------------------
# comment: Create directory using function checkDir.
# syntax: 
#   createDir DIR
#
function createDir() {

local dir=$1

if ! checkDir "$dir"; then
    echo "[INFO] Creating $dir..."
    mkdir -p "$dir"
else
    echo "[INFO] Directory exists."
fi
}


#--------------------------------------------------------
# comment: Count lines of file
# sintax:
#   countLines FILE
# return: number of lines in file
#
function countLines() {

local file="$1";
local lines=$(wc -l < "$file")

echo "$lines"
}


#------------------------------------------------------------
# comment: Execute the command and show in output (use for debug of commands)
# sintax:
#   debug COMMAND
# requirement: create variable _DEBUG_COMMAND.
#    Use the value 'on' for enable this funcion.
#    Use the value 'off' for disable this function
# Reference: https://www.cyberciti.biz/tips/debugging-shell-script.html
#
# how_to:
#
# Example 1:
# debug echo "File is $filename"
#
# Example 2:
# debug set -x
# Cmd1
# Cmd2
# debug set +x
#
function debug(){
    [ "$_DEBUG_COMMAND" == "on" ] && "$@"
}


#------------------------------------------------------------
# comment: Only show all input in output (use for mock tests)
# sintax:
#   mock "TEXT or COMMAND"
#
function mock(){
    echo "$@"
}
