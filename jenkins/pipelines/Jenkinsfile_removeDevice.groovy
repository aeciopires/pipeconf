/*
  Keeps only logs of the last 10 job runs and for 30 days.
  Allows you to select the Jenkins slave node that will run the Job. 
  It is expected to fail the first time, as this is when the pipeline will be read and the parameters will be created.
*/
properties([
  buildDiscarder(
    logRotator(
      artifactDaysToKeepStr: '',
      artifactNumToKeepStr: '',
      daysToKeepStr: '30',
      numToKeepStr: '10',
    )
  ),
  parameters([
    [$class: 'NodeParameterDefinition',
      allowedSlaves: ['pipeconf-node', 'pipeconf-node-gcp'],
      defaultSlaves: ['pipeconf-node'],
      description: 'Selecione o node PipeConf para executar este job.',
      name: 'PIPECONF_NODE',
      nodeEligibility: [$class: 'IgnoreOfflineNodeEligibility'],
      triggerIfResult: 'multiSelectionDisallowed'],
    string(name: 'DEVICES', defaultValue: 'device1,device2,router/device3_pillar.sls', description: 'Nome(s) do(s) ativo(s) de rede a ser(em) removido(s)', trim: true),
  ]),
])

pipeline {
/*
  removeDevice

  This text will be ignored during the execution of the pipeline.

  # --------------------- TASKS PIPELINE REQUIREMENTS -------------------- #

  # ------ STEPS TO BE EXECUTED ONLY IN THE 1st BUILD ------------------ #
  1) You need to add in Jenkins a new credential with the IDs:

    'git-access' => access credentials for the Git repository that
                    will store the settings of the network asset.
                    The repository URL must be defined in the variables
                   'GIT_PROTOCOL' and 'GIT_URL' in this same pipeline.
    'slack' => secret text type credential that has the Slack access ID,
               see step 7 for the ID.

  Access the Jenkins web interface and click on the menu:
  Jenkins Dashboard -> Credentials -> System.
  Then click on 'Global credentials (unrestricted)' and then click
  in: 'Add Credentials'.

  2) Configure the integration between Jenkins and each project at GIT using the
  webhook.

  3) Configure the email sending service on the host 'pipeconf-node'.
  Example: Postfix https://kifarunix.com/configure-postfix-to-use-gmail-smtp-on-ubuntu-18-04/

  4) Install and configure the Slack Notification plugin. See the tutorial on the page:
  https://plugins.jenkins.io/slack/
*/

  // Keeps only logs of the last 10 job runs and for 30 days.
  //options {
  //    buildDiscarder logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '30', numToKeepStr: '10')
  //}

  // The 'agent' section defines the name of the node registered in Jenkins that should execute this pipeline
  //agent { label 'pipeconf-node'}
  agent {
    node {
      label "${PIPECONF_NODE}"
    }
  }

  // The Parameters section is used to receive user input. 
  // It is expected to fail the first time, as this is when the pipeline will be read and the parameters will be created
  /*
  parameters {
      string(name: 'DEVICES', defaultValue: 'device1,device2,router/device3_pillar.sls', description: 'Nome(s) do(s) ativo(s) de rede a ser(em) removido(s)', trim: true),
  }
  */

  // The 'environment' section contains the declaration of variables used in the pipeline
  environment {
    // Environment variables should preferably be capitalized
    // to facilitate the differentiation of pipeline reserved words.
    // 'null', means that the variable will start with the null value (empty)
    LAST_TAG = null

    // Single quotes are used to unify the text
    WORKSPACE_DIR      = pwd()
    GIT_PROTOCOL       = 'http'
    GIT_URL            = 'pipeconf-server.domain.com.br:81/root/pipeconf'
    GIT_BRANCH         = 'master'
    MAIL_SENDER        = 'jenkins@pipeconf-server.domain.com.br'
    MAIL_SYSADMIN      = 'aeciopires@gmail.com'
    //MAIL_SYSADMIN      = 'jenkins@pipeconf-server.domain.com.br'
    MAIL_TEAM          = 'jenkins@pipeconf-server.domain.com.br'
    DEBUG              = true
    MAIL_NOTIFICATION  = true
    SLACK_NOTIFICATION = true

    // Salt values
    DIR_SALT_BASE         = "${WORKSPACE_DIR}/saltstack/srv"
    DIR_SALT_PROXY_PILLAR = "${DIR_SALT_BASE}/pillar/"
  }

  stages {
    stage('Checking conditions') {
      steps {
        checkConditions()
      }
    }
    stage('Remove file asset') {
      steps {
        sh """chmod +x jenkins/pipelines/*.sh;
              bash jenkins/pipelines/removeDevice.sh ${DEVICES} ${DIR_SALT_PROXY_PILLAR};
           """
      }
    }
    stage('Send changes to git') {
      steps {
        dir("$WORKSPACE_DIR") {
          // Sending the registration of network assets to Git
          withCredentials([usernamePassword(credentialsId: 'git-access', passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
            sh("""git config --global user.email \$USER@\$(hostname --fqdn);
                  git config --global user.name \$USER;
                  git config --global push.default matching;
                  git checkout -B ${GIT_BRANCH};
                  git add *;
                  git pull origin ${GIT_BRANCH};
                  if git diff-index --quiet HEAD; then
                    echo "[INFO] Nothing to commit";
                  else
                    echo "[INFO] Send changes to git repository";
                    git commit -m '${currentBuild.fullDisplayName} - Job de remoção de ativos de rede';
                    git push ${GIT_PROTOCOL}://${GIT_USERNAME}:${GIT_PASSWORD}@${GIT_URL} ${GIT_BRANCH};
                  fi""")
          }
        }
      }
    }
  }
  post {
    always {
      echo 'Removendo arquivo temporários do workspace.'
      deleteDir()
    }
    success {
      script {
        if (MAIL_NOTIFICATION == 'true') {
          mailSuccess()
        } else {
          echo "[INFO] Email notification is disabled."
        }
      }
    }
    failure {
      script {
        if (MAIL_NOTIFICATION == 'true') {
          mailFailure()
        } else {
          echo "[INFO] Email notification is disabled."
        }

        if (SLACK_NOTIFICATION == 'true') {
          slackFailure()
        } else {
          echo "[INFO] Slack notification is disabled."
        }
      }
    }
  }
}

/*
  My functions
*/

void checkConditions() {
  script {
    if (DEVICES == ''){
      error '[ERROR] Não foi informado um ou mais nomes de ativos a serem removidos.'
    } else {
      input (message: "Tem certeza de que deseja remover esse(s) ativo(s): ${DEVICES} ?")
    }
  }
}

void mailSuccess() {
  // When the pipeline runs successfully, the following email will be sent
  mail to: "${MAIL_SYSADMIN}", cc: '', bcc: '',
  from: "${MAIL_SENDER}",
  subject: "[OK] Removido(s) o(s) ativo(s) de rede: '${DEVICES}' - Job: ${currentBuild.fullDisplayName}",
  body: " Foi removido o arquivo de configuração do ativo de rede '${DEVICES}'. \
        \n Acesse o Dashboard a seguir para visualizar o resumo do log. \
        \n ${env.JENKINS_URL}/blue/organizations/jenkins/${env.JOB_NAME}/detail/${env.JOB_NAME}/${env.BUILD_NUMBER}/pipeline \
        \n \
        \n Ou acesse a URL ${currentBuild.absoluteUrl}/consoleFull para visualizar o log completo. \
        \n \
        \n \
        \n Detalhes do job: \
        \n Nome e ID: ${currentBuild.fullDisplayName} \
        \n Duração: ${currentBuild.durationString} \
        \n Nome do node que executou o job: ${PIPECONF_NODE}"
}

void mailFailure() {
  // When the pipeline execution fails, the following email will be sent
  mail to: "${MAIL_SYSADMIN}", cc: '', bcc: '',
  from: "${MAIL_SENDER}",
  subject: "[ERROR] Falha ao remover o(s) ativo(s) de rede: '${DEVICES}' - Job: ${currentBuild.fullDisplayName}",
  body: " Falha ao remover o ativo de rede: '${DEVICES}'. \
        \n Acesse o Dashboard a seguir para visualizar o resumo do log. \
        \n ${env.JENKINS_URL}/blue/organizations/jenkins/${env.JOB_NAME}/detail/${env.JOB_NAME}/${env.BUILD_NUMBER}/pipeline \
        \n \
        \n Ou acesse a URL ${currentBuild.absoluteUrl}/consoleFull para visualizar o log completo. \
        \n \
        \n \
        \n Detalhes do job: \
        \n Nome e ID: ${currentBuild.fullDisplayName} \
        \n Duração: ${currentBuild.durationString} \
        \n Nome do node que executou o job: ${PIPECONF_NODE} \
        \n Diretório do node utilizado pelo job: ${WORKSPACE_DIR}"
}

void slackFailure() {
  // A notification will also be sent to the slack... on the #pipeconf channel
  slackSend (color: '#FF0000', message: "@here [ERROR] Falha ao remover o(s) ativo(s) de rede '${DEVICES}' a partir do node '${PIPECONF_NODE}' - Job '${currentBuild.fullDisplayName} - Acesse o log em: ${currentBuild.absoluteUrl}/consoleFull .");
}