{
	"data": "ENC[AES256_GCM,data:IHyLHjf3VBNHy6bDFSiOIPFUu3E7DTHmX8G3sqk0rceSFwU2XTlKthTLap8QuhKLmMWNFUb4O5BlYDHUwaEGXPLWs6C+cHEzgqCdWvusYM/YyFfYpcjmPGhJ7DmUBhiaRgaPEawVyTyWKHRY+txAapTa92SOghVgqtWjx2kbG8Mmi4xXwA2t7IdoXHeuAjmuPTOGr6QkaoKtCKW3hsBCd4qSepkfQrIg1gnud9iln83eIDeTmTKFmTLSXbmHc4lTMOLhAtlleqV1DdtRHqsGCusHPml6K8+BI5G1N4WiX3Elvp4uBVRaH6OBh6Il/JTxlzfahVwGVUXxjz7Csnw+T3UL2YNgAL/YpKTUnSPGf01cEms+JnkME5bPPtDV8mN2cRKyZj6DFEgseUSSjMHJocswUWXw2Wv1nyfCgEG18AeHVAfUuSy+ob3UH+0uXiRpSuS3GezW4mFkWTm10SMiIgUXpYW6JS1vBHo=,iv:K6Imj0U7eQIf1KsJMneYCAX6+zkVle/nExmkMte1ua0=,tag:4Dw4tludxkaX1Gjyl+DBdQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:32:39Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwGJKt5nNYBkcuWJWv+WQ0IOAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMo8S3adIEftOHa23JAgEQgDtUXtErbyy7VGnGOigA+FozreoRGZd/1k00IsKRTFR9WH7T40vTxnyLW44cLJkwM0QLpz392UKarDPNUw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:32:40Z",
		"mac": "ENC[AES256_GCM,data:ea49aVOvIkiAN0qpdN7op7zPoSPx4i4Miw/AGCBeynh+xZ6Qqv+5WFdNSGrACFA0QUDPZnQFXMyLrZF6wsIxX1S/V30LpnLHWK6/mobVmj35QKN8rZn5ES1hcTlfh8JvYQ956QtozHrL/NtjYVDliNCUm7mzqzsjwp6JDNpKkko=,iv:V3d5pi4YNyL8zt0FgdzvhOddAlBg5L34Ah1JNIoaQdg=,tag:+2ZVGffywTUszzz8Sjx9Fg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}