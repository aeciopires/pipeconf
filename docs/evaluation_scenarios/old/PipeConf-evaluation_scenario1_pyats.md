<!-- TOC -->

- [Workflow do cenário 1 de avaliação do PipeConf](#workflow-do-cenário-1-de-avaliação-do-pipeconf)
- [Atividades automatizadas pelo PipeConf](#atividades-automatizadas-pelo-pipeconf)
  - [Configurações do ativo de rede](#configurações-do-ativo-de-rede)
  - [Instalando o pyATS via Docker](#instalando-o-pyats-via-docker)
    - [Requisitos](#requisitos)
    - [Utilizando o pyATS](#utilizando-o-pyats)

<!-- TOC -->

# Workflow do cenário 1 de avaliação do PipeConf

**Objetivo**: Comparar as métricas de CPU, memória e o tempo que o pyATS (solução da Cisco) e o PipeConf utilizam para gerenciar a configuração de **n** ativos de rede como código, sendo **n=[1, 2, 4, 8, 16, 32, 64]**.

Edite os arquivos de pillar dos ativos de rede e adicione a seguinte configuração.

```yaml
ntp.servers:
 - a.ntp.br
```

Faça commit das mudanças.

Localização dos arquivos de pillar para roteadores: http://pipeconf-server.domain.com.br:81/root/pipeconf/src/master/saltstack/srv/pillar/router

Fonte:

* S. House and M. Ulinic, Network Automation at Scale, O’Reilly Media,Sebastopol, vol. 1, 2017, pag 48.

# Atividades automatizadas pelo PipeConf

**Passo 1)** Acessar o ativo de rede via SSH (*Secure Shell*) a partir do **PipeConf-Server**, informando o login, porta, senha. O servidor **PipeConf-Server** foi escolhido por ter acesso direto à rede na qual estão conectados os ativos de rede;

**Passo 2)** Habilitar o **modo administrador** do ativo de rede, informando a senha;

**Passo 3)** Acessar a seção **configuration** do ativo de rede;

**Passo 4)** Obter o backup da configuração do ativo a partir da memória (``running-config``), antes de aplicar qualquer alteração na configuração;

**Passo 5)** Configurar o NTP Server no ativo de rede, para manter a hora sincronizada;

**Passo 6)** Aplicar a configuração na inicialização do ativo (``startup-config``), para que a mesma não seja perdida se o mesmo for desligado ou reiniciado;

**Passo 7)** Obter o backup da configuração do ativo a partir da memória (``running-config``) e da inicialização (``startup-config``), após aplicar a configuração;

**Passo 8)** Encerrar a sessão remota no ativo de rede;

**Passo 9)** Criptografar os arquivos de backup da configuração usando o sops;

**Passo 10)** Enviar os arquivos de configuração para serem versionados no Git.

## Configurações do ativo de rede

Roteador Cisco modelo c3640

| Parâmetros                  | Valores       |
|:----------------------------|:--------------|
| Usuário SSH                 | aecio         |
| Senha SSH                   | teste         |
| Senha do modo administrador | ''            |
| Porta SSH                   | 22/TCP        |
| Endereço IP                 | 192.168.122.X |

Os endereços IP dos ativos de rede variam de 192.168.122.2 a 192.168.122.254, conforme a atribuição automática de endereços pelo serviço DHCP (*Dynamic Host Configuration Protocol*).

## Instalando o pyATS via Docker

Baixe a imagem Docker do pyATS com o seguinte comando:

```bash
docker pull ciscotestautomation/pyats:latest
```

Execute o contêiner com um dos seguintes comandos:

Modo python:

```bash
docker run -it ciscotestautomation/pyats:latest
```

Modo shell:

```bash
docker run -it ciscotestautomation/pyats:latest /bin/bash
```

### Requisitos

*Hardware*

O pyATS requer 1 GB de memória RAM e 1 vCPU. Se você tiver uma rede com grande quantidade de ativos de rede, precisará incrementar a quantidade de memória e CPU a serem utilizadas pelo pyATS.

*Software*

O pyATS dá suporte aos seguintes sistemas operacionais:

* Linux: Ubuntu, Debian, CentOS e Fedora, por exemplo;
* macOS (previously Mac OS X);
* Docker containers;
* Windows Subsystem para Linux (WSL).

O pyATS não dá suporte ao ecosistema Windows.

Versões do Python suportadas:

* Python 3.5.x
* Python 3.6.x
* Python 3.7.x
* Python 3.8.x

**Ativos de rede suportados**

A lista completa de ativos de rede suportados pelo pyATS está disponível em: https://pubhub.devnetcloud.com/media/unicon/docs/user_guide/supported_platforms.html


### Utilizando o pyATS

Baixe alguns códigos para utilizar o pyATS para aplicar configurações de exemplos nos ativos de rede:

```bash
sudo mkdir -p /docker/pyats
cd /docker/pyats
sudo git clone https://github.com/CiscoTestAutomation/examples
sudo chmod -R 775 /docker/pyats
```

Crie o arquivo ``my_devices.csv`` e adicione as informações de acesso aos ativos de rede conforme mostrado no exemplo a seguir.

```csv
hostname,ip,username,password,protocol,os,port,type,series
router1,192.168.122.205,aecio,teste,ssh,ios,22,router,iosv
router2,192.168.122.104,aecio,teste,ssh,ios,22,router,iosv
```

Execute o seguinte comando para montar os arquivos para a imagem Docker e utilizar os exemplos e configurações dos ativos de rede junto com o pyATS.

Modo shell:

```bash
docker run -it -v /docker/pyats/examples:/examples ciscotestautomation/pyats:latest /bin/bash
```

No prompt de comandos do contêiner do pyATS, execute os seguintes comandos para instalar os pacotes e dependências de software:

```bash
apt-get update

apt-get install vim

mkdir ~/.ssh

pip install pyats.contrib

pip install pyats[full]

pyats version check
```

Crie/Edite o arquivo ``~/.ssh/config`` e informe o seguinte conteúdo para permitir a conexão via SSH com os ativos de rede utilizando o algoritmo **diffie-hellman** (inseguro, mas padrão dos ativos):

```text
Host 192.168.122.*
    Ciphers aes128-cbc
    KexAlgorithms +diffie-hellman-group1-sha1
    StrictHostKeyChecking no

Host device*
    Ciphers aes128-cbc
    KexAlgorithms +diffie-hellman-group1-sha1
    StrictHostKeyChecking no

Host router*
    Ciphers aes128-cbc
    KexAlgorithms +diffie-hellman-group1-sha1
    StrictHostKeyChecking no
```

Execute os seguintes comandos para gerar o arquivo ``.yaml`` suportado pelo pyATS a partir do arquivo ``.csv``, que contém as informações de acesso aos ativos.

```bash
pyats create testbed file --path /examples/my_devices.csv --output /examples/my_devices.yaml

pyats validate testbed /examples/my_devices.yaml

genie parse "show version" --testbed-file /examples/my_devices.yaml -vvv

pyats validate testbed /examples/my_devices.yaml --connect

pyats shell --testbed-file our-testbed-file.yaml

pyats learn config --testbed-file /examples/my_devices.yaml --output good_config
```


Referências:

* https://hub.docker.com/r/ciscotestautomation/pyats/
* https://developer.cisco.com/pyats/
* https://developer.cisco.com/docs/pyats-getting-started/
* https://developer.cisco.com/docs/pyats
* https://pubhub.devnetcloud.com/media/pyats-getting-started/docs/intro/introduction.html
* https://github.com/CiscoTestAutomation/xpresso
* https://github.com/CiscoTestAutomation
* https://hub.docker.com/u/ciscotestautomation
* https://www.youtube.com/watch?v=ktEBApbHlEA
* https://developer.cisco.com/docs/genie-docs/
* https://www.youtube.com/watch?v=AfmzZGJzBcQ
* https://www.youtube.com/watch?v=THgHwS-zVt8
* https://www.youtube.com/watch?v=23hPg88pZBo
* https://www.youtube.com/results?search_query=how+to+use+pyats
* https://codingnetworks.blog/how-to-install-pyats-in-ubuntu-linux-starting-the-journey/
* https://www.rogerperkin.co.uk/network-automation/python/pyats-genie-tutorial/
* https://github.com/CiscoDevNet/pyats-coding-101
* https://developer.cisco.com/codeexchange/github/repo/juliogomez/netdevops/
* https://netdevops-labs.readthedocs.io/en/sevt-review/labs/nso-with-ansible/
* https://community.cisco.com/t5/tools/pyats-genie-connect-function-can-t-ssh-to-asr9k-with-interactive/td-p/4070060
* https://developer.cisco.com/docs/pyats/#!connection-to-devices/device-connections
* https://pubhub.devnetcloud.com/media/pyats-getting-started/docs/intro/introduction.html
* https://developer.cisco.com/docs/unicon/
