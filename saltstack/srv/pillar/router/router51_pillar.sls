{
	"data": "ENC[AES256_GCM,data:RxUQn1rx9zC7eJvmzvNCCRsUxeMg3w0997n0brYfTPVSq2PVaOYZAq5ZY7pAvAUgoGg0SKR2SxjVKHlJu43bRgqGOrZjTZr+rKbjNyDCWyOmqTQhLaakH68V1A6CO0XTkFJ9wxratWhogXXmavsopAoZNcsSRjsIOUJoi48SkExAUGrQxs5pg0ACp4Vst/wJRVCtMuLKQtHriGJVBMrkmQfd6PTzh96jdI9QX/T2F1aXqQwSrCaQ5t0atBjGcGVgPmqFkxjBF5i28mQvCGl5ziDTIIqEOzcA57okz8NoCE2I/9tapSkOfbG/3uFCgoexXWJsYmX2MMTMZsRQxOeQCy82jNVlyw7wsZOvXKDUEzsVmYG9jG+tY4RHzsqTR/i705Z8cxa5BJyGb+QueENK5TQSrn2Bn+GzczMsBr2Fs4lsR9rjZUeWvfxOyIR9OvgpPLifpnFIbnz3NtVLCszygG4+EjhAOk7pGhxtKrXb7aZl2LcbPJUmlGNmdOlYxwGkktfAig==,iv:CNbJWKps4SAgFxCyWqDevZR8Y3RGWu0EgKiw70IbDSg=,tag:G/UAcPcejEoIReEfF9j/oQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:29Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwGwSFJZOiTDht/UzU1Y35SjAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMPDa6PZzZ5kPmK+sNAgEQgDsubUL8YiVjmKfE5H7hqtiX+SjrxowgOLdnId0x8ofda+1Y513f7usdwxF7gMU2aIDFHd0SyqN/aXWdOQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:30Z",
		"mac": "ENC[AES256_GCM,data:05Il7N9RcgkFH34ekSlL/juv7raHvjO8m6ZB6ABajQp3zhb7T09iMUwrSv5UApYPsOyAOaq6yt7XXJa4xsDzyM0lZugJQZosSh0G8w5M7lMIz1GA4RzTFg9F6maljAGcoYy/gv1KNmnhoJwZwYoGyQLdcAxlHBpzA/xga/emRho=,iv:RQhEK8dAdFfJwwMJlAJ3q3vsbvxwv+C76J8QWWihZF0=,tag:aU1VFGIDBOWacKVBq6I6HA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}