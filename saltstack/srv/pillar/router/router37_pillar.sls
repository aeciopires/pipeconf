{
	"data": "ENC[AES256_GCM,data:i0vrefE4+Nag+4zqXg/wRRgGg/xytuf/RkXrY8sMJNf5PsURmCgrfflXDhrZRVZ8akgTlOw2jMW5A9n+dWxyZX1oR89zi0g+7wPvxIp9BVlb/OP1ZMLNy/QLqUF/wZ7hIc06Qf668rnxbQPvQU7SwtaRPfUw0CIbI7VZ/EeVciOaarFcD2QBI7lelBOQgOfwL/E9r7JZISNvrUOvW4z+F8ktO1kec4pJZBDMwC581VdDPUVI+TgFHQYxLE+8l5TodklHIjhywapZmkdVscsAJA+2DQL6qhAFY+3aQG34kLZLuNMK1QhxiSKQSX1GmR02OptOz64a4+aLsxWKwLDo7NJRsZhRsmVYHOVoH1kKOh5+WH3QYIenJ2rvP7mypPdZJr53ChUtpU7V239r8+Sv/cKZHUFZTITK063jVTOBkHlG2/ugxG9RjMMvk2Xry4Dxs1BIRhnBUeEOGpHBzwWe4GF7WCLhXva/CS2FdtSzLCA6byKmYy/RYOqmvL9R041oITdmPA==,iv:8UWx8TUW64Nu8l4sXSej8P2brjF+KCLMK+SuUBFQvFM=,tag:2GWox91uPfvqNwPWF4YTgA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:18Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwE+TEUHFFTFFkzeDtsok792AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMR/+lT+AAFb89ZkFnAgEQgDuE5iZ3f7cFynKgf5ijfmc5s35QdXgU6fv2iFHHHu8Y387Y9PZBhn4Vqc+R9QMslWfD6dW2gimLbkY40w==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:19Z",
		"mac": "ENC[AES256_GCM,data:xQHofBn1/4jU3XY/ibiU8yH8HkqC34YEhbKGssAn1j0cTVaJ1/MykuU5/XXHyMi3XcyruwVf4t8wV55BkHxBUP1LS3/zwoZ/T++iC6TD5hegCmc6NSSZSipSyDm36VrWPDhSGRJuR3Bazkp42G6e3yBf9cxcFoyGuD/HLfxBROY=,iv:89cY456W7uF1geMjF+cAKR2Wb0cvxBPPWr/eiB1motI=,tag:sJNVPAf72bHo/jjpQew2Gg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}