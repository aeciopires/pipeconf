{
	"data": "ENC[AES256_GCM,data:UuFt6nJArirqOOAPvQQMEjrQ1rrmgA/WmqaaaX1bod5NA8AurrgtSwEZX/+Y5aPvgfzgQJojYkUsVa7P55h7rUKgxHXwlGo1Av1Owrdz/oSt1SlnzrqZgIQuJCfM91hYqPcjmgM3TsrYxRrtInueZgwWuHbpRYaayqaLYVT5B87vEMzjqUvycoDgY47V55jYncjpPNVfdh1TPKm8MrbYv2ZN62/7AjLKenec+ICsbXqiMTA7VJr26E5Jc/ANGgA4kZFAAO1osa9Rg+z1pbwpCOwG/ikfc6gceBeZWUK3OeARoP6CVQ57W24qJxXEL63TlQVg3NWITcSlYyQ2dOgUOPLszxe6ylUIqKq2v1jTrIG0wYjWJ4xvOuvn1xa1koGYRzuzMKP9Dx4g1wlKW5nrRnEzMlNHHC5VFMZG8rJtQqlfHZGMUI8x2Mi3w+ZFyzi8TCK+q8Durj5bc3q3afr8l6tuRJDK33sRnFnPrjXabwnmsXHiNa3TTPGpfFdajyILJYs5xGo=,iv:Ym1GTH8qL8sT9uHBu/qi9BWLl3I2Rww7LDZLrqtFm4s=,tag:aKLUKkYkTHgZyu6L/o+jMg==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:24Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwF0nxvPUFL8vGVSJXgeIw03AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMKXWlKut7FjKrj6c8AgEQgDv3nhJkYAgZdg3SN9J9X80YS7/N7l1h51BZoYgdzdQkL/gihG/zRfh79Ah89myIPy+eirK0uv0LxC4vww==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:25Z",
		"mac": "ENC[AES256_GCM,data:2GW1t3s/6JZkCmZ6ZgWgiL2h+hw6K4KAveB3/0zjHnlh/M8+jxS9a95hWFwiqHdXkb0YzH8uSQBpmDfLi78vrf/sLFaLRhVivZn3MrEecIk/DYHRB/Z5tqHkJSRr92z0cP3gEc3yfOEYOH0PqW8+wq5DUVi+b6t8VoXPVn/JPgQ=,iv:voh1Lx6U1dqRGqYeNUf0BfeFtFeV9ckYlvvN6sbcpCo=,tag:01wtJtfvzsrpJ2km2Zt+oQ==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}