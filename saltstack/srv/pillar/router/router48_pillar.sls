{
	"data": "ENC[AES256_GCM,data:teqyQpF5UYGjN/H/E8vBMmX9busrgqg+SuoDOv9AzWN503ymomU5LL1FTAf3nwzOcGPX2a8ydHjLfZ9Q6wFsmtAc6Lkn/gA1snh++g3bAkOLY+hjBWqWxUQzlIzHYqzHw0Fb5FPdJdXkuD/kVfzpx9iqgRHrph5gkD1dQoIKzhKniNKl7umAHcDkgYphheicHffHcFdd/+cJY10KmEMtZ0b8N510G4xzI9NA/Yw409+o6q3hl/w5jdhNEGdcRXYeNy3tvv486xYBZc85Cg9VojT9XDLYRG4iwxNKOsBntIbsTVgs7cc9Ja5AnaJv3kl7TEM/leMVN/UOvKUxCmCqZG9GnHb873Uf8eAxhgRCnDnO10+XzUTPEn7FuGHQNZJbeMMVqFIc2WH3bLGRWO0QSqEj98nbei0OK0O36GyW7gNlRS3O3jDCvo0+iSwr9GZBkMfOZRgsh7fBLdMKPju6AnwwWebCsVujF+rNKNSf/09l9HQzdDdX9GWQOsyULdW5ol4Z+vc=,iv:Z3JsCTfsNs0c+Gp5eTVwH/EMYUhDD5x/4aZtISrROB4=,tag:drqZzaVW2RM5pGbjhIAZWQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:27Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwFGkw6q7BNK/9HAsCKZeL48AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMs8Ej9wlODXUzwm8CAgEQgDtSEjEmM8MwwoH9Szj5skGHvP2oEti4+MH9JAPXbQTuHELSsV91iXhFh1qRRsTxVnlbHJO6Z8epd+INfg==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:27Z",
		"mac": "ENC[AES256_GCM,data:02FnHVLqlZWiL7A0/nMzay7c1hrv7UGg3Z0B/aULEiywebNggIPBBlofsprYVwPlCT/JXFdZwcBgGCQe3dDu6jzWCIv4aJVoxchDqiF/nzdq99fQa8tNzOtVNDBYq17K3AeRCsJ7nTDp4C6Zl9rGUp5KEFC9PpVs56nndJgfx/s=,iv:YAlDeIl3XZEJFZ3dZecQNp5JEFD5An0wmIhselZccdM=,tag:ZzpUSPxuItsLrB1PhvWy8Q==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}