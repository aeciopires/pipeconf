{
	"data": "ENC[AES256_GCM,data:jJ8iZ6si4gUPK9b/VhboWhiD0edg8tObNCk2+g/LEsgoEM0gQ4VSFLXMKf/o0jVxXJ5blpZK4dw/68Xm70slxe5xtUna6XBbSPIGUOzIzWbBE9iHd6z6D9BwisK/roRFLjs2lFNSQZAw3VvIXn9p+4sYbbQkK9a9z55xBl/9LW0PS1oIAYhcESVRZRNTsXZym1znbQ6DB/6ZUZW5+x2TCfjeNp7qFxwgz9a+kPw5TSHc4bfXYwkxLT/LCSRyWIpIy25LHevpPJhOYpDweHhmeoySwxgosu7IGc7FsLqTLIFjwBjWUKvIupP2Qefy8U6+vFksBH2v62kGzmyXJp6iU/P5qZ4sSTYUfoyM1nehlbUmHur62QEZpSv8xsX0lj4sOh5pVYy9mCVfsLKssPa/C5rdXwq91QPrn9zap96x1FeXyeCfhmoUeXlp2N9GBQmY8gTWBsKoQgBvbS4Gj7d0Or7UljlyutaO0rvnKFDW4loUxhJfITd13OdayHwpmOXqn+Ru0SvdBNJVcDJlq8IznCQ+iRY77oWAHewY653YsGdkXTDWBw==,iv:OpS0hPJHtIaHbIbPh6kSlIlipAKcnxUyhuFqZWNR4Nk=,tag:pN6f0Z/J4k+SeYLBEeFcbw==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:16:40Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwEMomqLqiNmBGR56914KGUkAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMKXU9T8r+rXcz50KOAgEQgDu6EkBjH6CkJFt/I4xVXBT4LTQZ0zKBe5sAUDqCb53JIbliWGifBABZBSnK0CRegDqNWZXea0YEpj7GJQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:16:40Z",
		"mac": "ENC[AES256_GCM,data:i3xXkd7ZpDIkeFajku3SAOUwV3i5xGeuIClDtVlgE1KNwB/hpqsAIHLJF1VWCr7B7wyiPkZ+p0/dPLLsssEYUjRb08kxgKSIYSBW/SHiWX3BpOxAUwev5PUZcrv2erOT1lufpFpkz8j8X+2nb5L40EK74dm17DCKxlhJq1EYcds=,iv:O7HKcXUwiU6L8Md8j8fTgGRusc8M+bCkQz8WmUYCeuw=,tag:2mpwgdTPABGKuKRApwLczg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}