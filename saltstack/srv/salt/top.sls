base:
  '*':
    # Obtem informações dos equipamentos
    - get_facts
    # Exporta o backup da configuração atual
    - export_config_before
    # Importa a nova configuração
    - import_config
    # Exporta o backup da configuração nova
    - export_config_after