{
	"data": "ENC[AES256_GCM,data:FUcnpxAWHq0dIaLNJGqCI6M9x+P0wY+Wh4kv86O7YhYUF3Ac3YFgIkpEtsan9h1u0L0cuqptrGg94NVh/fI2YT8atU0DXmDOwNYO4xSSTtI3usTNaQkPi95UOdm0D1UVguFxqUKfMuanIOym0wlM7LQaJnv/mmHn3zo1LFNB71OInVKqRnesNvrWA6sMWDlQn9OcSq7iMv9e5Et4r4LP827ENufnSAVLkZDuc5y9sezzFWnMrEg8De+2XHuYHXdGh3xbSBsLnX0pZWReOstC5+lJYInd4fC7haOLb6Q/5eXqIn9GTWVtzrPZQEbXGGNEaps12O2WvWc3i03PbijNJXnXhO5f3p2VTQBuEZHkiKBVfyrpnEn4vBqdM4D+yDR7m+ZivV0wUrnREE8ClAmOrQ19FBCPe/28RItA4lh5V1tL6C8SXMQDX3PotLLaoONkzHJuFH0ft3THLaKvzNldIie6deUF+/PotEGYLF2PY6+lmi4an7K2WZlVBEks7hAT7JVvgA==,iv:X8NM/rscXZvDKg/Yiky1Y+p4tLuYsGIAhvRckKXmA/8=,tag:DGr54dYUfL6KuV1XHjygXQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:22Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwH6lY4CEJCCLq+vWKeNGRSSAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMp7xk3F9Hxz08BkbXAgEQgDusvWmuXd9dMbrhylxzl6mffE8Wl031ew86U3v2SZI0JVZWPdeeIbML95cf97w0KFMnZPdrYEvS3LkxVg==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:23Z",
		"mac": "ENC[AES256_GCM,data:+PJ5YRZYo0nCLuFx/fAHqtTOaicaXtuC/46W3xyJ4k8k+jCcWQbDbWur4go6NYXpnVMnnNYQLn5nPhdnCUgXBRLlJN3moZIZ1LMYDarPjXj4/GYHPS6DTSlGpCmSnjnvVrTox72FJ58tHFJWoY6oqDu3wanX9IL7M8fJPUOkA+A=,iv:2JFgeu4TzkZ70DkCh9zjY2f6x2T5oprSkCw1CV6uLwQ=,tag:ujKiGmwFO9aGlYyvV5+MhQ==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}