{
	"data": "ENC[AES256_GCM,data:Rrog1K3iXG7v5c9Ges4TCC6o8JALfO2uVgxseFu+91du/hWkkjZ+3e6wNtjgRUxZC11Q76xwPczd22OCjx2G1y2Xw9vUjtBwjDQQ60PUyliM/iddYGFSV72APfJ+t8W6glm2iCss4kF9Y8JOjLLqQj5cm1B3JHSPlH8QXVVNoQ4V69rDm+v5HlV3vohpKLU3HeG5g1HsiwuxLNniOjBzGBX3ZzceMcZZHiljOyLhELLW37CMBcewmDx5GWwEVpUcQH48nb9hSCPGA8qEv4GkG9zAjDtSPWUsJ8s90Wmc0jKg38HfM5KdWp0BCQcLhAOnxLiwHOF2UjnqNm16Vpx5VraP3wMpijx0Nv4409bnUf5Ztr1AKsMy8jX4bTsV5JkC8dVobgsL9lF/OKbRYo/ULwdTIUqf0lWX5QM/EGusUNV9k22McapOrFmCwfE1h3TzSrLFvXyspeKGYQ==,iv:Zf+YiYmnnaPfW0PNgfVFm5MqAUe9ousQ/hWBtlryxno=,tag:2qD/oL4WburDnrWDw85JRA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-21T21:37:41Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwG4LLBn/Pr2FNjT+Jsyc1gxAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM+Tf/Du8d/AQmkTE5AgEQgDuXOWkbf7hDsEnaIcNl72xCBJvr/c2qhBTOdCTNkM71nP2ejgjinsF6mT2+gv9aroa9Rh/2z3525ZZLkA==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-21T21:37:42Z",
		"mac": "ENC[AES256_GCM,data:7xQozhS6VWb1ilhualcH7XpGGs+bK0jpe7VkSweduHhhgp79BnATyCFegJPAa2pVOjSVaFg+e9OAuVD3NomHLwgLLo7JSvb9sgdSw9oHFdZjaUzvgclLl9LiE5LJnvh7dZwT5usnsZT4gBvlPZWXF1lPRCMWN3Xjy3KVYKoMB9I=,iv:kMuU5oNXHN1S29xDWQ+czWwHoxoXTGkXEKVr/7hiRhE=,tag:+NTyfpMOQJjpsdTCBMuUnA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}