{
	"data": "ENC[AES256_GCM,data:IWNPl3nA5iuGDIJ5IwFG/ne+AvmdTKfJ6F6etCG9o/V0nOQKI4oai8S1GvBr64dm+3fQ0CIpu521UAt2u6GKgavZGsRTNd4AMObQjvC9fF8Sor93BjyguW4wdgHEPApAS9p/azpAMxd0zqAO6/z9Ij6XhU2KvxFOrf2utOhbqICEUGToJRpruJDfyHxMbkeNdPMb5Xqx7ZNSN9GaBUDFz5bf16ZS1qQgEi385esOPdatUfO+Yf3mmcHwRA0/2RdXRFnDkwpVmN9VCElBb4bckCEc5e2oTaW0+VZMl2GhCI3mdV+CNfM2c4lo6PBevzX+z6WQ8nv7b7phnAbGUfa5/IMJz36LQUAJ3nTTSrZOLOYwQj3DcNjeQb9cTvs/dVfo8B0gwo5LSUw5N24bZSphhrO39noLul5U2Ww1drkJUBvMnqJqP24kF5dqxf3fd85Pwwa8M7q2u1e4npXrVY+k8cwvxiYht5eU0XENkIAkGUdd2qjy1QsB5zZ71+PoP+f53nIJO92QdnSp90FQdEdz7yZJSfVTwueG7VZKEGJRgmlbU2OdHQ==,iv:AIPmyV6jUixBJbOeV7K7v2cAftDFRVlFbQPLy5X9eMI=,tag:63qAgsrFp5caoLJ47vKB9Q==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2021-02-16T22:17:26Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwG7DM/ng4LrvnjON1dI4QD9AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMM8LuAdJbfTTgmCvDAgEQgDtPGN+91/+jqehtnl9jbQjeNTnPjwUM8pIYEu/CNo8ayQ8cJYyvH5Mweo5ANLRQ3B9Fi5p04zVrpqHLoQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"hc_vault": null,
		"lastmodified": "2021-02-16T22:17:27Z",
		"mac": "ENC[AES256_GCM,data:/RuwTH64oltJTQ/d4IFYZVJQEv/G7EhrgcLCRaNPuPy7NVQ8N3TVkvq0Y0c9pZqsTQtWvM7hQxhlA3smPZRCtdMqOdjof9a5FyPWF9wpTvN8O5C68I9nvGg/EoJMkMH8tXcjpnj4sK4IfTl0tQ65sj/1hTIA6l0yJIzWNWy+8SE=,iv:OoADIsD7fV7YKOIlB/Rml3vCadFucPrc8zJwCHc0lsM=,tag:s7KZrQ2mPEhmZdqdJfiz/A==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.6.0"
	}
}