{
	"data": "ENC[AES256_GCM,data:O2QaPVqT8ojQsJ7FK9k8W/Y9VBsXZXWfh/i6jSuk7bFn2fZvmdljoC5xHw3aegIxGYnh4opTA3ouZdVynlBDiRNfyUui44N+VKZ2SHTZ/em56LI+JqZsoneI8UrTIotHN9MEEMJRhG5Jg6x65lqm1ooOggHBlyB67KbAnkNQzN4WkLNChAkwpHf2zWBR/Ns5hjz5HuGtrjid9iXF9h3c9Z7qGPtjlHZuXUwj2NyNNXMR65fdttq9uXJ7yH4+cZ71aSMqGJZ7UjTsc/wZIOYPB3BL/6FOsjLL2e/ObtXWJTXrsg5RRT/asRTa2TiM0Iiv0PpZmvF9Wr2Xz0C1PN121B/xb1zuVH4XR7qvsj5yfyOMdnXYSoLpd55GzpcmEV6B/nBdSkhhrpheEiO+FAvh0SqVi/siIGdagLA1nqsVl7qzhK/GA96M45LDeEILBKJufC7i397f9rLLSw==,iv:MIzgZZqBihDcC999P9ZRIleH94R6Wetfj1T1c5KGdf0=,tag:Qju534XPEX4wyx9A9NeOGA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-21T21:36:35Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHfKOp3kUXl/xE4I4AvgjNCAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMZ7C5nDdFiNslJHIVAgEQgDtxflOeJ0NQ1OH9FYVsozVajhTlv2XV7sZYfjHKyNihqFsXByUy5d7DoPrh7pGwziMh5mcA9DZAyWnPWQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-21T21:36:36Z",
		"mac": "ENC[AES256_GCM,data:dtec8r4LgkwZvkWKIWgfGpFbkAXYCR+o0HRPrdWWWce608svh2vocvDB8aURQE+AQM/usonEAk+TJv/Oy1oI+BANVcx/DOifMCTK1efoQvIHG+n0MtBCAMzcIt0td/HEslPHsJgsv+JZDfKhLZFc9y+z7naP4VjagcYvkirnBk8=,iv:Sx7CcbqIT3fqfZY/fWgf0KxDZYAhebR7EveO5+xhAPo=,tag:3GA3wjqbIWyFRvgDyfVjHg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}