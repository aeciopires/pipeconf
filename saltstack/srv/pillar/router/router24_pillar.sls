{
	"data": "ENC[AES256_GCM,data:mLJ8AcXv0uSsryoF+vLD+N6bjLgSDxyW2YPfzwfx4jwB+qRV0dYL8Nhgf5Jm6jY/MIzM7svkNMQ8MIx07TNOYuBlD+UeyVOegQHKC7tJDW5VFSZBKHBFPYnbhTgn2KpemMmSrXCBuUtd+BrpEjxI/4M7gCOcHDkBrLi0/31VxRKOv8kRLXPn6VERZNNcqdKife67Uo1ycihB+95sdITr/9DmWJMXhfaJStDgMz9QvviOhwPJRAlq6YR+AHyzxuRqUt6qSKR4nCPABXAcHI9+LC11rKvdnB+7cptIJ2k6AKF6yRrMpfmUSk9xL+2YKi7uESrcjU9729DzMx6TzhOY6A4Mnb7UqQeRw1GuGQcsXY1KdKCdFRU3IY1wDHI2EW6hXCyzJTriTiFu4ZuAQv6oTlNG7uioNAPzFbI3N0nRUdDjnzyqPd7pylDX0Lq2Mlij8YFQWYSAqQcjQw==,iv:vo8Es7FTyiSbImY1SqfZvu6dw71GirgeGrXz/lJhE9M=,tag:hO+z1NkoCGT2HvGOGZzSpg==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-21T21:31:02Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwELOQLIJUZzTRZ2mi6ELiTnAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMOIaJDivTsc53m5WTAgEQgDt/T38Hwl1uvKelgYUrQvoPbRlcM3ZhElZAn2jb/2K7A4CCR3Q0cGfcYDKE0SoOOBwfpBXCSRSoM2+ovw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-21T21:31:03Z",
		"mac": "ENC[AES256_GCM,data:lifzCj+ML4TipLDClVgOHOlnQxynnUrZ125/8+YHfofkaGBlIbsXrsiAtPpmwImHm34Yan4mPv6Zsir0gU1kE9ZFC3UzajFJwnOWZjfWFj6QjleYsJC9/d/GXkMRwTD492/XKIG7ge/b7LbPNhOFE68u3dQZl2n1+V2S41HB34s=,iv:TdmJ9s21v2IzqIbp0KfFnNoR5A+yRHn3mVTYiLkANUw=,tag:t+SE/zxPg6pyn0rAwKaTNg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}