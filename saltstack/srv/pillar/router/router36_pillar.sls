{
	"data": "ENC[AES256_GCM,data:gplr35sez6XREkvEhqxfM5wq4eFgJ5hgCjiMpYMOsTjZjJGQsriQHVjr3iBHYMZrqxw98pH/56TpGDFba4mv5ia1jdqlQ7X0e5ooJFFPrm/egO3KflKPzOSS0QMIMA4il8/swsPPSGLb1PMVpqUgZLLdr5lqrkSNiUF71drqFYXaPdCi8/mnodAZy3pRkbFs/+scNx/pyWsU2AG+8ZgvigSVvvj6+iMbpzHMrGhDtNaBFnb/LsHOd0CALMxTNwuWZvKFgLFJK6Y2wmHTDazlGXLgNKhNxGIgpg2sIQtsk+M4/pXO7jyG4dzK5B0WtmDqNbuy6184ncj9fcSNNMIz39zKkcSjnut9GfofRH7BUyNgzppx8dKarvAqlMFBxy1hn+9pqvohjxyX0TSbDa+0S+5EoJgN6BC6J5BxqsfqxJI80/o2W1y6J11RxzyMflKG2JoKOjnZr5rqiqgfirp86Y/MPMlEU5p1Z20IlhCYsGtdxrmsrcxgflIS4nxPyk+kdhIpq2I=,iv:p4h3UF72cfnkODxfkOtCEfHIlE4omhBciWTG217YBkM=,tag:Wc5Xmy8X88My7956b5mBkQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:17Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwE00ddcDJEv85D9Qjy3sGbrAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMx3AfXX4hwT+DzW8YAgEQgDuTFWYngBbRVrAEabliI/Ccud9ov+IqQsS2r1rfd2dZz5OEPh6D/0PexDeoU1t3DEuqPIfy7zuV1BoenQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:18Z",
		"mac": "ENC[AES256_GCM,data:NIcn5HF5uledp6XQa5q8gLivLj6fbZw2nes5n5zMWLepO8qYJeO8TgWlMMr9OVEqniQ/zFTvPeTEWXgoEfW3gI47iJxq/L0BbdLkxcxAlWhad9DeD0smIOq+oTJjG9s9Z30q28m8TXgpmwUAp2/rxKUA53H/MKjzv8Gg6j5AUFk=,iv:0+Vn5037m3kKTh6FnQp9JJwdjhz3jYGijbUJJMYHujo=,tag:FMtjF/gdiTDg4DV7u4vx3g==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}