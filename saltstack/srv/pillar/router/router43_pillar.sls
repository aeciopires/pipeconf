{
	"data": "ENC[AES256_GCM,data:HXR8hS1B9L8rrrRiNpaD8jNrw1ajR5+rQcpLW3gnOv7JJPKEWANKN597zs8mzzpZ53/ypVLREZMGzoyoaYaBshpuB2tUbQsqQMDWA/xXtJHKWaQmFH5AoyFQBevWh2OVD8HaGYDYTlH3/6h/k2jUXogUfEb7He4gwmNJkxEXGNAhUocYZwiyGwMU5tFI5vsVOPhj2Ek3mBD52RUBGAv3ApwkcyIIZvUCT/lkoDcgMKcRN3xtSSC1Al1PANJCegz4WAo5sZ4s+JnM91rZonttf8FgU/ltyGG6Q4KpkP1NWYXZRJ4uNsp2bjPoKo0vB9Lx2wCYfaqVh+ZGfXq5ZGkngmVTkzeSxXeDp9/hh/JW6AhWQ8KeC0c6SYjVokP+nzeJdJOknhBKg9ELS90Deia+nYuyvTAi9IkEsqe2a4rCxQoe3ZlE+a4lpTmpS9Vnwu+Puu/fkHM5gtvJqxhz9Z+r14gNEBhp6SftFO0yRbAjOhtFY3x02Pq/crblTBYPMRqoPELgSw==,iv:4g2nj8/wNHk/wSYG4Hwcl70tCiNPtlkVd/Z2XSXgXLw=,tag:0h0BhRxaEDmsgG/472Xrfg==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:23Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwEnqz5sOjdiGrEJPpKuHBL3AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMK1E3oiIv15/u4G9WAgEQgDtacj8SeKwoRvy9e9NvMtIsy1qYj6QvtFbGQlawT+e0x0g8jmdUzTWRkVrQA6aXpmOAgC5SM7LkoMQSWQ==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:24Z",
		"mac": "ENC[AES256_GCM,data:e5VfAstHUZwfJrRvMYT5I9b2ZtJqGwR2x8wqrzsVIaa7DtY+Dx65YF/ZXKacGPpIgqIkQUW58wSbf07KqioyYWGZ5F38LCJg34LleLKvzHrNvOktlX1nWHHb4xFkAJhRkIwNJtyxeCdFLAzn1Hs6H61o76QZx3q8Us79m08yJx8=,iv:Ze5/ejmyoz7g1MjaZ20wVTg+XjO3LTRqzoPuQvnatSE=,tag:LtXdZxVVTRhNWAA7NfKRMw==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}