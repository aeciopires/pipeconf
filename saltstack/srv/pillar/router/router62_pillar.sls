{
	"data": "ENC[AES256_GCM,data:hEeDeQ4YiMLe6zKekMIPWgzuvPSbU1SnT/A4aFJIJlbrf+Mcn5NCGqN6ylu4R0pL/lFFMlYQ8OyeHdxCOhpbwJPUTdzuJKrD6tTQOFxJBPBcd3ljiDcHKsib/zZ4EE1qYjlBD4i3rpLjltmEM5gsi4i4FBAKlRgLBiH+QrNmEE9xjnbnjcBJQT2FuBuPx0SAxvujYQCUgSnOKeW/CsGDWsp/jV2WH6h3uvEV2hIEPX+g4wRNJbsPqDwbREfo/rm8RZ6eyhLVMwZpcLvoGmAzJnW/5DOEqTe67KC0Y9CweHX5KIvEWQ8vaR4rPldwtL4lnjanNthRKj1l3s4FOjbfKzfFkkNQOMPGY9xXdsk8kCT3C4r1TyME6ofVExVKHn/+vaeYyuaBrL/nsvDwyReTCugnRiij3eA7Fb1rerB8LwWv9okN7FSQ4czU1OSqBHyGd1KPE0ADie7g5CQ3ylDRMOpgpo4ptjc+md2WpzRsEdf2yQT1iJYveS9vMuO3E/FwejWSWg==,iv:bz1OJ1udI2KAXYqYDot9kWRIwstqwNm2EmYIKvBPL68=,tag:yBewxn1x84uaho1GTBw+jA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:37Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHDoKhbvbV0MPq0MFjIu4sKAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMse3v2sYmcM3ztGmoAgEQgDs60+KJxpO4jAA6EBlplHG0RrUathWiA8xfGij9Byw9RGLWg0Meqznvj06CGGj3/whaBYjNqyRVBadELw==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:38Z",
		"mac": "ENC[AES256_GCM,data:DtEqujWn8gqS143lGQk4MQ1v6ePFnWRkyAd2pgFfwGrH8XdVau19o9C0Hw3kbp+AbkXMJIm5OsqRXhzl9ZBs3K/7BUD6AHj0ozmnluLKF7GkzUObLdbR0uajjGEopz4YBt/H2yHy4oQNCSNS/hCXtJ1WPDbN2mRnhgmcrTEcJQI=,iv:vAmFJjLOHEvyb7X2O6Yc+c/IAPLIqagE88RkGSXBcIc=,tag:NLhY9lz8+lCDKYjx/XoFVA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}