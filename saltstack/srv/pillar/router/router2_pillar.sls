{
	"data": "ENC[AES256_GCM,data:cV7O988A43Is9FZRZHPjQPdl+mSlznLhWl54G3n6abRsaA0MXrkFT4JP+12N6ts9lUgk/kYhhvlH64GBM6l3XBDNtqvulmd2o4VvU/AvcboUugmqpKWXW/kCRVIEkC6hUDHSW94blXN6a3PitD7OSpsQLOacztBzqP16qrrMcx7yEPtbnYbD2JJwbWNfS/TSOiOf2JgyTRJG+eX/RyiUd8Q6mhUgfUv6lm2n6gWtUos8aoC9Fu2vFjpNGjI0j/ljyBS4W33gwTL0WvVSTcEYmzjw3hoo51TvE7cAFtwh6qyKjcacPyNWGKQ9LcHTbHIQ+NXjvslMmBcstSssb/VIzzoJOA6q4sW0PcUnSGgzInc3jMXvntMNOElEgWIW2FND+F4v5z8orL8FyWc7RMDeB8IolnTFDLG3RJTZDKIcFWKcnpAp9MlQFkpyHdvq/52CaHyjy3MDxOGlDU9sO0SGi4H7LIIwPIZiphyXPJ6JmU4Yu6+RRiNaGiNZoiV6YAGlf+NeBy8UxRxYNxoqI0y07obl0B2td0EK,iv:wW189MXfikDdq3LnzDLc0WhlN9PjztqAhe4ROug59Zs=,tag:cKJhuqsHiP5TuIL8YacWVA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-15T07:00:52Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHzbKkUYf11YCzlneraFUEHAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMyDgZ+bUpPvx4Ho3yAgEQgDs/v7j29t93QHBe+bQaUxODJ02yM2FiI7CmlE1u7TvyxDtbpIip4xxfDjB/lsKp2RvytAvPayh8s09UYg==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-15T07:00:53Z",
		"mac": "ENC[AES256_GCM,data:iTU87YE0YRbFNuLAM+kKkpIUUCLLKSEyj5Tw/+ZJKc/oWxmYgdzVqg6nx+Vmw687GmguOQmvwH0X8jMQlvqR7fajt24JmhYVX+01g4LZALyctrRCKF3NPlyqgunFrPczfLBHeJQa7qh0O4fXqjrMDOb58TDMJyyxX//zD+mhs8o=,iv:qRaG/tfIJJViejSBugccmCT7Gpz33BrDobWTd5d1DSo=,tag:WZ3FKB7Wb1EW6x7EarOEAA==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}