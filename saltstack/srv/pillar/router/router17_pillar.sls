{
	"data": "ENC[AES256_GCM,data:8e86jgez/z7PV+oAYogfB1RjW7r3wAQCV50A3533cWDGZEXRfBBHEULRJwqJb4i+/3TWBGq6PcwbBxK0e829qWANBlgG8R8zGmlIh51mOPLxokepW9ydE/z1qlqwRWXsU2gzDHbQ23h7FyqHKq13BT4D59Rr0YrEK1TEX0v429GnN67TWGuW/KiLXelQEsbdqkbTjTSDPBBp2e1jhsBDQuTPhtCneNzC5pcLFoiwFl9aY1TVPvbSWzAPorFq/pOcy0uC0gMWysxmpeKq02KtlcTjO0KbzkbwdkTqOnjU4Ct5MxJFjt/K/6kouVODk0IkGPZ1nL70l4qQkujGaCR45gFSMgPsTLHYaG1wT3+pNzD6lArPsN17WTwNWXw4F6WcqHX+Ihx/mCEGCCOoYsnHYdaHWatFCREgc3j8rt59ZXYKJyDVVwgtlHgIR929WRWoFo0QPuucxDjHYe7BEe29Jzv52lOiKPl2/qk=,iv:1VB/thahE4g4vislymd7n3CF8prUK2c02EtF2YwHaxM=,tag:yuCocdNKrmsxCPWK3SsWAA==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:32:44Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHPJ7dPaJdM0a09dHlxkj7DAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM7aoP8USPeTQW1WUeAgEQgDspXpW+MiZMi1YqYD9gD+WBKW/jB1rf3cgCWmBo3u8lfRopvVKPycJk/5vLgKTf9wleGFVV7dP3CpreJg==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:32:45Z",
		"mac": "ENC[AES256_GCM,data:Sdh/cMKI9mMHxm14F+nDrxas6Jkc/wLk1uevY+E92ToVIJeVs/cm/S2l8KCENU6TnEF9QZpFZMQ4kmyJo4afrpk6pjCDrjPiRDUHVzPDtb39F7o1VoR60xPQDCbbSxv3TrQxDCtVY1jmOaiaOS6iJ9sm/AXJWdJPVJU+l/WNLvY=,iv:YA3hHNUfcLDbQTJWT1isMKCnWwus3q/OcgpikImY1Fw=,tag:iw0JGdIaKRmEbUWUzbL+Ww==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}