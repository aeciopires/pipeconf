{
	"data": "ENC[AES256_GCM,data:95U9eedHYUyQadi5awfwfH5kH16CSxkqukxVnS8nOd+0h/rOLC5QT5+xbCxdqxVBvL8DAGlj5Z5avGAl5cliklTsigkZIEnPTJaM928YXAnsqkBkmsxNmDVNu3moF9YIrTiPyXW8jQtd3gTaoc+4pSCJN/Zujo2i9PulAYOURxb//cDZSNAEFsOEsX89TepQpsBlaxBcvKO+Q1FDzBkm4hdPrgJICGIoWapbz7gTFCmZq8q05G73FkUbktjdTyG74iK/8EkWU4em/l4purbi3mX5eSJCQJ0U5dIYq+PjGHEACmKm1mmqU/F6mok7ixN9SsaRVKBod5CH0/jQekseFtFMrjvg+usxMtEt7UqD6k2ouIdJ/EeIzBqzQcWfL4uPrem1e0OzEg8WR1N/6c0DxXSlxWFu0/R+e3J48lMK9p17bPV0Xii2EdrSbekEFza+Q8FZu6iIkN+7twtvMn6rw6rplWPV4FYQ8T8J,iv:oQb8/dAahNbor+1krqq47ZefZSJH+OzL9zBE9//vUDM=,tag:6TYdOh46M5njQj0dlbSfZQ==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-04-23T06:33:17Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwHRb8OyXofgyxSQFEJe0dx4AAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM4hzspBk1aqC8r+SUAgEQgDtmO7Uk60RNJrMIFK8WVx4O+yAeU10CRAEWgjhc5tBa3QWV7CeD2q3w9/gjj3sZy0PTQJJzJ0bJwOekig==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-04-23T06:33:18Z",
		"mac": "ENC[AES256_GCM,data:gG393bhzkQacbYHXB0Li5KNmjB64nj8C/LSKL8bvZTLVrwQzj4pr4MQyZbTcXFz3iYH9MvEcgPSIx+ML8AUIAjAKuvJTXMt4t8CG+8MIsXAQuskVL2FuESmR4uH+sv4ZitlRXeER1a2ILR5M7ETT89d+lphj8ngrg3xehvg26vM=,iv:JIJeC0Wk518R5Cmf+5rsE2IN4o6t0tAPMeDD0I73/sY=,tag:Wp5IPPEE7yicVwU4FSHeIg==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}