/*
  Keeps only logs of the last 10 job runs and for 30 days.
  Allows you to select the Jenkins slave node that will run the Job. 
  It is expected to fail the first time, as this is when the pipeline will be read and the parameters will be created.
*/
properties([
  buildDiscarder(
    logRotator(
      artifactDaysToKeepStr: '',
      artifactNumToKeepStr: '',
      daysToKeepStr: '30',
      numToKeepStr: '10',
    )
  ),
  parameters([
    [$class: 'NodeParameterDefinition',
      allowedSlaves: ['pipeconf-node', 'pipeconf-node-gcp'],
      defaultSlaves: ['pipeconf-node'],
      description: 'Selecione o node PipeConf para executar este job.',
      name: 'PIPECONF_NODE',
      nodeEligibility: [$class: 'IgnoreOfflineNodeEligibility'],
      triggerIfResult: 'multiSelectionDisallowed'],
    string(name: 'DEVICE', defaultValue: 'device1', description: 'Nome do ativo de rede', trim: true),
    choice(name: 'DEVICE_TYPE', choices: ['router', 'switch'], description: 'Tipo do ativo de rede.'),
    string(name: 'ADDRESS', defaultValue: '127.0.0.1', description: 'Endereço de rede IPv4 ou nome de DNS', trim: true),
    string(name: 'DRIVER_NAME', defaultValue: 'ios', description: 'Nome do driver suportado pelo Napalm. Veja https://napalm.readthedocs.io/en/latest/support/', trim: true),
    string(name: 'USERNAME', defaultValue: 'root', description: 'Usuário para acesso ao ativo de rede', trim: true),
    password(name: 'PASSWORD', defaultValue: '', description: 'Senha para acesso ao ativo de rede'),
    string(name: 'PROTOCOL', defaultValue: 'ssh', description: 'Protocolo para acesso ao ativo de rede de acordo com o driver.Veja https://napalm.readthedocs.io/en/latest/support/', trim: true),
    string(name: 'PORT', defaultValue: '22', description: 'Porta para acesso ao ativo de rede de acordo com o protocolo definido anteriormente.', trim: true),
    password(name: 'SECRET', defaultValue: '', description: 'Senha para o modo administrador do ativo de rede, varia de acordo com o driver. Veja https://napalm.readthedocs.io/en/latest/support/'),
  ]),
])

pipeline {
/*
  addDevice

  This text will be ignored during the execution of the pipeline.

  # --------------------- TASKS PIPELINE REQUIREMENTS -------------------- #

  # ------ STEPS TO BE EXECUTED ONLY IN THE 1st BUILD ------------------ #
  1) You need to add in Jenkins a new credential with the IDs:

    'git-access' => access credentials for the Git repository that
                    will store the settings of the network asset.
                    The repository URL must be defined in the variables
                   'GIT_PROTOCOL' and 'GIT_URL' in this same pipeline.
    'pipeconf-node-ssh-access' => credentials for accessing the Salt-SProxy server,
                               that will have the access settings to the
                               network asset
    'slack' => secret text type credential that has the Slack access ID,
               see step 7 for the ID.

  Access the Jenkins web interface and click on the menu:
  Jenkins Dashboard -> Credentials -> System.
  Then click on 'Global credentials (unrestricted)' and then click
  in: 'Add Credentials'.

  2) Configure the integration between Jenkins and each project at GIT using the
  webhook.

  3) Configure a node-slave in Jenkins with the ID: 'pipeconf-node'. This node
  it must be the same one that will have the Salt-SProxy service. To do this, access the
  Jenkins in the Jenkins Dashboard menu -> Manage Jenkins -> Manage Nodes -> New Node.
  The access credentials to be used to access this host must be the
  which are defined in the ID 'pipeconf-node-ssh-access'

  4) Configure the access information for network asset on the host that
  runs the Salt-SProxy service.

  5) Configure the email sending service on the host 'pipeconf-node'.
  Example: Postfix https://kifarunix.com/configure-postfix-to-use-gmail-smtp-on-ubuntu-18-04/

  6) Configure the user registered on the host 'pipeconf-node' to access it
  via SSH and use sudo without password (https://www.vivaolinux.com.br/dica/Liberar-comandos-de-root-no-sudo-sem-pedir-senha).

  7) Install and configure the Slack Notification plugin. See the tutorial on the page:
  https://plugins.jenkins.io/slack/
*/

  // Keeps only logs of the last 10 job runs and for 30 days.
  //options {
  //    buildDiscarder logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '30', numToKeepStr: '10')
  //}

  // The 'agent' section defines the name of the node registered in Jenkins that should execute this pipeline
  //agent { label 'pipeconf-node'}
  agent {
    node {
      label "${PIPECONF_NODE}"
    }
  }

  // The Parameters section is used to receive user input. 
  // It is expected to fail the first time, as this is when the pipeline will be read and the parameters will be created
  /*
  parameters {
      string(name: 'DEVICE', defaultValue: 'device1', description: 'Nome do ativo de rede', trim: true)
      choice(name: 'DEVICE_TYPE', choices: ['router', 'switch'], description: 'Tipo do ativo de rede.')
      string(name: 'ADDRESS', defaultValue: '127.0.0.1', description: 'Endereço de rede IPv4 ou nome de DNS', trim: true)
      string(name: 'DRIVER_NAME', defaultValue: 'ios', description: 'Nome do driver suportado pelo Napalm. Veja https://napalm.readthedocs.io/en/latest/support/', trim: true)
      string(name: 'USERNAME', defaultValue: 'root', description: 'Usuário para acesso ao ativo de rede', trim: true)
      password(name: 'PASSWORD', defaultValue: '', description: 'Senha para acesso ao ativo de rede')
      string(name: 'PROTOCOL', defaultValue: 'ssh', description: 'Protocolo para acesso ao ativo de rede de acordo com o driver.Veja https://napalm.readthedocs.io/en/latest/support/', trim: true)
      string(name: 'PORT', defaultValue: '22', description: 'Porta para acesso ao ativo de rede de acordo com o protocolo definido anteriormente.', trim: true)
      password(name: 'SECRET', defaultValue: '', description: 'Senha para o modo administrador do ativo de rede, varia de acordo com o driver. Veja https://napalm.readthedocs.io/en/latest/support/')
  }
  */

  // The 'environment' section contains the declaration of variables used in the pipeline
  environment {
    // Environment variables should preferably be capitalized
    // to facilitate the differentiation of pipeline reserved words.
    // 'null', means that the variable will start with the null value (empty)
    LAST_TAG = null

    // Single quotes are used to unify the text
    WORKSPACE_DIR      = pwd()
    GIT_PROTOCOL       = 'http'
    GIT_URL            = 'pipeconf-server.domain.com.br:81/root/pipeconf'
    GIT_BRANCH         = 'master'
    MAIL_SENDER        = 'jenkins@pipeconf-server.domain.com.br'
    MAIL_SYSADMIN      = 'aeciopires@gmail.com'
    //MAIL_SYSADMIN      = 'jenkins@pipeconf-server.domain.com.br'
    MAIL_TEAM          = 'jenkins@pipeconf-server.domain.com.br'
    MAIL_NOTIFICATION  = true
    SLACK_NOTIFICATION = true

    // Salt values
    DIR_SALT_BASE    = "${WORKSPACE_DIR}/saltstack/srv"
    DIR_PILLAR       = "${DIR_SALT_BASE}/pillar/${DEVICE_TYPE}/"
    FILE_PILLAR      = "${DIR_PILLAR}/${DEVICE_TYPE}_${DEVICE}_pillar.sls"
    SOPS_CONFIG_FILE = "/home/pipeconf/.sops.yaml"
  }

  stages {
    stage('Checking conditions') {
      steps {
        checkConditions()
      }
    }
    stage('Add network asset') {
      steps {
        sh """
            if [ -f ${FILE_PILLAR} ]; then
                echo "[ERROR] O ativo de rede '${DEVICE_TYPE}_${DEVICE}' já está cadastrado no arquivo ${FILE_PILLAR}.";
                exit 1;
            else
                echo "[INFO] Criando o arquivo de configuração para o ativo '${DEVICE_TYPE}_${DEVICE}'."
                mkdir -p ${DIR_PILLAR};
                echo "# Source:" >> ${FILE_PILLAR};
                echo "# https://docs.saltstack.com/en/latest/ref/proxy/all/salt.proxy.napalm.html" >> ${FILE_PILLAR};
                echo "# https://napalm.readthedocs.io/en/develop/support/index.html" >> ${FILE_PILLAR};
                echo "proxy:" >> ${FILE_PILLAR};
                echo "  proxytype: napalm" >> ${FILE_PILLAR};
                echo "  driver: ${DRIVER_NAME}" >> ${FILE_PILLAR};
                echo "  host: ${ADDRESS}" >> ${FILE_PILLAR};
                echo "  username: '${USERNAME}'" >> ${FILE_PILLAR};
                echo "  passwd: '${PASSWORD}'" >> ${FILE_PILLAR};
                echo "  optional_args:" >> ${FILE_PILLAR};
                echo "    port: ${PORT}" >> ${FILE_PILLAR};
                echo "    transport: ${PROTOCOL}" >> ${FILE_PILLAR};
                echo "    secret: '${SECRET}'" >> ${FILE_PILLAR};
                echo "    dest_file_system: 'flash:'" >> ${FILE_PILLAR};

                echo "[INFO] Criptografando o arquivo de configuração."
                sops --config ${SOPS_CONFIG_FILE} -e -i ${FILE_PILLAR};
            fi
        """
      }
    }
    stage('Send file to git') {
      steps {
        dir("$WORKSPACE_DIR") {
          // Sending the registration of network assets to Git
          withCredentials([usernamePassword(credentialsId: 'git-access', passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
            sh("""git config --global user.email \$USER@\$(hostname --fqdn);
                  git config --global user.name \$USER;
                  git config --global push.default matching;
                  git checkout -B ${GIT_BRANCH};
                  git add *;
                  git pull origin ${GIT_BRANCH};
                  if git diff-index --quiet HEAD; then
                    echo "[INFO] Nothing to commit";
                  else
                    echo "[INFO] Send changes to git repository";
                    git commit -m '${currentBuild.fullDisplayName} - Job de cadastro de ativos de rede';
                    git push ${GIT_PROTOCOL}://${GIT_USERNAME}:${GIT_PASSWORD}@${GIT_URL} ${GIT_BRANCH};
                  fi""")
          }
        }
      }
    }
  }
  post {
    always {
      echo 'Removendo arquivo temporários do workspace.'
      deleteDir()
    }
    success {
      script {
        if (MAIL_NOTIFICATION == 'true') {
          mailSuccess()
        } else {
          echo "[INFO] Email notification is disabled."
        }
      }
    }
    failure {
      script {
        if (MAIL_NOTIFICATION == 'true') {
          mailFailure()
        } else {
          echo "[INFO] Email notification is disabled."
        }

        if (SLACK_NOTIFICATION == 'true') {
          slackFailure()
        } else {
          echo "[INFO] Slack notification is disabled."
        }
      }
    }
  }
}

/*
  My functions
*/

void checkConditions() {
  sh '''
    echo "Validando o campo name...";
    if [ ! ${DEVICE} = $(echo "${DEVICE}" | tr -dc '[A-Za-z0-9.\\-_ ]') ]; then
        echo "[ERROR] O nome do ativo de rede '${DEVICE}' não contém uma string que atenda a regex [A-Za-z0-9.-_ ]";
        exit 1;
    fi

    echo "Validando o campo type...";
    if [ ! ${DEVICE_TYPE} = $(echo "${DEVICE_TYPE}" | tr -dc '[A-Za-z0-9.\\-_ ]') ]; then
        echo "[ERROR] O tipo do ativo de rede '${DEVICE_TYPE}' não contém uma string que atenda a regex [A-Za-z0-9.-_ ]";
        exit 1;
    fi

    echo "Validando o campo address...";
    if ! [ ${ADDRESS} = $(echo "${ADDRESS}" | tr -dc '[a-z0-9.\\-_ ]')  ]; then
        echo "[ERROR] O endereço de rede'${ADDRESS}' não é um nome DNS válido.";
        exit 1;
    elif expr "${ADDRESS}" : '[0-9][0-9]*\\.[0-9][0-9]*\\.[0-9][0-9]*\\.[0-9][0-9]*$' >/dev/null; then
        IFS=.
        set ${ADDRESS}
        for aux in 1 2 3 4; do
            if eval [ \\$$aux -gt 255 ]; then
                echo "[ERROR] O endereço de rede '${ADDRESS}' não é corresponde a um IPv4 válido.";
                exit 1;
            fi
        done
    fi

    echo "Validando o campo driver...";
    if [ ! ${DRIVER_NAME} = $(echo "${DRIVER_NAME}" | tr -dc '[a-z\\-_ ]') ]; then
        echo "[ERROR] O nome do driver '${DRIVER_NAME}' não contém uma string que atenda a regex [a-z-_ ]";
        exit 1;
    fi

    echo "Validando o campo username...";
    if [ ! ${USERNAME} = $(echo "${USERNAME}" | tr -dc '[A-Za-z0-9.\\-_ ]') ]; then
        echo "[ERROR] O nome de usuário '${USERNAME}' não contém uma string que atenda a regex [A-Za-z0-9.-_ ]";
        exit 1;
    fi

    echo "Validando o campo password...";
    if [ ! ${PASSWORD} = $(echo "${PASSWORD}" | tr -dc '[A-Za-z0-9.\\-@#_)(+=% ]') ]; then
        echo "[ERROR] A senha '${PASSWORD}' não contém uma string que atenda a regex [A-Za-z0-9.-@#_)(+=% ]";
        exit 1;
    fi

    echo "Validando o campo protocol...";
    if [ ! ${PROTOCOL} = $(echo "${PROTOCOL}" | tr -dc '[a-z ]') ]; then
        echo "[ERROR] O protocolo '${PROTOCOL}' não contém uma string que atenda a [a-z ]";
        exit 1;
    fi

    echo "Validando o campo port...";
    if [ ! ${PORT} = $(echo "${PORT}" | tr -dc '[0-9 ]') ]; then
        echo "[ERROR] A porta '${PORT}' não contém um número que atenda a regex [0-9 ]";
        exit 1;
    fi

    echo "Validando o campo secret...";
    if [ ! ${SECRET} = $(echo "${SECRET}" | tr -dc '[A-Za-z0-9.\\-@#_)(+=% ]') ]; then
        echo "[ERROR] A senha para o campo secret '${SECRET}' não contém uma string que atenda a regex [A-Za-z0-9.-@#_)(+=% ]";
        exit 1;
    fi
  '''
}

void mailSuccess() {
  // When the pipeline runs successfully, the following email will be sent
  mail to: "${MAIL_SYSADMIN}", cc: '', bcc: '',
  from: "${MAIL_SENDER}",
  subject: "[OK] Cadastro do ativo de rede: '${DEVICE_TYPE}_${DEVICE}' - Job: ${currentBuild.fullDisplayName}",
  body: " O cadastro da configuração do ativo de rede '${DEVICE_TYPE}_${DEVICE}' foi realizado com sucesso. \
        \n Acesse o Dashboard a seguir para visualizar o resumo do log. \
        \n ${env.JENKINS_URL}/blue/organizations/jenkins/${env.JOB_NAME}/detail/${env.JOB_NAME}/${env.BUILD_NUMBER}/pipeline \
        \n \
        \n Ou acesse a URL ${currentBuild.absoluteUrl}/consoleFull para visualizar o log completo. \
        \n \
        \n \
        \n Detalhes do job: \
        \n Nome e ID: ${currentBuild.fullDisplayName} \
        \n Duração: ${currentBuild.durationString} \
        \n Nome do node que executou o job: ${PIPECONF_NODE}"
}

void mailFailure() {
  // When the pipeline execution fails, the following email will be sent
  mail to: "${MAIL_SYSADMIN}", cc: '', bcc: '',
  from: "${MAIL_SENDER}",
  subject: "[ERROR] Falha ao cadastrar o ativo de rede: '${DEVICE_TYPE}_${DEVICE}' - Job: ${currentBuild.fullDisplayName}",
  body: " Falha ao cadastrar o ativo de rede: '${DEVICE_TYPE}_${DEVICE}'. \
        \n Acesse o Dashboard a seguir para visualizar o resumo do log. \
        \n ${env.JENKINS_URL}/blue/organizations/jenkins/${env.JOB_NAME}/detail/${env.JOB_NAME}/${env.BUILD_NUMBER}/pipeline \
        \n \
        \n Ou acesse a URL ${currentBuild.absoluteUrl}/consoleFull para visualizar o log completo. \
        \n \
        \n \
        \n Detalhes do job: \
        \n Nome e ID: ${currentBuild.fullDisplayName} \
        \n Duração: ${currentBuild.durationString} \
        \n Nome do node que executou o job: ${PIPECONF_NODE} \
        \n Diretório do node utilizado pelo job: ${WORKSPACE_DIR}"
}

void slackFailure() {
  // A notification will also be sent to the slack... on the #pipeconf channel
  slackSend (color: '#FF0000', message: "@here [ERROR] Falha ao cadastrar o ativo de rede '${DEVICE_TYPE}_${DEVICE}' a partir do node '${PIPECONF_NODE}' - Job '${currentBuild.fullDisplayName} - Acesse o log em: ${currentBuild.absoluteUrl}/consoleFull .");
}