{
	"data": "ENC[AES256_GCM,data:0uJVpZhM9TlupTe2CeucfN6deJtdF9xv5VymDgoyqNQGaCvzP+n6LR6FpKWrTL7jWrKJ/qqOXR1sLpYMApNjHxzfNqXJHBLdPNTMiCdUaInU5YhsvzpRL1eWJd0uNAJTDR2doHmQw3vNO5tDf03sibvtGlTgd+n76woM0Da3FRYbRsmSY66D8pG2Fvmb1XAL4q8LHDOG8RKEJslwZV4hWG6jlZ8/WOueo7SW/iusjLCQOItSsOKWLsURJ60tOSP/NdrqnWwVkaFb22K0KIcfqRsRq5FhBGUUEcOO1WI4j02Z+lliqzmm5/M/pnLbXuHThhmXjf9qpyG5QMW/sglo9x5mkvKBT9NZK+jK1bqZ0wJYuBMw9SyOKU1lD7nFdt0gNYdm3hXUGTR/YCmiuacA/iugHfophQZc2LCkyWS+8XT8SiyU1BA2p4mosWTFFz0uo/egfJGGmmAfOU1eKolAXYFyw0i+BfVOlFNwnm0w2Hcfno4DzPZQYac5rJcX4BvxH2BK1LE=,iv:6Wd0wqm7VzDNixDmGqua/rbO2x0z0RJ7zQ+1BvWTZKU=,tag:RCdxV9aLYRtQzuzn8Nf3Dw==,type:str]",
	"sops": {
		"kms": [
			{
				"arn": "arn:aws:kms:us-east-2:255686512659:key/d38c3af4-e577-4634-81b2-26a54a7ba9b6",
				"created_at": "2020-07-25T22:18:26Z",
				"enc": "AQICAHj6EaqIUNSpEVcDMZk537+sJjKTaAzr9+wsh3RzF5CgpwF/eWIZPXcHvnQAMfz7Kn3UAAAAfjB8BgkqhkiG9w0BBwagbzBtAgEAMGgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMdbxgt8bHf5dk6xV7AgEQgDtq8nvGk3muOC0NSLFQH5Qe7zgm8CiI9Qt7K1JzuXvH8lemaP52tx/p2TBq6W6O89wlOo0YrDJDBi0Ckg==",
				"aws_profile": "default"
			}
		],
		"gcp_kms": null,
		"azure_kv": null,
		"lastmodified": "2020-07-25T22:18:27Z",
		"mac": "ENC[AES256_GCM,data:MQHQbeaERWJ2XdV7/Jzjy+T17QlS8J3AxV6G4QngSHqXCwzg0g9ttbzXfTNDM3w5coeL8r80I0Msk1gvpHUU3AMXFpkSAOqlkznokdB6pwi3r2cYjElrK2zSOlnQgXmz0oCHmPiK3CbjD3YezZG0Y0Y9mcJLd3XF8+G4haqCP8I=,iv:pSDkyqrVm8/x0dWUydZViSwQTjRpQ55LZBpwEYiBR/k=,tag:N65jkxT+PQ2F5z8C5kCpAQ==,type:str]",
		"pgp": null,
		"unencrypted_suffix": "_unencrypted",
		"version": "3.5.0"
	}
}